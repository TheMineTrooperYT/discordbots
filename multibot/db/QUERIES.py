
create_birthday_table = 'create table birthday (id text primary key, day numeric, month numeric, year NUMERIC )'

create_channel_data_table = 'create table channel (id text primary key, channel text )'

delete_birthday_table = 'drop table if exists birthday'

delete_channel_table = 'drop table if exists channel'

create_birthday = "insert into birthday (id, day, month, year ) values (%s, %s, %s, %s)"

update_birthday = "update birthday set month = %s, day = %s, year = %s  where id = %s"

delete_birthday = "delete from birthday where id=%s"

delete_channel = "delete from channel where id=%s"

get_birthday_all = "select * from birthday"

get_birthday_one = "select * from birthday where id = %s"

set_channel = 'insert into channel (id, channel) values (%s, %s)'

update_channel = 'update channel set channel = %s where id = %s'

get_channel = ' select * from channel where id = %s'
