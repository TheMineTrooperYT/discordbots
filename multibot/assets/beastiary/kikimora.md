"Kikimora";;;_size_: Medium fey
_alignment_: chaotic neutral
_challenge_: "5 (1800 XP)"
_languages_: "Common, Sylvan"
_skills_: "Deception +7, Persuasion +7, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_resistances_: "bludgeoning, piercing, and slashing from"
_speed_: "30 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "15 (natural armor)"
_stats_: | 13 (+1) | 18 (+4) | 15 (+2) | 12 (+1) | 16 (+3) | 21 (+5) |

___Magic Resistance.___ The kikimora has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting.___ The kikimora's innate spellcasting ability is Charisma (spell save DC 15). She can innately cast the following spells, requiring no material components:

At will: invisibility (self only), mage hand, mending, minor illusion, prestidigitation

3/day each: animal friendship, blinding smite, sleep

1/day each: insect plague, major image

**Actions**

___Multiattack.___ The kikimora makes two claw attacks.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.

___Hidey-Hole.___ When a kikimora chooses a house to inhabit, she scrawls a symbol on a wall, baseboard, cupboard, or semi.permanent object (like a stove) to be her tiny domain. This ability creates a hidden extra-dimensional dwelling. After creating a hidey-hole, a kikimora can teleport herself and up to 50 lb of objects to the designated location instead of making a normal move. This extradimensional space can only be entered by the kikimora or by a creature using a plane shift spell or ability. The location can be determined by casting detect magic in the area of the sigil, but it takes a successful DC 15 Intelligence (Arcana) check to plane shift into the space. Inside the hidey-hole, a kikimora can see what is going on outside the space through a special sensor. This sensor functions like a window, and it can be blocked by mundane objects placed in front of the sigil. If she leaves an item in her space, it remains there even if she removes the sigil and places it in another location. If someone else removes the sigil, all contents are emptied into the Ethereal Plane (including any beings within her hidey-hole at the time). In this case, the kikimora can attempt a DC 15 Charisma saving throw to instead eject herself (but none of her possessions) into a space adjacent to the sigil.

