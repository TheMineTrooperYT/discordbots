"Iron Cobra";;;_size_: Small construct
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_skills_: "Perception +3, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "27 (6d6 + 6)"
_armor class_: "13 (natural armor)"
_stats_: | 12 (+1) | 15 (+2) | 13 (+1) | 5 (-3) | 12 (+1) | 1 (-5) |

___Find Target.___ The iron cobra knows the location
of a specific target creature as long as that creature
is within 1 mile of it. If the creature is moving, it
knows the direction if that creature’s movement.
If the target is beyond this distance, the iron cobra
can’t locate the target creature.

___Immutable Form.___ The iron cobra is immune to any
spell or effect that would alter its form.

___Magic Resistance.___ The iron cobra has advantage on
saving throws against spells and other magical effects.

___Magic Weapon.___ The iron cobra’s weapon attacks
are magical.

___Poison.___ The iron cobra contains enough venom
for three attacks. After that, it does not deal the poison
damage listed in its bite attack.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4 + 2) piercing damage, and the target must make a DC 11 Constitution
saving throw, taking 10 (3d6) poison damage on a failed saving throw, or
half as much damage on a successful one.
