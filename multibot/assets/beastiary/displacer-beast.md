"Displacer Beast";;;_size_: Large monstrosity
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_senses_: "darkvision 60 ft."
_speed_: "40 ft."
_hit points_: "85 (10d10+30)"
_armor class_: "13 (natural armor)"
_stats_: | 18 (+4) | 15 (+2) | 16 (+3) | 6 (-2) | 12 (+1) | 8 (-1) |

___Avoidance.___ If the displacer beast is subjected to an effect that allows it to make a saving throw to take only half damage, it instead takes no damage if it succeeds on the saving throw, and only half damage if it fails .

___Displacement.___ The displacer beast projects a magical illusion that makes it appear to be standing near its actual location, causing attack rolls against it to have disadvantage. If it is hit by an attack, this trait is disrupted until the end of its next turn. This trait is also disrupted while the displacer beast is incapacitated or has a speed of 0.

**Actions**

___Multiattack.___ The displacer beast makes two attacks with its tentacles.

___Tentacle.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 7 (1d6 + 4) bludgeoning damage plus 3 (1d6) piercing damage.