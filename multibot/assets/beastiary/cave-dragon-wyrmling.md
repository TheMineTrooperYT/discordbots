"Cave Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Draconic"
_skills_: "Perception +2, Stealth +5"
_senses_: "blindsight 120 ft., passive Perception 12"
_saving_throws_: "Dex +3, Con +5, Cha +3"
_damage_immunities_: "acid, poison, thunder"
_condition_immunities_: "poisoned"
_speed_: "30 ft., burrow 20 ft., fly 20 ft."
_hit points_: "75 (10d8 + 30)"
_armor class_: "16 (natural armor)"
_stats_: | 19 (+4) | 12 (+1) | 17 (+3) | 8 (-1) | 10 (+0) | 12 (+1) |

___Tunneler.___ The cave dragon can burrow through solid rock at half its burrowing speed and leaves a 5-foot wide, 5-foot high tunnel in its wake.

___Innate Spellcasting.___ The dragon's innate spellcasting ability is Charisma. It can innately cast the following spell, requiring no material components:

* 3/day: _darkness_

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) piercing damage plus 3 (1d6) poison damage.

___Poison Breath (Recharge 5-6).___ The dragon exhales a cone of black poison gas in a 15-foot cone. Each creature in that area must make a DC 13 Constitution saving throw, taking 14 (4d6) poison damage on a failed save and the target is poisoned if it is a creature. The poisoned condition lasts until the target takes a long or short rest or removes the condition with lesser restoration. If the save is successful, the target takes half the damage and does not become poisoned.

