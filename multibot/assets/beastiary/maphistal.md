"Maphistal, Second of Orcus";;;_size_: Large fiend (demon lord)
_alignment_: chaotic evil
_challenge_: "26 (90,000 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Deception +14, Intimidation +14, Perception +13"
_senses_: "truesight 120 ft., passive Perception 23"
_saving_throws_: "Con +15, Wis +13, Cha +14"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_speed_: "40 ft., fly 80 ft."
_hit points_: "400 (32d10 + 224)"
_armor class_: "21 (natural armor)"
_stats_: | 28 (+9) | 16 (+3) | 24 (+7) | 18 (+4) | 20 (+5) | 23 (+6) |

___Innate Spellcasting.___ Maphistal’s spellcasting ability is Charisma
(spell save DC 22, +14 to hit with spell attacks). It can innately cast the following spells requiring no material components:

* At will: _circle of death, detect evil and good, detect magic, dispel magic_

* 3/day: _animate dead, blight, hallow, suggestion, telekinesis_

* 1/day: _conjure fiend, fire storm, power word stun_

___Legendary Resistance (3/day).___ If Maphistal fails a saving throw, he can
choose to succeed instead.

___Magic Resistance.___ Maphistal has advantage on saving throws against
spells and other magical effects.

___Magic Weapon.___ Maphistal’s melee attacks are magical.

___Rampage.___ When Maphistal reduces a creature to 0 hit points with a
melee attack on its turn, Maphistal can take a bonus action to move up to
half its speed and make a bite attack.

___Unholy Aura.___ Malevolent shadows swirl around Maphistal and radiate
out from it in a 30-foot radius. Non-evil creatures in this area have
disadvantage on attack rolls against Maphistal and its allies.

**Actions**

___Multiattack.___ Maphistal makes three attacks: one with its bite and two
with its claws.

___Bite.___ Melee Weapon Attack: +17 to hit, reach 5 ft., one target. Hit:
23 (4d6 + 9) piercing damage. If the target is a creature, it must
succeed on a DC 20 Constitution saving throw against disease or
become poisoned until the disease is cured. Every 24 hours that
elapse, the target must repeat the saving throw, reducing its hit point
maximum by 5 (1d10) on a failure. The disease is cured on a success.
The target dies if the disease reduces its hit point maximum to 0. This
reduction to the target’s hit point maximum lasts until the disease is
cured. The disease can be magically cured by a greater restoration
or heal spell.

___Claws.___ Melee Weapon Attack: +17 to hit, reach 10 ft., one target. Hit:
27 (4d8 + 9) slashing damage. If the target is a creature, it must make
a DC 20 Constitution saving throw. On a failure, the target’s Dexterity
score is reduced by 1d4. The target dies if this reduces its Dexterity to 0.
Otherwise, the reduction lasts until the target finishes a long rest.

___Teleport.___ Maphistal magically teleports, along with any equipment it
is wearing or carrying, up to 120 feet to an unoccupied space it can see.

___Summon (1/day).___ Maphistal summons 1d12 dretches, 1d6 glabrezus,
1d6 hezrous, 1d4 nalfeshnees, 1d3 marilith, or 1 balor. The summoned
demon appears in an unoccupied space within 60 feet of Maphistal, but
can’t summon other demons. It remains for 1 minute, until it or Maphistal
is slain, or until Maphistal takes an action to dismiss it.

**Legendary** Actions

Maphistal can take 3 legendary actions, choosing from the options
below. Only one legendary action can be used at a time and only at the
end of another creature’s turn. Maphistal regains spent legendary actions
at the start of its turn.

___Attack.___ Maphistal makes one claw attack.

___Circle of Death.___ Maphistal casts circle of death.

___Teleport.___ Maphistal uses its Teleport action.
