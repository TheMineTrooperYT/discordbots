"Ratatosk";;;_size_: Tiny celestial
_alignment_: chaotic neutral
_challenge_: "4 (1100 XP)"
_languages_: "Celestial, Common; telepathy 100 ft."
_skills_: "Deception +6, Persuasion +6, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Wis +4, Cha +6"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons"
_speed_: "20 ft., climb 20 ft."
_hit points_: "42 (12d4 + 12)"
_armor class_: "14"
_stats_: | 4 (-3) | 18 (+4) | 12 (+1) | 17 (+3) | 10 (+0) | 18 (+4) |

___Innate Spellcasting.___ The ratatosk's spellcasting attribute is Charisma (save DC 14). It can innately cast the following spells without requiring material or somatic components:

At will: animal messenger, message, vicious mockery

1/day each: commune, mirror image

3/day each: sending, suggestion

___Skitter.___ The ratatosk can take the Dash, Disengage, or Hide action as a bonus action on each of its turns.

**Actions**

___Gore.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 1 piercing damage plus 14 (4d6) psychic damage and the target must make a successful DC 14 Wisdom saving throw or be charmed for 1 round. While charmed in this way, the creature regards one randomly determined ally as a foe.

___Divisive Chatter (recharge 5-6).___ Up to six creatures within 30 feet that can hear the ratatosk must make DC 14 Charisma saving throws. On a failure, the creature is affected as if by a confusion spell for 1 minute. An affected creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success.

**Reactions**

___Desperate Lies.___ A creature that can hear the ratatosk must make a DC 14 Wisdom saving throw when it attacks the ratatosk. If the saving throw fails, the creature still attacks, but it must choose a different target creature. An ally must be chosen if no other enemies are within the attack's reach or range. If no other target is in the attack's range or reach, the attack is still made (and ammunition or a spell slot is expended, if appropriate) but it automatically misses and has no effect.

