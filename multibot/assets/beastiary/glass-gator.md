"Glass Gator";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "--"
_skills_: "Perception +2, Stealth +4"
_senses_: "Blindsight 30 ft., passive Perception 12"
_speed_: "30 ft, swim 50 ft"
_hit points_: "45 (7d10 + 7)"
_armor class_: "15 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 12 (+1) | 4 (-3) | 10 (+0) | 5 (-3) |

___Amphibious.___ The glass gator can breathe air and water.

___Lunge.___ When the glass gator leaps at least 10 feet toward a creature and hits that creature with a claws attack on the same turn, it can immediately constrict the target as a bonus action.

___Transparency.___ The glass gator has advantage on Dexterity (Stealth) checks while underwater or in dim light.

___Standing Leap.___ The glass gator can long jump up to 15 feet from water or up to 10 feet on land, with or without a running start.

**Actions**

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4 + 2) slashing damage, and the target is grappled (escape DC 12). Until this grapple ends, the target is restrained and the glass gator can't attack a different target.

___Constrict.___ One creature that's already grappled by the glass gator takes 7 (2d4 + 2) bludgeoning damage plus 7 (2d6) poison damage, or half as much poison damage with a successful DC 11 Constitution saving throw.

**Reactions**

___Silt Cloud (Recharges after a Short or Long Rest).___ After taking damage while in water, the glass gator thrashes to stir up a 10-foot-radius cloud of silt around itself. The area inside the sphere is heavily obscured for 1 minute (10 rounds) in still water or 5 (2d4) rounds in a strong current. After stirring up the silt, the glass gator can move its speed.

