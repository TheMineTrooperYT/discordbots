"Fireheart Devotee";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "Any two languages"
_skills_: "History +4, Insight +7, Religion +4, Persuasion +3"
_senses_: "passive Perception 14"
_saving_throws_: "Wis +7, Cha +3"
_speed_: "30 ft."
_hit points_: "55 (10d8 + 10)"
_armor class_: "18 (breastplate, shield)"
_stats_: | 14 (+2) | 10 (+0) | 15 (+2) | 12 (+1) | 18 (+4) | 10 (+1) |

___Spellcasting.___ The devotee is a 6th-level spellcaster.
Its spellcasting ability is Wisdom (spell save DC 15, +7 to hit with spell attacks). The devotee has the
following cleric spells prepared:
Cantrips (at will): guidance, light, spare the dying

* 1st level (4 slots): _burning hands, faerie fire, guiding bolt, sanctuary_

* 2nd level (3 slots): _aid, flaming sphere, scorching ray, spiritual weapon_

* 3rd level (3 slots): _clairvoyance, daylight, fireball, mass healing word_

___Radiance of the Dawn (2/Short Rest).___ The devotee
raises its holy symbol and dispels any magical
darkness within 30 feet. Additionally, each creature
hostile to the devotee must make a DC 15
Constitution saving throw, taking 17 (2d10 + 6)
radiant damage on a failed saving throw, or half as
much damage on a successful one.

___Warding Flame (4/Day).___ When the devotee or one of
its allies is attacked by a creature it can see within
30 feet, the devotee can use its reaction to give
that creature disadvantage on the attack. Creatures
immune to the blinded condition are not affected
by this ability.

**Actions**

___Warhammer.___ Melee Weapon Attack: +5 to hit, reach
5 ft., one target. Hit: 6 (1d8 + 2) bludgeoning
damage.
