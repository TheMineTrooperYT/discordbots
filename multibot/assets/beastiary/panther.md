"Panther";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_skills_: "Perception +4, Stealth +6"
_speed_: "50 ft., climb 40 ft."
_hit points_: "13 (3d8)"
_armor class_: "12"
_stats_: | 14 (+2) | 15 (+2) | 10 (0) | 3 (-4) | 14 (+2) | 7 (-2) |

___Keen Smell.___ The panther has advantage on Wisdom (Perception) checks that rely on smell.

___Pounce.___ If the panther moves at least 20 ft. straight toward a creature and then hits it with a claw attack on the same turn, that target must succeed on a DC 12 Strength saving throw or be knocked prone. If the target is prone, the panther can make one bite attack against it as a bonus action.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) slashing damage.