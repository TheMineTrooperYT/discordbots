"Elemental Earth Dragon";;;_size_: Huge elemental
_alignment_: neutral evil
_challenge_: "19 (22,000 XP)"
_languages_: "Terran, Common"
_skills_: "Arcana +8, Nature +8, Perception +15, Stealth +6"
_senses_: "tremorsense 60 ft., blindsight 60 ft., darkvision 120 ft., passive Perception 25"
_saving_throws_: "Dex +6, Con +14, Wis +9, Cha +10"
_damage_immunities_: "acid, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "40 ft., burrow 40 ft., fly 80 ft."
_hit points_: "362 (25d12 + 200)"
_armor class_: "20 (natural armor)"
_stats_: | 28 (+9) | 10 (+0) | 26 (+8) | 14 (+2) | 16 (+3) | 19 (+4) |

___Earth Glide.___ The dragon can burrow through nonmagical, unworked
earth and stone. While doing so, the elemental doesn’t disturb the material
it moves through.

___False Appearance.___ While the dragon remains motionless, it is
indistinguishable from a normal statue of a dragon.

___Innate Spellcasting.___ The dragon’s innate spellcasting ability is
Charisma (spell save DC 18, +10 to hit with spell attacks). It can cast the
following spells, requiring no material components.

* At will: _meld into stone, stone shape_

* 3/day: _wall of stone_

* 1/day: _plane shift_

___Legendary Resistance (3/day).___ If the dragon fails a saving throw, it can
choose to succeed instead.

___Siege Monster.___ The dragon deals double damage to objects and
structures.

**Actions**

___Multiattack.___ The dragon can make three attacks: one with its bite and
two with its claws.

___Bite.___ Melee Weapon Attack: +15 to hit, reach 10 ft., one target.
Hit: 20 (2d10 + 9) piercing damage plus 10 (3d6) fire damage.

___Claw.___ Melee Weapon Attack: +15 to hit, reach 5 ft., one
target. Hit: 16 (2d6 + 9) slashing damage.

___Tail.___ Melee Weapon Attack: +15 to hit, reach 15 ft., one
target. Hit: 18 (2d8 + 9) bludgeoning damage.

___Shale and Stone Breath (Recharge 5–6).___ The dragon
releases a 60-foot cone of sand and gravel. Creatures
within the area must make a DC 22 Constitution saving
throw, taking 35 (10d6) fire damage plus 35 (10d6)
bludgeoning damage on a failed saving throw, or
half as much damage on a successful one. A
creature slain by this damage has its body
pulverized; it can only be restored to life by
_true resurrection_ or _wish_.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing from the
options below. Only one legendary action option can be used at a
time and only at the end of another creature’s turn. The dragon
regains spent legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its
wings. Each creature within 15 feet of the dragon must
succeed on a DC 23 Dexterity saving throw or take 16 (2d6 + 9) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.
