"Stone Defender";;;_page_number_: 126
_size_: Medium construct
_alignment_: unaligned
_challenge_: "4 (1100 XP)"
_languages_: "understands one language of its creator but can't speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical attacks that aren't adamantine"
_speed_: "30 ft."
_hit points_: "52  (7d8 + 21)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_stats_: | 19 (+4) | 10 (0) | 17 (+3) | 3 (-3) | 10 (0) | 1 (-4) |

___False Appearance.___ While the stone defender remains motionless against an uneven earthen or stone surface, it is indistinguishable from that surface.

___Magic Resistance.___ The stone defender has advantage on saving throws against spells and other magical effects.

**Actions**

___Slam___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage, and if the target is Large or smaller, it is knocked prone.