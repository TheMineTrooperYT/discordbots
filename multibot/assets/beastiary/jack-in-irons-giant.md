"Jack-in-Irons Giant";;;_size_: Huge giant
_alignment_: chaotic evil
_challenge_: "7 (2,900 XP)"
_languages_: "Giant"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "40 ft."
_hit points_: "138 (12d12 + 60)"
_armor class_: "15 (chain scraps)"
_stats_: | 24 (+7) | 12 (+1) | 20 (+5) | 9 (-1) | 12 (+1) | 8 (-1) |

**Actions**

___Multiattack.___ The jack-in-irons giant uses its shake the earth ability, and
then makes two club attacks and either a chain or fist attack.

___Club.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 16 (2d8 + 7) bludgeoning damage.

___Chain.___ Melee Weapon Attack: +10 to hit, reach 15 ft., one target. Hit: 14 (2d6 + 7) bludgeoning damage and the target must succeed on a DC 18 Dexterity saving throw or be grappled and restrained (escape DC 18) and the jack-in-irons giant cannot grapple another target. At the beginning of the giant’s turn, it can smash a grappled target into the ground or another solid object within 15 feet of the giant, dealing 11
(1d8 + 7) bludgeoning damage.

___Fist.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 14 (2d6 + 7) bludgeoning damage and the target must succeed on a DC 18 Strength saving throw or be knocked prone.

___Rock.___ Ranged Weapon Attack: +10 to hit, range 60/240 ft., one target. Hit: 23 (3d10 + 7) bludgeoning damage.

___Shake the Earth.___ Creatures within 10 feet of the jack-in-irons giant
must succeed on a DC 18 Dexterity saving throw or fall prone.
