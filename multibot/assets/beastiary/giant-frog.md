"Giant Frog";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "darkvision 30 ft."
_skills_: "Perception +2, Stealth +3"
_speed_: "30 ft., swim 30 ft."
_hit points_: "18 (4d8)"
_armor class_: "11"
_stats_: | 12 (+1) | 13 (+1) | 11 (0) | 2 (-4) | 10 (0) | 3 (-4) |

___Amphibious.___ The frog can breathe air and water

___Standing Leap.___ The frog's long jump is up to 20 ft. and its high jump is up to 10 ft., with or without a running start.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) piercing damage, and the target is grappled (escape DC 11). Until this grapple ends, the target is restrained, and the frog can't bite another target.

___Swallow.___ The frog makes one bite attack against a Small or smaller target it is grappling. If the attack hits, the target is swallowed, and the grapple ends. The swallowed target is blinded and restrained, it has total cover against attacks and other effects outside the frog, and it takes 5 (2d4) acid damage at the start of each of the frog's turns. The frog can have only one target swallowed at a time. If the frog dies, a swallowed creature is no longer restrained by it and can escape from the corpse using 5 ft. of movement, exiting prone.