"Black Bear";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_speed_: "40 ft., climb 30 ft."
_hit points_: "19 (3d8+6)"
_armor class_: "11 (natural armor)"
_stats_: | 15 (+2) | 10 (0) | 14 (+2) | 2 (-4) | 12 (+1) | 7 (-2) |

___Keen Smell.___ The bear has advantage on Wisdom (Perception) checks that rely on smell.

**Actions**

___Multiattack.___ The bear makes two attacks: one with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Claws.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 7 (2d4 + 2) slashing damage.