"Ultroloth";;;_size_: Medium fiend (yugoloth)
_alignment_: neutral evil
_challenge_: "13 (10,000 XP)"
_languages_: "Abyssal, Infernal, telepathy 120 ft."
_senses_: "truesight 120 ft."
_skills_: "Intimidation +9, Perception +7, Stealth +8"
_damage_immunities_: "acid, poison"
_speed_: "30 ft., fly 60 ft."
_hit points_: "153 (18d8+72)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "charmed, frightened, poisoned"
_damage_resistances_: "cold, fire, lightning, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 16 (+3) | 16 (+3) | 18 (+4) | 18 (+4) | 15 (+2) | 19 (+4) |

___Innate Spellcasting.___ The ultroloth's innate spellcasting ability is Charisma (spell save DC 17). The ultroloth can innately cast the following spells, requiring no material components:

* At will: _alter self, clairvoyance, darkness, detect magic, detect thoughts, dispel magic, invisibility _(self only)_, suggestion_

* 3/day each: _dimension door, fear, wall of fire_

* 1/day each: _fire storm, mass suggestion_

___Magic Resistance.___ The ultroloth has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The ultroloth's weapon attacks are magical.

**Actions**

___Multiattack.___ The ultroloth can use its Hypnotic Gaze and makes three melee attacks.

___Longsword.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage, or 8 (1d10 + 3) slashing damage if used with two hands.

___Hypnotic Gaze.___ The ultroloth's eyes sparkle with opalescent light as it targets one creature it can see within 30 feet of it. If the target can see the ultroloth, the target must succeed on a DC 17 Wisdom saving throw against this magic or be charmed until the end of the ultroloth's next turn. The charmed target is stunned. If the target's saving throw is successful, the target is immune to the ultroloth's gaze for the next 24 hours.

___Teleport.___ The ultroloth magically teleports, along with any equipment it is wearing or carrying, up to 60 feet to an unoccupied space it can see.

___Variant: Summon Yugoloth (1/Day).___ The yugoloth chooses what to summon and attempts a magical summoning.

An ultroloth has a 50 percent chance of summoning 1d6 mezzoloths, 1d4 nycaloths, or one ultroloth.

A summoned yugoloth appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other yugoloths. The summoned yugoloth remains for 1 minute, until it or its summoner dies, or until its summoner takes a bonus action to dismiss it.
