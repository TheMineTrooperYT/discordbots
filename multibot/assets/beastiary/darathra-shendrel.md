"Darathra Shendrel";;;_size_: Medium humanoid (illuskan humanoid)
_alignment_: lawful good
_challenge_: "0 (10 XP)"
_languages_: "Common"
_skills_: "History +2, Intimidation +4, Investigation +2, Perception +2, Presuasion +4"
_speed_: "30 ft."
_hit points_: "52 (8d8+16)"
_armor class_: "14 (breastplate)"
_senses_: " passive Perception 12"
_stats_: | 16 (+3) | 11 (0) | 14 (+2) | 11 (0) | 11 (0) | 15 (+2) |

___Brave.___ Darathra has advantage on saving throws against being frightened

**Actions**

___Multiattack.___ Darthra makes two melee attacks.

___Greatsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6+3) slashing damage

___Heavy Crossbow.___ Ranged Weapon Attack: +2 to hit, range 100/400 ft., one target. Hit: 5 (1d10) piercing damage. Darathra carries twenty crossbow bolts.

**Roleplaying** Information

As the Lord Protector of Triboar and a secret agent of the Harpers, Darathra has sworn an oath to defend the town. She takes her duty very seriously. In addition to her gear, Darathra has an unarmored warhorse named Buster.

**Ideal:** "Good people should be given every chance to prosper, free of tyranny."

**Bond:** "I'll lay down my life to protect Triboar and its citizens."

**Flaw:** "I refuse to back down. Push me, and I'll push back."