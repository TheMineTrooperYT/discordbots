"Mind Flayer Psion";;;_size_: Medium aberration
_alignment_: lawful evil
_challenge_: "8 (3,900 XP)"
_languages_: "Deep Speech, Undercommon, telepathy 120 ft."
_senses_: "darkvision 120 ft."
_skills_: "Arcana +7, Deception +6, Insight +6, Perception +6, Persuasion +6, Stealth +4"
_saving_throws_: "Int +7, Wis +6, Cha +6"
_speed_: "30 ft."
_hit points_: "71 (13d8+13)"
_armor class_: "15 (breastplate)"
_stats_: | 11 (0) | 12 (+1) | 12 (+1) | 19 (+4) | 17 (+3) | 17 (+3) |

___Magic Resistance.___ The mind flayer has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting (Psionics).___ The mind flayer is a 10th-level spellcaster. Its innate spellcasting ability is Intelligence (spell save DC 15; +7 to hit with spell attacks). It can innately cast the following spells, requiring no components:

* At will: _guidance, mage hand, vicious mockery, true strike_

* 1st level (4 slots): _charm person, command, comprehend languages, sanctuary_

* 2nd level (3 slots): _crown of madness, phantasmal force, see invisibility_

* 3rd level (3 slots): _clairvoyance, fear, meld into stone_

* 4th level (3 slots): _confusion, stone shape_

* 5th level (2 slots): _scrying, telekinesis_

**Actions**

___Tentacles.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one creature. Hit: 15 (2dl0+4) psychic damage. If the target is Medium or smaller, it is grappled (escape DC 15) and must succeed on a DC 15 Intelligence saving throw or be stunned until this grapple ends.

___Extract Brain.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one incapacitated humanoid grappled by the mind flayer. Hit: The target takes 55 (10d10) piercing damage. If this damage reduces the target to 0 hit points, the mind flayer kills the target by extracting and devouring its brain.

___Mind Blast (Recharge 5-6).___ The mind flayer magically emits psychic energy in a 60-foot cone. Each creature in that area must succeed on a DC 15 Intelligence saving throw or take 22 (4d8+4) psychic damage and be stunned for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
