"Crawling Claw";;;_size_: Tiny undead
_alignment_: neutral evil
_challenge_: "0 (10 XP)"
_languages_: "understands Common but can't speak"
_senses_: "blindsight 30 ft. (blind beyond this radius)"
_damage_immunities_: "poison"
_speed_: "20 ft., climb 20 ft."
_hit points_: "2 (1d4)"
_armor class_: "12"
_condition_immunities_: "poisoned"
_stats_: | 13 (+1) | 14 (+2) | 11 (0) | 5 (-3) | 10 (0) | 4 (-3) |

___Turn Immunity.___ The claw is immune to effects that turn undead.

**Actions**

___Claw.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) bludgeoning or slashing damage (claw's choice) .