"Arcane Entity";;;_size_: Large elemental
_alignment_: chaotic neutral
_challenge_: "7 (2,900 XP)"
_languages_: "Primordial"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Int +7, Wis +4"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "0 ft., fly 40 ft. (hover)"
_hit points_: "135 (18d10 + 36)"
_armor class_: "15 (natural armor)"
_stats_: | 7 (-2) | 16 (+3) | 14 (+2) | 18 (+4) | 12 (+1) | 10 (+0) |

___Magic Resistance.___ The entity has advantage on saving
throws against spell and other magical effects.

___Magic Weapons.___ The entity’s weapons are magical.

___Arcane Aura.___ The entity sheds bright light in a 15-foot
radius and dim light for an additional 15 feet. Any
magical items that are within this area become
mundane. This effect wears off when the entity dies or
when the item is taken out of the aura. Enemies that
end their turn in the bright light take 5 (1d10) force
damage.

**Actions**

___Multiattack.___ The entity makes two attacks with its claws
or two attacks with its whip.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5ft., one
target. Hit: 12 (2d8 + 3) slashing damage plus 9 (2d8)
force damage.

___Arcane Whip.___ Melee Weapon Attack: +6 to hit, reach
15ft., one target. Hit: 7 (2d4 + 3) slashing damage plus
13 (3d8) force damage.

___Arcane Barrage (Recharge 5-6).___ The entity unleashes 10
(4d4) missiles of arcane power at randomly selected
enemies within 120 feet. The missiles automatically hit
and deal 6 (1d4 + 4) damage each.
