"Black Earth Guard";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Common"
_skills_: "Intimidation +1, Perception +2"
_speed_: "30 ft."
_hit points_: "39 (6d8+12)"
_armor class_: "18 (plate)"
_stats_: | 17 (+3) | 11 (0) | 14 (+2) | 10 (0) | 10 (0) | 9 (-1) |

**Actions**

___Multiattack.___ The guard makes two melee attacks.

___Morningstar.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage.

**Reactions**

___Unyielding.___ When the guard is subjected to an effect that would move it, knock it prone, or both, it can use its reaction to be neither moved nor knocked prone.