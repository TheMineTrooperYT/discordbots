"Dire Troll";;;_page_number_: 243
_size_: Huge giant
_alignment_: chaotic evil
_challenge_: "13 (10,000 XP)"
_languages_: "Giant"
_senses_: "darkvision 60 ft., passive Perception 15"
_skills_: "Perception +5"
_saving_throws_: "Wis +5, Cha +2"
_speed_: "40 ft."
_hit points_: "172  (15d12 + 75)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "frightened, poisoned"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 22 (+6) | 15 (+2) | 21 (+5) | 9 (0) | 11 (0) | 5 (-2) |

___Keen Senses.___ The troll has advantage on Wisdom (Perception) checks that rely on smell or sight.

___Regeneration.___ The troll regains 10 hit points at the start of its turn. If the troll takes acid or fire damage, it regains only 5 hit points at the start of its next turn. The troll dies only if it is hit by an attack that deals 10 or more acid or fire damage while the troll has 0 hit points.

**Actions**

___Multiattack___ The troll makes five attacks: one with its bite and four with its claws.

___Bite___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 10 (1d8 + 6) piercing damage plus 5 (1d10) poison damage.

___Claws___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 16 (3d6 + 6) slashing damage.

___Whirlwind of Claws (Recharge 5-6)___ Each creature within 10 feet of the troll must make a DC 19 Dexterity saving throw, taking 44 (8d10) slashing damage on a failed save, or half as much damage on a successful one.