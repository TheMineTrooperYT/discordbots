"Ventrunculus";;;_size_: Small construct
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "understands the languages of its creator but can't speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "30 ft."
_hit points_: "97 (15d6 + 45)"
_armor class_: "13"
_stats_: | 12 (+1) | 16 (+3) | 17 (+3) | 6 (-2) | 10 (+0) | 5 (-3) |

___Bigger on the Inside.___ The ventrunculus's stomach is an extradimensional space. The stomach is a 15-foot cube, able to hold one Huge creature, four Large creatures or sixteen Medium creatures.
   Additionally, corpses or other remains in the ventrunculus' stomach are under the effect of a gentle repose spell.

___Magic Resistance.___ The ventrunculus has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The ventrunculus makes two bite attacks.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) piercing damage, and the target is grappled (escape DC 13) if it is a Huge or smaller creature.

___Swallow.___ The ventrunculus makes a bite attack against creature it is grappling. If the attack hits, the target is swallowed, and the grapple ends. The swallowed creature is blinded and restrained, and it has total cover against attacks and other effects outside the ventrunculus. At the start of each of the ventrunculus's turns, each swallowed creature must succeed on a DC 13 Wisdom saving throw or fall unconcious. While unconscious, the creature doesn't need to breathe, eat, or drink, and it doesn’t age. Divination magic can’t locate or perceive the target.
   While the ventrunculus isn't incapacitated, it can regurgitate swallowed creatures at any time (no action required) into spaces within 5 feet of it. The creatures exit prone. If the ventrunculus dies, it likewise regurgitates swallowed creatures. Unconscious creatures regain consciousness when regurgitated.
