"Spy";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1 (200 XP)"
_languages_: "any two languages"
_skills_: "Deception +5, Insight +4, Investigation +5, Perception +6, Persuasion +5, Sleight of Hand +4, Stealth +4"
_speed_: "30 ft."
_hit points_: "27 (6d8)"
_armor class_: "12"
_stats_: | 10 (0) | 15 (+2) | 10 (0) | 12 (+1) | 14 (+2) | 16 (+3) |

___Cunning Action.___ On each of its turns, the spy can use a bonus action to take the Dash, Disengage, or Hide action.

___Sneak Attack (1/Turn).___ The spy deals an extra 7 (2d6) damage when it hits a target with a weapon attack and has advantage on the attack roll, or when the target is within 5 ft. of an ally of the spy that isn't incapacitated and the spy doesn't have disadvantage on the attack roll.

**Actions**

___Multiattack.___ The spy makes two melee attacks.

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Hand Crossbow.___ Ranged Weapon Attack: +4 to hit, range 30/120 ft., one target. Hit: 5 (1d6 + 2) piercing damage.