"Myconid Alchemist I";;;_size_: Medium plant
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "Undercommon"
_skills_: "Stealth +5"
_senses_: "darkvision 120 ft., passive Perception 12"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "6 (1d8 + 2)"
_armor class_: "14 (natural armor)"
_stats_: | 12 (+1) | 16 (+3) | 14 (+2) | 12 (+1) | 10 (+0) | 8 (-1) |

___Stalk-still.___ A myconid that remains perfectly still has advantage on
Dexterity (Stealth) checks made to hide.

**Actions**

___Stone Knuckles.___ Melee weapon attack: +5 to hit. Hit: 6 (1d6+3)
slashing damage.

___Spore Bomb.___ Ranged weapon attack: +3 to hit, range 30/60 ft. Hit: 2
(1d4) poison damage and the target must succeed on a DC 12 Constitution
saving throw or see 4 versions of the myconid for 1 minute. An affected
creature making an attack against one of the visions has a 25% chance to
hit the real myconid. The affected creature can attempt the saving throw
at the end of each of its turns, ending the effect on a success.
