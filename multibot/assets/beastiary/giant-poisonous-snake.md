"Giant Poisonous Snake";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "blindsight 10 ft."
_skills_: "Perception +2"
_speed_: "30 ft., swim 30 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "14"
_stats_: | 10 (0) | 18 (+4) | 13 (+1) | 2 (-4) | 10 (0) | 3 (-4) |

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 6 (1d4 + 4) piercing damage, and the target must make a DC 11 Constitution saving throw, taking 10 (3d6) poison damage on a failed save, or half as much damage on a successful one.