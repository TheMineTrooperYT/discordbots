"Sirac of Suzail";;;_size_: Medium humanoid (chondathan human)
_alignment_: lawful good
_challenge_: "0 (10 XP)"
_languages_: "Common, Orc"
_skills_: "Athletics +4, Insight +3, Survival +3"
_speed_: "30 ft."
_hit points_: "22 (5d8)"
_armor class_: "14 (leather)"
_senses_: "passive Perception 11"
_stats_: | 14 (+2) | 17 (+3) | 11 (0) | 12 (+1) | 13 (+1) | 16 (+3) |

**Actions**

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6+3) piercing  damage.

___Dart.___ Ranged Weapon Attack: +5 to hit, range 20/60 ft., one target. Hit: 5 (1d4+3) piercing damage. Sirac carries six darts.

**Reactions**

___Parry.___ Sirac adds 2 to his AC against on melee attack that would hit him. To do so, Sirac must see the attacker and be wielding a melee weapon.

**Roleplaying** Information
An acolyte of Torm, Sirac grew up on the streets of Suzail, the capital of Cormyr. He came to Icewind Dale to become a knucklehead trout fishe but instead found religion. The misbegotten son of Artus Climber, a renowened human adventurer, Sirac hasn't seen his father since he was a baby.

**Ideal:** "Without duty or loyalty, a man is nothing."

**Bond:** "Icewind Dale is where i belong for the rest of my life."

**Flaw:** "I am honest to a fault."