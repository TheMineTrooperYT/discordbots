"Titivilus";;;_page_number_: 179
_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "16 (15,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "darkvision 120 ft., passive Perception 16"
_skills_: "Deception +13, Insight +11, Intimidation +13, Persuasion +13"
_damage_immunities_: "fire, poison"
_saving_throws_: "Dex +11, Con +8, Wis +11, Cha +13"
_speed_: "40 ft., fly 60 ft."
_hit points_: "127  (17d8 + 51)"
_armor class_: "20 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 19 (+4) | 22 (+6) | 17 (+3) | 24 (+7) | 22 (+6) | 26 (+8) |

___Innate Spellcasting.___ Titivilus's innate spellcasting ability is Charisma (spell save DC 21). He can innately cast the following spells, requiring no material components:

* At will: _alter self, animate dead, bestow curse, confusion, major image, modify memory, nondetection, sending, suggestion_

* 3/day each: _greater invisibility _(self only)_, mislead_

* 1/day each: _feeblemind, symbol _(discord or sleep only)

___Legendary Resistance (3/Day).___ If Titivilus fails a saving throw, he can choose to succeed instead.

___Magic Resistance.___ Titivilus has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Titivilus's weapon attacks are magical.

___Regeneration.___ Titivilus regains 10 hit points at the start of his turn. If he takes cold or radiant damage, this trait doesn't function at the start of his next turn. Titivilus dies only if he starts his turn with 0 hit points and doesn't regenerate. Ventriloquism. Whenever Titivilus speaks, he can choose a point within 60 feet; his voice emanates from that point.

**Actions**

___Multiattack___ Titivilus makes one sword attack and uses his Frightful Word once.

___Silver Sword___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage plus 16 (3d10) necrotic damage, or 9 (1d10 + 4) slashing damage plus 16 (3d10) necrotic damage if used with two hands. If the target is a creature, its hit point maximum is reduced by an amount equal to half the necrotic damage it takes.

___Frightful Word___ Titivilus targets one creature he can see within 10 feet of him. The target must succeed on a DC 21 Wisdom saving throw or become frightened for 1 minute. While frightened in this way, the target must take the Dash action and move away from Titivilus by the safest available route on each of its turns, unless there is nowhere to move, in which case it needn't take the Dash action. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Teleport___ Titivilus magically teleports, along with any equipment he is wearing and carrying, up to 120 feet to an unoccupied space he can see.

___Twisting Words___ Titivilus targets one creature he can see within 60 feet of him. The target must make a DC 21 Charisma saving throw. On a failure the target is charmed for 1 minute. The charmed target can repeat the saving throw if Titivilus deals any damage to it. A creature that succeeds on the saving throw is immune to Titivilus's Twisting Words for 24 hours.

**Legendary** Actions

Titivilus can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Titivilus regains spent legendary actions at the start of his turn.

___Assault (Costs 2 Actions)___ Titivilus attacks with his silver sword or uses his Frightful Word.

___Corrupting Guidance___ Titivilus uses Twisting Words. Alternatively, he targets one creature charmed by him that is within 60 feet of him; that charmed target must make a DC 21 Charisma saving throw. On a failure, Titivilus decides how the target acts during its next turn.

___Teleport___ Titivilus uses his Teleport action.
