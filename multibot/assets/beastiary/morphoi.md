"Morphoi";;;_size_: Medium plant
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "30 ft., swim 30 ft."
_hit points_: "33 (6d8 + 6)"
_armor class_: "13 (may be higher with armor)"
_stats_: | 11 (+0) | 16 (+3) | 13 (+1) | 14 (+2) | 10 (+0) | 15 (+2) |

___Amphibious.___ The morphoi can breathe air and water.

___Immunity to Temporal Effects.___ The morphoi is immune to all time-related spells and effects.

___Shapeshifter.___ The morphoi can use its action to polymorph into a Medium creature or back into its true form. Its statistics are the same in each form. Any equipment the morphoi is carrying or wearing isn't transformed. The morphoi reverts to its true form when it dies.

**Actions**

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage.

___Trident.___ Melee or Ranged Weapon Attack: +2 to hit, reach 5 ft., or +5 to hit, range 20/60 ft., one target. Hit: 4 (1d8) piercing damage if used with both hands to make a melee attack, or 6 (1d6 + 3) if thrown.

___Shortbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

