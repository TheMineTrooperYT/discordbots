"Githzerai Anarch";;;_page_number_: 207
_size_: Medium humanoid (gith)
_alignment_: lawful neutral
_challenge_: "16 (15,000 XP)"
_languages_: "Gith"
_senses_: "passive Perception 20"
_skills_: "Arcana +9, Insight +10, Perception +10"
_saving_throws_: "Str +8, Dex +10, Int +9, Wis +10"
_speed_: "30 ft., fly 40 ft. (hover)"
_hit points_: "144  (17d8 + 68)"
_armor class_: "20"
_stats_: | 16 (+3) | 21 (+5) | 18 (+4) | 18 (+4) | 20 (+5) | 14 (+2) |

___Innate Spellcasting (Psionics).___ The anarch's innate spellcasting ability is Wisdom (spell save DC 18, +10 to hit with spell attacks). It can innately cast the following spells, requiring no components:

* At will: _mage hand _(the hand is invisible)

* 3/day each: _feather fall, jump, see invisibility, shield, telekinesis_

* 1/day each: _globe of invulnerability, plane shift, teleportation circle, wall of force_

___Psychic Defense.___ While the anarch is wearing no armor and wielding no shield, its AC includes its Wisdom modifier.

**Actions**

___Multiattack___ The anarch makes three unarmed strikes.

___Unarmed Strike___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) bludgeoning damage plus 18 (4d8) psychic damage.

**Legendary** Actions

The anarch can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The anarch regains spent legendary actions at the start of its turn.

___Strike___ The anarch makes one unarmed strike.

___Teleport___ The anarch magically teleports, along with any equipment it is wearing and carrying, to an unoccupied space it can see within 30 feet of it.

___Change Gravity (Costs 3 Actions)___ The anarch casts the reverse gravity spell. The spell has the normal effect, except that the anarch can orient the area in any direction and creatures and objects fall toward the end of the area.
