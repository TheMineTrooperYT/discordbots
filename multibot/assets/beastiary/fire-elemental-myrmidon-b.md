"Fire Elemental Myrmidon (B)";;;_page_number_: 203
_size_: Medium elemental
_alignment_: neutral
_challenge_: "7 (2900 XP)"
_languages_: "Ignan, one language of its creator's choice"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "fire, poison"
_speed_: "40 ft."
_hit points_: "123  (19d8 + 38)"
_armor class_: "18 (plate)"
_condition_immunities_: "paralyzed, petrified, poisoned, prone"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 13 (+1) | 18 (+4) | 15 (+2) | 9 (0) | 10 (0) | 10 (0) |

___Illumination.___ The myrmidon sheds bright light in a 20-foot radius and dim light in a 40-foot radius.

___Magic Weapons.___ The myrmidon's weapon attacks are magical.

___Water Susceptibility.___ For every 5 feet the myrmidon moves in 1 foot or more of water, it takes 2 (1d4) cold damage.

**Actions**

___Multiattack___ The myrmidon makes three scimitar attacks.

___Scimitar___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) slashing damage.

___Fiery Strikes (Recharge 6)___ The myrmidon uses Multiattack. Each attack that hits deals an extra 5 (1d10) fire damage.