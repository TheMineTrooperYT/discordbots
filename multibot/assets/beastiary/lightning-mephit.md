"Lightning Mephit";;;_size_: Small elemental
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "Auran"
_skills_: "Perception +4, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 14"
_damage_immunities_: "lightning, poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft., fly 30 ft."
_hit points_: "21 (6d6)"
_armor class_: "13"
_stats_: | 10 (+0) | 17 (+3) | 10 (+0) | 6 (-2) | 11 (+0) | 15 (+2) |

___Death Burst.___ When the mephit dies, it explodes in a flare of lightning
in a 15-foot radius. Creatures in the area must make a DC 12 Dexterity
saving throw, taking 9 (2d8) lightning damage on a successful saving
throw, or half as much damage on a failed one.

___Innate Spellcasting.___ The mephit’s spellcasting ability is Charisma
(spell save DC 12, +4 to hit with spell attacks). It can innately cast
_shocking grasp_ at will requiring no material components.

**Actions**

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5
(1d4 + 3) piercing damage plus 2 (1d4) lightning damage.

___Lightning Breath (Recharge 6).___ The mephit exhales lightning in a 15-
foot line that is 1-foot wide. All creatures within that area must make a
DC 12 Dexterity saving throw, taking 9 (2d8) lightning damage on a failed
save, or half as much damage on a successful one.
