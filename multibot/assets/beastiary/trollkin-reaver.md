"Trollkin Reaver";;;_size_: Medium humanoid
_alignment_: neutral
_challenge_: "4 (1100 XP)"
_languages_: "Common, Trollkin"
_skills_: "Intimidation +5, Survival +3"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Con +5, Wis +3, Cha +3"
_speed_: "30 ft."
_hit points_: "82 (11d8 + 33)"
_armor class_: "14 (hide armor)"
_stats_: | 19 (+4) | 13 (+1) | 16 (+3) | 11 (+0) | 12 (+1) | 13 (+1) |

___Regeneration.___ The trollkin reaver regains 10 hit points at the start of its turn. This trait doesn't function if the trollkin took acid or fire damage since the end of its previous turn. The trollkin dies if it starts its turn with 0 hit points and doesn't regenerate.

___Thick Hide.___ The trollkin reaver's skin is thick and tough, granting it a +1 bonus to AC. This bonus is already factored into the trollkin's AC.

**Actions**

___Multiattack.___ The trollkin raider makes three melee attacks: two with its claws and one with its bite, or two with its battleaxe and one with its handaxe, or it makes two ranged attacks with its handaxes.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d4 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d4 + 4) slashing damage.

___Battleaxe.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage or 9 (1d10 + 4) slashing damage if used with two hands. Using the battleaxe two-handed prevents using the handaxe.

___Handaxe.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 7 (1d6 + 4) slashing damage.

___Howl of Battle (Recharge 6).___ Up to three allies who can hear the trollkin reaver and are within 30 feet of it can each make one melee attack as a reaction.

