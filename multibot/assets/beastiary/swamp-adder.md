"Swamp Adder";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_senses_: "blindsight 10 ft., passive Perception 10"
_speed_: "30 ft."
_hit points_: "18 (4d6 + 4)"
_armor class_: "13"
_stats_: | 4 (-3) | 16 (+3) | 12 (+1) | 1 (-5) | 10 (+0) | 4 (-3) |

___Swamp Camouflage.___ The swamp adder has advantage on Dexterity (Stealth) checks while in swamp terrain.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 1 piercing damage, and the target must make a successful DC 11 saving throw or become poisoned. While poisoned this way, the target is paralyzed and takes 3(1d6) poison damage at the start of each of its turns. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself with a success.

