"Ogre Warchief";;;_size_: Large giant
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Giant"
_skills_: "Athletics +7, Intimidation +3"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "40 ft."
_hit points_: "76 (8d12 + 24)"
_armor class_: "16 (chainmail)"
_stats_: | 20 (+5) | 8 (-1) | 17 (+3) | 9 (-1) | 10 (+0) | 8 (-1) |

**Actions**

___Multiattack.___ The ogre makes two attacks with its
greatmaul or with its javelin.

___Greatmaul.___ Melee Weapon Attack: +7 to hit, reach
5ft., one target. Hit: 14 (2d8 + 5) bludgeoning
damage.

___Javelin.___ Melee or Ranged Weapon Attack: +7 to hit,
reach 5 ft. or range 30/120 ft., one target., one
target. Hit: 12 (2d6 + 5) piercing damage.

___Punt.___ Melee Weapon Attack: +7 to hit, reach 5ft.,
one target. Hit: 23 (4d8 + 5) bludgeoning damage
and the target must make a DC 15 Strength saving
throw, being thrown 20 ft. and knocked prone on a
failed save or pushed 10 ft. and not knocked prone
on a success.

___War Drums (Recharge 5-6).___ The warchief play the
drums of war. All other ogres within 120 ft. that
can hear the drums may use their reaction to make
a melee attack against an enemy in range.
