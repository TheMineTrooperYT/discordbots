"Illusionist";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "any four languages"
_skills_: "Arcana +5, History +5"
_saving_throws_: "Int +5, Wis +2"
_speed_: "30 ft."
_hit points_: "38 (7d8+7)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 9 (-1) | 14 (+2) | 13 (+1) | 16 (+3) | 11 (0) | 12 (+1) |

___Spellcasting.___ The illusionist is a 7th-level spellcaster. its spellcasting ability is Intelligence (spell save DC 13, +5 to hit with spell attacks). The illusionist has the following wizard spells prepared:

* Cantrips (at will): _dancing lights, mage hand, minor illusion, poison spray_

* 1st level (4 slots): _color spray\*, disguise self\*, mage armor, magic missile_

* 2nd level (3 slots): _invisibility\*, mirror image\*, phantasmal force_

* 3rd level (3 slots): _major image\*, phantom steed* _

* 4th level (1 slot): _phantasmal killer* _

*Illusion spell of 1st level or higher

___Displacement (Recharges after the Illusionist Casts an Illusion Spell of 1st Level or Higher).___ As a bonus action, the illusionist projects an illusion that makes the illusionist appear to be standing in a place a few inches from its actual location, causing any creature to have disadvantage on attack rolls against the illusionist. The effect ends if the illusionist takes damage, it is incapacitated, or its speed becomes 0.

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +1 to hit, reach 5 ft., one target. Hit: 2 (1d6-1) bludgeoning damage, or 3 (1d8-1) bludgeoning damage if used with two hands.
