"Dolphin";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_senses_: "blindsight 60 ft."
_skills_: "Perception +3"
_speed_: "0 ft., swim 60 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "12 (natural armor)"
_stats_: | 14 (+2) | 13 (+1) | 13 (+1) | 6 (-2) | 12 (+1) | 7 (-2) |

___Charge.___ If the dolphin moves at least 30 feet straight toward a target and then hits it with a slam attack on the same turn, the target takes an extra 3 (1d6) bludgeoning damage.

___Hold Breath.___ The dolphin can hold its breath for 20 minutes.

**Actions**

___Slam.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6+2) bludgeoning damage.