"Yuan-ti Malison Type 2";;;_size_: Medium monstrosity (shapechanger yuan-ti)
_challenge_: "3 (700 XP)"
_languages_: "Abyssal, Common, Draconic"
_senses_: "darkvision 60 ft."
_skills_: "Deception +5, Stealth +4"
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "66 (12d8+12)"
_armor class_: "12"
_condition_immunities_: "poisoned"
_stats_: | 16 (+3) | 14 (+2) | 13 (+1) | 14 (+2) | 12 (+1) | 16 (+3) |

___Shapechanger.___ The yuan-ti can use its action to polymorph into a Medium snake, or back into its true form. Its statistics are the same in each form. Any equipment it is wearing or carrying isn't transformed. It doesn't change form if it dies.

___Innate Spellcasting (Yuan_ti Form Only).___ The yuan_ti's innate spellcasting ability is Charisma (spell save DC 13). The yuan_ti can innately cast the following spells, requiring no material components:

At will: animal friendship (snakes only)

3/day: suggestion

___Magic Resistance.___ The yuan-ti has advantage on saving throws against spells and other magical effects.

___Malison Type.___ The yuan_ti has one of the following types:

Type 1: Human body with snake head

Type 2: Human head and body with snakes for arms

Type 3: Human head and upper body with a serpentine lower body instead of legs

**Actions**

___Multiattack (Yuan_ti Form Only).___ The yuan_ti makes two bite attacks using its snake arms.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 5 (1d4 + 3) piercing damage plus 7 (2d6) poison damage.