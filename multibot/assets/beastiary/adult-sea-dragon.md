"Adult Sea Dragon";;;_size_: Huge dragon
_alignment_: neutral evil
_challenge_: "16 (15000 XP)"
_languages_: "Common, Draconic"
_skills_: "Perception +12, Stealth +5"
_senses_: ", passive Perception 22"
_saving_throws_: "Dex +5, Con +11, Wis +7, Cha +9"
_damage_immunities_: "cold"
_speed_: "40 ft., fly 80 ft., swim 60 ft."
_hit points_: "225 (18d12 + 108)"
_armor class_: "19 (natural armor)"
_stats_: | 25 (+7) | 10 (+0) | 23 (+6) | 17 (+3) | 15 (+2) | 19 (+4) |

___Amphibious.___ The dragon can breathe air and water.

___Legendary Resistance (3/Day).___ If the dragon fails a saving throw, it can choose to succeed instead.

___Siege Monster.___ The dragon deals double damage to objects and structures.

**Actions**

___Multiattack.___ The dragon can use its Frightful Presence. It then makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +12 to hit, reach 10 ft., one target. Hit: 18 (2d10 + 7) piercing damage plus 5 (1d10) cold damage.

___Claw.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 14 (2d6 + 7) slashing damage.

___Tail.___ Melee Weapon Attack: +12 to hit, reach 15 ft., one target. Hit: 16 (2d8 + 7) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon's choice that is within 120 feet of the dragon and aware of it must succeed on a DC 17 Wisdom saving throw or become frightened for 1 minute. A creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the dragon's Frightful Presence for the next 24 hours.

___Tidal Breath (Recharge 5-6).___ The dragon exhales a crushing wave of frigid seawater in a 60-foot cone. Each creature in that area must make a DC 19 Dexterity saving throw. On a failure, the target takes 33 (6d10) bludgeoning damage and 33 (6d10) cold damage, and is pushed 30 feet away from the dragon and knocked prone. On a successful save the creature takes half as much damage and isn't pushed or knocked prone.

**Legendary** Actions

___The dragon can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. The dragon regains spent legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each creature within 15 feet of the dragon must succeed on a DC 20 Dexterity saving throw or take 14 (2d6 + 7) bludgeoning damage and be knocked prone. The dragon can then move up to half its flying speed, or half its swim speed if in the water.

