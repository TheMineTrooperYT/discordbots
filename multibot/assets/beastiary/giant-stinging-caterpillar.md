"Giant Stinging Caterpillar";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_speed_: "10 ft., fly 50 ft., swim 50 ft."
_hit points_: "14 (3d8 + 1)"
_armor class_: "12"
_stats_: | 10 (0) | 14 (+2) | 12 (+1) | 1 (-5) | 8 (-1) | 3 (-4) |

**Actions**

___Sting.___ Sting. Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 5 (1d6 + 2) piercing damage, and the target must make a DC 11 Constitution saving throw, taking 10 (3d6) poison damage on a failed save, or half as much damage on a successful one. If the poison damage reduces the target to 0 hit points, the target is stable but poisoned for 1 hour, even after regaining hit points, and is paralyzed while poisoned in this way.
