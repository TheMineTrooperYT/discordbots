"Mudbog";;;_size_: Large ooze
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_skills_: "Stealth +1"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 10"
_damage_immunities_: "acid, psychic"
_damage_resistances_: "fire"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, prone"
_speed_: "10 ft., swim 10 ft."
_hit points_: "51 (6d10 + 18)"
_armor class_: "9"
_stats_: | 20 (+5) | 8 (-1) | 17 (+3) | 1 (-5) | 10 (+0) | 3 (-4) |

___Acid.___ A mudbog secretes a digestive acid that quickly dissolves organic
material, but not metal or stone. A creature that attacks the mudbog takes
3 (1d6) acid damage. Any wood or other organic material that touches
the mudbog is pitted. Wooden weapons suffer a cumulative –1 penalty to
damage rolls made with it unless it is magical. When this penalty reaches –5, the weapon is destroyed.

___Amorphous.___ The ooze can move through a space as narrow as 1 inch
wide without squeezing.

___False Appearance.___ The mudbog, while not moving, is indistinguishable
from a muddy puddle.

**Actions**

___Engulf.___ The mudbog moves up to its speed. While doing so, it can
enter Medium or smaller creatures’ spaces. Whenever the mudbog enters
a creature’s space, the creature must make a DC 13 Dexterity saving throw.
On a successful save, the creature can choose to be pushed 5 feet back or
to the side of the mudbog. A creature that chooses not to be pushed suffers
the consequences of a failed saving throw. On a failed save, the creature
is engulfed and the mudbog enters the creature’s space. The creature takes
10 (3d6) acid damage. The engulfed creature can’t breathe, is restrained,
and takes 21 (6d6) acid damage at the start of each of the ooze’s turns.
When the mudbog moves, the engulfed creature moves with it.

An engulfed creature can try to escape by taking an action to make a
DC 13 Strength check. On a success, the creature escapes and enters a
space of its choice within 5 feet of the ooze.
