"Almiraj";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "darkvision 30ft., passive Perception 14"
_skills_: "Perception +4, Stealth +5"
_speed_: "50 ft."
_hit points_: "3 (1d6)"
_armor class_: "13"
_stats_: | 2 (-4) | 16 (+3) | 10 (0) | 2 (-4) | 14 (+2) | 10 (0) |

___Keen Senses.___ The almiraj has advantage on Wisdom (Perception) checks that rely on hearing or sight.

**Actions**

___Horn.___ Mele Weapon Attack: +5 to hit, reach 5ft., one target. Hit: 5 (1d4 +3) piercing damage.