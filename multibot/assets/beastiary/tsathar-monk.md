"Tsathar Monk";;;_size_: Medium monstrosity
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Abyssal, Tsathar"
_skills_: "Nature +5, Perception +5, Religion +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_speed_: "30 ft., swim 30 ft."
_hit points_: "52 (8d8 + 16)"
_armor class_: "14"
_stats_: | 15 (+2) | 18 (+4) | 14 (+2) | 12 (+1) | 16 (+3) | 11 (+0) |

___Amphibious.___ The tsathar can breathe air and water.

___Keen Smell.___ The tsathar has advantage on Wisdom (Perception) checks
that rely on smell.

___Slimy.___ Tsathar continuously cover themselves with muck and slime.
Creatures attempting to grapple a tsathar do so with disadvantage.

___Standing Leap.___ The tsathar’s long jump is up to 20 feet and its high
jump is up to 10 feet, with or without a running start.

**Actions**

___Multiattack.___ The tsathar can make three melee attacks: two with its
claws and one bite. It can use its flurry of blows or stunning strike ability
in place of one of the claw attacks.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit:
8 (1d8 + 4) slashing damage, and the target must succeed on a DC 13
Constitution saving throw or become the living host to a tsathar egg,
which over the course of the egg maturing, migrates to the chest cavity of
the host. The host creature must make another DC 13 Constitution saving
throw after 24 hours of the egg having been implanted. A failed saving
throw results in the host becoming violently ill, followed by a deep comalike state that lasts 2d6 + 2 days. At the end of each day, the host can
attempt another saving throw with a success indicating that its body has
Tsathar managed to destroy the egg through normal immune response. At the end
of the incubation period, the host awakes to excruciating pain as the young
tsathar, freed from its egg, tears its way out of the host, who is reduced to
0 hit points in the process.

A DC 16 Wisdom (Medicine) check can be attempted to surgically
extract an egg from the host. A lesser restoration spell will also cure the
condition and purge the host of the egg.

___Flurry of Blows (3/day).___ Melee Weapon Attack: +6 to hit, reach
5 ft., one target. Hit: 14 (3d6 + 4) bludgeoning damage, and the
target suffers one of the following effects of its choice:
* _Prone._ The target must succeed on a Dexterity saving throw
(DC 14) or be knocked prone.
* _Pushed._ The target must make a Strength saving throw or
be pushed up to 15 feet away from the tsathar.
* _Agog._ The target can’t take reactions until the end of the
tsathar’s next turn.

___Stunning Strike (3/day).___ Melee Weapon Attack: +6 to hit,
reach 5 ft., one target. Hit: 8 (1d8 + 4) bludgeoning damage,
and the target must succeed on a DC 14 Constitution saving
throw or be stunned until the end of the tsathar’s next turn.

**Reactions**

___Deflect Missiles.___ If the tsathar has one hand free, it
can use its reaction in response to being hit with a ranged
weapon attack. It reduces the damage by 14 (1d10 + 9). If it
reduces the damage to 0, it can catch the missile if it is small
enough for it to hold with one hand.
