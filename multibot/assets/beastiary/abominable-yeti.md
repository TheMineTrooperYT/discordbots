Abominable Yeti;;;_size_: Huge monstrosity
_alignment_: chaotic evil
_challenge_: 9 (5,000 XP)
_languages_: Yeti
_senses_: darkvision 60 ft.
_skills_: Perception +5, Stealth +4
_damage_immunities_: cold
_speed_: 40 ft., climb 40 ft.
_hit points_: 137 (11d12+66)
_armor class_: 15 (natural armor)
_stats_: | 24 (+7) | 10 (0) | 22 (+6) | 9 (-1) | 13 (+1) | 9 (-1) |

___Fear of Fire.___ If the yeti takes fire damage, it has disadvantage on attack rolls and ability checks until the end of its next turn.

___Keen Smell.___ The yeti has advantage on Wisdom (Perception) checks that rely on smell.

___Snow Camouflage.___ The yeti has advantage on Dexterity (Stealth) checks made to hide in snowy terrain.

**Actions**

___Multiattack.___ The yeti can use its Chilling Gaze and makes two claw attacks.

___Claw.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 14 (2d6 + 7) slashing damage plus 7 (2d6) cold damage.

___Chilling Gaze.___ The yeti targets one creature it can see within 30 feet of it. If the target can see the yeti, the target must succeed on a DC 18 Constitution saving throw against this magic or take 21 (6d6) cold damage and then be paralyzed for 1 minute, unless it is immune to cold damage. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If the target's saving throw is successful, or if the effect ends on it, the target is immune to this yeti's gaze for 1 hour.

___Cold Breath (Recharge 6).___ The yeti exhales a 30-foot cone of frigid air. Each creature in that area must make a DC 18 Constitution saving throw, taking 45 (10d8) cold damage on a failed save, or half as much damage on a successful one.