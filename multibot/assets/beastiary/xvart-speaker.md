"Xvart Speaker";;;_size_: Small humanoid (xvart)
_alignment_: chaotic evil
_challenge_: "1/8 (25 XP)"
_languages_: "Abyssal plus one other language, usually Common or Goblin"
_senses_: "darkvision 30 ft."
_skills_: "Stealth +4"
_speed_: "30 ft."
_hit points_: "7 (2d6)"
_armor class_: "13 (leather armor)"
_stats_: | 8 (-1) | 14 (+2) | 10 (0) | 11 (0) | 7 (-2) | 7 (-2) |