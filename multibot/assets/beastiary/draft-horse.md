"Draft Horse";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_speed_: "40 ft."
_hit points_: "19 (3d10+3)"
_armor class_: "10"
_stats_: | 18 (+4) | 10 (0) | 12 (+1) | 2 (-4) | 11 (0) | 7 (-2) |

**Actions**

___Hooves.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (2d4 + 4) bludgeoning damage.