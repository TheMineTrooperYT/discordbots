"Sea Spawn";;;_size_: Medium humanoid
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "understands Aquan and Common but can't speak"
_senses_: "darkvision 120 ft."
_speed_: "20 ft., swim 30 ft."
_hit points_: "32 (5d8+10)"
_armor class_: "11 (natural armor)"
_stats_: | 15 (+2) | 8 (-1) | 15 (+2) | 6 (-2) | 10 (0) | 8 (-1) |

___Limited Amphibiousness.___ The sea spawn can breathe air and water, but needs to be submerged in the sea at least once a day for 1 minute to avoid suffocating.

**Actions**

___Multiattack.___ The sea spawn makes three attacks: two unarmed strikes and one with its Piscine Anatomy.

___Unarmed Strike.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4+2) bludgeoning damage.

___Piscine Anatomy.___ The sea spawn has one or more of the following attack options, provided it has the appropriate anatomy:

* ___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 4 (1d4+2) piercing damage.

* ___Poison Quills.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 3 (1d6) poison damage, and the target must succeed on a DC 12 Constitution saving throw or be poisoned for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

* ___Tentacle.___ Melee Weapon Attack: +5 to hit, reach 10 ft., one target. Hit: 5 (1d6+2) bludgeoning damage, and the target is grappled (escape DC 12) if it is a Medium or smaller creature. Until this grapple ends, the sea spawn can't use this tentacle on another target.
