"Aeshma (Rage Demon)";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "10 (5,900 XP)"
_languages_: "Abyssal, Celestial, Draconic, telepathy 120 ft."
_skills_: "Intimidation +7, Perception +11, Survival +7"
_senses_: "darkvision 120 ft., passive Perception 21"
_saving_throws_: "Dex +8, Wis +7"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "40 ft., fly 60 ft."
_hit points_: "216 (16d10 + 128)"
_armor class_: "19 (natural armor)"
_stats_: | 22 (+6) | 18 (+4) | 26 (+8) | 14 (+2) | 17 (+3) | 17 (+3) |

___Innate Spellcasting.___ The demon’s spellcasting ability is Charisma
(spell save DC 15, +7 to hit with spell attacks). The demon can innately
cast the following spells, requiring no material components:

* At will: _blight, detect evil and good, dispel magic_

* 3/day each: _bestow curse, web_

___Magic Resistance.___ The aeshma demon has advantage on saving throws
against spells and other magical effects.

___Magic Weapons.___ The aeshma demon’s weapon attacks are magical.

___Reckless.___ At the start of its turn, the aeshma demon can gain advantage
on all melee weapon attack rolls it makes during that turn, but attack rolls
against it have advantage until the start of its next turn.

**Actions**

___Multiattack.___ The aeshma demon makes two melee attacks.

___Spear.___ Melee or Ranged Weapon Attack: +10 to hit, reach 10 ft. or
range 20/60 ft., one target. Hit: 19 (3d8 + 6) piercing damage.

___Claws.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 15 (2d8 + 6) slashing damage.

___Summon Demon (1/day).___ The demon chooses what to summon
and attempts a magical summoning. An aeshma has a 30% chance of
summoning 1d3 vrocks, 1d2 hezrous, or one glabrezu.
A summoned demon appears in an unoccupied space within 60 feet of
its summoner, acts as an ally of its summoner, and can’t summon other
demons. It remains for 1 minute, until it or its summoner dies, or until its
summoner dismisses it as an action.
