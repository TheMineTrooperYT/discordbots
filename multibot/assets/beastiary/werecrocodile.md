"Werecrocodile";;;_size_: Medium humanoid (human, shapechanger)
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common (can't speak in crocodile form)"
_skills_: "Stealth +3"
_senses_: "passive Perception 10"
_damage_immunities_: "bludgeoning, piercing, and slashing from nonmagical attacks not made with silvered weapons"
_speed_: "30 ft. (20 ft., swim 30 ft. in crocodile or hybrid form)"
_hit points_: "104 (16d8 + 32)"
_armor class_: "10 in humanoid form, 12 (natural armor) in crocodile or hybrid form"
_stats_: | 16 (+3) | 10 (+0) | 14 (+2) | 10 (+0) | 10 (+0) | 8 (-1) |

___Shapechanger.___ The werecrocodile can use its action to
polymorph into a Large crocodile-humanoid hybrid or
into a Large crocodile, or back into its true form, which
is humanoid. Its statistics, other than its size and AC,
are the same in each form. Any equipment is is wearing
or carrying isn't transformed. It reverts to its true form
if it dies.

___Hold Breath.___ The werecrocodile can hold its breath for
15 minutes.

**Actions**

___Multiattack (Humanoid or Hybrid Form Only).___ The
werecrocodile makes two attacks with its halberd.

___Halberd.___ Melee Weapon Attack: +6 to hit, reach 10 ft.,
one target. Hit: 8 (1d10 + 3) slashing damage.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one
creature. Hit: 8 (1d10 + 3) piercing damage, and the
target is grappled (escape DC 13). Until this grapple
ends, the target is restrained, and the crocodile can't
bite another target. If the target is a humanoid, it must
succeed on a DC 13 Constitution saving throw or be
cursed with werecrocodile lycanthropy.
