"Demonblood Orc Warrior";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Orc"
_skills_: "Intimidation +2, Athletics +5"
_senses_: "darkvision 60 ft., truesight 30 ft., passive Perception 11"
_speed_: "30 ft."
_hit points_: "76 (8d10 + 32)"
_armor class_: "18 (chainmail, shield)"
_stats_: | 16 (+3) | 12 (+1) | 19 (+4) | 9 (-1) | 12 (+1) | 10 (+0) |

___Aggressive.___ As a bonus action, the orc can move up
to its speed toward a hostile creature that it can
see.

___Demon’s Sight.___ The orc has enhanced sight from its
demonic blood. It has truesight out to 30 ft.

**Actions**

___Multiattack.___ The orc makes two attacks with its
longsword or its spear.

___Longsword.___ Melee Weapon Attack: +5 to hit, reach
5 ft., one target. Hit: 7 (1d8 + 3) slashing damage,
or 8 (1d10 + 3) if used with two hands.

___Spear.___ Melee or Ranged Weapon Attack: +5 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 6
(1d6 + 3) piercing damage, or 7 (1d8 + 3) piercing
damage if used with two hands to make a melee
attack.

___Precision Attacks (Recharge 5-6).___ As a bonus action,
the orc taps into its demonic sight and finds its
opponent’s weak spots. Each weapon attack the orc
makes against an enemy within 30 feet this turn is
made with advantage.

**Reactions**

___Demonic Endurance.___ The orc reduces the damage
dealt by a single source to 10.
