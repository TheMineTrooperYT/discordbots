"Shoalar Quanderil";;;_size_: Medium humanoid (water genasi)
_alignment_: lawful evil
_challenge_: "4 (1,100 XP)"
_languages_: "Aquan, Common"
_skills_: "Arcana +4, Deception +5, Insight +2, Persuasion +5"
_speed_: "30 ft., swim 30 ft."
_hit points_: "60 (8d8+24)"
_armor class_: "10 (13 with mage armor)"
_damage_resistances_: "acid"
_stats_: | 11 (0) | 12 (+1) | 16 (+3) | 14 (+2) | 10 (0) | 17 (+3) |

___Amphibious.___ Shoalar can breathe air and water.

___Innate Spellcasting.___ Shoalar's innate spellcasting ability is Constitution (spell save DC 13, +5 to hit with spell attacks). He can innately cast the following spells:

* At will: _shape water_

* 1/day: _create or destroy water_

___Spellcasting.___ Shoalar is a 5th-level spellcaster. His spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). He knows the following sorcerer spells:

* Cantrips (at will): _acid splash, chill touch, friends, prestidigitation, ray of frost_

* 1st level (4 slots): _disguise self, mage armor, magic missile_

* 2nd level (3 slots): _hold person, misty step_

* 3rd level (2 slots): _tidal wave_

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or ranged 20/60 ft., one target. Hit: 3 (1d4 + 1) piercing damage.
