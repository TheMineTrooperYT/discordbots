"Minotaur Skeleton";;;_size_: Large undead
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "understands Abyssal but can't speak"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "40 ft."
_hit points_: "67 (9d10+18)"
_armor class_: "12 (natural armor)"
_damage_vulnerabilities_: "bludgeoning"
_condition_immunities_: "exhaustion, poisoned"
_stats_: | 18 (+4) | 11 (0) | 15 (+2) | 6 (-2) | 8 (-1) | 5 (-3) |

___Charge.___ If the skeleton moves at least 10 feet straight toward a target and then hits it with a gore attack on the same turn, the target takes an extra 9 (2d8) piercing damage. If the target is a creature, it must succeed on a DC 14 Strength saving throw or be pushed up to 10 feet away and knocked prone.

**Actions**

___Greataxe.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 17 (2d12 + 4) slashing damage.

___Gore.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) piercing damage.