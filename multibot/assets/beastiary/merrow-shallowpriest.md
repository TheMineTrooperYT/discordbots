"Merrow Shallowpriest";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Abyssal, Aquan"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "10 ft., swim 40 ft."
_hit points_: "75 (10d10 + 20)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 14 (+2) | 15 (+2) | 11 (+0) | 16 (+3) | 9 (-1) |

___Amphibious.___ The merrow can breathe air and water.

___Spellcasting.___ The merrow is a 6th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 13, +5 to hit with spell attacks). The merrow has the following druid spells prepared:

* Cantrips (at will): _druidcraft, minor illusion, shocking grasp_

* 1st level (4 slots): _cure wounds, fog cloud, thunderwave_

* 2nd level (3 slots): _hold person, mirror image, misty step_

* 3rd level (3 slots): _dispel magic, lightning bolt, sleet storm_

**Actions**

___Harpoon.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 11 (2d6 + 4) piercing damage. If the target is a Medium or smaller creature, the merrow can pull it 10 feet closer.
