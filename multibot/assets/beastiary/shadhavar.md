"Shadhavar";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "2 (450 XP)"
_languages_: "understands Elvish and Umbral but can't speak"
_skills_: "Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 12"
_speed_: "50 ft."
_hit points_: "97 (13d10 + 26)"
_armor class_: "14 (natural)"
_stats_: | 14 (+2) | 15 (+2) | 14 (+2) | 8 (-1) | 10 (+0) | 16 (+3) |

___Innate Spellcasting.___ A shadhavar's innate spellcasting ability score is Charisma (spell save DC 13). It can cast the following spells, requiring no components:

* At will: _disguise self _(as horse or unicorn only)

* 2/day: _darkness _(centered on itself, moves with the shadhavar)

___Magic Weapons.___ A shadhavar's gore attacks are magical.

___Shadesight.___ A shadhavar's darkvision functions in magical darkness.

**Actions**

___Multiattack.___ A shadhavar makes one gore attack and one hooves attack.

___Gore.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage.

___Hooves.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 12 (3d6 + 2) bludgeoning damage.

**Bonus** Actions

___Plaintive Melody (3/day).___ A shadhavar plays a captivating melody through its hollow horn. Creatures within 60 feet that can hear the shadhavar must make a successful DC 13 Wisdom saving throw or be charmed until the start of the shadhavar's next turn. A creature charmed in this way is incapacitated, its speed is reduced to 0, and a shadhavar has advantage on attack rolls against it.
