"Chelicerae";;;_size_: Large aberration
_alignment_: neutral evil
_challenge_: "7 (2900 XP)"
_languages_: "--"
_skills_: "Acrobatics +6, Athletics +9, Perception +5, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Dex +6, Wis +5, Cha +5"
_damage_immunities_: "poison"
_condition_immunities_: "charmed, poisoned"
_speed_: "40 ft., climb 30 ft."
_hit points_: "153 (18d10 + 54)"
_armor class_: "16 (natural armor)"
_stats_: | 22 (+6) | 17 (+3) | 17 (+3) | 14 (+2) | 15 (+2) | 14 (+2) |

___Magic Resistance.___ The chelicerae has advantage on saving throws against spells and other magical effects.

___Spellcasting.___ The chelicerae is an 8th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 13, +5 to hit with spell attacks). It requires no material components to cast its spells. The chelicerae has the following wizard spells prepared:

Cantrips: acid splash, mage hand, minor illusion, true strike

1st level: burning hands, detect magic, expeditious retreat, ray of sickness

2nd level: hold person, invisibility, scorching ray

3rd level: animate dead, haste, lightning bolt

4th level: phantasmal killer

___Siphon Spell Slots.___ The chelicerae cannot replenish its spells naturally. Instead, it uses grappled spellcasters as spell reservoirs, draining uncast spells to power its own casting. Whenever the chelicerae wishes to cast a spell, it consumes a number of spell slots from its victim equal to the spell slots necessary to cast the spell. If the victim has too few spell slots available, the chelicerae cannot cast that spell. The chelicerae can also draw power from drained spellcasters or creatures without magic ability. It can reduce a grappled creature's Wisdom by 1d4, adding 2 spell slots to its spell reservoir for every point lowered. A creature reduced to 0 Wisdom is unconscious until it regains at least one point, and can't offer any more power. A creature regains all lost Wisdom when it finishes a long rest.

___Spider Climb.___ Chelicerae can climb difficult surfaces, including upside down on ceilings, without requiring an ability check.

**Actions**

___Multiattack.___ The chelicerae makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 17 (2d10 + 6) piercing damage, and the target is grappled (escape DC 16). The target must also make a successful DC 16 Constitution saving throw or become poisoned. While poisoned this way, the target is unconscious and takes 1d4 Strength damage at the start of each of its turns. The poisoning ends after 4 rounds or when the target makes a successful DC 16 Constitution save at the end of its turn.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 15 (2d8 + 6) slashing damage.

