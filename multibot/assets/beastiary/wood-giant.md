"Wood Giant";;;_size_: Large giant
_alignment_: chaotic good
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Giant, Sylvan"
_skills_: "Acrobatics +5, Athletics +7, Perception +4, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "40 ft."
_hit points_: "76 (9d10 + 27)"
_armor class_: "16 (natural armor)"
_stats_: | 20 (+5) | 16 (+3) | 17 (+3) | 14 (+2) | 14 (+2) | 16 (+3) |

___Innate Spellcasting.___ The giant’s innate spellcasting ability is Charisma
(spell save DC 13, +5 to hit with spell attacks). It can innately cast the following spell, requiring no material components.

* At will: _alter self_

**Actions**

___Multiattack.___ The wood giant makes two greatsword or two longbow
attacks.

___Greatsword.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target.
Hit: 12 (2d6 + 5) slashing damage.

___Longbow.___ Ranged Weapon Attack: +5 to hit, range 150/600 ft., one
target. Hit: 12 (2d8 + 3) piercing damage.
