"Monolith Footman";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "Elvish, Umbral"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison, bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "40 ft."
_hit points_: "60 (8d10 + 16)"
_armor class_: "14 (natural armor)"
_stats_: | 18 (+4) | 12 (+1) | 14 (+2) | 10 (+0) | 10 (+0) | 10 (+0) |

___Blatant Dismissal.___ While in the courts or castles of the fey, a monolith footman that scores a successful hit with its longsword can try to force the substitution of the target with a shadow double. The target must succeed at a DC 10 Charisma saving throw or become invisible, silent, and paralyzed, while an illusory version of itself remains visible and audible.and under the monolith footman's control, shouting for a retreat or the like. Outside fey locales, this ability does not function.

___Fey Flame.___ The ritual powering a monolith footman grants it an inner flame that it can use to enhance its weapon or its fists with additional fire or cold damage, depending on the construct's needs.

___Simple Construction.___ Monolith footmen are designed with a delicate fey construction. They burst into pieces and are destroyed when they receive a critical hit.

**Actions**

___Longsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage plus 9 (2d8) cold or fire damage.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) bludgeoning damage plus 9 (2d8) cold or fire damage.

