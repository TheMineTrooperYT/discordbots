"Giant Centipede";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "blindsight 30 ft."
_speed_: "30 ft., climb 30 ft."
_hit points_: "4 (1d6+1)"
_armor class_: "13 (natural armor)"
_stats_: | 5 (-3) | 14 (+2) | 12 (+1) | 1 (-5) | 7 (-2) | 3 (-4) |

**Actions**

___Bite.___ Bite. Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 4 (1d4 + 2) piercing damage, and the target must succeed on a DC 11 Constitution saving throw or take 10 (3d6) poison damage. If the poison damage reduces the target to 0 hit points, the target is stable but poisoned for 1 hour, even after regaining hit points, and is paralyzed while poisoned in this way.