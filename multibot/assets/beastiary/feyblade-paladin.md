"Feyblade Paladin";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "Any three languages"
_skills_: "Arcana +6, Nature +6, Performance +8, Persuasion +8"
_senses_: "passive Perception 10"
_saving_throws_: "Wis +4, Cha +8"
_speed_: "30 ft."
_hit points_: "86 (8d10 + 4d8 + 24)"
_armor class_: "20 (plate, shield)"
_stats_: | 16 (+3) | 8 (-1) | 14 (+2) | 15 (+2) | 10 (+0) | 18 (+4) |

___Combat Caster.___ The paladin can perform the somatic
components of spells even when it has weapons or a
shield in one or both hands.

___Divine Smite.___ When the paladin hits with a melee
weapon attack, it can expend a spell slot to deal an
addition 9 (2d8) radiant damage, in addition to the
weapon's damage. This damage increases by 1d8
radiant damage per spell slot level above 1st.

___Dueling Fighting Style.___ The slayer gains a +2 bonus to
damage rolls while wielding a melee weapon in one
hand and no other weapons (included in the attack).

___Fey-Infused Weapon.___ The paladin's weapon is infused
with energies of fey. Whenever an enemy is struck by
its blade, it must succeed on a DC 16 Wisdom saving
throw or be affected by the _faerie fire_ spell for one
minute or until the paladin strikes another target with
its weapon.

___Pact Magic.___ The paladin is a 9th-level spellcaster. Its
spellcasting ability is Charisma (spell save DC 16, +8 to
hit with spell attacks). It regains these expended
spellslots when it finishes a short or long rest. The
paladin has the following warlock spells prepared:

* Cantrips (at will): _blade ward, eldritch blast, true strike_

* 1st-2nd level (2 2nd-level slots): _calm emotions, darkness, faerie fire, phantasmal force, sleep_

___Spellcasting.___ The paladin is a 9th-level spellcaster. Its
spellcasting ability is charisma (spell save DC 16, +8 to
hit with spell attacks). The paladin has the following
paladin spells prepared:

* 1st level (4 slots): _bless, detect good and evil, ensnaring strike, speak with animals_

* 2nd level (2 slots): _find steed, lesser restoration, magic weapon, moonbeam, misty step_

**Actions**

___Multiattack.___ The paladin makes two attacks with its
longsword.

___Longsword.___ Melee Weapon Attack: +7 to hit, reach 5 ft.,
one target. Hit: 10 (1d8 + 6) slashing damage, or 9
(1d10 + 4) slashing damage if used with two hands.
