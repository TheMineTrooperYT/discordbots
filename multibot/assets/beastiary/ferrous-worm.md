"Ferrous Worm";;;_size_: Medium monstrosity
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Perception +3, Stealth +7"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 14"
_saving_throws_: "Con +3"
_condition_immunities_: "prone"
_speed_: "30 ft., burrow 30 ft."
_hit points_: "27 (5d8 + 2)"
_armor class_: "16 (natural armor)"
_stats_: | 13 (+1) | 17 (+3) | 12 (+1) | 12 (+1) | 13 (+1) | 5 (-3) |

___Earth Glide.___ The ferrous worm can burrow through nonmagical,
unworked earth and stone. While doing so, the ferrous worm doesn’t
disturb the material it moves through.

___Camouflage.___ The ferrous worm has advantage on Dexterity (Stealth)
checks made to hide in rocky terrain and treasure piles.

___Treasure Sense.___ The ferrous worm can pinpoint, by scent, the location
of precious metals and stones, such as coins and gems, within 60 feet of it.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit:
10 (2d6 + 3) piercing damage, and the target must succeed on a DC 13
Constitution saving throw or be poisoned and take an additional 10 (3d6)
poison damage each day until cured. The poison can only be cured by
magical means such as _lesser restoration_.
