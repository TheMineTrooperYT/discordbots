"Evoker";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "9 (5,000 XP)"
_languages_: "any four languages"
_skills_: "Arcana +7, History +7"
_saving_throws_: "Int +7, Wis +5"
_speed_: "30 ft."
_hit points_: "66 (12d8+12)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 9 (-1) | 14 (+2) | 12 (+1) | 17 (+3) | 12 (+1) | 11 (0) |

___Spellcasting.___ The evoker is a 12th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 15, +7 to hit with spell attacks). The evoker has the following wizard spells prepared:

Cantrips (at will): _fire bolt* , light* , prestidigitation, ray of frost* _

1st level (4 slots): _burning hands* , mage armor, magic missile* _

2nd level (3 slots): _mirror image, misty step, shatter* _

3rd level (3 slots): _counterspell, fireball* , lightning bolt* _

4th level (3 slots): _ice storm* , stoneskin_

5th level (2 slots): _Bigby's hand* , cone of cold* _

6th level (1 slot): _chain lightning* , wall of ice* _

*Evocation spell

___Sculpt Spells.___ When the evoker casts an evocation spell that forces other creatures it can see to make a saving throw, it can choose a number of them equal to 1+the spell's level. These creatures automatically succeed on their saving throws against the spell. If a successful save means a chosen creature would take half damage from the spell, it instead takes no damage from it.

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 2 (1d6-1) bludgeoning damage, or 3 (1d8-1) bludgeoning damage if used with two hands.
