"Fogwarden";;;_size_: Medium aberration
_alignment_: neutral evil
_challenge_: "4 (1,100 XP)"
_languages_: "Auran, Common"
_skills_: "Perception +4"
_damage_immunities_: "cold, lightning, poison"
_damage_vulnerabilities_: "radiant"
_condition_immunities_: "poisoned"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "fly 40 ft."
_hit points_: "27 (5d8 + 5)"
_armor class_: "14 (natural armor)"
_stats_: | 11 (+0) | 16 (+3) | 12 (+1) | 12 (+1) | 11 (+0) | 14 (+2) |

___Animate Dead.___ The electrical aura of the fogwarden can animate
up to four dead creatures within 20 feet. The animated creatures
resemble zombies and are under the control of the fogwarden. If
the fogwarden is slain or moves more than 20 feet from a zombie,
the animated creature collapses and cannot be animated again.
The animated creatures use zombie statistics.

___Electricity Discharge.___ A creature that touches the
fogwarden or hits it with a melee attack while within 5
feet of it takes 14 (4d6) lightning damage. If the creature
is wielding a metal weapon or wearing metal armor, it must
succeed on a DC 14 Constitution saving throw or be stunned until the end
of the fogwarden’s next turn.

___Gaseous Form.___ While in this form, the fogwarden’s only method of
movement is a flying speed of 10 feet. The Fogwarden can enter and
occupy the space of another creature. The Fogwarden has resistance
to nonmagical damage, is still hypersensitive to sunlight, and it has
advantage on Strength, Dexterity, and Constitution saving throws. The
Fogwarden can pass through small holes, narrow openings, and even mere
cracks, though it treats liquids as though they were solid. The Fogwarden
can’t fall and remains hovering in the air even when stunned or otherwise
incapacitated.

___Innate Spellcasting.___ The fogwarden’s spellcasting ability is Charisma
(spell save DC 12, +4 to hit with spell attacks). The fogwarden can
innately cast the following, requiring no material components.

* At will: _animate dead_

___Sunlight Hypersensitivity.___ The fogwarden takes 20 radiant damage
when it starts its turn in sunlight. While in sunlight, it has disadvantage on
attack rolls and ability checks.

**Actions**

___Shock.___ Melee Spell Attack: +4 to hit, reach 5 ft., one creature. Hit: 9 (2d8) lightning damage.
