"Knight";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "any one language (usually Common)"
_saving_throws_: "Con +4, Wis +2"
_speed_: "30 ft."
_hit points_: "52 (8d8+16)"
_armor class_: "18 (plate)"
_stats_: | 16 (+3) | 11 (0) | 14 (+2) | 11 (0) | 11 (0) | 15 (+2) |

___Brave.___ The knight has advantage on saving throws against being frightened.

**Actions**

___Multiattack.___ The knight makes two melee attacks.

___Greatsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) slashing damage.

___Heavy Crossbow.___ Ranged Weapon Attack: +2 to hit, range 100/400 ft., one target. Hit: 5 (1d10) piercing damage.

___Leadership (Recharges after a Short or Long Rest).___ For 1 minute, the knight can utter a special command or warning whenever a nonhostile creature that it can see within 30 ft. of it makes an attack roll or a saving throw. The creature can add a d4 to its roll provided it can hear and understand the knight. A creature can benefit from only one Leadership die at a time. This effect ends if the knight is incapacitated.

**Reactions**

___Parry.___ The knight adds 2 to its AC against one melee attack that would hit it. To do so, the knight must see the attacker and be wielding a melee weapon.