"Shoggoth";;;_size_: Huge aberration
_alignment_: chaotic neutral
_challenge_: "19 (22000 XP)"
_languages_: "Void Speech"
_skills_: "Perception +9"
_senses_: "darkvision 120 ft., tremorsense 60 ft., passive Perception 19"
_damage_immunities_: " cold, thunder, slashing"
_damage_resistances_: "fire, bludgeoning, piercing"
_condition_immunities_: " blinded, deafened, prone, stunned, unconscious"
_speed_: "50 ft., climb 30 ft., swim 30 ft."
_hit points_: "387 (25d12 + 225)"
_armor class_: "18 (natural armor)"
_stats_: | 26 (+8) | 14 (+2) | 28 (+9) | 12 (+1) | 16 (+3) | 13 (+1) |

___Anaerobic.___ A shoggoth doesn't need oxygen to live. It can exist with equal comfort at the bottom of the ocean or in the vacuum of outer space.

___Absorb Flesh.___ The body of a creature that dies while grappled by a shoggoth is completely absorbed into the shoggoth's mass. No portion of it remains to be used in raise dead, reincarnate, and comparable spells that require touching the dead person's body.

___Amorphous.___ A shoggoth can move through a space as small as 1 foot wide. It must spend 1 extra foot of movement for every foot it moves through a space smaller than itself, but it isn't subject to any other penalties for squeezing.

___Hideous Piping.___ The fluting noises made by a shoggoth are otherworldly and mind-shattering. A creature that can hear this cacophony at the start of its turn and is within 120 feet of a shoggoth must succeed on a DC 15 Wisdom saving throw or be confused (as the spell confusion) for 1d4 rounds. Creatures that roll a natural 20 on this saving throw become immune to the Hideous Piping for 24 hours. Otherwise, characters who meet the conditions must repeat the saving throw every round.

___Keen Senses.___ A shoggoth has advantage on Wisdom (Perception) checks that rely on hearing or smell.

___Rolling Charge.___ If the shoggoth moves at least 20 feet straight toward a creature and hits it with a slam attack on the same turn, that creature must succeed on a DC 20 Dexterity saving throw or be knocked prone. If the creature is knocked prone, the shoggoth immediately moves into the creature's space as a bonus action and crushes the creature beneath its bulk. The crushed creature can't breathe, is restrained, and takes 11 (2d10) bludgeoning damage at the start of each of the shoggoth's turns. A crushed creature remains in its space and does not move with the shoggoth. A crushed creature can escape by using an action and making a successful DC 19 Strength check. On a success, the creature crawls into an empty space within 5 feet of the shoggoth.

**Actions**

___Multiattack.___ The shoggoth makes 1d4 + 1 slam attacks. Reroll the number of attacks at the start of each of the shoggoth's turns.

___Slam.___ Melee Weapon Attack: +14 to hit, reach 15 ft., one target. Hit: 30 (4d10 + 8) bludgeoning damage, and the target is grappled (escape DC 18) and restrained. The shoggoth can grapple any number of creatures simultaneously, and this has no effect on its number of attacks.

