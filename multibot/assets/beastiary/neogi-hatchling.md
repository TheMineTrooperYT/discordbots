"Neogi Hatchling";;;_size_: Tiny aberration
_alignment_: lawful evil
_challenge_: "1/8 (25 XP)"
_senses_: "darkvision 60 ft."
_speed_: "20 ft., climb 20 ft."
_hit points_: "7 (3d4)"
_armor class_: "11"
_stats_: | 3 (-4) | 13 (+1) | 10 (0) | 6 (-2) | 10 (0) | 9 (-1) |

___Mental Fortitude.___ The hatchling has advantage on saving throws against being charmed or frightened, and magic can't put the hatchling to sleep.

___Spider Climb.___ The hatchling can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4+1) piercing damage plus 7 (2d6) poison damage, and the target must succeed on a DC 10 Constitution saving throw or become poisoned for 1 minute. A target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.