"Deep One";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Void Speech"
_senses_: "darkvision 120 ft., passive Perception 9"
_saving_throws_: "Str +5, Con +4, Cha +3"
_damage_vulnerabilities_: "fire"
_damage_resistances_: "cold"
_speed_: "30 ft., swim 30 ft."
_hit points_: "91 (14d8 + 28)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 12 (+1) | 14 (+2) | 10 (+0) | 8 (-1) | 12 (+1) |

___Amphibious.___ A deep one can breathe air or water with equal ease.

___Frenzied Rage.___ On its next turn after a deep one takes 10 or more damage from a single attack, it has advantage on its claws attack and adds +2 to damage.

___Lightless Depths.___ A deep one is immune to the pressure effects of the deep ocean.

___Ocean Change.___ A deep one born to a human family resembles a human child, but transforms into an adult deep one between the ages of 16 and 30.

**Actions**

___Claws.___ Melee Weapon Attack. +5 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) slashing damage.

