"Helmed Horror";;;_size_: Medium construct
_alignment_: unaligned
_challenge_: "4 (1,100 XP)"
_languages_: "understands the languages of its creator but can't speak"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_skills_: "Perception +4"
_damage_immunities_: "force, necrotic, poison"
_speed_: "30 ft., fly 30 ft."
_hit points_: "60 (8d8+24)"
_armor class_: "20 (plate, shield)"
_condition_immunities_: "blinded, charmed, deafened, frightened, paralyzed, petrified, poisoned, stunned"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_stats_: | 18 (+4) | 13 (+1) | 16 (+3) | 10 (0) | 10 (0) | 10 (0) |

___Magic Resistance.___ The helmed horror has advantage on saving throws against spells and other magical effects.

___Spell Immunity.___ The helmed horror is immune to three spells chosen by its creator. Typical immunities include fireball, heat metal, and lightning bolt.

**Actions**

___Multiattack.___ The helmed horror makes two longsword attacks.

___Longsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage, or 9 (1d10 + 4) slashing damage if used with two hands.