"Sahuagin Priestess";;;_size_: Medium humanoid (sahuagin)
_alignment_: lawful evil
_challenge_: "2 (450 XP)"
_languages_: "Sahuagin"
_senses_: "darkvision 120 ft."
_skills_: "Perception +6, Religion +3"
_speed_: "30 ft., swim 40 ft."
_hit points_: "33 (6d8+6)"
_armor class_: "12 (natural armor)"
_stats_: | 13 (+1) | 11 (0) | 12 (+1) | 12 (+1) | 14 (+2) | 13 (+1) |

___Blood Frenzy.___ The sahuagin has advantage on melee attack rolls against any creature that doesn't have all its hit points.

___Limited Amphibiousness.___ The sahuagin can breathe air and water, but it needs to be submerged at least once every 4 hours to avoid suffocating.

___Shark Telepathy.___ The sahuagin can magically command any shark within 120 feet of it, using a limited telepathy.

___Spellcasting.___ The sahuagin is a 6th-level spellcaster. Her spellcasting ability is Wisdom (spell save DC 12, +4 to hit with spell attacks). She has the following cleric spells prepared:

* Cantrips (at will): _guidance, thaumaturgy_

* 1st level (4 slots): _bless, detect magic, guiding bolt_

* 2nd level (3 slots): _hold person, spiritual weapon _(trident)

* 3rd level (3 slots): _mass healing word, tongues_

**Actions**

___Multiattack.___ The sahuagin makes two melee attacks: one with her bite and one with her claws.

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) piercing damage.

___Claws.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) slashing damage.
