"Brown Bear";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_skills_: "Perception +3"
_speed_: "40 ft., climb 30 ft."
_hit points_: "34 (4d10+12)"
_armor class_: "11 (natural armor)"
_stats_: | 19 (+4) | 10 (0) | 16 (+3) | 2 (-4) | 13 (+1) | 7 (-2) |

___Keen Smell.___ The bear has advantage on Wisdom (Perception) checks that rely on smell.

**Actions**

___Multiattack.___ The bear makes two attacks: one with its bite and one with its claws.

___Bite.___ Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage.
