"Vaettir";;;_size_: Medium undead
_alignment_: lawful evil
_challenge_: "4 (1100 XP)"
_languages_: "the languages it knew in life"
_senses_: "truesight 30 ft., darkvision 60 ft., passive Perception 11"
_saving_throws_: "Dex +4, Con +5, Wis +3, Cha +4"
_damage_immunities_: "necrotic, poison"
_condition_immunities_: "charmed, frightened, poisoned"
_speed_: "30 ft."
_hit points_: "120 (16d8 + 48)"
_armor class_: "15 (chain shirt)"
_stats_: | 20 (+5) | 14 (+2) | 16 (+3) | 10 (+0) | 12 (+1) | 14 (+2) |

___Covetous Bond.___ Corpse-black vaettir can see the face of any creature holding or carrying any item the vaettir ever claimed as its own. It also detects the direction and distance to items it ever owned, so long as that item is currently owned by another. If the item changes hands, the new owner becomes the target of the vaettir's hunt. Bone-white vaettir see individuals who have offended them. Neither time nor distance affects these abilities, so long as both parties are on the same plane.

___Deathless.___ The vaettir is destroyed when reduced to 0 hit points, but it returns to unlife where it fell on the next nightfall with full hit points. It can be killed only by removing its head, burning the corpse, and dumping the ashes in the sea, or by returning it to its burial mound, placing an open pair of scissors on its chest, and driving pins through its feet.

___Innate Spellcasting.___ The vaettir's innate spellcasting ability is Charisma (spell save DC 12). It can innately cast the following spells, requiring no material components:

2/day each: gaseous form, hunter's mark

1/day each: enlarge/reduce, phantom steed

1/week each: bestow curse, geas, remove curse

___Sunlight Sensitivity.___ Vaettir avoid daylight. A vaettir in direct sunlight has disadvantage on attack rolls and ability checks.

**Actions**

___Multiattack.___ The vaettir makes two greataxe attacks or two longbow attacks.

___Greataxe.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (1d12 + 5) slashing damage plus 3 (1d6) necrotic damage.

___Longbow.___ Ranged Weapon Attack: +4 to hit, range 150/600 ft., one target. Hit: 6 (1d8 + 2) piercing damage.

___Corpse Breath (Recharge 5-6).___ The vaettir spews forth a 15.foot cone of putrid gas. Those caught in the area must succeed on a DC 13 Constitution saving throw or become poisoned for 1d4 rounds.

___Maddening Gaze (1/Day).___ The vaettir can lock eyes with a creature and drive it mad. Any creature within 30 feet of a vaettir that is the focus of its gaze must make a DC 12 Charisma saving throw or become confused (as the spell) for 1d4 rounds. If the save is successful, the target is immune to the effect for 24 hours.

