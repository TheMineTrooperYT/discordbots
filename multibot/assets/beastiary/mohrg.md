"Mohrg";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "8 (3,900 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned, stunned"
_speed_: "30 ft."
_hit points_: "112 (15d8 + 45)"
_armor class_: "12"
_stats_: | 18 (+4) | 14 (+2) | 17 (+3) | 11 (+0) | 10 (+0) | 8 (-1) |

___Create Spawn.___ Any humanoid creature slain by the mohrg rises as a
zombie at the beginning of the mohrg’s next turn. If this occurs, the mohrg
regains 10 hit points, and the morhg can immediately make one slam
attack as a reaction.

**Actions**

___Multiattack.___ The mohrg makes two slam attacks and one attack with
its tongue.

___Slam.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13
(2d8 + 4) bludgeoning damage, and the target is grappled and restrained
(escape DC15), and the morhg can’t grapple another creature or use its
slam attack.

___Tongue.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit:
The target must make a DC 16 Constitution saving throw. On a failed
save, the target takes 21 (6d6) necrotic damage and is paralyzed for 1
minute. The creature can repeat the saving throw at the end of each of its
turns, ending the effect on a success.
