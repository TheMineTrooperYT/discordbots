"Sea Giant";;;_size_: Large giant
_alignment_: chaotic neutral
_challenge_: "9 (5,000 XP)"
_languages_: "Aquan, Common, Giant"
_skills_: "Acrobatics +6, Athletics +13, Intimidation +8, Perception +8, Stealth +6"
_senses_: "darkvision 120 ft., passive Perception 18"
_saving_throws_: "Con +9"
_speed_: "40 ft., swim 30 ft."
_hit points_: "178 (17d10 + 85)"
_armor class_: "17 (natural armor)"
_stats_: | 29 (+9) | 14 (+2) | 20 (+5) | 17 (+3) | 18 (+4) | 19 (+4) |

___Amphibious.___ The sea giant can breathe air and water.

___Innate Spellcasting.___ The giant’s innate spellcasting ability is Charisma
(spell save DC 16, +8 to hit with spell attacks). It can innately cast the
following spells, requiring no material components:

* At will: _create or destroy water, detect magic_

* 5/day: _control water_

* 3/day: _control weather_

**Actions**

___Multiattack.___ The sea giant makes two slam attacks.

___Slam.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 18 (2d8 + 9) bludgeoning damage.

___Rock.___ Ranged Weapon Attack: +13 to hit, range 60/240 ft., one target.
Hit: 27 (4d8 + 9) bludgeoning damage.

___Crushing Pressure (Recharge 5–6).___ The sea giant chooses an area of
water no larger than a 50-foot cube with 30 ft. of it. The water pressure
within the space magically increases, and creatures within the area treat
it as difficult terrain. In addition, any creature who enters or begins its
turn within the area must make a DC 18 Constitution saving throw, taking
18 (4d8) bludgeoning damage on a failed saving throw, or half as much
damage on a successful one. The area remains affected by this magic for
1 minute, until the sea giant dismisses it as an action, or the sea giant dies.
