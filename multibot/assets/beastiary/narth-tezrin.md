"Narth Tezrin";;;_size_: Medium humanoid (tethyrian human)
_alignment_: chaotic good
_challenge_: "0 (10 XP)"
_languages_: "Common, Dwarvish"
_skills_: "Insight +4, Investigation +3, Perception +6, Persuasion +5"
_speed_: "30 ft."
_hit points_: "18 (4d8)"
_armor class_: "12"
_senses_: " passive Perception 16"
_stats_: | 10 (0) | 15 (+2) | 10 (0) | 12 (+1) | 14 (+2) | 16 (+3) |

___Cunning Action.___ On each of his turns, Narth can use a bonus action to take the Dash, Disengage, or Hide action.

**Actions**

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6+2) piercing damage.

___Hand Crossbow.___ Ranged Weapon Attack: +4 to hit, range 30/120 ft., one target. Hit: 5 (1d6+2) piercing damage. Narth carries twenty crossbow bolts.

**Roleplaying** Information

Narth sells gear to adventurers, and he also has an adventurous spirit. The Lionshield Coster pays him well, but he longs to make a name for himself. At the same time, he runsa a business with his partner Alaestra and knows she wouldn't forgive him if he ran off and never returned.

**Ideal:** "The bigger the risk, the greater the reward."

**Bond:** "I adore my colleague Alestra, and I'd like to do something to impress her."

**Flaw:** "I'll risk life and limb to become a legend.-"