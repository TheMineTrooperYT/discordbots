"Coh-Leop";;;_size_: Medium humanoid
_alignment_: chaotic neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Pheromones (between coh-leop only), Undercommon"
_senses_: "Darkvision 60 ft., Passive Perception 14"
_skills_: "Perception +4"
_speed_: "25 ft., burrow 20 ft., climb 25 ft."
_hit points_: "26 (4d8 + 8)"
_armor class_: "14 (natural armor)"
_stats_: | 14 (+2) | 9 (-1) | 14 (+2) | 10 (+0) | 10 (+0) | 6 (-2) |

___Multifaceted Perspective.___ Because of their large
and multifaceted eyes, coh-leop are hard to surprise.
The coh-leop has advantage on Wisdom (Perception)
checks that rely on sight and on all initiative rolls.

___Spider Climb.___ The coh-leop can climb difficult
surfaces, including upside down on ceilings, without
needing to make an ability check.

**Actions**

___Multiattack.___ The coh-leop makes two claw attacks.

___Macuahuitl.___ Melee Weapon Attack: +5 to hit, reach
5 ft., one target. Hit: 11 (2d6 +2) slashing damage.
When weapon damage is rolled for a savage weapon,
any damage result of 1 is treated as though it was a 2.

___Claws.___ Melee Weapon Attack: +5 to hit,
reach 5 ft., one target, Hit: 5 (2d4) slashing damage.

**Reactions**

___Stink.___ When the coh-leop is the target of an opportunity
attack it can use its reaction to spray a cloud
of noxious gas from its abdomen. Each creature
within 5 ft. must make a DC 12 Constitution saving
throw. On a failed save the attack is lost and the
creature cannot make opportunity attacks against
the coh-leop until the start of its next turn.
