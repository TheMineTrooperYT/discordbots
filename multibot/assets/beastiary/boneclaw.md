"Boneclaw";;;_page_number_: 121
_size_: Large undead
_alignment_: chaotic evil
_challenge_: "12 (8400 XP)"
_languages_: "Common plus the main language of its master"
_senses_: "darkvision 60 ft., passive Perception 16"
_skills_: "Perception +6, Stealth +7"
_saving_throws_: "Dex +7, Con +6, Wis +6"
_speed_: "40 ft."
_hit points_: "127  (17d10 + 34)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_damage_resistances_: "cold, necrotic; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 19 (+4) | 16 (+3) | 15 (+2) | 13 (+1) | 15 (+2) | 9 (0) |

___Rejuvenation.___ While its master lives, a destroyed boneclaw gains a new body in 1d10 hours, with all its hit points. The new body appears within 1 mile of the boneclaw's master.

___Shadow Stealth.___ While in dim light or darkness, the boneclaw can take the Hide action as a bonus action.

**Actions**

___Multiattack___ The boneclaw makes two claw attacks.

___Piercing Claw___ Melee Weapon Attack: +8 to hit, reach 15 ft., one target. Hit: 20 (3d10 + 4) piercing damage. If the target is a creature, the boneclaw can pull the target up to 10 feet toward itself, and the target is grappled (escape DC 14). The boneclaw has two claws. While a claw grapples a target, the claw can attack only that target.

___Shadow Jump___ If the boneclaw is in dim light or darkness, each creature of the boneclaw's choice within 5 feet of it must succeed on a DC 14 Constitution saving throw or take 34 (5d12 + 2) necrotic damage.
The boneclaw then magically teleports up to 60 feet to an unoccupied space it can see. It can bring one creature it's grappling, teleporting that creature to an unoccupied space it can see within 5 feet of its destination. The destination spaces of this teleportation must be in dim light or darkness.