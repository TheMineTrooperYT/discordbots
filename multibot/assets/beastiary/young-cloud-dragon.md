"Young Cloud Dragon";;;_size_: Large dragon
_alignment_: neutral
_challenge_: "8 (3,900 XP)"
_languages_: "Auran, Aquan, Common, Draconic"
_skills_: "History +6, Insight +4, Perception +7"
_senses_: "blindsight 120 ft., darkvision 120 ft., passive Perception 17"
_saving_throws_: "Dex +4, Con +6, Wis +4, Cha +5"
_damage_immunities_: "cold, lightning"
_speed_: "40 ft., fly 80 ft."
_hit points_: "136 (16d10 + 48)"
_armor class_: "16 (natural armor)"
_stats_: | 19 (+4) | 12 (+1) | 17 (+3) | 16 (+3) | 13 (+1) | 15 (+2) |

___Windborn.___ The dragon cannot be moved or knocked prone by
magical or nonmagical winds of any kind.

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two
with its claws.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 15
(2d10 + 4) piercing damage plus 7 (2d6) cold damage.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11
(2d6 + 4) slashing damage.

___Breath Weapons (Recharge 5–6).___ The dragon uses one of the following
breath weapons.

* _Cloud Breath._ The dragon exhales an icy blast in a 90-foot cone that
spreads around corners. Each creature in that area must make a DC 14
Constitution saving throw, taking 45 (10d8) cold damage on a failed
saving throw, or half as much damage on a successful one.

* _Wind Breath._ The dragon exhales gale-force winds in a 30-foot line that
is 10-feet wide. Creatures in this area must make a DC 14 Strength saving
throw, taking 36 (8d8) bludgeoning damage on a failed saving throw and
are pushed 30 feet directly away from the dragon. On a successful saving
throw, the creature takes half damage and is not pushed. A creature who
is pushed and strikes a solid surface takes 1d6 bludgeoning damage per
10 feet traveled. Nonmagical flames are completely extinguished, and
magical flames created by a spell of 3rd level or lower are immediately
dispelled.

___Cloud Form.___ The cloud dragon polymorphs into a Large cloud of mist,
or back into its true form. While in mist form, the dragon can’t take any
actions, speak, or manipulate objects. It is weightless, has a flying speed of
20 feet, can hover, and can enter a hostile creature’s space and stop there.
In addition, if air can pass through a space, the mist can do so without
squeezing, and it can’t pass through water. It has advantage on Strength,
Dexterity, and Constitution saving throws, it is immune to all nonmagical
damage, and still benefits from its Windborn feature.
