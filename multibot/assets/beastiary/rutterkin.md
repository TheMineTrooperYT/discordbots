"Rutterkin";;;_page_number_: 136
_size_: Medium fiend (demon)
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "understands Abyssal but can't speak"
_senses_: "darkvision 120 ft., passive Perception 11"
_damage_immunities_: "poison"
_speed_: "20 ft."
_hit points_: "37  (5d8 + 15)"
_armor class_: "12"
_condition_immunities_: "charmed, frightened, poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 14 (+2) | 15 (+2) | 17 (+3) | 5 (-2) | 12 (+1) | 6 (-2) |

___Crippling Fear.___ When a creature that isn't a demon starts its turn within 30 feet of three or more rutterkins, it must make a DC 11 Wisdom saving throw. The creature has disadvantage on the save if it's within 30 feet of six or more rutterkins. On a successful save, the creature is immune to the Crippling Fear of all rutterkins for 24 hours. On a failed save, the creature becomes frightened for 1 minute. While frightened in this way, the creature is restrained. At the end of each of the frightened creature's turns, it can repeat the saving throw, ending the effect on itself on a success.

**Actions**

___Bite___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 12 (3d6 + 2) piercing damage. If the target is a creature, it must succeed on a DC 13 Constitution saving throw against disease or become poisoned. At the end of each long rest, the poisoned target can repeat the saving throw, ending the effect on itself on a success. If the target is reduced to 0 hit points while poisoned in this way, it dies and instantly transforms into a living abyssal wretch. The transformation of the body can be undone only by a wish spell. (See notes)