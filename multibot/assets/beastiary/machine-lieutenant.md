"Machine Lieutenant";;;_size_: Medium construct
_alignment_: neutral
_challenge_: "4 (1,100 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_skills_: "Perception +4"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 14"
_damage_immunities_: "force, necrotic, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "blinded, charmed, deafened, frightened, paralyzed, petrified, poisoned, stunned"
_speed_: "40 ft."
_hit points_: "60 (8d8 + 24)"
_armor class_: "20 (plate, shield)"
_stats_: | 18 (+4) | 13 (+2) | 16 (+3) | 10 (+0) | 10 (+0) | 10 (+0) |

___Magic Resistance.___ The machine lieutenant has
advantage on saving throws against spells and
other magical effects.

___Both Guard and Ward.___ While the lieutenant is
within 5 feet of the attuned owner of the codex
automata, any attack targeting the codex’s
owner has a 50% chance to instead target the
machine lieutenant.

**Actions**

___Multiattack.___ The lieutenant makes two
longsword attacks.

___Longsword.___ Melee Weapon Attack: +6 to hit,
reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing
damage or 9 (1d10 + 4) slashing damage if used
with two hands.
