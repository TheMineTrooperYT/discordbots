"Woods Ape";;;_size_: Medium monstrosity
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_languages_: "Druidic"
_skills_: "Nature +6, Perception +7, Stealth +4 (+7 in forested terrain), Survival +10"
_senses_: "darkvision 60 ft., passive Perception 17"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft., climb 30 ft."
_hit points_: "82 (11d8 + 33)"
_armor class_: "11 (16 with barkskin)"
_stats_: | 17 (+3) | 13 (+1) | 16 (+3) | 11 (+0) | 18 (+4) | 7 (-2) |

___Land’s Stride.___ The woods ape can move through nonmagical difficult
terrain without using extra movement and can pass through nonmagical
plants without being slowed by them and without taking damage if they
have thorns, spines, or a similar hazard. In addition, the woods ape has
advantage on saving throws against plants that are magically created or
manipulated to impede movement.

___Innate Spellcasting.___ The woods ape’s innate spellcasting ability is
Wisdom (spell save DC 15, +7 to hit with spell attacks). It can cast the
following spells, requiring no material components:

At will: guidance, mending, create or destroy water, detect poison and
disease, entangle, speak with animals, speak with plants, tree stride

3/day each: animal messenger, barkskin, call lightning, locate animals
or plants, pass without trace

1/day each: awaken, commune with nature, conjure animals, plant
growth

**Actions**

___Multiattack.___ The woods ape makes two claw attacks.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10
(2d6 + 3) slashing damage.
