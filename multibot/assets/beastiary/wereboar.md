"Wereboar";;;_size_: Medium humanoid (human shapechanger)
_challenge_: "4 (1,100 XP)"
_languages_: "Common (can't speak in boar form)"
_skills_: "Perception +2"
_damage_immunities_: "bludgeoning, piercing, and slashing damage from nonmagical weapons that aren't silvered"
_speed_: "30 ft. (40 ft. in boar form)"
_hit points_: "78 (12d8+24)"
_armor class_: "10 (in humanoid form, 11 in boar and hybrid forms )"
_stats_: | 17 (+3) | 10 (0) | 15 (+2) | 10 (0) | 11 (0) | 8 (-1) |

___Shapechanger.___ The wereboar can use its action to polymorph into a boar-humanoid hybrid or into a boar, or back into its true form, which is humanoid. Its statistics, other than its AC, are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

___Charge (Boar or Hybrid Form Only).___ If the wereboar moves at least 15 feet straight toward a target and then hits it with its tusks on the same turn, the target takes an extra 7 (2d6) slashing damage. If the target is a creature, it must succeed on a DC 13 Strength saving throw or be knocked prone.

___Relentless (Recharges after a Short or Long Rest).___ If the wereboar takes 14 damage or less that would reduce it to 0 hit points, it is reduced to 1 hit point instead.

**Actions**

___Multiattack (Humanoid or Hybrid Form Only).___ The wereboar makes two attacks, only one of which can be with its tusks.

___Maul (Humanoid or Hybrid Form Only).___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) bludgeoning damage.

___Tusks (Boar or Hybrid Form Only).___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) slashing damage. If the target is a humanoid, it must succeed on a DC 12 Constitution saving throw or be cursed with wereboar lycanthropy.