"Seraph (SaF)";;;_size_: Large celestial
_alignment_: any good alignment
_challenge_: "10 (5,900 XP)"
_languages_: "all, telepathy 60 ft."
_senses_: "truesight 60 ft., passive Perception 23"
_saving_throws_: "Str +8, Con +9, Wis +9, Cha +9"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "radiant; bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: " charmed, exhaustion, frightened, poisoned"
_speed_: "30 ft."
_hit points_: "189 (18d10 + 90)"
_armor class_: "17 (natural armor)"
_stats_: | 18 (+4) | 18 (+4) | 20 (+5) | 18 (+4) | 20 (+5) | 20 (+5) |

___Angelic Weapons.___ The Seraph’s weapon attacks
are magical. When the Seraph hits with any
weapon, the weapon deals an extra 9 (2d8) radiant
damage (included in the attack).

___Magic Resistance.___ The Seraph has advantage on
saving throws against spells and other magical
effects from evil characters and sources.

___Aura of Protection Against Evil.___ Evil creatures
have disadvantage on attack rolls against all allies
within 5 feet of the Seraph. Allies in this area can’t
be charmed, frightened, or possessed by evil creatures. If an ally is already charmed, frightened, or
possessed by evil magic, the ally has advantage on
any new saving throw against the relevant effect.

___Vigilant.___ The Seraph cannot be flanked or surprised.

**Actions**

___Multiattack.___ The Seraph makes four attacks with
any combination of Good, Law, Alpha, and Omega,
but no more than two with the same weapon.

___Good.___ Melee Weapon Attack: +8 to hit, reach 5 ft.,
one target. Hit: 7 (1d6 + 4) bludgeoning damage
plus 9 (2d8) radiant damage. If the target is evil
and has 25 hit points or fewer after taking this
damage, it must succeed on a DC 17 Wisdom
saving throw or be destroyed. On a successful save,
the creature becomes frightened of the Seraph
until the end of the Seraph’s next turn.

___Law.___ Melee Weapon Attack: +8 to hit, reach 5 ft.,
one target. Hit: 7 (1d6 + 4) bludgeoning damage
plus 9 (2d8) radiant damage. If the target is
chaotic and has 25 hit points or fewer after taking
this damage, it must succeed on a DC 17 Wisdom
saving throw or be destroyed. On a successful save,
the creature becomes frightened of the Seraph
until the end of the Seraph’s turn.

___Alpha.___ Ranged Weapon Attack: +8 to hit, range
150/600 ft., one target. Hit: 13 (2d8 + 4) piercing
damage plus 9 (2d8) radiant damage. On a hit,
all allies adjacent to the target restore 1d8 + 5
hit points.

___Omega.___ Ranged Weapon Attack: +8 to hit, range
150/600 ft., one target. Hit: 13 (2d8 + 4) piercing
damage plus 9 (2d8) radiant damage. If the target
is chaotic or evil, it suffers the effects of a bane
spell until the end of its next turn.
