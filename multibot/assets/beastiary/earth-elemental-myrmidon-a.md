"Earth Elemental Myrmidon (A)";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "Terran, one language of its creator's choice"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "127 (17d8+51)"
_armor class_: "18 (plate)"
_condition_immunities_: "paralyzed, petrified, poisoned, prone"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 18 (+4) | 10 (0) | 17 (+3) | 8 (-1) | 10 (0) | 10 (0) |

___Magic Weapons.___ The myrmidon's weapon attacks are magical.

**Actions**

___Multiattack.___ The myrmidon makes two maul attacks.

___Maul.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage.

___Thunderous Strike (Recharge 6).___ The myrmidon makes one maul attack. If the attack hits, it deals an extra 16 (3d10) thunder damage, and the target must succeed on a DC 14 Strength saving throw or be knocked prone.