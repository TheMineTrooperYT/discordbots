"Mastodon";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "9 (5,000 XP)"
_languages_: "--"
_senses_: "passive Perception 11"
_speed_: "40 ft."
_hit points_: "270 (20d12 + 140)"
_armor class_: "15 (natural armor)"
_stats_: | 28 (+9) | 10 (+0) | 24 (+7) | 2 (-4) | 13 (+1) | 6 (-2) |

___Keen Smell.___ The mastodon has advantage on Wisdom (Perception)
checks that rely on smell.

___Trampling Charge.___ If the mastodon moves at least 20 feet straight
towards a creature and then hits with a gore attack on the same turn, that
creature must succeed on a DC 19 Strength check or be knocked prone.
If the target is prone, the mastodon can make one stomp attack against it
as a bonus action.

**Actions**

___Gore.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 27
(4d8 + 9) piercing damage.

___Stomp.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 48
(6d12 + 9) bludgeoning damage and the target must succeed on a DC 17
Constitution saving throw or be stunned until the end of the mammoth’s
next turn.
