"Folk Of Leng";;;_size_: Medium humanoid
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Void Speech"
_skills_: "Arcana +4, Deception +8, Perception +5"
_senses_: ", passive Perception 15"
_saving_throws_: "Dex +4, Wis +5"
_damage_immunities_: "necrotic"
_damage_resistances_: "cold"
_condition_immunities_: "frightened"
_speed_: "30 ft."
_hit points_: "68 (8d8 + 32)"
_armor class_: "14 (studded leather)"
_stats_: | 12 (+1) | 15 (+2) | 18 (+4) | 14 (+2) | 16 (+3) | 22 (+6) |

___Innate Spellcasting.___ The folk of Leng's innate spellcasting ability is Charisma (spell save DC 16). It can innately cast the following spells, requiring no material components:

* At will: _comprehend languages, minor illusion_

* 3/day each: _disguise self, suggestion_

* 1/day each: _dream, etherealness_

___Regeneration.___ The folk of Leng regains 5 hit points at the start of its turn. If the folk of Leng takes fire or radiant damage, this trait does not function at the start of its next turn. The folk of Leng dies only if it starts its turn with 0 hit points and does not regenerate. Even if slain, their bodies reform in a crypt of Leng and go on about their business.

___Void Stare.___ The folk of Leng can see through doors and around corners as a bonus action. As a result, they are very rarely surprised.

___Void Sailors.___ The folk of Leng can travel the airless void without harm.

**Actions**

___Etheric Harpoon.___ Ranged Weapon Attack: +8 to hit, range 30 ft., one target. Hit: 10 (1d8 + 6) necrotic damage, and the target must make a successful DC 13 Wisdom saving throw or be grappled (escape DC 13). In addition, armor has no effect against the attack roll of an etheric harpoon; only the Dexterity modifier factored into the target's AC is considered.

___Psychic Scimitar.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) slashing damage plus 3 (1d6) psychic damage.

___Hooked Spider Net (Recharge 5-6).___ Ranged Weapon Attack. +4 to hit, range 20/50 ft., one target. Hit: 3 (1d6) piercing damage plus 19 (3d12) poison damage, and the target is restrained. A successful DC 14 Constitution saving throw halves the poison damage.

