"Wind Goblin";;;_size_: Small humanoid
_alignment_: chaotic neutral
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Goblin, Sylvan"
_skills_: "Arcana +7, Investigation +7"
_senses_: "darkvision 120 ft., passive Perception 11"
_saving_throws_: "Int +7, Wis +3"
_damage_vulnerabilities_: "acid"
_damage_immunities_: "lightning"
_damage_resistances_: "poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "20 ft., fly 40 ft."
_hit points_: "36 (8d6 + 8)"
_armor class_: "14"
_stats_: | 13 (+1) | 19 (+4) | 13 (+1) | 20 (+5) | 13 (+1) | 11 (+0) |

___Innate Spellcasting.___ The wind goblin’s spellcasting ability is
Intelligence (spell save DC 15, +7 to hit with spell attacks). The wind
goblin can innately cast the following spells, requiring no material
components:

* At will: _dancing lights, mage hand, prestidigitation, shocking grasp_

* 3/day each: _detect magic, eldritch blast, faerie fire, gust of wind, identify_

* 1/day: _wind wall, call lightning_

___Scientific Spellcasting.___ Wind goblins have a strange relationship
with science and are able to use what they sincerely believe is scientific
invention to mimic the abilities of spellcasters. They also describe arcane
concepts in science-like jargon.

The following is a typical allocation of “scientific”, gadget-based spells
that a wind goblin might be able to cast, but specific spells vary from
goblin to goblin. Wind goblin “science” magic never requires a verbal
or material component. Instead, all spells require a gadget focus and the
somatic component of turning the cranks and working the levers (etc.) on
said focus. The process is usually loud and often smelly or smoky, with
superfluous electrical arcing.

The wind goblin is a 4th level spellcaster. Its spellcasting ability is
Intelligence (spell save DC 15, +7 to hit with spell attacks). It can cast the
following spells, requiring only somatic components:

* Cantrips (at will): _fire bolt, light, mending, message, minor illusion_

* 1st level (4/day\*): _expeditious retreat, fog cloud, thunderwave_

* 2nd level (3/day\*): _hold person, knock_

\* The uses per day represent fuel-related limitations.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or 20/60
ft., one target. Hit: 6 (1d4 + 4) piercing damage.

___Hand Crossbow.___ Ranged Weapon Attack: +6 to
hit, range 30/120 ft., one target. Hit: 7 (1d6 + 4)
piercing damage.
