"Yan-C-Bin";;;_size_: Huge elemental
_alignment_: neutral evil
_challenge_: "18 (20,000 XP)"
_languages_: "Auran"
_senses_: "blindsight 120 ft."
_damage_immunities_: "lightning, poison, thunder"
_saving_throws_: "Dex +13, Wis +11, Cha +12"
_speed_: "50 ft., fly 150 ft."
_hit points_: "283 (21d12+147)"
_armor class_: "22 (natural armor)"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, poisoned, prone, restrained"
_damage_resistances_: "cold, fire;  bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 18 (+4) | 24 (+7) | 24 (+7) | 16 (+3) | 21 (+5) | 23 (+6) |

___Air Form.___ Yan-C-Bin can enter a hostile creature's space and stop there. He can move through a space as narrow as 1 inch wide without squeezing if air could pass through that space.

___Empowered Attacks.___ Yan-C-Bin's slam attacks are treated as magical for the purpose of bypassing resistance and immunity to nonmagical weapons.

___Innate Spellcasting.___ Yan-C-Bin's innate spellcasting ability is Charisma (spell save DC 20, +12 to hit with spell attacks). He can innately cast the following spells, requiring no material components:

* At will: gust of wind, invisibility, lightning bolt

* 2/day each: chain lightning, cloudkill, haste

___Legendary Resistance (3/Day).___ If Yan-C-Bin fails a saving throw, he can choose to succeed instead.

___Magic Resistance.___ Yan-C-Bin has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ Yan-C-Bin makes two slam attacks.

___Slam.___ Melee Weapon Attack: +14 to hit, reach 10 ft., one target. Hit: 20 (3d8 + 7) force damage plus 10 (3d6) lightning damage.

___Thundercrack (Recharges after a Short or Long Rest).___ Yan-C-Bin unleashes a terrible thundercrack in a 100-foot- radius sphere centered on himself. All other creatures in the area must succeed on a DC 24 Constitution saving throw or take 31 (9d6) thunder damage and be deafened for 1 minute. On a successful save, a creature takes half as much damage and is deafened until the start of Yan-C-Bin's next turn.

___Change Shape.___ Yan-C-Bin polymorphs into a Medium humanoid. While in polymorphed form, a swirling breeze surrounds him, his eyes are pale and cloudy, and he loses the Air Form trait. He can remain in polymorphed form for up to 1 hour. Reverting to his true form requires an action.

___Summon Elementals (1/Day).___ Yan-C-Bin summons up to three air elementals and loses 30 hit points for each elemental he summons. Summoned elementals have maximum hit points, appear within 100 feet of Yan-C-Bin, and disappear if Yan-C-Bin is reduced to 0 hit points.

**Legendary** Actions

The yan-c-bin can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The yan-c-bin regains spent legendary actions at the start of its turn.

___Peal of Thunder.___ Yan-C-Bin unleashes a peal of thunder that can be heard out to a range of 300 feet. Each creature within 30 feet of Yan-C-Bin takes 5 (1d10) thunder damage.

___Teleport (Costs 2 Actions).___ Yan-C-Bin magically teleports up to 120 feet to an unoccupied space he can see. Anything Yan-C-Bin is wearing or carrying is teleported with him.

___Suffocate (Costs 3 Actions).___ Yan-C-Bin steals the air of one breathing creature he can see within 60 feet of him. The target must make a DC 21 Constitution saving throw. On a failed save, the target drops to 0 hit points and is dying. On a successful save, the target can't breathe or speak until the start of its next turn.