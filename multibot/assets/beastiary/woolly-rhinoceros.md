"Woolly Rhinoceros";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "6 (2,300 XP)"
_languages_: "--"
_skills_: "Perception +7"
_senses_: "passive Perception 17"
_speed_: "30 ft."
_hit points_: "172 (15d10 + 90)"
_armor class_: "14 (natural armor)"
_stats_: | 21 (+5) | 10 (+0) | 22 (+6) | 3 (-4) | 13 (+1) | 2 (-4) |

___Improved Critical.___ Gore attacks score a critical hit on a roll of 19 or 20.

___Keen Smell.___ The woolly rhinoceros has advantage on Wisdom
(Perception) checks that rely on smell.

___Charge.___ If the woolly rhinoceros moves at least 20 feet straight toward
a creature and then hits it with a gore attack on the same turn, the target
takes an extra 9 (2d8) piercing damage. If the target is a creature, it must
succeed on a DC 15 Strength saving throw or be knocked prone. If the
target is prone, the woolly rhinoceros can make one stomp attack against
it as a bonus action.

**Actions**

___Gore.___ Melee Weapon Attack: +8 to hit, reach 5 ft.,
one target. Hit: 14 (2d8 + 5) piercing damage.

___Stomp.___ Melee Weapon Attack: +8 to hit, reach 5
ft., one target. Hit: 18 (2d12 + 5) piercing damage.
