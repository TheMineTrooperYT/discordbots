"Centaur Mystic";;;_size_: Large monstrosity
_alignment_: neutral good
_challenge_: "2 (450 XP)"
_languages_: "Elvish, Sylvan"
_skills_: "Nature +2, Survival +5"
_senses_: "passive Perception 13"
_speed_: "50 ft."
_hit points_: "39 (6d10 + 6)"
_armor class_: "12"
_stats_: | 14 (+2) | 14 (+2) | 12 (+1) | 10 (+0) | 16 (+3) | 10 (+0) |

___Spellcasting.___ The centaur is a 3rd-level spellcaster.
Its spellcasting ability is Wisdom (spell save DC 13, +5 to hit with spell attacks). It has the following
Druid spells prepared:

* Cantrips (at will): _druidcraft, thornwhip_

* 1st level (4 slots): _animal friendship, faerie fire, healing word_

* 2nd level (2 slots): _barkskin, moonbeam_

**Actions**

___Multiattack.___ The centaur makes two attacks: one
with its quarterstaff and one with its hooves or two
with its longbow.

___Quarterstaff.___ Melee Weapon Attack: +4 to hit, reach
5ft., one target. Hit: 5 (1d6 + 2) bludgeoning
damage.

___Hooves.___ Melee Weapon Attack: +4 to hit, reach 5ft.,
one target. Hit: 7 (2d4 + 2) bludgeoning damage.

___Longbow.___ Ranged Weapon Attack: +4 to hit, range
150/600 ft., one target. Hit: 6 (1d8 + 2) piercing
damage.
