"Miremal";;;_size_: Small fey
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Sylvan, Umbral"
_skills_: "Perception +3, Stealth +5, Survival +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "30 ft., swim 30 ft."
_hit points_: "22 (5d6 + 5)"
_armor class_: "13"
_stats_: | 10 (+0) | 16 (+3) | 12 (+1) | 10 (+0) | 12 (+1) | 8 (-1) |

___Amphibious.___ The miremal can breathe air and water.

___Swamp Camouflage.___ The miremal has advantage on Dexterity (Stealth) checks made to hide in swampy terrain.

___Savage Move.___ If the miremal surprises a creature, it gets a bonus action it can use on its first turn of combat for a claw attack, a bite attack, or a Bog Spew attack.

**Actions**

___Multiattack.___ The miremal makes two attacks, one of which must be a claw attack.

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Bog Spew (Recharge 5-6).___ The miremal spews a noxious stream of bog filth mixed with stomach acid at a target up to 20 feet away. Target must succeed on a DC 11 Constitution saving throw or be blinded for 1d4 rounds.

**Reactions**

___Muddled Escape (1/Day).___ If an attack would reduce the miremal's hit points to 0, it collapses into a pool of filth-laden swamp water and its hit points are reduced to 1 instead. The miremal can move at its normal speed in this form, including moving through spaces occupied by other creatures. As a bonus action at the beginning of the miremal's next turn, it can reform, still with 1 hit point.

