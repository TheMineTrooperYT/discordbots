"Adult Sapphire Dragon";;;_size_: Huge dragon
_alignment_: neutral
_challenge_: "17 (18,000 XP)"
_languages_: "Common, Draconic, telepathy 120 ft."
_skills_: "Arcana +12, Insight +9, Perception +9, Religion +12"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 25"
_saving_throws_: "Dex +12, Int +12, Wis +9, Cha +12"
_damage_vulnerabilities_: "psychic"
_damage_immunities_: "fire, lightning"
_damage_resistances_: "bludgeoning, slashing, and piercing from nonmagical attacks"
_condition_immunities_: "stunned"
_speed_: "40 ft., fly 80 ft. (hover)"
_hit points_: "161 (19d12 + 38)"
_armor class_: "21 (natural armor)"
_stats_: | 21 (+5) | 23 (+6) | 14 (+2) | 22 (+6) | 17 (+3) | 22 (+6) |

___Legendary Resistance (3/Day).___ If the dragon fails
a saving throw, it can choose to succeed instead.

___Awe Aura.___ All creatures within 30 feet must
make a DC 20 Charisma saving throw in order to
attack this dragon. On a failed save, the attacking
creature’s turn ends immediately. On a success,
that creature is immune to the Awe Aura of all
gemstone dragons for 1 week.

**Psionics**

___Charges:___ 19 | ___Recharge:___ 1d8 | ___Fracture:___ 23

**Actions**

___Multiattack.___ The dragon makes three attacks: one
with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 10 ft.,
one target. Hit: 16 (2d10 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +11 to hit, reach 5 ft.,
one target. Hit: 12 (2d6 + 5) slashing damage.

___Tail.___ Melee Weapon Attack: +11 to hit; reach 15 ft.,
one target. Hit: 14 (2d8 + 5) bludgeoning damage.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing
from the options below. Only one legendary action
option can be used at a time and only at the end of
another creature’s turn. The dragon regains spent
legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) Check.

___Psionics.___ The dragon uses a psionic ability.

___Psionic Shift (Costs 2 Actions).___ The dragon
releases a wave of telekinetic energy from its mind.
Every creature within 15 feet must make a DC 24
Intelligence saving throw or take 13 (2d6 + 6) force
damage and be knocked prone. The dragon then
can move up to half its movement speed.
