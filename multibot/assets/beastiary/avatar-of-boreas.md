"Avatar Of Boreas";;;_size_: Medium elemental
_alignment_: chaotic evil
_challenge_: "17 (18000 XP)"
_languages_: "Common, Dwarvish, Giant, Infernal"
_skills_: "Deception +11, Nature +10, Perception +10, Stealth +12"
_senses_: "darkvision 60 ft., truesight 120 ft., passive Perception 20"
_saving_throws_: "Dex +12, Wis +10, Cha +11"
_damage_immunities_: "cold, lightning, poison, thunder"
_damage_resistances_: "acid"
_condition_immunities_: "petrification, poisoned"
_speed_: "50 ft., fly 120 ft."
_hit points_: "168 (16d8 + 96)"
_armor class_: "20 (natural armor)"
_stats_: | 25 (+7) | 22 (+6) | 22 (+6) | 18 (+4) | 19 (+4) | 21 (+5) |

___Chilling Presence.___ Boreas freezes everything within 150 feet of him. After 5 rounds, nonmagical fires up to the size of a campfire are quenched. Water freezes within 1 minute. Spells that protect against cold are subjected to an immediate dispel magic (at +10 spellcasting ability) when within 150 feet of Boreas.

___Wind Form.___ Boreas can shift between his humanoid body and a body made of wind and mist as an action; he can never be forced to shift forms. In wind form, he can use a whirlwind blast attack and use his spells, but no weapon attack. Truesight reveals both forms at once.

___Freedom of the Wind.___ Locks, shackles, ropes, and other bindings cannot hold Boreas.

___Innate Spellcasting.___ Boreas's innate spellcasting ability is Charisma (spell save DC 19). He can innately cast the following spells, requiring no material components:

At will: create water, detect magic, guidance, invisibility, polymorph, speak with animals, true seeing, wind wall

3/day each: call lightning, control weather, cure wounds, dispel magic, ice storm, lesser restoration

1/day each: chain lightning, earthquake, finger of death, heal, shapechange, wall of ice, word of recall

___Regeneration.___ The avatar of Boreas regains 10 hit points at the start of its turn. If the avatar of Boreas takes fire damage, this trait does not function at the start of its next turn. The avatar of Boreas dies only if it starts its turn with 0 hit points and does not regenerate.

**Actions**

___Multiattack.___ Boreas makes 4 spear attacks, or 4 longbow attacks, or 2 whirlwind blasts.

___Ice Spear (Humanoid Form).___ Melee Weapon Attack. +13 to hit, reach 5 ft., one target. Hit: 11 (1d8 + 7) piercing damage plus 17 (5d6) cold damage.

___North Wind Longbow.___ Ranged Weapon Attack: +12 to hit, range 150/600 ft., one target. Hit: 10 (1d8+6) piercing damage plus 9 (2d8) cold damage.

___Whirlwind Blast (Wind Form Only).___ Ranged Spell Attack: +11 to hit, range 50 ft., one target. Hit: 37 (5d12 + 5) slashing damage.

**Legendary** Actions

___Boreas can take 3 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. Boreas regains spent legendary actions at the start of its turn.

___Ice Spear.___ Boreas makes an ice spear attack.

___North Wind's Glare.___ Boreas freezes a foe with a look. One creature within 60 feet that the avatar of Boreas can see must make a successful DC 19 Constitution saving throw or be petrified until the start of the avatar's next turn.

___Whirlwind Movement (Costs 2 Actions).___ Boreas erupts into an icy whirlwind. Mundane and magical light sources alike within 20 feet are automatically extinguished. Each creature within 20 feet of the avatar must succeed on a DC 21 Dexterity saving throw or take 14 (4d6) cold damage. The avatar can then fly up to half its flying speed.

