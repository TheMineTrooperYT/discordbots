"Crushing Wave Reaver";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_skills_: "Athletics +4, Stealth +4"
_speed_: "30 ft."
_hit points_: "22 (4d8+4)"
_armor class_: "14 (shield)"
_stats_: | 15 (+2) | 14 (+2) | 13 (+1) | 10 (0) | 11 (0) | 8 (-1) |

**Actions**

___Sharktoothed Longsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) slashing damage, or 7 (1d10 + 2) slashing damage if used with two hands. Against a target is wearing no armor, the reaver deals an extra die of damage with this sword.

___Javelin.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 30/120 ft., one target. Hit: 5 (1d6 + 2) piercing damage.