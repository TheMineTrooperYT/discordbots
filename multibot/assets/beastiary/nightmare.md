"Nightmare";;;_size_: Large fiend
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "understands Abyssal, Common, and Infernal but can't speak"
_damage_immunities_: "fire"
_speed_: "60 ft., fly 90 ft."
_hit points_: "68 (8d10+24)"
_armor class_: "13 (natural armor)"
_stats_: | 18 (+4) | 15 (+2) | 16 (+3) | 10 (0) | 13 (+1) | 15 (+2) |

___Confer Fire Resistance.___ The nightmare can grant resistance to fire damage to anyone riding it.

___Illumination.___ The nightmare sheds bright light in a 10-foot radius and dim light for an additional 10 feet.

**Actions**

___Hooves.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) bludgeoning damage plus 7 (2d6) fire damage.

___Ethereal Stride.___ The nightmare and up to three willing creatures within 5 feet of it magically enter the Ethereal Plane from the Material Plane, or vice versa.