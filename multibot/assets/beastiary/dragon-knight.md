"Dragon Knight";;;_size_: Medium humanoid
_alignment_: evil (50 percent) or lawful good (50 percent)
_challenge_: "6 (2,300 XP)"
_languages_: "Common, Draconic, and the languages of the race of the knight"
_skills_: "Athletics +7, Insight +4, Intimidation +5, Perception +4"
_senses_: "passive Perception 14"
_saving_throws_: "Str +7, Con +6"
_damage_resistances_: "see <i>Draconic Worship</i> below"
_speed_: "30 ft."
_hit points_: "128 (15d10 + 45)"
_armor class_: "19 (splint mail, shield)"
_stats_: | 19 (+4) | 8 (-1) | 16 (+3) | 10 (+0) | 12 (+1) | 14 (+2) |

___Draconic Worship.___ The dragon knight worships a
metallic or chromatic dragon and uses abilities that
correspond to the dragon’s breath weapon damage.
The knight also has resistance to that type of damage.
This worship is made obvious in the knight’s armor and
weaponry.

___Draconic Insight.___ The knight automatically succeeds on
saving throws against breath weapons.

**Actions**

___Multiattack.___ The knight makes three attacks: two with
its longsword and one with its shield slam.

___Longsword.___ Melee Weapon Attack: +7 to hit, reach 5ft.,
one target. Hit: 8 (1d8 + 4) slashing damage.

___Shield Slam.___ Melee Weapon Attack: +7 to hit, reach 5ft.,
one target. Hit: 6 (1d4 + 4) bludgeoning damage and
the target must succeed on a DC 14 Strength saving
throw or be pushed 5ft.

___Shield Flare (Recharge 5-6).___ The knight holds up its
shield and channels magical energy through it to
release a burst of power in a 15 foot cone. Each enemy
in that area must make a DC 14 Dexterity saving throw,
taking 27 (6d8) damage (corresponding to the type of
dragon the knight worships).

___Dragon’s Fury (1/Day).___ The knight makes four longsword
attacks this turn and the attacks are considered
magical.
