"Fortress Rhino";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "12 (8,400 XP)"
_languages_: "--"
_senses_: "passive Perception 11"
_speed_: "40 ft."
_hit points_: "210 (20d12 + 80)"
_armor class_: "16 (natural armor)"
_stats_: | 24 (+7) | 6 (-2) | 19 (+4) | 4 (-3) | 12 (+1) | 6 (-2) |

___Mobile Fortress.___ The rhino can hold up to four
medium creatures within the outpost on its back.

___Siege Monster.___ The rhino deals double damage to
objects and structures.

___Stampeding Charge.___ If the rhino moves at least 30
feet straight towards a target and hits it with a gore
attack on the same turn, the target takes an
additional 18 (4d8) bludgeoning damage. If the
target is a creature, it must succeed on a DC 18
Strength saving throw or be knocked prone. In
addition, any Medium or smaller creatures in the
rhino's path while performing this charge must
succeed on a DC 15 Dexterity saving throw or take
18 (4d8) bludgeoning damage and be knocked
prone as it is trampled underfoot.

**Actions**

___Gore.___ Melee Weapon Attack: +11 to hit, reach 5 ft.,
one target. Hit: 34 (5d10 + 7) piercing damage and
if the target is a Large creature or smaller it must
succeed on a DC 18 Dexterity saving throw or
become impaled by the rhino's horn. A creature
that is impaled in this way is grappled and takes 18
(4d8) piercing damage at the start of each of the
rhino's turns. While a creature is impaled, the rhino
cannot use its gore attack on another creature.

An impaled creature can use its action to pull itself
free from the horn, but continues to take 4 (1d8)
piercing damage at the beginning of each of its
turns until it is magically healed or a creature uses
its action and succeeds on a DC 15 Wisdom
(Medicine) check to patch up the wound.

___Stomp.___ Melee Weapon Attack: +11 to hit, reach 5
ft., one target. Hit: 46 (6d12 + 7) bludgeoning
damage and the target is a creature it must succeed
on a DC 18 Strength saving throw or be knocked
prone.
