"Wood Woad";;;_size_: Medium plant
_alignment_: lawful neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Sylvan"
_senses_: "darkvision 60 ft."
_skills_: "Athletics +7, Perception +4, Stealth +4"
_speed_: "30 ft., climb 30 ft."
_hit points_: "75 (10d8+30)"
_armor class_: "18 (natural armor, shield)"
_damage_vulnerabilities_: "fire"
_condition_immunities_: "charmed, frightened"
_damage_resistances_: "bludgeoning, piercing"
_stats_: | 18 (+4) | 12 (+1) | 16 (+3) | 10 (0) | 13 (+1) | 8 (-1) |

___Magic Club.___ In the wood woad's hand, its club is magical and deals 7 (3d4) extra damage (included in its attacks).

___Plant Camouflage.___ The wood woad has advantage on Dexterity (Stealth) checks it makes in any terrain with ample obscuring plant life.

___Regeneration.___ The wood woad regains 10 hit points at the start of its turn if it is in contact with the ground. If the wood woad takes fire damage, this trait doesn't function at the start of the wood woad's next turn. The wood woad dies only if it starts its turn with 0 hit points and doesn't regenerate.

___Tree Stride.___ Once on each of its turns, the wood woad can use 10 feet of its movement to step magically into one living tree within 5 feet of it and emerge from a second living tree within 60 feet of it that it can see, appearing in an unoccupied space within 5 feet of the second tree. Both trees must be Large or bigger.

**Actions**

___Multiattack.___ The wood woad makes two attacks with its club.

___Club.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 14 (4d4+4) bludgeoning damage.