"Overgrowth Ghoul";;;_size_: Medium undead (plant)
_alignment_: chaotic evil
_challenge_: "1/4 (50 XP)"
_languages_: "understands the languages it knew in life but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 9"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "17 (3d8 + 4)"
_armor class_: "9"
_stats_: | 14 (+2) | 9 (-1) | 13 (+1) | 5 (-3) | 9 (-1) | 3 (-4) |

**Actions**

___Floral Fortification.___ The ghoul can use its action to
plant its body into the ground and become fortified
or it can uproot and return to its mobile ghoul
form. While fortified, the ghoul has its AC increased
to 14, its movement speed is reduced to 0 ft., and
as long as it has at least 1 hit point it regains 1d4
hit points at the beginning of its turn.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5ft.,
one target. Hit: 5 (1d6 + 2) slashing damage.

___Root Slash (Fortified Form Only).___ Melee Weapon
Attack: +4 to hit, reach 10 ft., one target. Hit: 6
(1d8 + 2) slashing damage.
