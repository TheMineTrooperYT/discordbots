"Spectator";;;_size_: Medium aberration
_alignment_: lawful neutral
_challenge_: "3 (700 XP)"
_languages_: "Deep Speech, Undercommon, telepathy 120 ft."
_senses_: "darkvision 120 ft."
_skills_: "Perception +6"
_speed_: "0 ft., fly 30 ft. (hover)"
_hit points_: "39 (6d8+12)"
_armor class_: "14 (natural armor)"
_condition_immunities_: "prone"
_stats_: | 8 (-1) | 14 (+2) | 14 (+2) | 13 (+1) | 14 (+2) | 11 (0) |

**Actions**

___Bite.___ Melee Weapon Attack: +1 to hit, reach 5 ft., one target. Hit: 2 (1d6 - 1) piercing damage.

___Eye Rays.___ The spectator shoots up to two of the following magical eye rays at one or two creatures it can see within 90 ft. of it. It can use each ray only once on a turn.

1. Confusion Ray. The target must succeed on a DC 13 Wisdom saving throw, or it can't take reactions until the end of its next turn. On its turn, the target can't move, and it uses its action to make a melee or ranged attack against a randomly determined creature within range. If the target can't attack, it does nothing on its turn.

2. Paralyzing Ray. The target must succeed on a DC 13 Constitution saving throw or be paralyzed for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

3. Fear Ray. The target must succeed on a DC 13 Wisdom saving throw or be frightened for 1 minute. The target can repeat the saving throw at the end of each of its turns, with disadvantage if the spectator is visible to the target, ending the effect on itself on a success.

4. Wounding Ray. The target must make a DC 13 Constitution saving throw, taking 16 (3d10) necrotic damage on a failed save, or half as much damage on a successful one.

___Create Food and Water.___ The spectator magically creates enough food and water to sustain itself for 24 hours.

**Reactions**

___Spell Reflection.___ If the spectator makes a successful saving throw against a spell, or a spell attack misses it, the spectator can choose another creature (including the spellcaster) it can see within 30 ft. of it. The spell targets the chosen creature instead of the spectator. If the spell forced a saving throw, the chosen creature makes its own save. If the spell was an attack, the attack roll is rerolled against the chosen creature.