"Giant Bat";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "blindsight 60 ft."
_speed_: "10 ft., fly 60 ft."
_hit points_: "22 (4d10)"
_armor class_: "13"
_stats_: | 15 (+2) | 16 (+3) | 11 (0) | 2 (-4) | 12 (+1) | 6 (-2) |

___Echolocation.___ The bat can't use its blindsight while deafened.

___Keen Hearing.___ The bat has advantage on Wisdom (Perception) checks that rely on hearing.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 5 (1d6 + 2) piercing damage.