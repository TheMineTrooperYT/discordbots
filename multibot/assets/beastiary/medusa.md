"Medusa";;;_size_: Medium monstrosity
_alignment_: lawful evil
_challenge_: "6 (2,300 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft."
_skills_: "Deception +5, Insight +4, Perception +4, Stealth +5"
_speed_: "30 ft."
_hit points_: "127 (17d8+51)"
_armor class_: "15 (natural armor)"
_stats_: | 10 (0) | 15 (+2) | 16 (+3) | 12 (+1) | 13 (+1) | 15 (+2) |

___Petrifying Gaze.___ When a creature that can see the medusa's eyes starts its turn within 30 ft. of the medusa, the medusa can force it to make a DC 14 Constitution saving throw if the medusa isn't incapacitated and can see the creature. If the saving throw fails by 5 or more, the creature is instantly petrified. Otherwise, a creature that fails the save begins to turn to stone and is restrained. The restrained creature must repeat the saving throw at the end of its next turn, becoming petrified on a failure or ending the effect on a success. The petrification lasts until the creature is freed by the greater restoration spell or other magic.

Unless surprised, a creature can avert its eyes to avoid the saving throw at the start of its turn. If the creature does so, it can't see the medusa until the start of its next turn, when it can avert its eyes again. If the creature looks at the medusa in the meantime, it must immediately make the save.

If the medusa sees itself reflected on a polished surface within 30 ft. of it and in an area of bright light, the medusa is, due to its curse, affected by its own gaze.

**Actions**

___Multiattack.___ The medusa makes either three melee attacks -  one with its snake hair and two with its shortsword - or two ranged attacks with its longbow.

___Snake Hair.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 4 (1d4 + 2) piercing damage plus 14 (4d6) poison damage.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Longbow.___ Ranged Weapon Attack: +5 to hit, range 150/600 ft., one target. Hit: 6 (1d8 + 2) piercing damage plus 7 (2d6) poison damage.