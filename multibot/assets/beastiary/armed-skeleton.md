"Armed Skeleton";;;_size_: Medium undead
_alignment_: lawful evil
_challenge_: "1 (200 XP)"
_languages_: "understands all languages it knew in life but can't speak"
_senses_: "darkvision 60 ft., passive Perception 9"
_skills_: "Athletics +3"
_damage_immunities_: "poison"
_damage_vulnerabilities_: "bludgeoning"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "26 (4d8 + 8)"
_armor class_: "13 (natural armor)"
_stats_: | 12 (+1) | 14 (+2) | 15 (+2) | 6 (-2) | 8 (-1) | 6 (-2) |

___Armed Advantage.___ The skeleton has advantage on Strength (Athletics) checks made to grapple.

**Actions**

___Multiattack.___ The skeleton makes four claw attacks.

___Claw.___ Melee Weapon Attack: +3 to hit, reach 5ft., one target.  Hit: 3 (1d4 + 1) slashing damage.
