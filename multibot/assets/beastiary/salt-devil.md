"Salt Devil";;;_size_: Medium fiend
_alignment_: lawful evil
_challenge_: "6 (2300 XP)"
_languages_: "Celestial, Common, Gnoll, Infernal, telepathy 120 ft."
_skills_: "Perception +5, Stealth +4"
_senses_: "darkvision 120 ft., passive Perception 15"
_saving_throws_: "Dex +4, Con +7, Cha +5"
_damage_immunities_: "fire, poison"
_damage_resistances_: "acid, cold; bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "93 (11d8 + 44)"
_armor class_: "13 (natural armor)"
_stats_: | 18 (+4) | 13 (+1) | 18 (+4) | 13 (+1) | 14 (+2) | 15 (+2) |

___Devil's Sight.___ Magical darkness doesn't impede the devil's darkvision.

___Magic Resistance.___ The devil has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting.___ The devil's spellcasting ability is Charisma (spell save DC 13). The devil can innately cast the following spells, requiring no material components:

At will: darkness

1/day each: harm, teleport

___Variant: Devil Summoning.___ Summon Devil (1/Day): The salt devil has a 40 percent chance of summoning one salt devil

**Actions**

___Multiattack.___ The devil makes two scimitar attacks.

___Scimitar.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) slashing damage. If the target is neither undead nor a construct, it also takes 5 (1d10) necrotic damage, or half damage with a successful DC 15 Constitution saving throw. Plants, oozes, and creatures with the Amphibious, Water Breathing, or Water Form traits have disadvantage on this saving throw. If the saving throw fails by 5 or more, the target also gains one level of exhaustion.

