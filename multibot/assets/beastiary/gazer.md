"Gazer";;;_size_: Tiny aberration
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +4, Stealth +2"
_saving_throws_: "Wis +2"
_speed_: "0 ft., fly 30 ft. (hover)"
_hit points_: "13 (3d4+6)"
_armor class_: "13"
_condition_immunities_: "prone"
_stats_: | 3 (-4) | 17 (+3) | 14 (+2) | 3 (-4) | 10 (0) | 7 (-2) |

___Aggressive.___ As a bonus action, the gazer can move up to its speed toward a hostile creature that it can see.

___Mimicry.___ The gazer can mimic simple sounds of speech it has heard, in any language. A creature that hears the sounds can tell they are imitations with a successful DC 10 Wisdom (Insight) check.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 1 piercing damage.

___Eye Rays.___ The gazer shoots two of the following magical eye rays at random (reroll duplicates), choosing one or two targets it can see within 60 feet of it:

7. Dazing Ray: The targeted creature must succeed on a DC 12 Wisdom saving throw or be charmed until the start of the gazer's next turn. While the target is charmed in this way, its speed is halved, and it has disadvantage on attack rolls.

2. Fear Ray: The targeted creature must succeed on a DC 12 Wisdom saving throw or be frightened until the start of the gazer's next turn.

3. Frost Ray: The targeted creature must succeed on a DC 12 Dexterity saving throw or take 10 (3d6) cold damage.

4. Telekinetic Ray: If the target is a creature that is Medium or smaller, it must succeed on a DC 12 Strength saving throw or be moved up to 30 feet directly away from the gazer.

If the target is an object weighing 10 pounds or less that isn't being worn or carried, the gazer moves it up to 30 feet in any direction. The gazer can also exert fine control on objects with this ray, such as manipulating a simple tool or opening a container.
