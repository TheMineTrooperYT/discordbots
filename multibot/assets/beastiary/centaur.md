"Centaur";;;_size_: Large monstrosity
_alignment_: neutral good
_challenge_: "2 (450 XP)"
_languages_: "Elvish, Sylvan"
_skills_: "Athletics +6, Perception +3, Survival +3"
_speed_: "50 ft."
_hit points_: "45 (6d10+12)"
_armor class_: "12"
_stats_: | 18 (+4) | 14 (+2) | 14 (+2) | 9 (-1) | 13 (+1) | 11 (0) |

___Charge.___ If the centaur moves at least 30 ft. straight toward a target and then hits it with a pike attack on the same turn, the target takes an extra 10 (3d6) piercing damage.

**Actions**

___Multiattack.___ The centaur makes two attacks: one with its pike and one with its hooves or two with its longbow.

___Pike.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 9 (1d10 + 4) piercing damage.

___Hooves.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage.

___Longbow.___ Ranged Weapon Attack: +4 to hit, range 150/600 ft., one target. Hit: 6 (1d8 + 2) piercing damage.