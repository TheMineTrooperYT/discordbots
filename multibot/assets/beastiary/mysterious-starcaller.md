"Mysterious Starcaller";;;_size_: Medium celestial
_alignment_: chaotic neutral
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Celestial, Draconic"
_skills_: "Arcana +8, Deception +6, Insight +5, Perception +5, Persuasion +6"
_senses_: "passive Perception 15"
_saving_throws_: "Int +8, Wis +5"
_damage_immunities_: "radiant"
_speed_: "30 ft.. fly 30 ft. (hover)"
_hit points_: "114 (16d6 + 48)"
_armor class_: "17 (radiant armor)"
_stats_: | 9 (-1) | 16 (+3) | 16 (+3) | 19 (+4) | 12 (+1) | 15 (+2) |

___Dancing Stars.___ Brilliant lights dance around the
starcaller, exuding bright light in a 15-foot radius and
dim light for an additional 15 feet.

___Retaliating Energies.___ Whenever an enemy within 200 ft.
hits the starcaller with a ranged attack, one of the stars
dancing around it launches at the attacker, dealing 7
(2d4 + 2) radiant damage.

___Radiant Armor.___ While the starcaller is wearing no armor
and wielding no shield, its AC incluces its Intelligence
modifier.

**Actions**

___Multiattack.___ The starcaller makes three attacks with its
radiant palm or two attacks with its shooting star.

___Radiant Palm.___ Melee Weapon Attack: +8 to hit, reach
5ft., one target. Hit: 7 (1d6 + 4) bludgeoning damage
plus 9 (2d8) radiant damage.

___Shooting Star.___ Ranged Weapon Attack: +8 to hit, range
150/600 ft., one target. Hit: 22 (5d8) radiant damage.

___Starfall (Recharge 5-6).___ The starcaller summons a
brilliant orb of radiant energy at a point in the sky
within 150-feet, then brings it crashing into the ground
in a 20-foot radius area. Each creature in that area must
make a DC 16 Dexterity saving throw, taking 36 (8d8)
radiant damage on a failed save, or half as much
damage on a successful one. Creatures that fail this
save by 5 or more are also blinded until the end of their
next turn.

___Step Through The Stars (3/Day).___ The starcaller vanishes
in a brilliant flash of light and reappears with an equally
bright flash in an unoccupied space within 200 ft. Each
creature within 10-feet of where the starcaller began or
ended must succeed on a DC 16 Constitution saving
throw or be blinded until the end of their next turn.
After teleporting, the starcaller may make a single
attack with radiant palm or shooting star.
