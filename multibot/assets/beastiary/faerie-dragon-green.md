"Faerie Dragon (Green)";;;_size_: Tiny dragon
_alignment_: chaotic good
_challenge_: "2 (450 XP)"
_languages_: "Draconic, Sylvan"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +4, Perception +3, Stealth +7"
_speed_: "10 ft., fly 60 ft."
_hit points_: "14 (4d4+4)"
_armor class_: "15"
_stats_: | 3 (-4) | 20 (+5) | 13 (+1) | 14 (+2) | 12 (+1) | 16 (+3) |

___The Colors of Age.___ A faerie dragon's scales change hue as it ages, moving through all the colors of the rainbow. All faerie dragons have innate spellcasting ability, gaining new spells as they mature.

Red - 5 years or less

Orange - 6-10 years

Yellow - 11-20 years

Green - 21-30 years

Blue - 31-40 years

Indigo - 41-50 years

Violet - 51 years or more

A green or older faerie dragon's CR increases to 2.

___Superior Invisibility.___ As a bonus action, the dragon can magically turn invisible until its concentration ends (as if concentrating on a spell). Any equipment the dragon wears or carries is invisible with it.

___Limited Telepathy.___ Using telepathy, the dragon can magically communicate with any other faerie dragon within 60 feet of it.

___Magic Resistance.___ The faerie dragon has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting.___ The dragon's innate spellcasting ability is Charisma (spell save DC 13). It can innately cast a number of spells, requiring no material components. As the dragon ages and changes color, it gains additional spells as shown below.

* 1/day each: _color spray, dancing lights, mage hand, minor illusion, mirror image, suggestion_

**Actions**

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one creature. Hit: 1 piercing damage.

___Euphoria Breath (Recharge 5-6).___ The dragon exhales a puff of euphoria gas at one creature within 5 feet of it. The target must succeed on a DC 11 Wisdom saving throw, or for 1 minute, the target can't take reactions and must roll a d6 at the start of each of its turns to determine its behavior during the turn:

* ___1-4.___ The target takes no action or bonus action and uses all of its movement to move in a random direction.

* ___5-6.___ The target doesn't move, and the only thing it can do on its turn is make a DC 11 Wisdom saving throw, ending the effect on itself on a success.
