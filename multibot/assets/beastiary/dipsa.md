"Dipsa";;;_size_: Tiny ooze
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_skills_: "Stealth +7"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 8"
_damage_resistances_: "acid"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, prone"
_speed_: "20 ft., climb 20 ft., swim 20 ft."
_hit points_: "27 (6d4 + 12)"
_armor class_: "15"
_stats_: | 3 (-4) | 17 (+3) | 14 (+2) | 1 (-5) | 6 (-2) | 1 (-5) |

___Swamp Stealth.___ The dipsa gains an additional +2 (+9 in total) to Stealth in swamp terrain.

___Amorphous.___ The dipsa can move through a space as narrow as 1 inch wide without squeezing.

___Discreet Bite.___ The bite of a dipsa is barely perceptible and the wound is quickly anesthetized. A creature bitten must succeed on a DC 15 Wisdom (Perception) check to notice the attack or any damage taken from it.

___Translucent.___ The dipsa can take the Hide action as a bonus action on each of its turns.

**Actions**

___Bite.___ Melee Weapon Attack: +7 to hit, reach 0 ft., one creature in the dipsa's space. Hit: 1 piercing damage, and the dipsa attaches to the target. A creature with a dipsa attached takes 3 (1d6) acid damage per round per dipsa, and it must make a successful DC 12 Constitution saving throw or have its hit point maximum reduced by an amount equal to the damage taken. If a creature's hit point maximum is reduced to 0 by this effect, the creature dies. This reduction to a creature's hit point maximum lasts until it is affected by a lesser restoration spell or comparable magic.

