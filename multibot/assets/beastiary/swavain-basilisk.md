"Swavain Basilisk";;;_size_: Huge monstrosity
_alignment_: unaligned
_challenge_: "7 (2,900 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 9"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "15 ft., swim 40 ft."
_hit points_: "85 (10d12 + 20)"
_armor class_: "16 (natural armor)"
_stats_: | 15 (+2) | 16 (+3) | 15 (+2) | 2 (-4) | 8 (-1) | 7 (-2) |

___Amphibious.___ The basilisk can breathe air and water.

___Petrifying Secretions.___ A creature must make a DC 13 Constitution saving throw if it hits the basilisk with a weapon attack while within 5 feet of it or if it starts its turn grappled by the basilisk. Unless the save succeeds, the creature magically begins to turn to stone and is restrained, and it must repeat the saving throw at the end of its next turn. On a successful save, the effect ends. On a failure, the creature is petrified.

**Actions**

___Multiattack.___ The basilisk makes two attacks: one with its bite and one with its tail.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one creature. Hit: 13 (3d6 + 3) piercing damage plus 10 (3d6) poison damage.

___Tail.___ Melee Weapon Attack: +6 to hit, reach 15 ft., one target. Hit: 14 (2d10 + 3) bludgeoning damage. If the target is a Large or smaller creature, it is grappled (escape DC 12).
