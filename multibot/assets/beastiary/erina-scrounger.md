"Erina Scrounger";;;_size_: Small humanoid
_alignment_: neutral
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Erina"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_resistances_: "poison"
_speed_: "20 ft., burrow 20 ft."
_hit points_: "22 (4d6 + 8)"
_armor class_: "12 (leather armor)"
_stats_: | 9 (-1) | 12 (+1) | 14 (+2) | 13 (+1) | 10 (+0) | 11 (+0) |

___Keen Smell.___ The erina has advantage on Wisdom (Perception) checks that rely on smell.

___Hardy.___ The erina has advantage on saving throws against poison.

___Spines.___ An enemy who hits the erina with a melee attack while within 5 feet of it takes 2 (1d4) piercing damage.

**Actions**

___Dagger.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) slashing damage.

___Sling.___ Ranged Weapon Attack: +3 to hit, range 30/120 ft., one target. Hit: 4 (1d4 + 2) bludgeoning damage.

