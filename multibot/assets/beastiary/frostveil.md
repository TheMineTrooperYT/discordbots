"Frostveil";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "4 (1100 XP)"
_languages_: "--"
_skills_: "Stealth +7"
_senses_: "blindsight 100 ft., passive Perception 10"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "cold"
_damage_resistances_: "bludgeoning and piercing from nonmagical weapons"
_condition_immunities_: "blinded, charmed, deafened, frightened, prone"
_speed_: "10 ft., fly (varies; see Windborne ability)"
_hit points_: "67 (9d8 + 27)"
_armor class_: "16"
_stats_: | 20 (+5) | 20 (+5) | 16 (+3) | 1 (-5) | 11 (+0) | 1 (-5) |

___Chilling Acid.___ The frostveil's frozen acidic mist breaks down flesh and organic materials into useable nutrients. Creatures who strike the frostveil with a non-reach melee weapon or an unarmed strike take 4 (1d8) acid damage.

___False Appearance.___ While the frostveil remains motionless, it is indistinguishable from a formation of frost and ice.

___Windborne.___ While in blowing wind, the frostveil can fly with a speed of 30 feet. In a strong wind this speed increases to 60 feet.

**Actions**

___Multiattack.___ The frostveil makes three tendril attacks.

___Tendril.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) bludgeoning damage. If two tendrils hit the same target in a single turn, the target is engulfed.

___Engulf.___ When a frostveil wraps itself around a Medium or smaller creature, the target takes 14 (2d8 + 5) bludgeoning damage plus 13 (3d8) acid damage and is grappled (escape DC 15). The target takes another 9 (2d8) bludgeoning damage plus 13 (3d8) acid damage at the end of each of its turns when it's still grappled by the frostveil. A frostveil can't attack while it has a creature engulfed. Damage from attacks against the frostveil is split evenly between the frostveil and the engulfed creature; the only exceptions are slashing and psychic damage, which affect only the frostveil.

___Spirit Spores (recharge 6).___ In distress, frostveils release a puff of psychotropic spores in a 10-foot cloud around themselves. Creatures within the cloud of spores must succeed on a DC 13 Constitution saving throw against poison or suffer hallucinations, as per a confusion spell, for 1d3 rounds.

