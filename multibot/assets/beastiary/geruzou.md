"Geruzou (Slime Demon)";;;_size_: Small fiend (demon)
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Abyssal, Common, telepathy 100 ft."
_skills_: "Perception +1, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning"
_condition_immunities_: "poisoned"
_speed_: "30 ft., fly 50 ft."
_hit points_: "31 (7d6 + 7)"
_armor class_: "15 (natural armor)"
_stats_: | 12 (+1) | 15 (+2) | 12 (+1) | 8 (-1) | 8 (-1) | 10 (+0) |

___Magic Resistance.___ The demon has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The demon’s weapon attacks are magical.

___Innate Spellcasting.___ The demon’s spellcasting ability is Charisma (spell
save DC 10, +2 to hit with spell attacks). The demon can innately cast
the following spells at will, requiring no material components: _darkness,
detect evil and good, invisibility_ (self only)

___Slimy Hide.___ The demon’s hide is extremely slick and oozes with slime.
Creatures attempting to grapple the demon do so with disadvantage.

**Actions**

___Multiattack.___ The demon makes three attacks: one with its bite and two
with its claws. It can use its Spit Slime in place of the bite.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target.
Hit: 5 (1d6 + 2) slashing damage.

___Spit Slime.___ Ranged Weapon Attack: +4 to hit, range 20/60 ft.,
one target. Hit: 10 (3d6) acid damage and the target must succeed
on a DC 13 Constitution saving throw or its speed is reduced by half
until the end of the demon’s next turn.
