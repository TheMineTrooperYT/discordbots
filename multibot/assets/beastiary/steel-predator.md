"Steel Predator";;;_page_number_: 239
_size_: Large construct
_alignment_: lawful evil
_challenge_: "16 (15,000 XP)"
_languages_: "understands Modron and the language of its owner but can't speak"
_senses_: "blindsight 30 ft., darkvision 60 ft., passive Perception 17"
_skills_: "Perception +7, Stealth +8, Survival +7"
_damage_immunities_: "poison, psychic; bludgeoning, piercing, and slashing from nonmagical attacks"
_speed_: "40 ft."
_hit points_: "207  (18d10 + 108)"
_armor class_: "20 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned, stunned"
_damage_resistances_: "cold, lightning, necrotic, thunder"
_stats_: | 24 (+7) | 17 (+3) | 22 (+6) | 4 (-3) | 14 (+2) | 6 (-2) |

___Innate Spellcasting.___ The steel predator's innate spellcasting ability is Wisdom. The steel predator can innately cast the following spells, requiring no components:

* 3/day each: _dimension door _(self only)_, plane shift _(self only)

___Magic Resistance.___ The steel predator has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The steel predator's weapon attacks are magical.

**Actions**

___Multiattack___ The steel predator makes three attacks: one with its bite and two with its claw.

___Bite___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 14 (2d6 + 7) piercing damage.

___Claw___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 16 (2d8 + 7) slashing damage.

___Stunning Roar (Recharge 5-6)___ The steel predator emits a roar in a 60-foot cone. Each creature in that area must make a DC 19 Constitution saving throw. On a failed save, a creature takes 27 (5d10) thunder damage, drops everything it's holding, and is stunned for 1 minute. On a successful save, a creature takes half as much damage. The stunned creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
