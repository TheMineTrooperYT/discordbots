"Amazon Warrior";;;_size_: Medium humanoid (human)
_alignment_: lawful neutral
_challenge_: "2 (450 XP)"
_languages_: "Draconic"
_skills_: "Athletics +5, Perception +3, Survival +3"
_senses_: "passive Perception 13"
_saving_throws_: "Str +5, Con +5"
_speed_: "30 ft."
_hit points_: "37 (5d8 + 15)"
_armor class_: "18 (breastplate, shield)"
_stats_: | 16 (+3) | 14 (+2) | 16 (+3) | 9 (-1) | 12 (+1) | 11 (+0) |

___Capitalize (1/Turn).___ If the Amazon hits a creature
that she can see with a melee weapon attack, she can
use her bonus action to immediately make another
melee weapon attack against the same creature. This
extra attack has disadvantage.

___Fear of Magic.___ If a creature casts a spell or uses another
magical effect within 30 feet of the Amazon and the
Amazon can see it, the Amazon must succeed on a
Wisdom saving throw with a DC equal to the
spellcaster's spell save DC. On a failed saving throw,
the Amazon is frightened of the spellcaster for 1
minute. The Amazon can repeat her saving throw at the
end of each of her turns, ending the frightened effect
on a success. If the Amazon succeeds on her initial
saving throw or the effect ends for her, this trait does
not function for 1 hour.

___Serpent Whisperer.___ Through sounds and gestures, the
Amazon can communicate simple ideas with snakes
and other serpents.

**Actions**

___Multiattack.___ The Amazon makes two attacks with her
macuahuitl.

___Macuahuitl.___ Melee Weapon Attack: +5 to hit, reach 5 ft.,
one target. Hit: 7 (1d8 + 3) bludgeoning or slashing
damage (Amazon's choice), or 8 (1d10 + 3)
bludgeoning or slashing damage (Amazon's choice)
when wielded with two hands.

___Javelin.___ Melee or Ranged Weapon Attack: +5 to hit,
reach 5 ft., or range 30/120 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Shield Bash.___ Melee Weapon Attack: +5 to hit, reach 5
ft., one target. Hit: 8 (2d4 + 3) bludgeoning damage. If
the target is a Medium or smaller creature, it must
succeed on a DC 13 Strength saving throw or be
knocked prone.
