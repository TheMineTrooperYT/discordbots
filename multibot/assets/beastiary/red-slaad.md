"Red Slaad";;;_size_: Large aberration
_alignment_: chaotic neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Slaad, telepathy 60 ft."
_senses_: "darkvision 60 ft."
_skills_: "Perception +1"
_speed_: "30 ft."
_hit points_: "93 (11d10+33)"
_armor class_: "14 (natural armor)"
_damage_resistances_: "acid, cold, fire, lightning, thunder"
_stats_: | 16 (+3) | 12 (+1) | 16 (+3) | 6 (-2) | 6 (-2) | 7 (-2) |

___Magic Resistance.___ The slaad has advantage on saving throws against spells and other magical effects

___Regeneration.___ The slaad regains 10 hit points at the start of its turn if it has at least 1 hit point.

___Variant: Control Gem.___ Implanted in the slaad's brain is a magic control gem. The slaad must obey whoever possesses the gem and is immune to being charmed while so controlled.

Certain spells can be used to acquire the gem. If the slaad fails its saving throw against imprisonment, the spell can transfer the gem to the spellcaster's open hand, instead of imprisoning the slaad. A wish spell, if cast in the slaad's presence, can be worded to acquire the gem.

A greater restoration spell cast on the slaad destroys the gem without harming the slaad.

Someone who is proficient in Wisdom (Medicine) can remove the gem from an incapacitated slaad. Each try requires 1 minute of uninterrupted work and a successful DC 20 Wisdom (Medicine) check. Each failed attempt deals 22 (4d10) psychic damage to the slaad.

**Actions**

___Multiattack.___ The slaad makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (2d4 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage. If the target is a humanoid, it must succeed on a DC 14 Constitution saving throw or be infected with a disease - a minuscule slaad egg.

A humanoid host can carry only one slaad egg to term at a time. Over three months, the egg moves to the chest cavity, gestates, and forms a slaad tadpole. In the 24-hour period before giving birth, the host starts to feel unwell, its speed is halved, and it has disadvantage on attack rolls, ability checks, and saving throws. At birth, the tadpole chews its way through vital organs and out of the host's chest in 1 round, killing the host in the process.

If the disease is cured before the tadpole's emergence, the unborn slaad is disintegrated.
