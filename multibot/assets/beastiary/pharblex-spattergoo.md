"Pharblex Spattergoo";;;_size_: Medium humanoid (bullywug)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Bullywug"
_skills_: "Perception +5, Religion +2, Stealth +3"
_saving_throws_: "Str +4, Con +6"
_speed_: "20 ft., swim 40 ft."
_hit points_: "59 (7d8+28)"
_armor class_: "15 (studded leather armor, shield)"
_stats_: | 15 (+2) | 12 (+1) | 18 (+4) | 11 (0) | 16 (+3) | 7 (-2) |

___Amphibious.___ Pharblex can breathe air and water.

___Poison Strike (3/Day).___ Once per turn, when Pharblex hits with a melee attack, he can expend a use of this trait to deal an extra 9 (2d8) poison damage.

___Spellcasting.___ Pharblex is a 6th-level spellcaster that uses Wisdom as his spellcasting ability (spell save DC 13, +5 to hit with spell attacks). Pharblex has the following spells prepared from the druid spell list:

* Cantrips (at will): _druidcraft, guidance, poison spray_

* 1st level (4 slots): _cure wounds, entangle, healing word, thunderwave_

* 2nd level (3 slots): _barkskin, beast sense, spike growth_

* 3rd level (3 slots): _plant growth, water walk_

___Standing Leap.___ As part of his movement and without a running start, Pharblex can long jump up to 20 feet and high jump up to 10 feet.

___Swamp Camouflage.___ Pharblex has advantage on Dexterity (Stealth) checks made to hide in swampy terrain.

**Actions**

___Multiattack.___ Pharblex attacks twice. Once with his bite and once with his spear.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Spear.___ Melee or Ranged Weapon Attack: +5 to hit. reach 5 ft. or ranged 20 ft./60 ft., one target. Hit: 5 (1d6 + 2) piercing damage.
