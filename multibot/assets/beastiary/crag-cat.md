"Crag Cat";;;_page_number_: 17
_size_: Large beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft."
_skills_: "Stealth +7"
_speed_: "40 ft."
_hit points_: "34 (4d10+12)"
_armor class_: "13"
_stats_: | 16 (+3) | 17 (+3) | 16 (+3) | 4 (-3) | 14 (+2) | 8 (-1) |

___Nondetection.___ The cat cannot be targeted or detected by any divination magic or perceived through magical scrying sensors.

___Pounce.___ If the cat moves at least 20 feet straight toward a creature then hits it with a claw attack on the same turn, that target must succeed on a DC13 Strength saving throw or be knocked prone.  If the target is prone, the cat can make one bite attack against it as a bonus action.

___Spell Turning.___ The cat has advantage on saving throws against any spell that targets only the cat (not an area). If the cat's saving throw succeeds and the spell is of 7th level or lower, the spell has no effect on the cat and instead targets the caster.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (1d10+3) piercing damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8+3) slashing damage.