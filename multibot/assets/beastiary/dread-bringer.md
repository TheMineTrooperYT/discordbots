"The Dread Bringer";;;_size_: Large aberration
_alignment_: chaotic evil
_challenge_: "11 (7,200 XP)"
_languages_: "Deep Speech, telepathy 120 ft."
_skills_: "Arcana +10, Deception +6, Perception +2"
_senses_: "passive Perception 12"
_saving_throws_: "Con +7, Int +9, Str +7"
_damage_immunities_: "psychic"
_speed_: "40 ft., fly 40 ft. (hover)"
_hit points_: "170 (20d10 + 60)"
_armor class_: "17 (natural armor)"
_stats_: | 16 (+3) | 15 (+2) | 17 (+3) | 21 (+5) | 6 (-2) | 15 (+2) |

___Legendary Resistance (2/Day).___ If the dread bringer fails a
saving throw, it can choose to succeed instead.

___Magic Resistance.___ The dread bringer has advantage on
saving throws against spells and other magical effects.

___Aura of Madness.___ Each creature that starts its turn
within 60 feet of the dread bringer or enters that area
for the first time on a turn must succeed on a DC 15
Intelligence saving throw or go mad for 1 minute. A
creature that has been driven mad cannot speak and
must use its action to attack the creature nearest to it.
A creature can repeat the saving throw at the end of
each of its turns, ending the effect on itself on a
success. If a creature's saving throw is successful or
the effect ends for it, the creature is immune to the
dread bringer's Aura of Madness for the next 24 hours.

**Actions**

___Multiattack.___ The dread bringer uses its Mark of the
Dread Bringer. It then makes two attacks with its claws,
or one attack with its claws and one attack with its
crush of tentacles.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one
target. Hit: 10 (2d6 + 3) slashing damage.

___Crush of Tentacles.___ A swarm of tentacles erupt from the
ground around a creature within 90 feet. That creature
must succeed on a DC 15 Dexterity saving throw or
take 16 (3d10) bludgeoning damage. If the creature
failed the saving throw, it must also succeed on a DC
15 Strength saving throw or become grappled (escape
DC 15).

___Beam of Unmaking (Recharge 5-6).___ Ranged Spell Attack:
+9 to hit, range 120 ft., one target. Hit: 45 (10d8)
necrotic damage and the target must succeed on a DC
17 Charisma saving throw or be banished until the end
of its next turn.

___Glimpse the Abyss (1/Day).___ The dread bringer removes
its mask and gives nearby creatures a glimpse into the
void beyond. Each creature within 60 feet of the dread
bringer that can see it must succeed on a DC 17
Intelligence saving throw or become stunned. A
creature that isn't surprised can avert its eyes to gain
advantage on this saving throw. If it does so, it can't
see the dread bringer until the end of its next turn. A
creature can repeat this saving throw at the end of
each of its turns, ending the effect on itself on a
success.

___Mark of the Dread Bringer.___ The dread bringer extends a
bony claw and places a random mark on the chest of an
unmarked creature within 120 feet. This mark persists
for 3 turns. The varieties of marks are as follows:
* ___Seal of Sovereignty.___ - The target is marked with a
glowing yellow seal. When the seal is placed, a small
yellow orb of light spawns 30 feet away from that
creature. On initiative count 20 (losing initiative
ties) the orb moves 20 feet closer to the marked
creature and that creature is filled with a looming
sense of dread. If the orb reaches the creature before
the mark expires, that creature must make a DC 17
Wisdom saving throw, taking 45 (10d8) psychic
damage on a failed save and becoming a puppet of
the dread bringer. On that creature's next turn, the
dread bringer controls all of its actions. On a
success, this deals half that much psychic damage
and the creature is not controlled.
* ___Seal of Stagnation.___ - The target is marked with a
glowing blue seal. That creature's feet seem to fuse
with the floor itself and even the slightest attempt to
move them sends jolts of pain up its body. The
creature takes 5 (1d10) psychic damage for every 5
feet it moves while affected by this seal.
* ___Seal of Solidarity.___ - The target is marked with a glowing
red seal. The creature sees all of its allies become
outlined in a faint red light and a fear of those
creatures sets in. If the marked creature ends its turn
within 15 feet of any allied creature, the seal
detonates. When this happens, each creature within
15 feet of the seal's marked target (including the
marked creature) must make a DC 17 Dexterity
saving throw, taking 28 (8d6) fire damage on a failed
saving throw, or half as much damage on a
successful one.

**Legendary** Actions

The dread bringer can take 3 legendary actions,
choosing from the options below. Only one legendary
action can be used at a time and only at the end of
another creature’s turn. The dread bringer regains
spent legendary actions at the start of its turn.

___Claw.___ The dread bringer makes one claw attack.

___Teleport.___ The dread bringer magically teleports, along
with any equipment it is wearing or carrying, up to 60
feet to an unoccupied space it can see.

___Mental Anguish (Costs 2 Actions).___ Target creature within
60 feet must succeed on a DC 17 Intelligence saving
throw or lose concentration on any spells it is
maintaining.
