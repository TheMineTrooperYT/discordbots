"Stone Cursed";;;_page_number_: 240
_size_: Medium construct
_alignment_: lawful evil
_challenge_: "1 (200 XP)"
_languages_: "the languages it knew in life"
_senses_: "passive Perception 9"
_damage_immunities_: "poison"
_speed_: "10 ft."
_hit points_: "19  (3d8 + 4)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, petrified, poisoned"
_stats_: | 16 (+3) | 5 (-2) | 14 (+2) | 5 (-2) | 8 (-1) | 7 (-1) |

___Cunning Opportunist.___ The stone cursed has advantage on the attack rolls of opportunity attacks.

___False Appearance.___ While the stone cursed remains motionless, it is indistinguishable from a normal statue.

**Actions**

___Petrifying Claws___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (1d10 + 3) slashing damage, or 14 (2d10 + 3) slashing damage if the attack roll had advantage. If the target is a creature, it must succeed on a DC 12 Constitution saving throw, or it begins to turn to stone and is restrained until the end of its next turn, when it must repeat the saving throw. The effect ends if the second save is successful; otherwise the target is petrified for 24 hours.