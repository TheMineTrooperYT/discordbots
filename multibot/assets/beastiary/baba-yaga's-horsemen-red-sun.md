"Baba Yaga's Horsemen, Red Sun";;;_size_: Medium fey
_alignment_: lawful neutral
_challenge_: "11 (7200 XP)"
_languages_: "Celestial, Common, Infernal; telepathy 100 ft."
_skills_: "Arcana +7, Athletics +10, History +7, Perception +8"
_senses_: ", passive Perception 18"
_saving_throws_: "Dex +4, Wis +8"
_damage_immunities_: "fire, lightning, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "blinded, charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "30 ft."
_hit points_: "171 (18d8 + 90)"
_armor class_: "20 (plate and shield)"
_stats_: | 22 (+6) | 11 (+0) | 21 (+5) | 16 (+3) | 18 (+4) | 18 (+4) |

___Innate Spellcasting.___ The horseman is a 12th-level spellcaster. Its spellcasting ability is Charisma (save DC 16, +8 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

1/day each: dimension door, fire shield, haste, slow

2/day each: continual flame, scorching ray

3/day each: ethereal jaunt, phantom steed (appears as a horse colored appropriately to the horseman), plane shift (self and steed only)

___Magic Resistance.___ The horseman has advantage on saving throws against spells and other magical effects.

___Peerless Rider.___ Any attacks directed at the horseman's mount targets the horseman instead. Its mount gains the benefit of the rider's damage and condition immunities, and if the horseman passes a saving throw against an area effect, the mount takes no damage.

___Quick Draw.___ The horseman can switch between wielding its lance and longsword as a bonus action.

**Actions**

___Multiattack.___ The horseman makes three attacks with its lance or longsword. It can use Temporal Strike with one of these attacks when it is available.

___Lance.___ Melee Weapon Attack: +10 to hit, reach 10 ft. (disadvantage within 5 ft.), one target. Hit: 12 (1d12 + 6) piercing damage.

___Longsword.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 10 (1d8 + 6) slashing damage.

___Temporal Strike (recharge 5-6).___ When the horseman strikes a target with a melee attack, in addition to taking normal damage, the target must succeed on a DC 17 Constitution saving throw or instantly age 3d10 years. A creature that ages this way has disadvantage on attack rolls, ability checks, and saving throws based on Strength, Dexterity, and Constitution until the aging is reversed. A creature that ages beyond its lifespan dies immediately. The aging reverses automatically after 24 hours, or it can be reversed magically by greater restoration or comparable magic. A creature that succeeds on the save is immune to the temporal strike effect for 24 hours.

