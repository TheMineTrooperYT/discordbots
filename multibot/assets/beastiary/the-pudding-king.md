"The Pudding King";;;_size_: Small humanoid (gnome
_alignment_: shapechanger)
_challenge_: "4 (1,100 XP)"
_languages_: "Abyssal, Gnomish, Terran, Undercommon"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +4, Perception +2, Stealth +6, Survival +2"
_saving_throws_: "Con +5, Cha +6"
_speed_: "30 ft."
_hit points_: "49 (9d6+18)"
_armor class_: "13 (16 with mage armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "acid, poison"
_stats_: | 10 (0) | 16 (+3) | 14 (+2) | 12 (+1) | 8 (-1) | 17 (+3) |

___Stone Camouflage.___ The Pudding King has advantage on Dexterity (Stealth) checks made to hide in rocky terrain.

___Gnome Cunning.___ The Pudding King has advantage on Intelligence, Wisdom, and Charisma saving throws against magic.

___Innate Spellcasting.___ The Pudding King's innate spellcasting ability is Intelligence (spell save DC 12). He can innately cast the following spells, requiring no material components:

* At will: _nondetection _(self only)

* 1/day each: _blindness/deafness, blur, disguise self_

___Insanity.___ The Pudding King has advantage on saving throws against being charmed or frightened.

___Spellcasting.___ The Pudding King is a 9th-level spellcaster. His spellcasting ability is Charisma (spell save DC 14, +6 to hit with spell attacks). The Pudding King knows the following sorcerer spells:

* Cantrips (at will): _acid splash, light, mage hand, poison spray, prestidigitation_

* 1st level (4 slots): _false life, mage armor, ray of sickness, shield_

* 2nd level (3 slots): _crown of madness, misty step_

* 3rd level (3 slots): _gaseous form, stinking cloud_

* 4th level (3 slots): _blight, confusion_

* 5th level (1 slot): _cloudkill_

**Actions**

___War Pick.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit 4 (1d8) piercing damage.

___Change Shape.___ The Pudding King magically transforms into an ooze, or back into his true form. He reverts to his true form if he dies. Any equipment he is wearing or carrying is absorbed by the new form. In ooze form, the Pudding King retains his alignment, hit points, Hit Dice, and Intelligence, Wisdom, and Charisma scores, as well as this action. His statistics and capabilities are otherwise replaced by those of the new form.

___Create Green Slime (Recharges after a Long Rest).___ The Pudding King creates a patch of green slime (see "Dungeon Hazards" in chapter 5 of the Dungeon Master's Guide). The slime appears on a section of wall, ceiling, or floor within 30 feet of the Pudding King.
