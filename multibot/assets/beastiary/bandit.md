"Bandit";;;_size_: Medium humanoid (any race)
_alignment_: any non-lawful alignment
_challenge_: "1/8 (25 XP)"
_languages_: "any one language (usually Common)"
_speed_: "30 ft."
_hit points_: "11 (2d8+2)"
_armor class_: "12 (leather armor)"
_stats_: | 11 (0) | 12 (+1) | 12 (+1) | 10 (0) | 10 (0) | 10 (0) |

**Actions**

___Scimitar.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) slashing damage.

___Light Crossbow.___ Ranged Weapon Attack: +3 to hit, range 80 ft./320 ft., one target. Hit: 5 (1d8 + 1) piercing damage.