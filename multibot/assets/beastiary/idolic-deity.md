"Idolic Deity";;;_size_: Small construct
_alignment_: neutral evil
_challenge_: "8 (3900 XP)"
_languages_: "telepathy 60 ft."
_skills_: "Deception +8, Stealth +8"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Wis +3"
_damage_vulnerabilities_: "fire"
_damage_immunities_: "poison"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "0 ft., fly 30 ft."
_hit points_: "90 (12d6 + 48)"
_armor class_: "17 (natural armor)"
_stats_: | 14 (+2) | 20 (+5) | 18 (+4) | 10 (+0) | 11 (+0) | 20 (+5) |

___Stealth in darkness.___ The idolic diety gains an additional +3 to Stealth (+11 in total) in dim light or darkness.

___Apostasy Aura.___ The idolic deity's presence causes devout followers to doubt their faith. A cleric or paladin that can see the idolic deity and wishes to cast a spell or use a class feature must make a DC 16 Wisdom saving throw. On a failed save, the spell or class feature is spent as if it was used, but it has no effect.

___Incorporeal Movement.___ The idolic deity can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Shadow Stealth.___ While in dim light or darkness, the idolic deity can take the Hide action as a bonus action.

**Actions**

___Multiattack.___ The idolic deity uses Seduce the Righteous and makes two slam attacks.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 8 (1d6 + 5) bludgeoning damage plus 18 (4d8) psychic damage.

___Seduce the Righteous.___ The idolic deity targets one creature it can see within 30 feet. The target has disadvantage on attack rolls, saving throws, or ability checks (the idolic deity chooses which) until the end of its next turn. A protection from evil and good spell cast on the target prevents this effect, as does a magic circle.

