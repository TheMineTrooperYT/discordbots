"Stryx";;;_size_: Tiny monstrosity
_alignment_: neutral
_challenge_: "1/8 (25 XP)"
_languages_: "Common, Elvish"
_skills_: "Perception +4, Stealth +5"
_senses_: "darkvision 120 ft., passive Perception 14"
_speed_: "10 ft., fly 60 ft."
_hit points_: "10 (4d4)"
_armor class_: "13"
_stats_: | 3 (-4) | 17 (+3) | 11 (+0) | 8 (-1) | 15 (+2) | 6 (-2) |

___False Appearance.___ Until a stryx speaks or opens its mouth, it is indistinguishable from a normal owl.

___Flyby.___ The stryx doesn't provoke opportunity attacks when it flies out of an enemy's reach.

___Innate Spellcasting.___ The stryx's innate spellcasting ability is Wisdom. It can cast the following spell, requiring no components:

* 3/day: _comprehend languages_

___Keen Hearing and Sight.___ The stryx has advantage on Wisdom (Perception) checks that rely on hearing or sight.

**Actions**

___Talons.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 1 slashing damage.

