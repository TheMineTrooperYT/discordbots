"Magmoid";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "8 (3,900 XP)"
_languages_: "Ignan, Terran"
_senses_: "blindsight 60 ft., passive Perception 10"
_damage_vulnerabilities_: "cold"
_damage_immunities_: "fire, lightning, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "40 ft."
_hit points_: "120 (16d10 + 32)"
_armor class_: "14 (natural armor)"
_stats_: | 14 (+2) | 17 (+3) | 15 (+2) | 4 (-3) | 10 (+0) | 5 (-3) |

___Illumination.___ The elemental sheds bright light in a 30-foot radius and
dim light in an additional 30 feet.

___Magma Form.___ The magmoid can move through a space as narrow as 1
inch wide without squeezing. A creature that touches the magmoid or hits
it with a melee attack while within 5 feet of it takes 9 (2d8) fire damage.
In addition, the magmoid can flow into a hostile creature’s space and
stop there. The first time it enters a hostile creature’s space on a turn, that
creature takes 9 (2d8) fire damage and catches fire; until someone takes
an action to douse the flames, the creature takes 4 (1d8) fire damage at the
start of each of its turns.

___Melt Weapons.___ Any nonmagical weapon made of metal that hits the
magmoid melts. After dealing damage, the weapon takes a permanent
and cumulative –1 penalty to damage rolls. If its penalty drops to –5, the
weapon is destroyed. Nonmagical ammunition made of metal or other
flammable material is melted or burned and is destroyed after dealing
damage.

___Siege Monster.___ The magmoid deals double damage to objects and
structures.

**Actions**

___Multiattack.___ The magmoid makes two slam attacks.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10
(2d6 + 3) bludgeoning damage and 9 (2d8) fire damage. If the target is a
creature or a flammable object, it ignites. Until a creature takes an action
to douse the fire, the target takes 4 (1d8) fire damage at the start of each
of its turns.

___Magma Blast (Recharge 6).___ The magmoid hurls a blast of magma in
a 60-foot line that is 5-foot-wide. Each creature in the line must make a
DC 15 Dexterity saving throw, taking 18 (4d8) fire damage on a failure
and half as much damage on a success. In addition, any creature or a
flammable object in the line ignites. Until a creature takes an action to
douse the fire, the target takes 4 (1d8) fire damage at the start of each of
its turns.
