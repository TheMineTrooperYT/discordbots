"Swarm of Grigs";;;_size_: Medium swarm
_alignment_: neutral good
_challenge_: "6 (2,300 XP)"
_languages_: "Common, Sylvan"
_skills_: "Acrobatics +5, Perception +4, Performance +6, Stealth +8"
_senses_: "passive Perception 14"
_saving_throws_: "Dex +5, Cha +6"
_speed_: "30 ft., fly 40 ft."
_hit points_: "110 (20d8 + 20)"
_armor class_: "14 (natural armor)"
_stats_: | 5 (-3) | 14 (+2) | 12 (+1) | 10 (+0) | 13 (+1) | 16 (+3) |

___Innate Spellcasting.___ The swarm of grig’s innate spellcasting ability is
Charisma (spell save DC14, +6 to hit with spell attacks). It can cast the
following spells innately, without requiring material components.

* At will: _druidcraft_

* 3/day each: _disguise self, entangle, invisibility_

___Surprise Attack.___ If the swarm of grigs surprises a creature and hits it
with an attack during the first round of combat, the target takes an extra 28
(8d6) damage from the attack.

___Swarm.___ The swarm can occupy another creature’s space and vice versa,
and the swarm can move through any opening large enough for a grig. The
swarm can’t regain hit points or gain temporary hit points.

**Actions**

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target.
Hit: 12 (3d6 + 2) piercing damage, or 5 (1d6 + 2) piercing damage if the
swarm has half its points or fewer.

___Fiddle (Recharge 5–6).___ The swarm of grigs begins playing a lively
tune with its fiddles. Creatures within 30 feet that can hear the fiddle
must make a DC 13 Wisdom saving throw or be charmed for 1 minute.
While charmed by the swarm, the target uses its action to dance in place,
capering comically. The creature can repeat the saving throw at the end
of each of its turns, ending the effect on a success. If the saving throw is
successful, or the effect ends for it, the creature is immune to that grig’s
Fiddle ability for 24 hours.
