"Chuul Spore Servant";;;_size_: Large plant
_alignment_: unaligned
_challenge_: "4 (1,100 XP)"
_languages_: "-"
_senses_: "blindsight 30 ft. (blind beyond this radius)"
_damage_immunities_: "poison"
_speed_: "30 ft., swim 30 ft."
_hit points_: "93 (11d10+33)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "blinded, charmed, frightened, paralyzed, poisoned"
_stats_: | 19 (+4) | 10 (0) | 16 (+3) | 2 (-4) | 6 (-2) | 1 (-5) |

**Actions**

___Multiattack.___ The spore servant makes two pincer attacks.

___Pincer.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage. The target is grappled (Escape DC 14) if it is a Large or smaller creature and the spore servant doesn't have two other creatures grappled.