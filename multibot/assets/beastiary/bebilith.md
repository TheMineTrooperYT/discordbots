"Bebilith";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "11 (7,200 XP)"
_languages_: "understands Abyssal but can't speak; telepathy 100 ft."
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Str +11, Dex +5, Con +10, Wis +5"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_speed_: "40 ft., climb 20 ft."
_hit points_: "150 (12d12+72)"
_armor class_: "18 (natural armor)"
_stats_: | 24 (+7) | 12 (+1) | 23 (+6) | 11 (+0) | 13 (+1) | 13 (+1) |

___Keen Smell.___ The bebilith has advantage on Wisdom (Perception) checks that rely on smell.

___Spider Climb.___ The bebilith can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Web Sense.___ While in contact with a web, the bebilith knows the exact location of any other creature in contact with the same web.

___Web Walker.___ The bebilith ignores movement restrictions caused by webbing.

**Actions**

___Multiattack.___ The bebilith makes three attacks: two with its claws and one with its bite. If both claw attacks hit one target, the target's armor or natural armor (if any) takes a permanent and cumulative −1 penalty to the AC it offers (to a minimum of 10 before other modifiers). Armor reduced to an AC of 10 is destroyed.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 14 (2d6 + 7) piercing damage, and the target must succeed on a DC 18 Constitution saving throw or take 17 (5d6) radiant damage. At the end of each of its turns, the creature must make another save, ending the effect on a success or taking 17 (5d6) radiant damage on a failure. If a creature dies while under this effect, the body ignites and burns until destroyed or targeted with bless, dispel evil and good, or similar magic.

___Claw.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 12 (2d4 + 7) slashing damage.

___Web (Recharge 5–6).___ Ranged Weapon Attack: +11 to hit, range 30/60 ft., one creature. Hit: The target is restrained by webbing. As an action, the restrained target can make a DC 18 Strength check, bursting the webbing on a success. The webbing can also be attacked and destroyed (AC 10; hp 5; resistance to fire damage; immunity to bludgeoning, poison, and psychic damage).
