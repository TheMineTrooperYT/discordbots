"Black Bulette";;;_size_: Huge monstrosity
_alignment_: lawful evil
_challenge_: "13 (10,000 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Perception +7"
_senses_: "blindsight 60 ft., darkvision 60 ft., passive Perception 17"
_saving_throws_: "Dex +7, Wis +7"
_damage_immunities_: "fire"
_speed_: "80 ft., burrow 80 ft."
_hit points_: "225 (18d12 + 108)"
_armor class_: "19 (natural armor)"
_stats_: | 22 (+6) | 14 (+2) | 22 (+6) | 6 (-2) | 14 (+2) | 10 (+0) |

___Deal of a Lifetime.___ If killed, the body of the black bulette shatters
into shards of obsidian glass, freeing the devil imprisoned within. The
devil may be of any random type (GM’s discretion) save that of a royal
rank. The devil will be grateful to whoever is responsible for ending its
centuries-long confinement, and will offer the responsible parties one
favor in payment before returning to the infernal realms of its home plane.
Wise recipients of this offer should bear in mind that the usual caveats of
treating with devils remain in full force when striking any bargains.

___Detect Evil and Good.___ The bulette knows if there is an aberration,
celestial, elemental, fey, fiend, or undead within 30 feet of it and where
the creature is located. The bulette also knows if there is a place or object
within 30 feet of it that has been consecrated or desecrated.

___Dual Planar Connection.___ The black bulette has a planar tie to the
Abyss, where it appears as a chained adamantine cask shaped like an
abstracted bulette.

___Infernal Stench.___ Any creature that starts its turn within 120 feet of
the bulette must succeed on a DC 18 Constitution saving throw or be
poisoned until the start of its next turn. Coming into physical contact with
a black bulette results in the creature being imparted with a horrid stench
that causes it to have disadvantage on Dexterity (Stealth) checks for 12
hours. The stench can be removed sooner with plenty of soap, water, and
two full hours of scrubbing.

___Magic Resistance.___ The bulette has advantage on saving throws against
spells and other magical effects.

**Actions**

___Bite.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 58
(8d12 + 6) piercing damage.

___Abyssal Breath (Recharge 5–6).___ The bulette exhales fire in a 60-foot
cone. Each creature in that area must make a DC 18 Dexterity saving
throw, taking 42 (12d6) fire damage on a failed save, or half as much
damage on a successful one.
