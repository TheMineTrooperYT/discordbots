"Drow Matron Mother";;;_page_number_: 186
_size_: Medium humanoid (elf)
_alignment_: neutral evil
_challenge_: "20 (25,000 XP)"
_languages_: "Elvish, Undercommon"
_senses_: "darkvision 120 ft., passive Perception 21"
_skills_: "Insight +11, Perception +11, Religion +9, Stealth +10"
_saving_throws_: "Con +9, Wis +11, Cha +12"
_speed_: "30 ft."
_hit points_: "262  (35d8 + 105)"
_armor class_: "17 (half plate)"
_condition_immunities_: "charmed, frightened, poisoned"
_stats_: | 12 (+1) | 18 (+4) | 16 (+3) | 17 (+3) | 21 (+5) | 22 (+6) |

___Fey Ancestry.___ The drow has advantage on saving throws against being charmed, and magic can't put the drow to sleep.

___Innate Spellcasting.___ The drow's innate spellcasting ability is Charisma (spell save DC 20). She can innately cast the following spells, requiring no material components:

* At will: _dancing lights, detect magic_

* 1/day each: _clairvoyance, darkness, detect thoughts, dispel magic, faerie fire , levitate _(self only)_, suggestion_

___Lolth's Fickle Favor.___ As a bonus action, the matron can bestow the Spider Queen's blessing on one ally she can see within 30 feet of her. The ally takes 7 (2d6) psychic damage but has advantage on the next attack roll it makes until the end of its next turn.

___Magic Resistance.___ The drow has advantage on saving throws against spells and other magical effects.

___Spellcasting.___ The drow is a 20th-level spellcaster. Her spellcasting ability is Wisdom (spell save DC 19, +11 to hit with spell attacks). The drow has the following cleric spells prepared:

* Cantrips (at will): _guidance, mending, resistance, sacred flame , thaumaturgy_

* 1st level (4 slots): _bane, command, cure wounds, guiding bolt_

* 2nd level (3 slots): _hold person, silence, spiritual weapon_

* 3rd level (3 slots): _bestow curse, clairvoyance, dispel magic, spirit guardians_

* 4th level (3 slots): _banishment, death ward, freedom of movement, guardian of faith_

* 5th level (3 slots): _contagion, flame strike , geas, mass cure wounds_

* 6th level (2 slots): _blade barrier, harm_

* 7th level (2 slots): _divine word, plane shift_

* 8th level (1 slot): _holy aura_

* 9th level (1 slot): _gate_

___Sunlight Sensitivity.___ While in sunlight, the drow has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack___ The matron mother makes two demon staff attacks or three tentacle rod attacks.

___Demon Staff___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) bludgeoning damage, or 8 (1d8 + 4) bludgeoning damage if used with two hands, plus 14 (4d6) psychic damage. In addition, the target must succeed on a DC19 Wisdom saving throw or become frightened of the drow for 1 minute. The frightened target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Tentacle Rod___ Melee Weapon Attack: +9 to hit, reach 15 ft., one target. Hit: 3 (1d6) bludgeoning damage. If the target is hit three times by the rod on one turn, the target must succeed on a DC 15 Constitution saving throw or suffer the following effects for 1 minute: the target's speed is halved, it has disadvantage on Dexterity saving throws, and it can't use reactions. Moreover, on each of its turns, it can take either an action or a bonus action, but not both. At the end of each of its turns, it can repeat the saving throw, ending the effect on itself on a success.

___Summon Servant (1/Day)___ The drow magically summons a retriever or a yochlol. The summoned creature appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other demons. It remains for 10 minutes, until it or its summoner dies, or until its summoner dismisses it as an action.

**Legendary** Actions

The drow can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The drow regains spent legendary actions at the start of her turn.

___Demon Staff___ The drow makes one attack with her demon staff.

___Compel Demon (Costs 2 Actions)___ An allied demon within 30 feet of the drow uses its reaction to make one attack against a target of the drow's choice that she can see.

___Cast a Spell (Costs 1-3 Actions)___ The drow expends a spell slot to cast a 1st-, 2nd-, or 3rd-level spell that she has prepared. Doing so costs 1 legendary action per level of the spell.
