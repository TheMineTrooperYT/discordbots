"Elegant Sparkmage";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1 (200 XP)"
_languages_: "Any two languages"
_skills_: "Arcana +5, Insight +5, Investigation +5"
_senses_: "passive Perception 12"
_saving_throws_: "Int +5, Wis +4"
_speed_: "30 ft."
_hit points_: "14 (4d6)"
_armor class_: "11 (14 with <i>mage armor</i>)"
_stats_: | 8 (-1) | 12 (+1) | 10 (+0) | 16 (+3) | 14 (+2) | 12 (+1) |

___Arcane Recovery (1/Day).___ When the sparkmage
finishes a short rest, it can regain up to 2 total
expended spell slots.

___Spellcasting.___ The sparkmage is a 3rd-level
spellcaster. Its spellcasting ability is Intelligence
(spell save DC 13, +5 to hit with spell attacks). The
sparkmage has the following wizard spells
prepared:

* Cantrips (at will): _dancing lights, mage hand, shocking grasp_

* 1st level (4 slots): _color spray, feather fall, mage armor, witch bolt_

* 2nd level (2 slots): _darkness, shatter, web_

___Spell Sculptor.___ When the sparkmage casts a spell, it
can choose up to 3 targets to automatically
succeed on the saving throw, and they take no
damage if they would normally take half damage on
a successful save.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +3 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 3
(1d4 + 1) piercing damage.
