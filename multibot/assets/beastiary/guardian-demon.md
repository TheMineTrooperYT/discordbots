"Guardian Demon";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "7 (2,900 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_skills_: "Perception +5"
_senses_: "darkvision 120 ft., passive Perception 15"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "85 (10d10 + 30)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 12 (+1) | 26 (+3) | 12 (+1) | 14 (+2) | 14 (+2) |

___Magic Resistance.___ The demon has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The demon’s weapon attacks are magical.

**Actions**

___Multiattack.___ The demon makes three attacks: one with its bite and two
with its claws.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 14 (3d6 + 4) slashing damage.

___Flame Breath (Recharge 5–6).___ The demon exhales fire in a 30-foot
cone. Each creature in that area must make a DC 15 Dexterity saving
throw, taking 28 (8d6) fire damage on a failed save, or half as much
damage on a successful one.
