"Elven Warden";;;_size_: Medium humanoid (elf)
_alignment_: chaotic good
_challenge_: "2 (450 XP)"
_languages_: "Common, Elvish"
_senses_: "darkvision 60 ft., Passive Perception 10"
_skills_: "Athletics +4, Deception +4"
_saving_throws_: "Str +4, Dex +6, Wis +2"
_speed_: "30 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "16 (studded leather)"
_stats_: | 15 (+2) | 18 (+4) | 14 (+2) | 15 (+2) | 11 (+0) | 14 (+2) |

___Fey Ancestry.___ The elven warden has advantage on
saving throws against being charmed, and magic
can’t put them to sleep.

**Actions**

___Multiattack.___ The elven warden makes three melee
attacks: two with their longsword and one with
their dagger. Or the warden makes two ranged attacks
with their daggers.

___Longsword.___ Melee Weapon Attack: +5 to hit, reach
5 ft., one target. Hit: 6 (1d8 +2) slashing damage,
or 7 (1d10 +2) slashing damage if used with both
hands.

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d4 +3) piercing damage.

**Reactions**

___Parry.___ The elven warden adds 2 to its AC against
one melee attack that would hit it. To do so, the
elven warden must see the attacker and be wielding
a melee weapon.
