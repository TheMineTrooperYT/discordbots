"Blue Abishai";;;_page_number_: 161
_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "17 (18,000 XP)"
_languages_: "Draconic, Infernal, telepathy 120 ft."
_skills_: "Arcana +12"
_senses_: "darkvision 120 ft., passive Perception 16"
_saving_throws_: "Int +12, Wis +12"
_damage_immunities_: "fire, lightning, poison"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_condition_immunities_: "poisoned"
_speed_: "30 ft., fly 50 ft."
_hit points_: "195  (26d8 + 78)"
_armor class_: "19 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 17 (+3) | 22 (+6) | 23 (+6) | 18 (+4) |

___Devil's Sight.___ Magical darkness doesn't impede the abishai's darkvision.

___Magic Resistance.___ The abishai has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The abishai's weapon attacks are magical.

___Spellcasting.___ The abishai is a 13th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 20, +12 to hit with spell attacks). The abishai has the following wizard spells prepared:

* Cantrips (at will): _friends, mage hand, message, minor illusion, shocking grasp_

* 1st level (4 slots): _chromatic orb, disguise self, expeditious retreat, magic missile, charm person, thunderwave_

* 2nd level (3 slots): _darkness, mirror image, misty step_

* 3rd level (3 slots): _dispel magic, fear, lightning bolt_

* 4th level (3 slots): _dimension door, greater invisibility, ice storm_

* 5th level (2 slots): _cone of cold, wall of force_

* 6th level (1 slot): _chain lightning_

* 7th level (1 slot): _teleport_

**Actions**

___Multiattack___ The abishai makes two attacks: one with its quarterstaff and one with its bite.

___Quarterstaff___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) bludgeoning damage, or 6 (1d8 + 2) bludgeoning damage if used with two hands.

___Bite___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 13 (2d10 + 2) piercing damage plus 14 (4d6) lightning damage.
