"Heir of Kong";;;_size_: Large beast
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "understands Draconic but can't speak"
_skills_: "Athletics +7, Perception +4"
_senses_: "passive Perception 14"
_speed_: "40 ft., climb 40 ft."
_hit points_: "47 (5d10 + 20)"
_armor class_: "12"
_stats_: | 18 (+4) | 14 (+2) | 18 (+4) | 9 (-1) | 14 (+2) | 12 (+1) |

___Cunning of Kong.___ The heir has advantage on saving
throws against illusions and enchantments.

**Actions**

___Multiattack.___ The heir makes two fist attacks.

___Fist.___ Melee Weapon Attack: +7 to hit, reach 5 ft.,
one target. Hit: 12 (2d6 + 5) bludgeoning damage.

___Rock.___ Ranged Weapon Attack: +7 to hit, range
30/60 ft., one target. Hit: 12 (2d6 + 5) bludgeoning
damage.

___Psychic Blast (Recharge 6).___ The heir emits a violent
wave of psychic energy in a 30-foot cone. Each
creature in the area must succeed on a DC 12
Intelligence saving throw or take 7 (2d6) psychic
damage and become stunned until the end of the
heir's next turn.
