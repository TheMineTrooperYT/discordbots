"Drow Mage";;;_size_: Medium humanoid (elf)
_alignment_: neutral evil
_challenge_: "7 (2,900 XP)"
_languages_: "Elvish, Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Arcana +6, Deception +5, Perception +4, Stealth +5"
_speed_: "30 ft."
_hit points_: "45 (10d8)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 9 (-1) | 14 (+2) | 10 (0) | 17 (+3) | 13 (+1) | 12 (+1) |

___Fey Ancestry.___ The drow has advantage on saving throws against being charmed, and magic can't put the drow to sleep.

___Innate Spellcasting.___ The drow's spellcasting ability is Charisma (spell save DC 12). It can innately cast the following spells, requiring no material components:

* At will: _dancing lights_

* 1/day each: _darkness, faerie fire, levitate _(self only)

___Spellcasting.___ The drow is a 10th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 14, +6 to hit with spell attacks). The drow has the following wizard spells prepared:

* Cantrips (at will): _mage hand, minor illusion, poison spray, ray of frost_

* 1st level (4 slots): _mage armor, magic missile, shield, witch bolt_

* 2nd level (3 slots): _alter self, misty step, web_

* 3rd level (3 slots): _fly, lightning bolt_

* 4th level (3 slots): _Evard's black tentacles, greater invisibility_

* 5th level (2 slots): _cloudkill_

___Sunlight Sensitivity.___ While in sunlight, the drow has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Staff.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 2 (1d6-1) bludgeoning damage, or 3 (1d8-1) bludgeoning damage if used with two hands, plus 3 (1d6) poison damage.

___Summon Demon (1/Day).___ The drow magically summons a quasit, or attempts to summon a shadow demon with a 50 percent chance of success. The summoned demon appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other demons. It remains for 10 minutes, until it or its summoner dies, or until its summoner dismisses it as an action.
