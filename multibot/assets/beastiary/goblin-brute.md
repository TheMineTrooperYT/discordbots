"Goblin Brute";;;_size_: Small humanoid (goblinoid)
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Goblin"
_skills_: "Athletics +5"
_senses_: "darkvision 60ft., passive Perception 9"
_speed_: "30 ft."
_hit points_: "55 (10d6 + 20)"
_armor class_: "11"
_stats_: | 16 (+3) | 12 (+1) | 14 (+2) | 10 (+0) | 8 (-1) | 8 (-1) |

___Slamming Charge.___ If the goblin moves at least 15
feet straight toward a creature and then hits it with
a battleaxe attack on the same turn, that target
must succeed on a DC 13 Strength saving throw or
be knocked prone.

___Relentless Bloodlust.___ As long as the goblin has 20
hit points or fewer, it makes its first attack on each
turn with advantage and has resistance to
bludgeoning, piercing, and slashing damage from
nonmagical weapons.

**Actions**

___Multiattack.___ The goblin makes two attacks with its
battleaxe.

___Battleaxe.___ Melee Weapon Attack: +5 to hit, reach 5
ft., one target. Hit: 7 (1d8 + 3) slashing damage, or
8 (1d10 +3) slashing damage if used with two
hands.

___Handaxe.___ Melee or Ranged Weapon Attack: +5 to
hit, range 20/60 ft., one target. Hit: 7 (1d6 + 3)
slashing damage.
