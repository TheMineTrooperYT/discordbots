"Giant Water Beetle";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_senses_: "blindsight 60 ft., passive Perception 10"
_speed_: "20 ft., swim 60 ft."
_hit points_: "37 (5d8 + 15)"
_armor class_: "13 (natural armor)"
_stats_: | 15 (+2) | 13 (+1) | 16 (+3) | 2 (-4) | 10 (+0) | 9 (-1) |

___Water Breathing.___ Giant water beetles can only breathe water
but can hold their breath for up to 8 hours out of water.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage.

___Ink Cloud (Recharge 6).___ A 10-foot radius cloud of ink extends all
around the water beetle if it is underwater. The area is heavily obscured
for 1 minute although a significant current can disperse the ink.
