"Adult Green Dragon";;;_size_: Huge dragon
_alignment_: lawful evil
_challenge_: "15 (13,000 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 60 ft., darkvision 120 ft."
_skills_: "Deception +8, Insight +7, Perception +12, Persuasion +8, Stealth +6"
_damage_immunities_: "poison"
_saving_throws_: "Dex +6, Con +10, Wis +7, Cha +8"
_speed_: "40 ft., fly 80 ft., swim 40 ft."
_hit points_: "207 (18d12+90)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "poisoned"
_stats_: | 23 (+6) | 12 (+1) | 21 (+5) | 18 (+4) | 15 (+2) | 17 (+3) |

___Amphibious.___ The dragon can breathe air and water.

___Legendary Resistance (3/Day).___ If the dragon fails a saving throw, it can choose to succeed instead.

**Actions**

___Multiattack.___ The dragon can use its Frightful Presence. It then makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 17 (2d10 + 6) piercing damage plus 7 (2d6) poison damage.

___Claw.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) slashing damage.

___Tail.___ Melee Weapon Attack: +11 to hit, reach 15 ft., one target. Hit: 15 (2d8 + 6) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon's choice that is within 120 feet of the dragon and aware of it must succeed on a DC 16 Wisdom saving throw or become frightened for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful or the effect ends for it, the creature is immune to the dragon's Frightful Presence for the next 24 hours .

___Poison Breath (Recharge 5-6).___ The dragon exhales poisonous gas in a 60-foot cone. Each creature in that area must make a DC 18 Constitution saving throw, taking 56 (16d6) poison damage on a failed save, or half as much damage on a successful one.

**Legendary** Actions

The adult green dragon can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. The adult green dragon regains spent legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each creature within 10 ft. of the dragon must succeed on a DC 19 Dexterity saving throw or take 13 (2d6 + 6) bludgeoning damage and be knocked prone. The dragon can then fly up to half its flying speed.