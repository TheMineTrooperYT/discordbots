"Kongamato";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "5 (1800 XP)"
_languages_: "--"
_skills_: "Perception +3"
_senses_: "passive Perception 13"
_speed_: "10 ft., fly 60 ft."
_hit points_: "112 (15d10 + 30)"
_armor class_: "16 (natural armor)"
_stats_: | 19 (+4) | 18 (+4) | 14 (+2) | 2 (-4) | 10 (+0) | 7 (-2) |

___Flyby.___ The kongamato doesn't provoke an opportunity attacks when it flies out of an enemy's reach.

___Breaker of Boats.___ The kongamato deals double damage to objects and structures made of wood or lighter materials.

**Actions**

___Multiattack.___ The kongamato makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 18 (4d6 + 4) piercing damage. If the target is a Medium or smaller creature, it is grappled (escape DC 14). Until this grapple ends, the target is restrained and the kongamato can't bite another target. When the kongamato moves, any target it is grappling moves with it.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 14 (3d6 + 4) slashing damage.

