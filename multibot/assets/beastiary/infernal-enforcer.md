"Infernal Enforcer";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "Any three languages"
_skills_: "Acrobatics +7, Arcana +4, Insight +6, Persuasion +8"
_senses_: "passive Perception 12"
_saving_throws_: "Wis +6, Cha +8"
_speed_: "40 ft."
_hit points_: "78 (12d8 + 24)"
_armor class_: "15"
_stats_: | 12 (+1) | 16 (+3) | 14 (+2) | 10 (+0) | 15 (+2) | 19 (+4) |

___Infernal Blade.___ The enforcer has an infernal blade
bound to it. This weapon ignores all resistances and
deals an extra 3 (1d6) fire damage with each attack
(included in the attack). In addition, while the
enforcer is wielding this blade, it cannot be
disarmed.

___Spellcasting.___ The enforcer is a 6th-level spellcaster.
Its spellcasting ability is Charisma (spell save DC
16, +8 to hit with spell attacks). It regains these
expended spellslots when it finishes a short or long
rest. The enforcer has the following warlock spells
prepared:

* Cantrips (at will): _blade ward, eldritch blast, mage hand_

* 1st-3rd level (3 3rd-level slots): _armor of Agathys, crown of madness, fear, hellish rebuke, hex, spider climb_

**Actions**

___Multiattack.___ The enforcer makes two attacks with its
felblade and one with its felstrike.

___Felblade.___ Melee Weapon Attack: +7 to hit, reach 5
ft., one target. Hit: 6 (1d6 + 3) piercing damage
plus 3 (1d6) fire damage.

___Felstrike.___ Melee Weapon Attack: +7 to hit, reach 5
ft., one target. Hit: 6 (1d6 + 3) bludgeoning
damage plus 3 (1d6) fire damage.

___Infernal Onslaught (3/Short Rest).___ The enforcer
performs a swift series of punches to launch bursts
of flame at up to four targets within 60 feet. Each
of those creatures must make a DC 16 Dexterity
saving throw, saving 17 (5d6) fire damage on a
failed save, or half as much damage on a successful
save.
