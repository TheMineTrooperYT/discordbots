"Horakh";;;_size_: Medium monstrosity
_alignment_: neutral
_challenge_: "9 (5000 XP)"
_languages_: "understands Undercommon"
_skills_: "Athletics +8, Perception +6, Stealth +8"
_senses_: "darkvision 60 ft., tremorsense 30 ft., passive Perception 20"
_saving_throws_: "Dex +12"
_speed_: "40 ft., climb 30 ft."
_hit points_: "161 (19d8 + 76)"
_armor class_: "17 (natural armor)"
_stats_: | 18 (+4) | 19 (+4) | 19 (+4) | 8 (-1) | 15 (+2) | 10 (+0) |

___Shadow Stealth.___ A horakh can hide as a bonus action if it's in dim light or darkness.

___Standing Leap.___ As part of its movement, the horakh can jump up to 20 feet horizontally and 10 feet vertically, with or without a running start.

**Actions**

___Multiattack.___ The horakh makes two claw attacks and one bite attack.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 22 (4d8 + 4) slashing damage. If the bite attack hits a target that's grappled by the horakh, the target must make a successful DC 16 Dexterity saving throw or one of its eyes is bitten out. A creature with just one remaining eye has disadvantage on ranged attack rolls and on Wisdom (Perception) checks that rely on sight. If both (or all) eyes are lost, the target is blinded. The regenerate spell and comparable magic can restore lost eyes. Also see Implant Egg, below.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 17 (3d8 + 4) piercing damage. If both attacks hit the same Medium or smaller target in a single turn, the target is grappled (escape DC 14).

___Implant Egg.___ If a horakh's bite attack reduces a grappled creature to 0 hit points, or it bites a target that's already at 0 hit points, it implants an egg in the creature's eye socket. The deposited egg grows for 2 weeks before hatching. If the implanted victim is still alive, it loses 1d2 Constitution every 24 hours and has disadvantage on attack rolls and ability checks. After the first week, the victim is incapacitated and blinded. When the egg hatches after 2 weeks, an immature horakh erupts from the victim's head, causing 1d10 bludgeoning, 1d10 piercing, and 1d10 slashing damage. A lesser restoration spell can kill the egg during its incubation.

