"Kenku";;;_size_: Medium humanoid (kenku)
_alignment_: chaotic neutral
_challenge_: "1/4 (50 XP)"
_languages_: "understands Auran and Common but speaks only through the use of its Mimicry trait"
_skills_: "Deception +4, Perception +2, Stealth +5"
_speed_: "30 ft."
_hit points_: "13 (3d8)"
_armor class_: "13"
_stats_: | 10 (0) | 16 (+3) | 10 (0) | 11 (0) | 10 (0) | 10 (0) |

___Ambusher.___ In the first round of a combat, the kenku has advantage on attack rolls against any creature it surprised

___Mimicry.___ The kenku can mimic any sounds it has heard, including voices. A creature that hears the sounds can tell they are imitations with a successful DC 14 Wisdom (Insight) check.

**Actions**

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Shortbow.___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 6 (1d6 + 3) piercing damage.