"Leucrotta";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Abyssal, Gnoll"
_senses_: "darkvision 60 ft."
_skills_: "Deception +2, Perception +3"
_speed_: "50 ft."
_hit points_: "67 (9d10+18)"
_armor class_: "14 (natural armor)"
_stats_: | 18 (+4) | 14 (+2) | 15 (+2) | 9 (-1) | 12 (+1) | 6 (-2) |

___Keen Smell.___ The leucrotta has advantage on Wisdom (Perception) checks that rely on smell.

___Kicking Retreat.___ If the leucrotta attacks with its hooves, it can take the Disengage action as a bonus action.

___Mimicry.___ The leucrotta can mimic animal sounds and humanoid voices. A creature that hears the sounds can tell they are imitations with a successful DC 14 Wisdom (Insight) check.

___Rampage.___ When the leucrotta reduces a creature to 0 hit points with a melee attack on its turn, it can take a bonus action to move up to half its speed and make an attack with its hooves.

**Actions**

___Multiattack.___ The leucrotta makes two attacks: one with its bite and one with its hooves.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8+4) piercing damage. If the leucrotta scores a critical hit, it rolls the damage dice three times, instead of twice.

___Hooves.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6+4) bludgeoning damage.