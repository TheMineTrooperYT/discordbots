"Ghoul, Iron";;;_size_: Medium undead
_alignment_: lawful evil
_challenge_: "5 (1800 XP)"
_languages_: "Common, Darakhul, Undercommon"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_immunities_: "poison"
_condition_immunities_: "charmed, exhaustion, poisoned"
_speed_: "30 ft., burrow 20 ft."
_hit points_: "143 (22d8 + 44)"
_armor class_: "16 (breastplate)"
_stats_: | 18 (+4) | 16 (+3) | 14 (+2) | 14 (+2) | 14 (+2) | 14 (+2) |

___Turning Defiance.___ The iron ghoul and any ghouls within 30 feet of it have advantage on saving throws against effects that turn undead.

**Actions**

___Multiattack.___ The iron ghoul makes one bite attack and one claw attack, or three glaive attacks.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 17 (3d8 + 4) piercing damage. If the target is a creature other than an elf or undead, it must succeed on a DC 13 Constitution saving throw or be paralyzed for 1 minute. The target repeats the saving throw at the end of each of its turns, ending the effect on itself on a success. If the target is humanoid, it must succeed on a separate DC 13 Constitution saving throw or contract darakhul fever.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 18 (4d6 + 4) slashing damage. If the target is a creature other than an elf or undead, it must succeed on a DC 13 Constitution saving throw or be paralyzed for 1 minute. The target repeats the saving throw at the end of each of its turns, ending the effect on itself on a success. If the target is humanoid, it must succeed on a separate DC 13 Constitution saving throw or contract darakhul fever.

___Glaive.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 20 (1d10 + 4) slashing damage.

___Heavy Bone Crossbow.___ Ranged Weapon Attack: +6 to hit, range 100/400, one target. Hit: 8 (1d10 + 3) piercing damage.

