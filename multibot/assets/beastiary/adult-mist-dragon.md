"Adult Mist Dragon";;;_size_: Huge dragon
_alignment_: neutral
_challenge_: "15 (13,000 XP)"
_languages_: "Auran, Aquan, Celestial, Common, Draconic, Ignan, Sylvan"
_skills_: "Perception +12, Stealth +7, Survival +7"
_senses_: "blindsight 120 ft., darkvision 120 ft., passive Perception 22"
_saving_throws_: "Dex +7, Con +12, Wis +7, Cha +10"
_damage_immunities_: "fire"
_speed_: "40 ft., fly 80 ft., swim 40 ft."
_hit points_: "189 (14d12 + 98)"
_armor class_: "18 (natural armor)"
_stats_: | 25 (+7) | 14 (+2) | 25 (+7) | 16 (+3) | 14 (+2) | 21 (+5) |

___Amphibious.___ The mist dragon can breathe both water and air.

___Legendary Resistance (3/day).___ If the dragon fails a saving throw, it can
choose to succeed instead.

**Actions**

___Multiattack.___ The dragon uses its Frightful Presence and makes three
attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +12 to hit, reach 20 ft., one target. Hit:
18 (2d10 + 7) piercing damage plus 10 (3d6) fire damage.

___Claw.___ Melee Weapon Attack: +12 to hit, reach 10 ft., one target.
Hit: 17 (3d6 + 7) slashing damage.

___Tail.___ Melee Weapon Attack: +12 to hit, reach 20 ft., one target.
Hit: 16 (2d8 + 7) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon’s choice that
is within 120 feet of the dragon and aware of it must succeed
on a DC 18 Wisdom saving throw or become frightened for 1
minute. A creature can repeat the saving throw at the end of
each of its turns, ending the effect on itself on a success. If a
creature’s saving throw is successful or the effect ends for it,
the creature is immune to the dragon’s Frightful Presence for the
next 24 hours.

___Breath Weapon (Recharge 5–6).___ The mist dragon uses one of the
following breath weapons.

1. _Obscuring Mist._ The dragon exhales a murky opaque mist in a 60-foot
cube that spreads around corners. This mist heavily obscures the area and
remains for 1 minute, or until dispersed with a moderate or stronger wind
(at least 10 miles per hour).

2. _Scalding Mist._ The dragon exhales a fiery blast of lingering mist in
a 60-foot cone that spreads around corners. The mist lightly obscures
the area and remains for 1 minute, or until dispersed with a moderate or
stronger wind (at least 10 miles per hour). Each creature that enters the
area or begins its turn there must make a DC 20 Constitution saving throw,
taking 63 (18d6) fire damage on a failed saving throw, or half as much
damage on a successful one. Being underwater doesn’t grant resistance
to this damage.

___Mist Form.___ The mist dragon polymorphs into a Huge cloud of
mist, or back into its true form. While in mist form, the dragon can’t take
any actions, speak, or manipulate objects. It is weightless, has a flying
speed of 20 feet, can hover, and can enter a hostile creature’s space and
stop there. In addition, if air can pass through a space, the mist can do so
without squeezing, and it can’t pass through water. It has advantage on
Strength, Dexterity, and Constitution saving throws, it is immune to all
nonmagical damage.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. The dragon regains spent legendary
actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each
creature within 15 feet of the dragon must succeed on a DC 20 Dexterity
saving throw or take 14 (2d6 + 7) bludgeoning damage and be knocked
prone. The dragon can then fly up to half its flying speed.
