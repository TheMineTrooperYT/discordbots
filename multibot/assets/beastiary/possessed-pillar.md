"Possessed Pillar";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "7 (2900 XP)"
_languages_: "understands the languages of its creator but can't speak"
_senses_: "darkvision 120 ft., passive Perception 10"
_damage_immunities_: "poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "20 ft."
_hit points_: "95 (10d10 + 40)"
_armor class_: "14 (natural armor)"
_stats_: | 20 (+5) | 8 (-1) | 19 (+4) | 3 (-4) | 11 (+0) | 1 (-5) |

___Immutable Form.___ The pillar is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The pillar has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The pillar's weapon attacks are magical.

___Steal Weapons.___ The eldritch magic that powers the pillar produces a magnetic power that seizes metal objects that touch it, including metal weapons. When a creature successfully strikes the pillar with a metal melee weapon, the attacker must make a successful DC 15 Strength or Dexterity saving throw or the weapon becomes stuck to the pillar until the pillar releases it or is destroyed. The saving throw uses the same ability as the attack used. The pillar can release all metal weapons stuck to it whenever it wants. A pillar always drops all weapons stuck to it when it believes it's no longer threatened. This ability affects armor only during a grapple.

___False Appearance.___ While the pillar remains motionless, it is indistinguishable from a statue or a carved column.

**Actions**

___Multiattack.___ The pillar makes two slam attacks.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 18 (3d8 + 5) bludgeoning damage.

