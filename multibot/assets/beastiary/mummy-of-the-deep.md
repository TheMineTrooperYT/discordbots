"Mummy of the Deep";;;_size_: Medium endead
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "the languages it knew in life"
_skills_: "Athletics +6, Perception +5, Stealth +3"
_senses_: "darkvision 60 ft., passive Perception 15"
_saving_throws_: "Con +5, Wis +5"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "fire; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "20 ft., swim 20 ft."
_hit points_: "52 (8d8 + 16)"
_armor class_: "14 (natural armor)"
_stats_: | 17 (+3) | 10 (+0) | 14 (+2) | 6 (-2) | 14 (+2) | 15 (+2) |

___Amphibious.___ Mummies of the deep can breathe air and water.

___Innate Spellcasting (1/day).___ The mummy can innately cast _control
water_, requiring no material components. Its innate spellcasting ability
is Wisdom

**Actions**

___Multiattack.___ The mummy can use its Dreadful Glare and makes one
attack with its necrotizing strike.

___Necrotizing Strike.___ Melee Weapon Attack: +6 to hit, reach 5ft., one
target. Hit: 10 (2d6 + 3) bludgeoning damage plus 10 (3d6) necrotic
damage. If the target is a creature, it must succeed on a DC 12 Constitution
saving throw or be cursed with necrotic fever. The cursed target can’t
regain hit points, and its hit point maximum decreases by 10 (3d6) for
every 24 hours that elapse. If the curse reduces the target’s hit point
maximum to 0, the target dies, and its body turns to pluff mud. The curse
lasts until removed by the remove curse spell or other magic.

___Dreadful Glare.___ The mummy targets one creature it can see within 60
feet of it. If the target can see the mummy, it must succeed on a DC 12
Wisdom saving throw against this magic or become frightened until the
end of the mummy’s next turn. If the target fails the saving throw by 5 or
more, it is also paralyzed for the same duration. A target that succeeds on
the saving throw is immune to the Dreadful Glare for all mummies (but no
mummy lords) for the next 24 hours.

___Drowning Breath.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one
target. Hit: The creature is grappled (escape DC 13). If the creature has
not broken the mummy’s grapple by the start of the mummy’s next turn,
the mummy uses its next action to press its lips against the creature’s and
regurgitates seawater into the creature’s lungs. The creature immediately
begins suffocating (see the fifth edition SRD for more information on
suffocation). While suffocating, the creature can only use its actions to
try and cough the seawater up, requiring a successful DC 12 Constitution
saving throw to do so.
