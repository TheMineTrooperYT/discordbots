"Zaratan";;;_page_number_: 201
_size_: Gargantuan elemental
_alignment_: neutral
_challenge_: "22 (41,000 XP)"
_languages_: "-"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 15"
_damage_immunities_: "poison"
_saving_throws_: "Wis +12, Cha +11"
_speed_: "40 ft., swim 40 ft."
_hit points_: "307  (15d20 + 150)"
_armor class_: "21 (natural armor)"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, stunned"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 30 (+10) | 10 (0) | 30 (+10) | 2 (-4) | 21 (+5) | 18 (+4) |

___Earth-Shaking Movement.___ As a bonus action after moving at least 10 feet on the ground, the zaratan can send a shock wave through the ground in a 120-foot-radius circle centered on itself. That area becomes difficult terrain for 1 minute. Each creature on the ground that is concentrating must succeed on a DC 25 Constitution saving throw or the creature's concentration is broken.
The shock wave deals 100 thunder damage to all structures in contact with the ground in the area. If a creature is near a structure that collapses, the creature might be buried; a creature within half the distance of the structure's height must make a DC 25 Dexterity saving throw. On a failed save, the creature takes 17 (5d6) bludgeoning damage, is knocked prone, and is trapped in the rubble. A trapped creature is restrained, requiring a successful DC 20 Strength (Athletics) check as an action to escape. Another creature within 5 feet of the buried creature can use its action to clear rubble and grant advantage on the check. If three creatures use their actions in this way, the check is an automatic success. On a successful save, the creature takes half as much damage and doesn't fall prone or become trapped.

___Legendary Resistance (3/Day).___ If the zaratan fails a saving throw, it can choose to succeed instead.

___Magic Weapons.___ The zaratan's weapon attacks are magical.

___Siege Monster.___ The elemental deals double damage to objects and structures (included in Earth-Shaking Movement).

**Actions**

___Multiattack___ The zaratan makes two attacks: one with its bite and one with its stomp.

___Bite___ Melee Weapon Attack: +17 to hit, reach 20 ft., one target. Hit: 28 (4d8 + 10) piercing damage.

___Stomp___ Melee Weapon Attack: +17 to hit, reach 20 ft., one target. Hit: 26 (3d10 + 10) bludgeoning damage.

___Spit Rock___ Ranged Weapon Attack: +17 to hit, range 120 ft./240 ft., one target. Hit: 31 (6d8 + 10) bludgeoning damage.

___Spew Debris (Recharge 5-6)___ The zaratan exhales rocky debris in a 90-foot cube. Each creature in that area must make a DC 25 Dexterity saving throw. A creature takes 33 (6d10) bludgeoning damage on a failed save, or half as much damage on a successful one. A creature that fails the save by 5 or more is knocked prone.

**Legendary** Actions

The zaratan can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The zaratan regains spent legendary actions at the start of its turn.

___Stomp___ The zaratan makes one stomp attack.

___Move___ The zaratan moves up to its speed.

___Spit (Costs 2 Actions)___ The zaratan uses Spit Rock.

___Retract (Costs 2 Actions)___ The zaratan retracts into its shell. Until it takes its Emerge action, it has resistance to all damage, and it is restrained. The next time it takes a legendary action, it must take its Revitalize or Emerge action.

___Revitalize (Costs 2 Actions)___ The zaratan can use this option only if it is retracted in its shell. It regains 52 (5d20) hit points. The next time it takes a legendary action, it must take its Emerge action.

___Emerge (Costs 2 Actions)___ The zaratan emerges from its shell and uses Spit Rock. It can use this option only if it is retracted in its shell.