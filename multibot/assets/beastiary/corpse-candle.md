"Corpse Candle";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Infernal"
_skills_: "Deception +5, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "acid, cold, fire, lightning, thunder; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, grappled, paralyzed, petrified, poisoned, prone, restrained"
_speed_: "fly 50 ft., swim 50 ft."
_hit points_: "55 (10d8 + 10)"
_armor class_: "12"
_stats_: | 6 (-2) | 15 (+2) | 13 (+1) | 13 (+1) | 13 (+1) | 17 (+3) |

___Incorporeal Movement.___ The corpse candle can move through other
creatures and objects in a body of water as if they were difficult terrain. It
takes 5 (1d10) force damage if it ends its turn inside an object or outside
its body of water.

___Sunlight Sensitivity.___ While in sunlight, the corpse candle has
disadvantage on attack rolls, as well as on Wisdom (Perception) checks
that rely on sight.

**Actions**

___Watery Touch.___ Melee Weapon Attack: +4 to hit, reach ft., one target.
Hit: 9 (2d6 + 2) necrotic damage, and the target’s Strength score is
reduced by 1d4. The target dies if this reduces its Strength to 0.
Otherwise, the reduction lasts until the target finishes a short or long
rest.
A humanoid slain by this attack rises 24 hours later as a zombie
unless the humanoid is restored to life or its body is destroyed.

___Hypnotic Lights.___ The corpse candle creates a twisting,
dancing pattern of ever-shifting colored lights in a 10-foot cube
at a point it can see within 60 feet of itself. The pattern appears
for a minute then vanishes. Each creature in the area who sees
the pattern must succeed on a DC 13 Wisdom saving throw.
On a failed save, the creature becomes charmed for 1 minute.
While charmed, the creature is incapacitated and has a speed
of 0. When a charmed creature takes damage it can repeat the
saving throw, ending the effect on a success.
A creature that starts its turn within the 10-foot cube of lights
must succeed on a DC 13 Constitution saving throw or the target’s
Constitution score is reduced by 1d4. The target dies if this reduces
its Constitution to 0. Otherwise, the reduction lasts until the target
finishes a short or long rest.
