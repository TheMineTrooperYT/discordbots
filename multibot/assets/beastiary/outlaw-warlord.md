"Outlaw Warlord";;;_size_: Medium humanoid
_alignment_: any non-lawful alignment
_challenge_: "6 (2,300 XP)"
_languages_: "Common and any two other languages"
_skills_: "Arcana +6, Athletics +7, Insight +5, Intimidation +6, Perception +5"
_senses_: "passive Perception 10"
_saving_throws_: "Str +7, Dex +3, Int +6"
_speed_: "30 ft."
_hit points_: "108 (18d8 + 36)"
_armor class_: "20 (plate, shield)"
_stats_: | 18 (+4) | 10 (+0) | 14 (+2) | 16 (+3) | 14 (+2) | 16 (+3) |

___Eldritch Knight Spellcasting.___ The warlord is a 9th level
spellcaster. Its spellcasting ability is
Intelligence (spell save DC 13, +5 to hit with spell
attacks). The warlord has the following wizard spells
prepared:

* Cantrips: _message, true strike_

* 1st level (4 slots): _charm person, fog cloud, magic missile, shield_

* 2nd level (2 slots): _magic weapon, misty step_

___Second Wind (Recharges after a Short or Long Rest).___
As a bonus action, the warlord regains 15 hit
points.

___War Caster.___ The warlord can perform the somatic
components of spells, even with a weapon or shield
equipped in one or both hands. In addition, the
warlord has advantage on concentration saving
throws.

**Actions**

___Multiattack.___ The commander makes three attacks
with its longsword or two with its javelins.

___Longsword.___ Melee Weapon Attack: +7 to hit, reach
5 ft., one target. Hit: 8 (1d8 + 4) slashing damage,
or 9 (1d10 +4) slashing damage is used with two
hands.

___Javelin.___ Melee or Ranged Weapon Attack: +6 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 7
(1d6 + 4) piercing damage.
