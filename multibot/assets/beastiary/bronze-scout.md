"Bronze Scout";;;_page_number_: 125
_size_: Medium construct
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "understands one language of its creator but can't speak"
_senses_: "darkvision 60 ft., passive Perception 16"
_skills_: "Perception +6, Stealth +7"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical attacks that aren't adamantine"
_speed_: "30 ft., burrow 30 ft."
_hit points_: "18  (4d8)"
_armor class_: "13"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_stats_: | 10 (0) | 16 (+3) | 11 (0) | 3 (-3) | 14 (+2) | 1 (-4) |

___Earth Armor.___ The bronze scout doesn't provoke opportunity attacks when it burrows.

___Magic Resistance.___ The bronze scout has advantage on saving throws against spells and other magical effects.

**Actions**

___Bite___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage plus 3 (1d6) lightning damage.

___Lightning Flare (Recharges after a Short or Long Rest)___ Each creature in contact with the ground within 15 feet of the bronze scout must make a DC 13 Dexterity saving throw, taking 14 (4d6) lightning damage on a failed save, or half as much damage on a successful one.
