"Deep Hunter Sea Serpent";;;_size_: Gargantuan dragon
_alignment_: lawful neutral
_challenge_: "21 (33,000 XP)"
_languages_: "Aquan, Draconic"
_skills_: "Athletics +15, Perception +8"
_senses_: "darkvision 60 ft., passive Perception 18"
_saving_throws_: "Str +15, Dex +10, Con +14"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned, prone"
_speed_: "0 ft., swim 80 ft."
_hit points_: "385 (22d20 + 154)"
_armor class_: "19 (natural armor)"
_stats_: | 26 (+8) | 14 (+2) | 25 (+7) | 7 (-2) | 13 (+1) | 14 (+2) |

___Amphibious.___ The deep hunter serpent can breathe air and water.

___Siege Monster.___ The deep hunter serpent deals double damage to objects
and structures.

**Actions**

___Multiattack.___ A deep hunter serpent attacks once with its tail and either
bites or uses its swallow attack.

___Bite.___ Melee Weapon Attack: +15 to hit, reach 10 ft., one target. Hit: 34
(4d12 + 8) piercing damage plus 22 (4d10) poison damage, and the target
is grappled (escape DC 23) and restrained. The deep sea hunter can’t make
a bit attack while it has a creature grappled with its bite.

___Tail Slap.___ Melee Weapon Attack: +15 to hit, reach 20 ft., one target. Hit:
30 (4d10 + 8) bludgeoning damage and the target is pushed 15 feet away
and knocked prone.

___Swallow.___ A Large or smaller target grappled by the serpent’s bite attack
is swallowed and the grapple ends. While swallowed, the creature is
blinded and restrained, it has total cover against attacks and other effects
outside the deep hunter serpent, and it takes 42 (12d6) acid damage at the
start of each of the deep hunter serpent’s turns. If the deep hunter takes
30 damage or more on a single turn from a creature inside it, the serpent
must succeed on a DC 25 Constitution saving throw at the end of that
turn or regurgitate all swallowed creatures, which fall prone in a space
within 10 feet of the serpent. If the serpent dies, a swallowed creature is
no longer restrained by it and can escape from the corpse using 15 feet of
movement, exiting prone.

**Legendary** Actions

The deep hunter sea serpent can take up to 3 legendary actions, choosing
from the options below. Only one legendary action option can be used at
a time and only at the end of another creature’s turn. The serpent regains
spent legendary actions at the start of its turn.

___Tail Attack.___ The deep hunter serpent uses its Tail Slap.

___Coiling Maneuver (Costs 2 Actions).___ The deep hunter sea serpent
attempts to grapple a Huge or smaller target. The target must make a DC
23 Strength saving throw or be grappled and restrained. The sea serpent
can’t use its other legendary actions or its Tail Slap if it’s grappling a
target. At the beginning of each of the brine serpent’s turns, the grappled
target takes 30 (4d10 + 8) bludgeoning damage.

___Unhinge Jaw (Costs 3 Actions).___ The deep hunter uses its bite attack
on all creatures of Large size or smaller in a 10-foot cube within 15 feet
of the deep hunter. If the attack hits on any of the creatures, it can use its
swallow ability.
