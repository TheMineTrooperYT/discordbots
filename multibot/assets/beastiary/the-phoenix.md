"The Phoenix (SaF)";;;_size_: Huge celestial
_alignment_: neutral good
_challenge_: "21 (33,000 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Perception +14"
_senses_: "truesight 120 ft., passive Perception 24"
_saving_throws_: "Int +14, Wis +14, Cha +16"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "radiant; bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_speed_: "30 ft., fly 3,450 ft. (hover)"
_hit points_: "243 (18d10 + 144)"
_armor class_: "21 (natural armor)"
_stats_: | 25 (+7) | 22 (+6) | 26 (+8) | 25 (+7) | 25 (+7) | 29 (+9) |

___Born of Fire.___ The phoenix radiates sunlight in
a 60-foot radius and dim light in a further 180-
foot radius.

___Magic Resistance.___ The phoenix has advantage
on saving throws against spells and other
magical effects.

___The Fire Is Alive.___ At the start of each of its turns, the
phoenix regains 3d12 hit points.

___Dazzling Plumage.___ The phoenix’s feathers cycle
through every color in the rainbow. Any evil creature
that begins its turn within 60 feet of the Defender
of All the Earth and does not avert its gaze must
succeed on a DC 15 Wisdom saving throw or be
charmed until the end of its next turn. Charmed
targets are stunned. If a target’s saving throw is
successful, it is immune to the phoenix’s Dazzling
Plumage for the next 24 hours.

**Actions**

___Multiattack.___ The phoenix makes two talon attacks
and one bite attack.

___Talon.___ Melee Weapon Attack: +14 to hit, reach 10 ft.,
one target. Hit: 25 (4d8 + 7) slashing damage.

___Bite.___ Melee Weapon Attack: + 14 to hit, reach 15 ft.,
one target. Hit: 18 (2d10 + 7) piercing damage plus 9
(2d8) fire damage.

___Light of All Colors (Recharge 4–6).___ The phoenix
exhales a beam of brilliant, prismatic light in a
line that is 10 feet wide and 1,000 feet long. Each
creature in that line must make a DC 22 Dexterity
saving throw, taking 45 (10d8) fire and 35 (10d6)
radiant damage on a failed save, or half as much
damage on a successful one.

In battle, the phoenix is a unit with Attack +17 and
Power +15 and can affect every unit in the battle.
