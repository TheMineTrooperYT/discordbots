"Corrupted Guardian";;;_size_: Medium celestial
_alignment_: chaotic evil
_challenge_: "9 (5,000 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Athletics +8, Perception +9"
_senses_: "darkvision 120ft., passive Perception 19"
_saving_throws_: "Wis +9, Cha +7"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhausted, frightened"
_speed_: "30 ft., fly 90 ft."
_hit points_: "119 (14d8 + 56)"
_armor class_: "19 (natural armor, shield)"
_stats_: | 18 (+4) | 14 (+2) | 18 (+4) | 16 (+3) | 20 (+5) | 16 (+3) |

___Disabling Strikes.___ The guardian's weapons are magical.
When the guardian hits with any weapon, the weapon
deals an extra 2d8 necrotic damage and the target
must succeed on a DC 15 Constitution saving throw or
the next weapon or spell attack it makes is made at
disadvantage (included in the attack).

___Magic Resistance.___ The guardian has advantage on saving
throws against spells and other magical effects.

**Actions**

___Multiattack.___ The guardian makes three melee attacks:
two with its flail and one with its shield crush.

___Flail.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one
target. Hit: 8 (1d8 + 4) bludgeoning damage plus 9
(2d8) necrotic damage and the target must succeed on
a DC 15 Constitution saving throw or the next weapon
or spell attack it makes is made at disadvantage.

___Shield Crush.___ Melee Weapon Attack: +8 to hit, reach 5
ft., one target. Hit: 8 (1d8 + 4) bludgeoning damage
and the target must succeed on a DC 15 Strength
saving throw or be pushed up to 15 feet and knocked
prone.

___Rapid Assault (Recharge 5-6).___ The guardian makes four
flail attacks against a single target with disadvantage.

**Reactions**

___Shield Wall (2/Day).___ The guardian raises its shield and
projects an ethereal barrier of energy against a ranged
spell or weapon attack that it can see. That attack deals
no damage. This ability does not work on spells of 5th
level or higher.
