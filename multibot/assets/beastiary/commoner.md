"Commoner";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "0 (10 XP)"
_languages_: "any one language (usually Common)"
_speed_: "30 ft."
_hit points_: "4 (1d8)"
_armor class_: "10"
_stats_: | 10 (0) | 10 (0) | 10 (0) | 10 (0) | 10 (0) | 10 (0) |

**Actions**

___Club.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 2 (1d4) bludgeoning damage.