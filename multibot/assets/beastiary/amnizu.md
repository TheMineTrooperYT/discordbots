"Amnizu";;;_page_number_: 164
_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "18 (20,000 XP)"
_languages_: "Common, Infernal, telepathy 1,000 ft."
_senses_: "darkvision 120 ft., passive Perception 17"
_skills_: "Perception +7"
_damage_immunities_: "fire, poison"
_saving_throws_: "Dex +7, Con +9, Wis +7, Cha +10"
_speed_: "30 ft., fly 40 ft."
_hit points_: "202  (27d8 + 81)"
_armor class_: "21 (natural armor)"
_condition_immunities_: "charmed, poisoned"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 11 (0) | 13 (+1) | 16 (+3) | 20 (+5) | 12 (+1) | 18 (+4) |

___Devil's Sight.___ Magical darkness doesn't impede the amnizu's darkvision.

___Innate Spellcasting.___ The amnizu's innate spellcasting ability is Intelligence (spell save DC 19, +11 to hit with spell attacks). The amnizu can innately cast the following spells, requiring no material components:

* At will: _charm person, command_

* 3/day each: _dominate person, fireball_

* 1/day each: _dominate monster, feeblemind_

___Magic Resistance.___ The amnizu has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack___ The amnizu uses Poison Mind. It also makes two attacks: one with its whip and one with its Disruptive Touch.

___Taskmaster Whip___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 10 (2d4 + 5) slashing damage plus 33 (6d10) force damage.

___Disruptive Touch___ Melee Spell Attack: +11 to hit, reach 5 ft., one target. Hit: 44 (8d10) necrotic damage.

___Poison Mind___ The amnizu targets one or two creatures that it can see within 60 feet of it. Each target must succeed on a DC 19 Wisdom saving throw or take 26 (4d12) necrotic damage and is blinded until the start of the amnizu's next turn.

___Forgetfulness (Recharge 6)___ The amnizu targets one creature it can see within 60 feet of it. That creature must make a DC 18 Intelligence saving throw and on a failure the target is stunned for 1 minute. A stunned creature repeats the saving throw at the end of each of its turns, ending the effect on itself on a success. If the target remains stunned for the full minute, it forgets everything it sensed, experienced, and learned during the last 5 hours.
