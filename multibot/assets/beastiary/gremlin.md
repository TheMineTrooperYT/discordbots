"Gremlin";;;_size_: Small fey
_alignment_: chaotic evil
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Goblin, Sylvan"
_skills_: "Acrobatics +5, Arcana +4, Perception +4, Sleight of Hand +5, Stealth +5"
_senses_: "passive Perception 14"
_speed_: "20 ft., climb 20 ft."
_hit points_: "10 (3d6)"
_armor class_: "14 (leather)"
_stats_: | 7 (-2) | 16 (+3) | 11 (+0) | 14 (+2) | 14 (+2) | 15 (+2) |

___Knot Expert.___ The gremlin has advantage on any check or saving throw
to break free of any effect grappling or restraining it when the effect is
made of rope or rope-like objects.

___Innate Spellcasting.___ The gremlin’s innate spellcasting ability is
Charisma (spell save DC 12, +4 to hit with spell attacks). It can cast the
following spells without requiring material components.

* At will: _arcane lock, knock_

* 3/day: _find traps_

* 1/day: _passwall_

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.
