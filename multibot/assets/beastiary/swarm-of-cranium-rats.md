"Swarm of Cranium Rats";;;_size_: Medium swarm
_alignment_: lawful evil
_challenge_: "5 (1,800 XP)"
_languages_: "telepathy 30 ft."
_senses_: "darkvision 30 ft."
_speed_: "30 ft."
_hit points_: "36 (8d8)"
_armor class_: "12"
_condition_immunities_: "charmed, frightened, grappled, paralyzed, petrified, prone, restrained, stunned"
_damage_resistances_: "bludgeoning, piercing, slashing"
_stats_: | 9 (-1) | 14 (+2) | 10 (0) | 15 (+2) | 11 (0) | 14 (+2) |

___Illumination.___ As a bonus action, the swarm can shed dim light from its brains in a 5-foot radius, increase the illumination to bright light in a 5- to 20-foot radius (and dim light for an additional number of feet equal to the chosen radius), or extinguish the light.

___Innate Spellcasting (Psionics).___ The swarm's innate spellcasting ability is Intelligence (spell save DC 13). As long as it has more than half of its hit points, it can innately cast the following spells, requiring no components::

* At will: command, comprehend languages, detect thoughts

* 1/day each: confusion, dominate monster

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny rat. The swarm can't regain hit points or gain temporary hit points.

___Telepathic Shroud.___ The swarm is immune to any effect that would sense its emotions or read its thoughts, as well as to all divination spells.

**Actions**

___Bites.___ Melee Weapon Attack: +5 to hit, reach 0 ft., one target in the swarm's space. Hit: 14 (4d6) piercing damage, or 7 (2d6) piercing damage if the swarm has half of its hit points or fewer.