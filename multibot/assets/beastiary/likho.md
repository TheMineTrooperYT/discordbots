"Likho";;;_size_: Medium monstrosity
_alignment_: chaotic evil
_challenge_: "6 (2300 XP)"
_languages_: "Common, Goblin, Void Speech"
_skills_: "Acrobatics +7, Perception +6, Stealth +10"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +7, Cha +8"
_speed_: "40 ft."
_hit points_: "90 (12d8 + 36)"
_armor class_: "15 (natural armor)"
_stats_: | 16 (+3) | 18 (+4) | 16 (+3) | 13 (+1) | 16 (+3) | 21 (+5) |

___Pounce.___ If the likho moves at least 20 feet straight toward a creature and then hits it with a claw attack on the same turn, that target must succeed on a DC 14 Strength saving throw or be knocked prone. If the target is prone, the likho can use a bonus action to make two additional claw attacks against it.

___Innate Spellcasting.___ The likho's innate spellcasting ability is Charisma (spell save DC 16). It can innately cast the following spells, requiring no material components:

At will: message

3/day each: crown of madness, mirror image, ray of enfeeblement

1/day: bestow curse

___Magic Resistance.___ The likho has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The likho makes two claw attacks.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) slashing damage.

___Disruptive Gaze.___ As a bonus action, the likho directs its gaze at any single creature it can see and afflicts it with a temporary bout of bad luck. The targeted creature has disadvantage on attack rolls, saving throws, and skill checks until the end of its next turn.

