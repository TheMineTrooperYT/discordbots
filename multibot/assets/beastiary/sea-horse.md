"Sea Horse";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_speed_: "swim 20 ft."
_hit points_: "1 (1d4-1)"
_armor class_: "11"
_stats_: | 1 (-5) | 12 (+1) | 8 (-1) | 1 (-5) | 10 (0) | 2 (-4) |

___Water Breathing.___ The sea horse can breathe only underwater.