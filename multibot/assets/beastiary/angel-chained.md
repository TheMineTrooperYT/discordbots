"Angel, Chained";;;_size_: Medium celestial
_alignment_: neutral evil
_challenge_: "8 (3900 XP)"
_languages_: "Common, Celestial, Infernal"
_skills_: "Perception +7"
_senses_: "darkvision 200 ft., passive Perception 17"
_saving_throws_: "Dex +6, Wis+7, Cha +8"
_damage_immunities_: "fire, radiant"
_damage_resistances_: "piercing"
_speed_: "30 ft., fly 60 ft."
_hit points_: "88 (16d8 + 16)"
_armor class_: "16 (natural armor)"
_stats_: | 18 (+4) | 16 (+3) | 12 (+1) | 12 (+1) | 18 (+4) | 20 (+5) |

___Redemption.___ Any caster brave enough to cast a knock spell on a chained angel can remove the creature's shackles, but this always exposes the caster to a blast of unholy flame as a reaction. The caster takes 16 (3d10) fire damage and 16 (3d10) radiant damage, or half as much with a successful DC 16 Dexterity saving throw. If the caster survives, the angel makes an immediate DC 20 Wisdom saving throw; if it succeeds, the angel's chains fall away and it is restored to its senses and to a Good alignment. If the saving throw fails, any further attempts to cast knock on the angel's chains fail automatically for one week.

**Actions**

___Multiattack.___ The chained angel makes two fiery greatsword attacks.

___Fiery Greatsword.___ Melee Weapon Attack. +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage plus 16 (3d10) fire damage.

___Fallen Glory (Recharge 5-6).___ All creatures within 50 feet of the chained angel and in its line of sight take 19 (3d12) radiant damage and are knocked prone, or take half damage and aren't knocked prone with a successful DC 15 Strength saving throw.

**Reactions**

___Fiendish Cunning.___ When a creature within 60 feet casts a divine spell, the chained angel can counter the spell if it succeeds on a Charisma check against a DC of 10 + the spell's level.

