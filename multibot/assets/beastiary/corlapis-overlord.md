"Corlapis Overlord";;;_size_: Medium elemental
_alignment_: lawful neutral
_challenge_: "7 (2,700 XP)"
_languages_: "Terran"
_skills_: "Stealth +4"
_senses_: "darkvision 90ft., tremorsense 30 ft., passive Perception 10"
_saving_throws_: "Str +7, Con +7, Cha +4"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "136 (16d8 + 64)"
_armor class_: "17 (natural armor)"
_stats_: | 19 (+4) | 12 (+1) | 19 (+4) | 18 (+4) | 10 (+0) | 13 (+1) |

___Hardened Exterior.___ Bludgeoning, piercing, and
slashing damage the overlord takes from nonmagical
weapons is reduced by 3 to a minimum of 1.

___Innate Spellcasting.___ The overlord's innate spellcasting
ability is Intelligence (spell save DC 13). It can
innately cast the following spells, requiring no
material components:

* At will: _light, shape stone_

* 3/day each: _transmute rock, wall of stone_

* 1/day each: _bones of the earth_

___Stone Camouflage.___ The overlord has advantage on
Dexterity (Stealth) checks to hide in rocky terrain.

___Sturdy.___ The overlord has advantage on saving throws
against effects that would cause it to move or be
knocked prone.

**Actions**

___Multiattack.___ The overlord makes two attacks with its
stone gauntlet or with its stone spike.

___Stone Gauntlet.___ Melee Weapon Attack: +7 to hit, reach
5 ft., one target. Hit: 13 (2d8 + 4) bludgeoning
damage.

___Stone Spike.___ Ranged Weapon Attack: +7 to hit, range
30/90 ft., one target. Hit: 11 (2d6 + 4) piercing
damage.

___Rupturing Wave (Recharge 5-6).___ The overlord slams its
gauntleted first to the ground and causes a 10-foot
wide, 60-foot long line of jagged stone erupts from the
ground. Each creature in the line must make a DC 14
Dexterity saving throw, taking 22 (4d8) bludgeoning
damage and 22 (4d8) piercing damage on a failed save,
or half as much damage on a successful one. The area
in this line is considered difficult terrain until the
rubble is cleared.

___From the Rubble (1/Day).___ The overlord uses spare stone
and rubble in the area to restore health to its allies. Up
to four other corlapis the overlord can see regain 22
(4d8 + 4) hit points.

**Legendary** Actions

The overlord can take 2 legendary actions, choosing
from the options below. Only one legendary action can
be used at a time and only at the end of another
creature’s turn. The overlord regains spent legendary
actions at the start of its turn.

___Punch.___ The overlord makes a stone gauntlet attack.

___Stone Cover.___ The overlord summons a 5-foot wide, 5-
foot tall, 3-inch thick wall of stone in an unoccupied
space within 10 feet. This wall has AC 15 and 30 hit
points and persists until it is destroyed.

___Giant Boulder (3 actions).___ The overlord summons a 15-
foot diameter boulder 30 feet in the air at a point it can
see within 120 feet. The boulder immediately falls to
the ground and any creature in the area of impact must
succeed on a DC 14 Dexterity saving throw or take 36
(8d8) bludgeoning damage and become restrained,
pinned beneath the boulder. The restrained creature
can use an action to make a DC 14 Strength or
Dexterity saving throw (creature's choice). On a
success, the creature is no longer restrained. A creature
that passed the initial saving throw moves to a location
adjacent to the boulder and takes no damage.
