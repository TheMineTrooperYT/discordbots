"Boloti";;;_size_: Tiny fey
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Primordial, Sylvan"
_skills_: "Perception +3, Stealth +7"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "20 ft., swim 60 ft."
_hit points_: "63 (14d4 + 28)"
_armor class_: "15"
_stats_: | 12 (+1) | 20 (+5) | 14 (+2) | 13 (+1) | 12 (+1) | 11 (+0) |

___Amphibious.___ The boloti can breathe air and water.

___Innate Spellcasting.___ The boloti's innate spellcasting ability is Intelligence (spell save DC 11). It can innately cast the following spells, requiring no material components:

* At will: _detect magic, water walk_

* 3/day: _control water, create or destroy water, fog cloud, invisibility, see invisibility, water breathing_

* 1/day: _wall of ice_

___Water Mastery.___ A boloti has advantage on attack rolls if both it and its opponent are in water. If the opponent and the boloti are both on dry ground, the boloti has disadvantage on attack rolls.

**Actions**

___Dagger.___ Melee Weapon Attack: +7 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 7 (1d4 + 5) piercing damage.

___Vortex (1/Day).___ A boloti can transform into a vortex of swirling, churning water for up to 4 minutes. This ability can be used only while the boloti is underwater, and the boloti can't leave the water while in vortex form. While in vortex form, the boloti can enter another creature's space and stop there in vortex form. In this liquid form, the boloti still takes normal damage from weapons and magic. A creature in the same space as the boloti at the start of the creature's turn takes 9 (2d8) bludgeoning damage unless it makes a successful DC 15 Dexterity saving throw. If the creature is Medium or smaller, a failed saving throw also means it is grappled (escape DC 11). Until this grapple ends, the target is restrained and unable to breathe unless it can breathe water. If the saving throw succeeds, the target is pushed 5 feet so it is out of the boloti's space.

