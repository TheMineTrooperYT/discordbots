"Nagpa";;;_page_number_: 215
_size_: Medium humanoid (nagpa)
_alignment_: neutral evil
_challenge_: "17 (18,000 XP)"
_languages_: "Common plus up to five other languages"
_senses_: "truesight 120 ft., passive Perception 20"
_skills_: "Arcana +12, Deception +11, History +12, Insight +10, Perception +10"
_saving_throws_: "Int +12, Wis +10, Cha +11"
_speed_: "30 ft."
_hit points_: "187  (34d8 + 34)"
_armor class_: "19 (natural armor)"
_stats_: | 9 (0) | 15 (+2) | 12 (+1) | 23 (+6) | 18 (+4) | 21 (+5) |

___Spellcasting.___ The nagpa is a 15th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 20, +12 to hit with spell attacks). A nagpa has the following wizard spells prepared:

* Cantrips (at will): _chill touch, fire bolt, mage hand, message, minor illusion_

* 1st level (4 slots): _charm person, detect magic, protection from evil and good, witch bolt_

* 2nd level (3 slots): _hold person, ray of enfeeblement, suggestion_

* 3rd level (3 slots): _counterspell, fireball, fly_

* 4th level (3 slots): _confusion, hallucinatory terrain, wall of fire_

* 5th level (2 slots): _dominate person, dream, geas_

* 6th level (1 slot): _circle of death, disintegrate_

* 7th level (1 slot): _etherealness, prismatic spray_

* 8th level (1 slot): _feeblemind_

**Actions**

___Staff___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) bludgeoning damage.

**Bonus** Actions

___Corruption.___ As a bonus action, the nagpa targets one creature it can see within 90 feet of it. The target must make a DC 20 Charisma saving throw. An evil creature makes the save with disadvantage. On a failed save, the target is charmed by the nagpa until the start of the nagpa's next turn. On a successful save, the target becomes immune to the nagpa's Corruption for the next 24 hours.

___Paralysis (Recharge 6).___ As a bonus action, the nagpa forces each creature within 30 feet of it to succeed on a DC 20 Wisdom saving throw or be paralyzed for 1 minute. A paralyzed target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success. Undead and constructs are immune to this effect.
