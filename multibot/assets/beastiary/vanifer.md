"Vanifer";;;_size_: Medium humanoid (tiefling)
_alignment_: neutral evil
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Ignan, Infernal"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +5, Deception +8, Performance +8"
_damage_immunities_: "fire"
_speed_: "30 ft."
_hit points_: "112 (15d8+45)"
_armor class_: "15 (studded leather)"
_stats_: | 11 (0) | 16 (+3) | 17 (+3) | 12 (+1) | 13 (+1) | 19 (+4) |

___Funeral Pyre.___ When Vanifer drops to 0 hit points, her body is consumed in a flash of fire and smoke. Anything she was wearing or carrying is left behind among ashes.

___Legendary Resistance (2/Day).___ If Vanifer fails a saving throw, she can choose to succeed instead.

___Spellcasting.___ Vanifer is a 10th-level spellcaster. Her spellcasting ability is Charisma (spell save DC 16, +8 to hit with spell attacks). Vanifer knows the following sorcerer spells:

* Cantrips (at will): _chill touch, fire bolt, friends, mage hand, message, produce flame, thaumaturgy_

* 1st level (4 slots): _burning hands, chromatic orb, hellish rebuke, shield_

* 2nd level (3 slots): _darkness, detect thoughts, misty step, scorching ray_

* 3rd level (3 slots): _counterspell, fireball, hypnotic pattern_

* 4th level (3 slots): _wall of fire_

* 5th level (2 slots): _dominate person_

**Actions**

___Multiattack.___ Vanifer makes two attacks.

___Tinderstrike.___ Melee or Ranged Weapon Attack: +9 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 7 (1d4 + 5) piercing damage plus 7 (2d6) fire damage.
