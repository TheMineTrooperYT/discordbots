"Shrieker";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "blindsight 30 ft. (blind beyond this radius)"
_speed_: "0 ft."
_hit points_: "13 (3d8)"
_armor class_: "5"
_condition_immunities_: "blinded, deafened, frightened"
_stats_: | 1 (-5) | 1 (-5) | 10 (0) | 1 (-5) | 3 (-4) | 1 (-5) |

___False Appearance.___ While the shrieker remains motionless, it is indistinguishable from an ordinary fungus.

**Actions**

___Shriek.___ When bright light or a creature is within 30 feet of the shrieker, it emits a shriek audible within 300 feet of it. The shrieker continues to shriek until the disturbance moves out of range and for 1d4 of the shrieker's turns afterward