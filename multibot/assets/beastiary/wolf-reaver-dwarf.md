"Wolf Reaver Dwarf";;;_size_: Medium Humanoid
_alignment_: any chaotic
_challenge_: "3 (700 XP)"
_languages_: "Common, Dwarvish"
_skills_: "Athletics +6, Intimidation +1"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "35 ft."
_hit points_: "76(9d8+36)"
_armor class_: "16 (chain shirt, shield)"
_stats_: | 18 (+4) | 12 (+1) | 19 (+4) | 9 (-1) | 11 (+0) | 9 (-1) |

___Danger Sense.___ The wolf reaver dwarf has advantage on Dexterity saving throws against attacks it can see when it is not blinded, deafened, or incapacitated.

___Dwarven Resistance.___ The wolf reaver dwarf has advantage on saving throws against poison.

___Pack Tactics.___ The wolf reaver dwarf has advantage on attacks if at least one of the dwarf's allies is within 5 feet of the target and the ally isn't incapacitated.

___Reckless.___ At the start of its turn, the wolf reaver dwarf can gain advantage on all melee weapon attack rolls during that turn, but attack rolls against it have advantage until the start of its next turn.

**Actions**

___Multiattack.___ The wolf reaver dwarf makes two melee or ranged attacks.

___Battleaxe.___ Melee Weapon Attack: +6 to hit, reach 5 ft, one target. Hit: 8 (1d8 + 4) slashing damage.

___Dagger.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 6 (1d4 + 4) piercing damage.

___Spear.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 7 (1d6 + 4) piercing damage, or 8 (1d8 + 4) piercing damage if used with two hands to make a melee attack.

