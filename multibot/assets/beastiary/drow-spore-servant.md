"Drow Spore Servant";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "1/8 (25 XP)"
_languages_: "-"
_senses_: "blindsight 30 ft. (blind beyond this radius)"
_speed_: "20 ft."
_hit points_: "13 (3d8)"
_armor class_: "15 (chain shirt)"
_condition_immunities_: "blinded, charmed, frightened, paralyzed"
_stats_: | 10 (0) | 14 (+2) | 10 (0) | 2 (-4) | 6 (-2) | 1 (-5) |

**Actions**

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.