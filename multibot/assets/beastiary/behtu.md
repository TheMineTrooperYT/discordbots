"Behtu";;;_size_: Small humanoid
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Behtu, Common, Infernal"
_skills_: "Athletics +5, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Dex +5"
_damage_resistances_: "cold, fire, lightning"
_speed_: "20 ft., climb 20 ft."
_hit points_: "52 (8d6 + 24)"
_armor class_: "14 (hide armor)"
_stats_: | 17 (+3) | 16 (+3) | 16 (+3) | 12 (+1) | 11 (+0) | 7 (-2) |

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Shortspear.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d4 + 3) piercing damage.

___Fire Breath (Recharge 6).___ The behtu exhales fire in a 15-foot cone. Each creature in that area takes 21 (5d8) fire damage, or half damage with a successful DC 13 Dexterity saving throw.

___Ichorous Infusions.___ Behtu war parties carry 1d6 vials of ichorous infusions. They often ingest an infusion before an ambush. For the next 2d6 rounds, the behtus gain a +4 bonus to their Strength and Constitution scores and quadruple their base speed (including their climb speed). Behtus also take a -4 penalty to their Intelligence and Wisdom scores for the duration of the infusion. A non-behtu character who ingests a behtu infusion becomes poisoned and takes 10 (3d6) poison damage; a successful DC 14 Constitution saving throw against poison reduces damage to half and negates the poisoned condition.

