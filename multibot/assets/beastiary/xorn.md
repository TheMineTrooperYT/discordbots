"Xorn";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Terran"
_senses_: "darkvision 60 ft., tremorsense 60 ft."
_skills_: "Perception +6, Stealth +3"
_speed_: "20 ft., burrow 20 ft."
_hit points_: "73 (7d8+42)"
_armor class_: "19 (natural armor)"
_damage_resistances_: "piercing and slashing from nonmagical weapons that aren't adamantine"
_stats_: | 17 (+3) | 10 (0) | 22 (+6) | 11 (0) | 10 (0) | 11 (0) |

___Earth Glide.___ The xorn can burrow through nonmagical, unworked earth and stone. While doing so, the xorn doesn't disturb the material it moves through.

___Stone Camouflage.___ The xorn has advantage on Dexterity (Stealth) checks made to hide in rocky terrain.

___Treasure Sense.___ The xorn can pinpoint, by scent, the location of precious metals and stones, such as coins and gems, within 60 ft. of it.

**Actions**

___Multiattack.___ The xorn makes three claw attacks and one bite attack.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13 (3d6 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.