"Cambion";;;_size_: Medium fiend
_alignment_: any evil alignment
_challenge_: "5 (1,800 XP)"
_languages_: "Abyssal, Common, Infernal"
_senses_: "darkvision 60 ft."
_skills_: "Deception +6, Intimidation +6, Perception +4, Stealth +7"
_saving_throws_: "Str +7, Con +6, Int +5, Cha +6"
_speed_: "30 ft., fly 60 ft."
_hit points_: "82 (11d8+33)"
_armor class_: "19 (scale mail)"
_damage_resistances_: "cold, fire, lightning, poison, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 18 (+4) | 18 (+4) | 16 (+3) | 14 (+2) | 12 (+1) | 16 (+3) |

___Fiendish Blessing.___ The AC of the cambion includes its Charisma bonus.

___Innate Spellcasting.___ The cambion's spellcasting ability is Charisma (spell save DC 14). The cambion can innately cast the following spells, requiring no material components:

* 3/day each: _alter self, command, detect magic_

* 1/day: _plane shift _(self only)

**Actions**

___Multiattack.___ The cambion makes two melee attacks or uses its Fire Ray twice.

___Spear.___ Melee or Ranged Weapon Attack: +7 to hit, reach 5 ft. or range 20/60 ft ., one target. Hit: 7 (1d6 + 4) piercing damage, or 8 (1d8 + 4) piercing damage if used with two hands to make a melee attack, plus 3 (1d6) fire damage.

___Fire Ray.___ Ranged Spell Attack: +7 to hit, range 120 ft., one target. Hit: 10 (3d6) fire damage.

___Fiendish Charm.___ One humanoid the cambion can see within 30 ft. of it must succeed on a DC 14 Wisdom saving throw or be magically charmed for 1 day. The charmed target obeys the cambion's spoken commands. If the target suffers any harm from the cambion or another creature or receives a suicidal command from the cambion, the target can repeat the saving throw, ending the effect on itself on a success. If a target's saving throw is successful, or if the effect ends for it, the creature is immune to the cambion's Fiendish Charm for the next 24 hours.
