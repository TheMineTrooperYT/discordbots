"Bastet Temple Cat";;;_size_: Small monstrosity
_alignment_: chaotic neutral
_challenge_: "1 (200 XP)"
_languages_: "Common, Nurian, and Sylvan"
_skills_: "Perception +5, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 15"
_speed_: "40 ft., climb 30 ft."
_hit points_: "40 (9d6 + 9)"
_armor class_: "14"
_stats_: | 8 (-1) | 19 (+4) | 12 (+1) | 12 (+1) | 16 (+3) | 18 (+4) |

___Keen Smell.___ The temple cat has advantage on Wisdom (Perception) checks that rely on smell.

___Innate Spellcasting.___ The temple cat's innate spellcasting ability is Charisma (spell save DC 14, +6 to hit with spell attacks). The temple cat can innately cast the following spells, requiring no material components:

* At will: _guidance_

* 3/day each: _charm person, cure wounds_

* 1/day: _enhance ability_ (only Cat's Grace)

___Priestly Purr.___ When a cleric or paladin who worships Bastet spends an hour preparing spells while a Bastet temple cat is within 5 feet, that spellcaster can choose two 1st-level spells and one 2nd-level spell that they are able to cast and imbue them into the temple cat. The temple cat can cast these spells 1/day each without a verbal component. These spells are cast as if they were included in the temple cat's Innate Spellcasting trait.

**Actions**

___Multiattack.___ The temple cat makes one bite attack and one claws attack.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one creature. Hit: 6 (1d4 + 4) piercing damage.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (2d4 + 4) slashing damage.

___Fascinating Lure.___ The temple cat purrs loudly, targeting a humanoid it can see within 30 feet that can hear the temple cat. The target must succeed on a DC 14 Wisdom saving throw or be charmed. While charmed by the temple cat, the target must move toward the cat at normal speed and try to pet it or pick it up. A charmed target repeats the saving throw at the end of each of its turns, ending the effect on itself on a success. If a creature's saving throw is successful, the creature is immune to the temple cat's Fascinating Lure for the next 24 hours. The temple cat has advantage on attack rolls against any creature petting or holding it.

