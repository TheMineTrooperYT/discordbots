"Fellforged";;;_size_: Medium construct
_alignment_: lawful evil
_challenge_: "5 (1800 XP)"
_languages_: "any languages it knew in life"
_senses_: "darkvision 60 ft., passive Perception 12"
_saving_throws_: "Str +8"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "acid, cold, fire, lightning"
_condition_immunities_: "charmed, exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "135 (18d8 + 54)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+2) | 12 (+1) | 17 (+3) | 12 (+1) | 14 (+2) | 15 (+2) |

___Expelled Spirit.___ While the fellforged body was made to bind spirits, the wraith within is vulnerable to turning attempts. Any successful turn attempt exorcises the wraith from its clockwork frame. The expelled wraith retains its current hp total and fights normally. The construct dies without an animating spirit.

___Sunlight Sensitivity.___ While in sunlight, the fellforged has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Unnatural Aura.___ All animals, whether wild or domesticated, can sense the unnatural presence of fellforged at a distance of 30 feet. They do not willingly approach nearer than that and panic if forced to do so, and they remain panicked as long as they are within that range.

**Actions**

___Multiattack.___ The fellforged makes two necrotic slam attacks.

___Necrotic Slam.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 11 (2d8 + 2) bludgeoning damage plus 4 (1d8) necrotic damage, and the target must succeed on a DC 14 Constitution saving throw or its hit point maximum is reduced by an amount equal to the total damage taken. This reduction lasts until the target finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.

___Violent Escapement.___ With little regard for the clockwork bodies they inhabit, fellforged wraiths can stress and strain their mechanisms in such a violent manner that flywheels become unbalanced, gears shatter, and springs snap. As a bonus action, this violent burst of gears and pulleys deals 7 (2d6) piercing damage to all foes within 5 feet who fail a DC 14 Dexterity saving throw. Each use of this ability imposes a cumulative reduction in movement of 5 feet upon the fellforged. If its speed is reduced to 0 feet, the fellforged becomes paralyzed.

