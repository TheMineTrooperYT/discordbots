"Aeorian Reverser";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "8 (3,900 XP)"
_languages_: "understands Draconic but can’t speak"
_skills_: "Perception +5, Stealth +6, Survival +5"
_senses_: "darkvision 120 ft., passive Perception 15"
_saving_throws_: "Wis +5, Cha +2"
_damage_immunities_: "radiant, necrotic"
_speed_: "40 ft., climb 40 ft."
_hit points_: "133 (14d10 + 56)"
_armor class_: "15 (natural armor)"
_stats_: | 21 (+5) | 16 (+3) | 18 (+4) | 6 (-2) | 14 (+2) | 8 (-1) |

___Magic Resistance.___ The reverser has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The reverser makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +8 to hit, range 5 ft., one creature. Hit: 11 (1d12 + 5) piercing damage plus 6 (1d12) force damage.

___Claws.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 8 (1d6 + 5) slashing damage plus 7 (2d6) force damage.

**Reactions**

___Reversal.___ When a creature the reverser can see within 30 feet of it regains hit points, the reverser reduces the number of hit points regained to 0, and the reverser deals 13 (3d8) force damage to the creature.
