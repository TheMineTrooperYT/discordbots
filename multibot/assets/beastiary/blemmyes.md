"Blemmyes";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "8 (3900 XP)"
_languages_: "Giant"
_skills_: "Intimidation +3"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "40 ft."
_hit points_: "168 (16d10 + 80)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 13 (+1) | 20 (+5) | 7 (-2) | 12 (+1) | 5 (-3) |

___Carnivorous Compulsion.___ If it can see an incapacitated creature, the blemmyes must succeed on a DC 11 Wisdom save or be compelled to move toward that creature and attack it.

**Actions**

___Multiattack.___ The blemmyes makes two slam attacks and one bite attack.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 19 (4d6 + 5) piercing damage. If the target is a Medium or smaller incapacitated creature, that creature is swallowed. While swallowed, the creature is blinded and restrained, it has total cover against attacks and other effects from outside the blemmyes, and it takes 14 (4d6) acid damage at the start of each of the blemmyes' turns. If the blemmyes takes 20 damage or more during a single turn from a creature inside it, the blemmyes must succeed on a DC 16 Constitution saving throw at the end of that turn or regurgitate the swallowed creature, which falls prone in a space within 5 feet of the blemmyes. The blemmyes can have only one target swallowed at a time. If the blemmyes dies, a swallowed creature is no longer restrained by it and can escape from the corpse using 5 feet of movement, exiting prone.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) bludgeoning damage. If the target is a creature, it must succeed on a DC 16 Wisdom saving throw or be stunned until the end of its next turn.

___Rock.___ Ranged Weapon Attack: +8 to hit, range 30/120 ft., one target. Hit: 27 (4d10 + 5) bludgeoning damage. If the target is a creature, it must succeed on a DC 16 Wisdom saving throw or be frightened until the end of its next turn.

