"Sunbird Phoenix";;;_size_: Gargantuan monstrosity
_alignment_: unaligned
_challenge_: "13 (10,000 XP)"
_languages_: "Giant Owl, understands Common but can’t speak it"
_skills_: "Perception +7"
_senses_: "darkvision 120 ft., passive Perception 17"
_saving_throws_: "Dex +5, Con +10, Wis +7, Cha +7"
_damage_immunities_: "fire"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_speed_: "20 ft., fly 120 ft."
_hit points_: "279 (18d20 + 90)"
_armor class_: "17 (natural armor)"
_stats_: | 28 (+9) | 10 (+0) | 20 (+5) | 13 (+1) | 14 (+2) | 15 (+2) |

___Death Throes.___ When the sunbird dies, it explodes, and each creature
within 30 feet of it must make a DC 18 Dexterity saving throw, taking
42 (12d6) fire damage on a failed save, or half as much damage on a
successful one. The explosion ignites flammable objects in that area
that aren’t being worn or carried. The sunbird’s body turns to ash,
but an egg is left where the sunbird was.

___Fire Aura.___ At the start of each of the sunbird’s turns, each creature
within 5 feet of it takes 11 (2d10) fire damage, and flammable objects
in the area that aren’t being worn or carried ignite. A creature that
touches the sunbird or hits it with a melee attack while within 5 feet
of it takes 11 (2d10) fire damage. The aura also sheds bright light in a
60-foot radius and dim light for an additional 60 feet.

___Flyby.___ The sunbird doesn’t provoke opportunity attacks when it flies
out of an enemy’s reach.

___Keen Hearing and Sight.___ The sunbird has advantage on Wisdom
(Perception) checks that rely on hearing or sight.

**Actions**

___Multiattack.___ The sunbird makes two talon attacks.

___Talon.___ Melee Weapon Attack: +14 to hit, reach 5 ft., one target. Hit:
16 (2d6 + 9) slashing damage plus 14 (4d6) fire damage, and the
target is grappled (escape DC 18). Until this grapple ends, the target
is restrained, and the sunbird can’t attack another target with that
talon. A grappled creature takes 11 (2d10) fire damage at the start of
each of the sunbird’s turns.
