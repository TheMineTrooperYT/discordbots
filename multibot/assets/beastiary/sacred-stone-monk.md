"Sacred Stone Monk";;;_size_: Medium humanoid (human)
_alignment_: lawful evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_senses_: "tremorsense 10 ft."
_skills_: "Acrobatics +4, Athletics +3, Perception +4"
_speed_: "40 ft."
_hit points_: "22 (4d8+4)"
_armor class_: "14"
_stats_: | 13 (+1) | 15 (+2) | 12 (+1) | 10 (0) | 14 (+2) | 9 (-1) |

___Unarmored Defense.___ While the monk is wearing no armor and wielding no shield, its AC includes its Wisdom modifier.

___Unarmored Movement.___ While the monk is wearing no armor and wielding no shield, its walking speed increases by 10 feet (included in its speed).

**Actions**

___Multiattack.___ The monk makes two melee attacks.

___Unarmed Strike.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) bludgeoning damage.

**Reactions**

___Parry.___ The monk adds 2 to its AC against one melee or ranged weapon attack that would hit it. To do so, the monk must see the attacker.