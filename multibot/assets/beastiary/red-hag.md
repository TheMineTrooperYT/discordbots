"Red Hag";;;_size_: Medium fey
_alignment_: neutral evil
_challenge_: "7 (2900 XP)"
_languages_: "Common, Druidic, Giant"
_skills_: "Arcana +9, Deception +5, Insight +7, Perception +9"
_senses_: "blood sense 90 ft., darkvision 60 ft., passive Perception 16"
_condition_immunities_: "charmed, poisoned"
_speed_: "30 ft., swim 30 ft."
_hit points_: "119 (14d8 + 56)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 16 (+3) | 18 (+4) | 18 (+4) | 22 (+6) | 15 (+2) |

___Amphibious.___ The hag can breathe air and water.

___Spellcasting.___ The hag is an 8th-level spellcaster. Her spellcasting ability is Wisdom (Spell save DC 17, +9 to hit with spell attacks). She requires no material components to cast her spells. The hag has the following druid spells prepared:

Cantrips (at will): animal friendship (red hags treat this as a cantrip), poison spray, thorn whip

1st level (4 slots): cure wounds, entangle, speak with animals

2nd level (3 slots): barkskin, flame blade, lesser restoration

3rd level (3 slots): call lightning, conjure animals, dispel magic, meld into stone

4th level (2 slots): control water, dominate beast, freedom of movement, hallucinatory terrain

___Magic Resistance.___ The hag has advantage on saving throws against spells and other magical effects.

___Blood Sense.___ A red hag automatically senses the presence of the blood of living creatures within 90 feet and can pinpoint their locations within 30 feet.

**Actions**

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 17 (3d8 + 4) slashing damage.

___Siphoning Aura (Recharge 5-6).___ The red hag radiates an aura in a 30-foot radius, lasting for 3 rounds, that draws all fluids out through a creature's mouth, nose, eyes, ears, and pores. Every creature of the hag's choosing that starts its turn in the affected area takes 18 (4d6 + 4) necrotic damage, or half damage with a successful DC 15 Constitution saving throw.

