"Blooming Badger";;;_size_: Tiny beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_senses_: "darkvision 30ft., passive Perception 11"
_speed_: "20 ft., burrow 5 ft."
_hit points_: "7 (2d4 + 2)"
_armor class_: "11"
_stats_: | 4 (-3) | 12 (+1) | 12 (+1) | 3 (-4) | 13 (+1) | 6 (-2) |

___Floral Camouflage.___ While the badger remains
motionless, it is indistinguishable from a very large
flower.

**Actions**

___Bite.___ Melee Weapon Attack: +2 to hit, reach 5 ft.,
one target. Hit: 3 (1d4 + 1) piercing damage.

___Petal Burst (1/Day).___ The badger arches its body and
fires a burst of razor sharp petals at a creature
within 20 feet. That creature must make a DC 10
Dexterity saving throw, taking 10 (4d4) slashing
damage on a failed save, or half as much damage on
a successful one.
