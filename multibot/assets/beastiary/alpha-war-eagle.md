"Alpha War Eagle";;;_size_: Large beast
_alignment_: neutral good
_challenge_: "9 (5,000 XP)"
_languages_: "understands Common and Auran but can’t speak them"
_skills_: "Athletics +8, Perception +11"
_senses_: "passive Perception 21"
_saving_throws_: "Dex +9, Wis +7"
_speed_: "20 ft., fly 80 ft."
_hit points_: "120 (16d10 + 32)"
_armor class_: "15"
_stats_: | 18 (+4) | 21 (+5) | 15 (+2) | 8 (-1) | 17 (+3) | 12 (+1) |

___Keen Sight.___ The eagle has advantage on Wisdom
(Perception) checks that rely on sight.

___Flyby.___ The eagle doesn't provoke an opportunity
attack when it flies out of an enemy's reach.

___Master of Winds.___ The eagle has perfect control of the air
around its body. At the beginning of each of its turns
the eagle can choose to gain one of the following
benefits until its next turn:
* Ranged attacks against the eagle are made with
disadvantage.
* The area in a 20 foot radius sphere centered on the
eagle is considered difficult terrain for other
creatures.
* All creatures that start their turn within 20 feet of
the eagle or enters that area for the first time on a
turn must succeed on a DC 15 Strength saving
throw or be pushed 15 feet directly away from the
eagle and knocked prone.

**Actions**

___Multiattack.___ The eagle makes three attacks: one with its
beak and two with its talons.

___Beak.___ Melee Weapon Attack: +9 to hit, reach 5ft., one
target. Hit: 12 (2d6 + 5) piercing damage.

___Talon.___ Melee Weapon Attack: +9 to hit, reach 5ft., one
target. Hit: 16 (2d10 + 5) slashing damage.

___Enchanted Feather (Recharge 5-6).___ The eagle flaps its
wing and releases a cluster of feathers that seek out up
to three targets within 90 feet. The targets must make
a DC 16 Wisdom save, taking 27 (5d10) psychic
damage and becoming restrained until the end of their
next turn on a failed save, or half as much damage and
not restrained on a successful one.

___Create Tornado (1/Day).___ The eagle gives a powerful flap
of its wings that conjures up a 10-foot-radius, 100-
foot-high tornado at a point within 120 feet. This
tornado travels along a straight line, moving 30 feet a
turn, for 1 minute. The area within the tornado is
heavily obscured. The tornado sucks up any Medium or
smaller objects that aren’t secured to anything and that
aren’t worn or carried by anyone.

A creature must make a DC 16 Dexterity saving throw
the first time on a turn that it enters the tornado or that
the tornado enters its space, including when the
tornado first appears. A creature takes 21 (6d6)
bludgeoning damage on a failed save, or half as much
damage on a successful one. In addition, a Large or
smaller creature that fails the save must succeed on a
DC 16 Strength saving throw or become restrained in
the tornado until it disperses. When a creature starts its
turn restrained within the tornado, the creature is
pulled 10 feet higher inside of it, unless a creature is at
the top.

A restrained creature moves with the whirlwind and
falls when the spell ends, unless a creature has some
means to stay aloft. A restrained creature can use an
action to make a DC 16 Strength or Dexterity check. If
successful, the creature is no longer restrained by the
tornado and is hurled 3d6 x 10 feet away from it in a
random direction.
