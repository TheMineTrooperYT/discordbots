"Blood Bush";;;_size_: Small plant
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_senses_: "blindsight 30 ft., passive Perception 11"
_damage_vulnerabilities_: "thunder"
_damage_immunities_: "lightning"
_damage_resistances_: "cold, fire"
_speed_: "0 ft."
_hit points_: "59 (7d6 + 35)"
_armor class_: "13 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 20 (+5) | 2 (-4) | 12 (+1) | 8 (-1) |

___Germinate.___ Creatures that are implanted with a blood bush seed begin to suffer
the effects of it rapidly germinating inside of them. Once implanted, the
seed quickly begins to grow and expand. For every 24 hours that elapse,
the target must repeat the DC 14 Constitution saving throw, reducing
its hit point maximum by 5 (1d10) on a failure. The germinating seed
is destroyed by the immune system on a success. The target dies if this
reduces its hit point maximum to 0. This reduction to the target’s hit point
maximum lasts until the blood bush seed is removed.

**Actions**

___Multiattack.___ The blood bush makes one attack with its flower darts and
two attacks with its tendrils.

___Tendril.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4
(1d4 + 2) slashing damage. If the target is a Medium or smaller creature,
it is grappled (escape DC 12) and restrained, and the blood bush cannot
grapple another target.

___Flower Dart.___ Ranged Weapon Attack: +4 to hit, reach 5 ft., one target.
Hit: 5 (1d6 + 2) piercing damage, and the target must succeed on a DC
14 Constitution saving throw or be implanted with a blood bush seed (see
Germinate).
