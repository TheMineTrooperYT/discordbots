"Small Air Elemental";;;_size_: Small elemental
_alignment_: neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Auran"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison, cold"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "0 ft., fly 50 ft. (hover)"
_hit points_: "19 (3d8 + 6)"
_armor class_: "13"
_stats_: | 12 (+1) | 16 (+3) | 14 (+2) | 6 (-2) | 10 (+0) | 6 (-2) |

___Air Form.___ The elemental can enter a hostile
creature's space and stop there. It can move
through a space as narrow as 1 inch wide without
squeezing.

**Actions**

___Slam.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 7 (1d8 + 3) bludgeoning damage.

___Whirlwind. (Recharge 6).___ Each creature in the
elemental's space must make a DC 11 Strength
saving throw. On a failure, a target takes 5 (1d8 +
1) bludgeoning damage and is flung up 10 feet
away from the elemental in a random direction and
knocked prone. If a thrown target strikes an object,
such as a·wall or floor, the target takes 3 (1d6)
bludgeoning damage. If the target is thrown at
another creature, that creature must succeed on a
DC 11 Dexterity saving throw or take the same
damage and be knocked prone.

If the saving throw is successful, the target takes
half the bludgeoning damage and isn't flung away
or knocked prone.
