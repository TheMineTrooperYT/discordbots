"Lorelei";;;_size_: Medium fey
_alignment_: chaotic evil
_challenge_: "5 (1800 XP)"
_languages_: "Common, Sylvan"
_skills_: "Deception +9, Performance +9, Persuasion +9"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Dex +8, Cha +9"
_speed_: "30 ft., swim 30 ft."
_hit points_: "76 (9d8 + 36)"
_armor class_: "15 (18 mage armor)"
_stats_: | 10 (+0) | 21 (+5) | 18 (+4) | 16 (+3) | 16 (+3) | 23 (+6) |

___Alluring Presence.___ All humanoids within 30 feet of a lorelei who look directly at her must succeed on a DC 17 Charisma saving throw or be drawn to her in the most direct path, regardless of the danger. This compulsion fades once the person gets within 5 feet of the lorelei. A creature can avoid this effect for one full round by choosing to avert its eyes at the start of its turn, but it then has disadvantage on any attacks or other rolls directed against the lorelei until the start of its next turn. A lorelei can suppress or resume this ability as a bonus action. Anyone who successfully saves against this effect cannot be affected by it from the same lorelei for 24 hours.

___Unearthly Grace.___ A lorelei applies her Charisma modifier to all of her saving throws in place of the normal ability modifier.

___Water Spirit.___ The lorelei is under the effect of freedom of movement whenever she is in contact with a body of water.

___Spellcasting.___ The lorelei is an 8th-level spellcaster. Her spellcasting ability is Charisma (spell save DC 17, +9 to hit with spell attacks). She requires no material components to cast her spells. The lorelei has the following sorcerer spells prepared:

Cantrips (at will): detect magic, guidance, light, mending, poison spray, prestidigitation

1st level (4 slots): comprehend languages, fog cloud, mage armor, ray of sickness

2nd level (3 slots): hold person, misty step, suggestion

3rdlevel (3 slots): hypnotic pattern, gaseous form, water walk

4th level (2 slots): dominate beast, ice storm

**Actions**

___Dagger.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 7 (1d4 + 5) piercing damage.

___Charm.___ The lorelei targets one humanoid she can see within 30 feet of her. If the target can see or hear the lorelei, it must succeed on a DC 17 Wisdom saving throw against this magic or be charmed by the lorelei. The charmed target regards the lorelei as its one, true love, to be heeded and protected. Although the target isn't under the lorelei's control, it takes the lorelei's requests or actions in the most favorable way it can. Each time the lorelei or her companions cause the target to take damage, directly or indirectly, it repeats the saving throw, ending the effect on itself on a success. Otherwise, the effect lasts 24 hours or until the lorelei is killed, is on a different plane of existence than the target, or takes a bonus action to end the effect.

___Stunning Glance.___ The lorelei mentally disrupts a creature within 30 feet with a look. The target must succeed on a DC 17 Wisdom saving throw or be stunned for 2 rounds. Anyone who successfully saves against this effect cannot be affected by it from the same lorelei for 24 hours.

