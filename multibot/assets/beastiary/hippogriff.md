"Hippogriff";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_skills_: "Perception +5"
_speed_: "40 ft, fly 60 ft."
_hit points_: "19 (3d10+3)"
_armor class_: "11"
_stats_: | 17 (+3) | 13 (+1) | 13 (+1) | 2 (-4) | 12 (+1) | 8 (-1) |

___Keen Sight.___ The hippogriff has advantage on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The hippogriff makes two attacks: one with its beak and one with its claws.

___Beak.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (1d10 + 3) piercing damage.

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) slashing damage.