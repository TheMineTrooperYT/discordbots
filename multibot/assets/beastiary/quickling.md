"Quickling";;;_size_: Tiny fey
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Sylvan"
_senses_: "darkvision 60 ft."
_skills_: "Acrobatics +8, Perception +5, Sleight of Hand +8, Stealth +8"
_speed_: "120 ft."
_hit points_: "10 (3d4+3)"
_armor class_: "16"
_stats_: | 4 (-3) | 23 (+6) | 13 (+1) | 10 (0) | 12 (+1) | 7 (-2) |

___Blurred Movement.___ Attack rolls against the quickling have disadvantage unless the quickling is incapacitated or restrained.

___Evasion.___ If the quickling is subjected to an effect that allows it to make a Dexterity saving throw to take only half damage, it instead takes no damage if it succeeds on the saving throw, and only half damage if it fails.

**Actions**

___Multiattack.___ The quickling makes three dagger attacks.

___Dagger.___ Melee or Ranged Weapon Attack: +8 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 8 (1d4+6) piercing damage.