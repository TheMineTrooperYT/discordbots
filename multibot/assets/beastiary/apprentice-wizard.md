"Apprentice Wizard";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1/4 (50 XP)"
_languages_: "any one language (usually Common)"
_skills_: "Arcana +4, History +4"
_speed_: "30 ft."
_hit points_: "9 (2d8)"
_armor class_: "10"
_stats_: | 10 (0) | 10 (0) | 10 (0) | 14 (+2) | 10 (0) | 11 (0) |

___Spellcasting.___ The apprentice is a 1st-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 12, +4 to hit with spell attacks). It has the following wizard spells prepared:

* Cantrips (at will): _fire bolt, mending, prestidigitation_

* 1st level (2 slots): _burning hands, disguise self, shield_

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +2 to hit, reach 5 ft. or range 20/60 ft, one target. Hit: 2 (1d4) piercing damage.
