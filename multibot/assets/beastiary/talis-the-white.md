"Talis the White";;;_size_: Medium humanoid (half-elf)
_alignment_: lawful evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Draconic, Elvish, Infernal"
_senses_: "darkvision 60 ft."
_skills_: "Deception +6, Insight +6, Perception +6, Persuasion +6"
_saving_throws_: "Wis +6, Cha +6"
_speed_: "30 ft."
_hit points_: "58 (9d8+18)"
_armor class_: "18 (scale mail +1, shield)"
_stats_: | 14 (+2) | 12 (+1) | 14 (+2) | 10 (0) | 16 (+3) | 16 (+3) |

___Special Equipment.___ Talis has +1 scale mail and a wand of winter.

___Fey Ancestry.___ Talis has advantage on saving throws against being charmed, and magic can't put her to sleep.

___Spellcasting.___ Talis is a 9th-level spellcaster that uses Wisdom as her spellcasting ability (spell save DC 14, +6 to hit with spell attacks). Talis has the following spells prepared from the cleric spell list:

* Cantrips (at will): _guidance, resistance, thaumaturgy_

* 1st level (4 slots): _command, cure wounds, healing word, inflict wounds_

* 2nd level (3 slots): _blindness/deafness, lesser restoration, spiritual weapon _(spear)

* 3rd level (3 slots): _dispel magic, mass healing word, sending_

* 4th level (3 slots): _death ward, freedom of movement_

* 5th level (1 slot): _insect plague_

___Winter Strike (3/Day).___ Once per turn, when Talis hits with a melee attack, she can expend a use of this trait to deal an extra 9 (2d8) cold damage.

**Actions**

___Spear.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or ranged 20 ft./60 ft., one target. Hit: 6 (1d6 + 2) piercing damage.
