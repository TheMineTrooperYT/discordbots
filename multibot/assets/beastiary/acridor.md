Acridor;;;_size_: Medium humanoid
_alignment_: chaotic neutral
_challenge_: 1 (200 XP)
_languages_: Pheromones (between acridor only), Undercommon
_senses_: Darkvision 60 ft., passive Perception 13
_skills_: Athletics +3, Perception +3, Survival +3
_speed_: 40 ft.
_hit points_: 39 (6d8 + 12)
_armor class_: 14
_stats_: | 14 (+2) | 12 (+1) | 14 (+2) | 8 (-1) | 12 (+1) | 6 (-2) |

___Natural Jumpers.___ The acridor’s long jump is up to
30 feet and its high jump is up to 15 feet, with or
without a running start.

___Limited Flight (Recharge 4-6).___ Acridor are not
skilled fliers, and can only fly over short distances.
As a move action, an acridor can fly 60 ft.

**Actions**

___Multiattack.___ The acridor makes two attacks, one with its
bite and one with its claw or kpinga, or two kpinga attacks.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one
target. Hit: 5 (1d6 +2) piercing and 3 (1d6) acid damage.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one target. Hit: 4 (1d4 +2) slashing damage.

___Kpinga.___ Melee or Ranged Weapon Attack: +4 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d6 +2) piercing damage.
