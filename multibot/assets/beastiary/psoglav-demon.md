"Psoglav Demon";;;_size_: Large fiend
_alignment_: chaotic evil
_challenge_: "7 (2900 XP)"
_languages_: "Common, Infernal; telepathy 60 ft."
_skills_: "Acrobatics +9, Perception +6, Intimidation +7, Stealth +9"
_senses_: "blindsight 30 ft., darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +9, Con +8, Wis +7, Cha +7"
_damage_immunities_: "fire, poison"
_damage_resistances_: "cold, lightning"
_condition_immunities_: "poisoned"
_speed_: "40 ft., fly 60 ft."
_hit points_: "115 (11d10 + 55)"
_armor class_: "17 (natural armor)"
_stats_: | 21 (+5) | 23 (+6) | 20 (+5) | 16 (+3) | 19 (+4) | 18 (+4) |

___Innate Spellcasting.___ The psoglav's innate spellcasting ability is Charisma (spell save DC 15). It can innately cast the following spell, requiring no material components:

1/day: greater invisibility

___Magic Weapon.___ The psoglav's weapon attacks are magical.

___Shadow Door (4/Day).___ The psoglav has the ability to travel between shadows as if by means of a dimension door spell. The magical transport must begin and end in an area with at least some dim light. The shadow door can span a maximum of 90 feet.

**Actions**

___Multiattack.___ The psoglav demon makes three bite attacks.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 18 (2d12 + 5).

___Shadow Stealing Ray (Recharge 5-6).___ The psoglav emits a beam from its single eye. One target within 60 feet of the psoglav is hit automatically by the ray. The target is knocked 20 feet back and must succeed on a DC 15 Dexterity saving throw or be knocked prone. The target's shadow stays in the space the target was originally in, and acts as an undead shadow under the command of the psoglav demon. If the creature hit with the shadow stealing ray flees the encounter, it is without a natural shadow for 1d12 days before the undead shadow fades and the creature's natural shadow returns. The undead shadow steals the body of its creature of origin if that creature is killed during the encounter; in that case, the creature's alignment shifts to evil and it falls under the command of the psoglav. The original creature regains its natural shadow immediately if the undead shadow is slain. A creature can only have its shadow stolen by the shadow stealing ray once per day, even if hit by the rays of two different psoglav demons, but it can be knocked back by it every time it is hit.

