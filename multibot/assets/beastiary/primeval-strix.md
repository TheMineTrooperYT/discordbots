"Primeval Strix";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "Understands Common and Abyssal but can't speak"
_skills_: "Perception +5, History +7"
_senses_: "passive Perception 15"
_saving_throws_: "Dex +7, Int +7, Cha +8"
_speed_: "20 ft., fly 90 ft."
_hit points_: "105 (14d10 + 28)"
_armor class_: "14"
_stats_: | 15 (+2) | 18 (+4) | 15 (+2) | 18 (+4) | 14 (+2) | 20 (+5) |

___Horrifying Visage.___ When a creature that can see the
strix's eyes starts its turn within 30 feet of the strix, the
strix can force it to make a DC 15 Wisdom saving
throw if the strix isn’t incapacitated and can see the
creature. If the creature fails the saving throw, it
becomes frightened. The frightened creature can
repeat the saving throw at the end of its next turn,
ending the effect on a success and becoming immune
to the strix's horrifying visage for 24 hours.

Unless surprised, a creature can avert its eyes to avoid
the saving throw at the start of its turn. If the creature
does so, it can't see the strix until the start of its next
turn, when it can avert its eyes again. If a creature looks
at the strix in the meantime, it must immediately make
the save.

___Flyby.___ The strix doesn't provoke attacks of opportunity
when it flies out of an enemy's reach.

___Keen Sight.___ The strix has advantage on Wisdom
(Perception) checks that rely on sight.

___Omen of Ill Fortune.___ Gazing upon a strix is considered
to be a terrible omen that is sure to bring misfortune
and suffering. A creature that sees the strix is plagued
with bad luck. For the next hour, when that creature
makes a skill check or an attack roll a d4 and subtract
the result from the total.

**Actions**

___Multiattack.___ The strix makes two attacks: one with its
beak and one with its talons.

___Beak.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one
target. Hit: 8 (1d8 + 4) piercing damage plus 4 (1d8)
necrotic damage.

___Talons.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one
target. Hit: 13 (2d8 + 4) slashing damage plus 4 (1d8)
necrotic damage.

___Thunderous Screech (Recharge 5-6).___ The strix unleashes
a shrill screech in a 30 ft. cone. Each creature must
make a DC 15 Constitution saving throw, taking 28
(8d6) thunder damage and becoming deafened for 1
minute on a failed save, or half as much damage and
not deafened on a successful one.
