"Captain Othelstan";;;_size_: Medium humanoid (human)
_alignment_: lawful evil
_challenge_: "5 (1,800 XP)"
_languages_: "Common, Draconic, Giant"
_skills_: "Athletics +7, Intimidation +7, Perception +5, Religion +4"
_saving_throws_: "Str +7, Con +6"
_speed_: "30 ft."
_hit points_: "93 (11d10+33)"
_armor class_: "19 (splint, shield)"
_stats_: | 19 (+4) | 10 (0) | 16 (+3) | 13 (+1) | 14 (+2) | 12 (+1) |

___Action Surge (Recharges on a Short or Long Rest).___ On his turn, Othelstan can take one additional action.

___Tiamat's Blessing of Retribution.___ When Othelstan takes damage that reduces him to 0 hit points, he immediately regains 20 hit points. If he has 20 hit points or fewer at the end of his next turn, he dies.

**Actions**

___Multiattack.___ Othelstan attacks twice with his flail or spear, or makes two ranged attacks with his spears.

___Flail.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) bludgeoning damage.

___Spear.___ Melee or Ranged Weapon Attack: +7 to hit, reach 5 ft. or ranged 20 ft./60 ft., one target. Hit: 7 (1d6 + 4) piercing damage.