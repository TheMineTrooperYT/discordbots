"Molydeus";;;_page_number_: 134
_size_: Huge fiend (demon)
_alignment_: chaotic evil
_challenge_: "21 (33,000 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_senses_: "truesight 120 ft., passive Perception 31"
_skills_: "Perception +21"
_damage_immunities_: "poison"
_saving_throws_: "Str +16, Con +14, Wis +14, Cha +14"
_speed_: "40 ft."
_hit points_: "216  (16d12 + 112)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "blinded, charmed, deafened, frightened, poisoned, stunned"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 28 (+9) | 22 (+6) | 25 (+7) | 21 (+5) | 24 (+7) | 24 (+7) |

___Innate Spellcasting.___ The molydeus's innate spellcasting ability is Charisma (spell save DC 22). It can innately cast the following spells, requiring no material components:

* At will: _dispel magic, polymorph, telekinesis, teleport_

* 3/day: _lightning bolt_

* 1/day: _imprisonment_

___Legendary Resistance (3/Day).___ If the molydeus fails a saving throw, it can choose to succeed instead.

___Magic Resistance.___ The molydeus has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The molydeus's weapon attacks are magical.

**Actions**

___Multiattack___ The molydeus makes three attacks: one with its weapon, one with its wolf bite, and one with its snakebite.

___Demonic Weapon___ Melee Weapon Attack: +16 to hit, reach 15 ft., one target. Hit: 20 (2d10 + 9) slashing damage. If the target has at least one head and the molydeus rolled a 20 on the attack roll, the target is decapitated and dies if it can't survive without that head. A target is immune to this effect if it takes none of the damage, has legendary actions, or is Huge or larger. Such a creature takes an extra 6d8 slashing damage from the hit.

___Wolf Bite___ Melee Weapon Attack: +16 to hit, reach 10 ft., one target. Hit: 16 (2d6 + 9) piercing damage.

___Snakebite___ Melee Weapon Attack: +16 to hit, reach 15 ft., one creature. Hit: 12 (1d6 + 9) piercing damage, and the target must succeed on a DC 22 Constitution saving throw or its hit point maximum is reduced by an amount equal to the damage taken. This reduction lasts until the target finishes a long rest. The target transforms into a manes if this reduces its hit point maximum to 0. This transformation can be ended only by a wish spell.

**Legendary** Actions

The molydeus can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The molydeus regains spent legendary actions at the start of its turn.

___Attack___ The molydeus makes one attack, either with its demonic weapon or with its snakebite.

___Move___ The molydeus moves without provoking opportunity attacks.

___Cast a Spell___ The molydeus casts one spell from its Innate Spellcasting trait.
