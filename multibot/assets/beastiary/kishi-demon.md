"Kishi Demon";;;_size_: Medium fiend
_alignment_: chaotic evil
_challenge_: "8 (3900 XP)"
_languages_: "Celestial, Common, Draconic, Infernal, telepathy"
_skills_: "Deception +9, Perception +3, Performance +9"
_senses_: "darkvision 120 ft., passive Perception 13"
_saving_throws_: "Dex +8, Con +7, Wis +3"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning, poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "50 ft."
_hit points_: "119 (14d8 + 56)"
_armor class_: "18 (natural armor, shield)"
_stats_: | 19 (+4) | 20 (+5) | 19 (+4) | 15 (+2) | 11 (+0) | 22 (+6) |

___Two Heads.___ The demon has advantage on Wisdom (Perception) checks and on saving throws against being blinded, charmed, deafened, frightened, stunned, and knocked unconscious.

___Innate Spellcasting.___ The demon's spellcasting ability is Charisma (spell save DC 17). The demon can innately cast the following spells, requiring no material components: At will: detect evil and good, detect magic, suggestion

3/day: glibness

1/day: dominate person

___Magic Resistance.___ The demon has advantage on saving throws against spells and other magical effects.

___Trophy Shield.___ If the kishi demon killed an opponent this turn, as a bonus action, it takes part of the slain creature's essence along with a grisly trophy and mounts it upon its shield. For 24 hours, the Armor Class of the kishi demon becomes 20, and creatures of the same race as the slain creature have disadvantage on attack rolls against the kishi demon.

___Variant: Demon Summoning.___ Some kishi demons have an action option that allows them to summon other demons.

Summon Demon (1/Day): The kishi demon has a 35 percent chance of summoning one kishi demon

**Actions**

___Multiattack.___ The demon makes one bite attack and three spear attacks.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 12 (2d6 + 5) piercing damage.

___Spear.___ Melee or Ranged Weapon Attack: +7 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 7 (1d6 + 4) piercing damage, or 8 (1d8 + 4) piercing damage if used with two hands to make a melee attack.

