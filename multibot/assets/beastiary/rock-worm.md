"Rock Worm";;;_size_: Huge monstrosity
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_senses_: "darkvision 60ft., tremorsense 60 ft., passive Perception 10"
_speed_: "30 ft., burrow 30 ft."
_hit points_: "80 (7d12 + 35)"
_armor class_: "16 (natural armor)"
_stats_: | 20 (+5) | 10 (+0) | 20 (+5) | 3 (-4) | 10 (+0) | 1 (-5) |

**Actions**

___Multiattack.___ The rock worm makes two attacks: one
with its bite and one with its tail.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft.,
one target. Hit: 9 (1d8 + 5) piercing damage. If the
target is a Medium or smaller creature, it must
succeed on a DC 13 Dexterity saving throw or be
swallowed by the worm. A swallowed creature is
blinded and restrained, it has total cover against
attacks and other effects outside the worm, and it
takes 10 (3d6) bludgeoning damage at the start of
each of the worm’s turns.

If the worm takes at least 10 damage in a single
turn from a creature inside it, the worm must
succeed on a DC 17 Constitution saving throw at
the end of that turn or regurgitate all swallowed
creatures, which fall prone in a space within 10 feet
of the worm. If the worm dies, a swallowed creature
is no longer restrained by it and can escape from
the corpse by using 20 feet of movement, exiting
prone.

___Tail.___ Melee Weapon Attack: +7 to hit, reach 10 ft.,
one target. Hit: 11 (2d6 + 4) bludgeoning damage.
If the target is a creature, it must succeed on a DC
14 Strength saving throw or be pushed up to 10
feet away from the worm and knocked prone.
