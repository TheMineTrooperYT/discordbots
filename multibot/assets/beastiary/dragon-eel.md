"Dragon Eel";;;_size_: Huge dragon
_alignment_: neutral
_challenge_: "12 (8400 XP)"
_languages_: "Common, Draconic, Primordial"
_skills_: "Acrobatics +5, Athletics +12, Insight +5, Perception +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Str +12, Dex +5, Int +6, Wis +5, Cha +6"
_damage_immunities_: "lightning"
_condition_immunities_: "paralyzed, prone"
_speed_: "20 ft., swim 60 ft."
_hit points_: "230 (20d12 + 100)"
_armor class_: "18 (natural armor)"
_stats_: | 26 (+8) | 12 (+1) | 20 (+5) | 14 (+2) | 13 (+1) | 14 (+2) |

___Limited Amphibiousness.___ The dragon eel can breathe air and water, but it needs to be submerged at least once every six hours to avoid suffocation.

___Shocking Touch.___ A dragon eel's body generates a potent charge of lightning. A creature that touches or makes a successful melee attack against a dragon eel takes 5 (1d10) lightning damage.

___Storm Glide.___ During storms, the dragon eel can travel through the air as if under the effects of a fly spell, except using its swim speed.

**Actions**

___Multiattack.___ The dragon eel makes one bite attack and one tail slap attack.

___Bite.___ Melee Weapon Attack: +12 to hit, reach 10 ft., one target. Hit: 26 (4d8 + 8) piercing damage plus 5 (1d10) lightning damage, and the target must succeed on a DC 18 Constitution saving throw or become paralyzed for 1d4 rounds.

___Tail Slap.___ Melee Weapon Attack: +12 to hit, reach 10 ft., one target. Hit: 30 (5d8 + 8) bludgeoning damage plus 5 (1d10) lightning damage and push the target up to 10 feet away.

___Lightning Breath (Recharge 6).___ The dragon eel exhales lightning in a 60-foot line that is 5 feet wide. Each target in that line takes 55 (10d10) lightning damage, or half damage with a successful DC 18 Dexterity saving throw.

