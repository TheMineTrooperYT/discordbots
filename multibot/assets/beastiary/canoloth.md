"Canoloth";;;_page_number_: 247
_size_: Medium fiend (yugoloth)
_alignment_: neutral evil
_challenge_: "8 (3900 XP)"
_languages_: "Abyssal, Infernal, telepathy 60 ft."
_senses_: "darkvision 60 ft., truesight 120 ft., passive Perception 19"
_skills_: "Investigation +3, Perception +9"
_damage_immunities_: "acid, poison"
_speed_: "50 ft."
_hit points_: "120  (16d8 + 48)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 18 (+4) | 10 (0) | 17 (+3) | 5 (-2) | 17 (+3) | 12 (+1) |

___Dimensional Lock.___ Other creatures can't teleport to or from a space within 60 feet of the canoloth. Any attempt to do so is wasted.

___Magic Resistance.___ The canoloth has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The canoloth's weapon attacks are magical.

___Uncanny Senses.___ The canoloth can't be surprised while it isn't incapacitated.

**Actions**

___Multiattack___ The canoloth makes two attacks: one with its tongue or its bite and one with its claws.

___Bite___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 25 (6d6 + 4) piercing damage.

___Claws___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 15 (2d10 + 4) slashing damage.

___Tongue___ Ranged Weapon Attack: +7 to hit, range 30 ft., one target. Hit: 17 (2d12 + 4) piercing damage. If the target is Medium or smaller, it is grappled (escape DC 15), pulled up to 30 feet toward the canoloth, and is restrained until the grapple ends. The canoloth can grapple one target at a time with its tongue.