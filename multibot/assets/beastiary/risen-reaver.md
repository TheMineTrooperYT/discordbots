"Risen Reaver";;;_size_: Large undead
_alignment_: chaotic evil
_challenge_: "7 (2900 XP)"
_languages_: "any languages it knew in life"
_skills_: "Perception +1"
_senses_: "darkvision 120 ft., passive Perception 11"
_saving_throws_: "Dex +6"
_speed_: "40 ft."
_hit points_: "168 (16d10 + 80)"
_armor class_: "15 (studded leather)"
_stats_: | 19 (+4) | 16 (+3) | 20 (+5) | 9 (-1) | 7 (-2) | 6 (-2) |

___Life Sense.___ The risen reaver automatically detects all living creatures within 120 feet. This sense is blocked by 3 feet of wood, 1 foot of earth or stone, an inch of metal, or a thin sheet of lead.

___Pounce.___ When the risen reaver hits an enemy with its blade attack after moving at least 20 feet, the target creature must make a DC 15 Strength saving throw. On a failure, the creature falls prone and the risen reaver can use a bonus action to make a single blade attack.

___Infused Arsenal.___ As a bonus action, the risen reaver can absorb one unattended weapon into its body. For every weapon it absorbs, it deals +1 damage with its blade attacks ( maximum of +3).

___Skitter.___ The risen reaver can take the Dash action as a bonus action.

**Actions**

___Multiattack.___ The risen reaver makes three blade attacks.

___Blade.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one creature. Hit: 15 (2d10 + 4) slashing damage.

