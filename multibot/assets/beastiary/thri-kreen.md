"Thri-kreen";;;_size_: Medium humanoid (thri-kreen)
_alignment_: chaotic neutral
_challenge_: "1 (200 XP)"
_languages_: "Thri-kreen"
_senses_: "darkvision 60 ft."
_skills_: "Perception +3, Stealth +4, Survival +3"
_speed_: "40 ft."
_hit points_: "33 (6d8+6)"
_armor class_: "15 (natural armor)"
_stats_: | 12 (+1) | 15 (+2) | 13 (+1) | 8 (-1) | 12 (+1) | 7 (-2) |

___Chameleon Carapace.___ The thri-kreen can change the color of its carapace to match the color and texture of its surroundings. As a result, it has advantage on Dexterity (Stealth) checks made to hide.

___Standing Leap.___ The thri-kreen's long jump is up to 30 feet and its high jump is up to 15 feet, with or without a running start.

**Actions**

___Multiattack.___ The thri-kreen makes two attacks: one with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one creature. Hit: 4 (1d6 + 1) piercing damage, and the target must succeed on a DC 11 Constitution saving throw or be poisoned for 1 minute. If the saving throw fails by 5 or more, the target is also paralyzed while poisoned in this way. The poisoned target can repeat the saving throw on each of its turns, ending the effect on itself on a success.

___Claws.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 6 (2d4 + 1) slashing damage.

___Variant: Weapons Multiattack.___ The thri-kreen makes two gythka attacks or two chatkcha attacks.

___Variant: Gythka.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 5 (1d8 + 1) slashing damage.

___Variant: Chatkcha.___ Ranged Weapon Attack: +4 to hit, range 30/120 ft., one target. Hit: 5 (1d6 + 2) slashing damage.