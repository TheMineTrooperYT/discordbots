"Memory Moss";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "passive Perception 10"
_damage_vulnerabilities_: "cold, fire"
_condition_immunities_: "charmed, frightened, poisoned, prone, stunned"
_speed_: "0 ft."
_hit points_: "5 (1d6 + 2)"
_armor class_: "5"
_stats_: | 1 (-5) | 1 (-5) | 15 (+2) | 1 (-5) | 10 (+0) | 1 (-5) |

___Steal Memories.___ When a living creature moves within 60 feet of a patch of memory
moss, the moss attacks by attempting to steal that creature’s memories.
It can target a single creature each round. A targeted creature must
succeed on a DC 14 Wisdom saving throw or lose all memories from
the last 24 hours. This is particularly nasty to spellcasters, who lose
all spells prepared within the last 24 hours. (Only those spells actually
prepared in the last 24 hours are lost; spells prepared longer than 24
hours ago are not lost.)

Once a memory moss steals a creature’s memories, it sinks back down
and does not attack again for one day. If a creature loses its memories
to the memory moss, it acts as if affected by a _confusion_ spell for the
next 1d4 hours. Lost memories can be regained by eating the memory
moss that absorbed them. Doing so requires a DC 11 Constitution saving
throw, with failure resulting in the creature being nauseated for 1d6
minutes and poisoned until it takes a long or short rest.

A creature that eats the memory moss temporarily gains the
memories currently stored therein (even if they are not the creature’s
own memories). Such creatures can even cast spells if the memory
moss has stolen these from a spellcasting creature. Any non-spellcaster
that attempts to cast a spell gained in this way must succeed on an
Intelligence check (DC 10 + spell level) or the spell fizzles away.
After 24 hours, the memories fade (including any spells not yet cast).
Creatures eating the memory moss to regain their own lost memories
do not lose them after 24 hours. Cold or fire damage kills a patch of
memory moss.

When first encountered there is a 25% chance that the memory moss
has eaten within the last day and does not attack by stealing memories.
In such a case, the moss contains 2d4 spells determined randomly from
any spell caster’s list with no spell over 4th level. When a living creature
moves within 60 feet of a sated memory moss, it assumes a vaguely
humanoid form and casts the stolen spells at its targets. The moss casts
these spells as a sorcerer of the minimum level necessary to cast the stolen
spell (save DC 10 + spell level, + spell level +2 to hit with spell attack).
