"Cloud Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "Auran, Aquan, Common, Draconic"
_skills_: "Insight +4, Perception +4"
_senses_: "blindsight 60 ft., darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +3, Con +3, Wis +2, Cha +3"
_damage_immunities_: "cold, lightning"
_speed_: "40 ft., fly 60 ft."
_hit points_: "44 (8d8 + 8)"
_armor class_: "15 (natural armor)"
_stats_: | 15 (+2) | 12 (+1) | 13 (+1) | 14 (+2) | 11 (+0) | 13 (+1) |

___Windborn.___ The dragon cannot be moved or knocked prone by
magical or nonmagical winds of any kind.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7
(1d10 + 2) piercing damage plus 3 (1d6) cold damage.

___Breath Weapons (Recharge 5–6).___ The dragon uses one of the following
breath weapons.

* _Cloud Breath._ The dragon exhales an icy blast in a
15-foot cone that spreads around corners. Each creature in that area must
make a DC 11 Constitution saving throw, taking 22 (5d8) cold damage on
a failed saving throw, or half as much damage on a successful one.

* _Wind Breath._ The dragon exhales gale-force winds in a 20-foot line that
is 10-feet wide. Creatures in this area must make a DC 11 Strength saving
throw, taking 18 (4d8) bludgeoning damage on a failed saving throw and
are pushed 20 feet directly away from the dragon. On a successful saving
throw, the creature takes half damage and is not pushed. A creature who
is pushed and strikes a solid surface takes 1d6 bludgeoning damage per
10 feet traveled. Nonmagical flames are completely extinguished, and
magical flames created by a spell of 2nd level or lower are immediately
dispelled.
