"Wrath Dragon";;;_size_: Huge dragon
_alignment_: any good alignment
_challenge_: "19 (22,000 XP)"
_languages_: "Celestial, Common, Draconic, Giant, Terran"
_skills_: "Insight +11, Persuasion +9, Perception +17, Religion +10, Stealth +7"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 27"
_saving_throws_: "Dex +7, Con +11, Wis +11, Cha +9"
_damage_immunities_: "fire, poison"
_condition_immunities_: "poisoned"
_speed_: "40 ft., fly 80 ft."
_hit points_: "356 (31d12 + 155)"
_armor class_: "19 (natural armor)"
_stats_: | 23 (+6) | 12 (+1) | 21 (+5) | 18 (+4) | 21 (+5) | 17 (+3) |

___Divine Aura.___ Each time an aberration, fiend, or undead attempts
to make a melee or ranged attack against the wrath dragon from
within 10 feet of the dragon, the creature must succeed on a
DC 19 Wisdom saving throw or be forced to target another
creature within range or lose the action. Once a creature
succeeds on this saving throw, it is immune to it for 24
hours.

___Legendary Resistance (3/day).___ If the dragon fails a
saving throw, it can choose to succeed instead.

___Spellcasting.___ The wrath dragon is a 15th level spellcaster. Its spellcasting ability is Wisdom (spell save DC 19, +11 to hit with spell attacks). It has the following cleric spells prepared:

* Cantrips (at will): _guidance, mending, resistance, sacred flame, thaumaturgy_

* 1st level (4 slots): _bane, bless, cure wounds, detect magic, guiding bolt_

* 2nd level (3 slots): _aid, find traps, hold person, lesser restoration, silence, spiritual weapon_

* 3rd level (3 slots): _beacon of hope, dispel magic, revivify_

* 4th level (3 slots): _banishment, death ward, freedom of movement, guardian of faith_

* 5th level (2 slots): _commune, dispel evil and good, greater restoration, mass cure wound, raise dead_

* 6th level (1 slot): _heal_

* 7th level (1 slot): _divine word_

* 8th level (1 slot): _holy aura_

**Actions**

___Multiattack.___ The wrath dragon makes three attacks: one
with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +12 to hit, reach 10 ft., one
target. Hit: 17 (2d10 + 6) piercing damage plus 9 (2d8) radiant damage.

___Claw.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one
target. Hit: 13 (2d6 + 6) slashing damage plus 9 (2d8) radiant damage.

___Tail.___ Melee Weapon Attack: +12 to hit, reach 15 ft., one target. Hit:
15 (2d8 + 6) bludgeoning damage plus 9 (2d8) radiant damage.
Divine Breath (Recharge 5–6). The wrath dragon releases a blast of
divine fire in a 60-foot cone. Creatures within the area must make a DC 19
Dexterity saving throw, taking 28 (8d6) fire damage and 28 (8d6) radiant
damage on a failed saving throw, or half as much damage on a successful
saving throw.

___Beckon Deva (1/day).___ The wrath dragon summons a deva from the deity
that it serve. The summoned deva appears in an unoccupied space within 60
feet of the wrath dragon. It remains for 1 minute, until it or the wrath dragon
is slain, or until the wrath dragon takes an action to dismiss it.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. The dragon regains spent legendary
actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each
creature within 15 feet of the dragon must succeed on a DC 20 Dexterity
saving throw or take 13 (2d6 + 6) bludgeoning damage and be knocked
prone. The dragon can then fly up to half its flying speed.
Preserve Life (Costs 3 Actions). The wrath dragon causes up to 6
creatures within 30 feet of it to regain 14 (2d6 + 7) hit points each.
