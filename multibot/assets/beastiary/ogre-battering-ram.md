"Ogre Battering Ram";;;_page_number_: 220
_size_: Large giant
_alignment_: chaotic evil
_challenge_: "4 (1100 XP)"
_languages_: "Common, Giant"
_senses_: "darkvision 60 ft., passive Perception 8"
_speed_: "40 ft."
_hit points_: "59  (7d10 + 21)"
_armor class_: "14 (ring mail)"
_stats_: | 19 (+4) | 8 (-1) | 16 (+3) | 5 (-2) | 7 (-1) | 7 (-1) |

___Siege Monster.___ The ogre deals double damage to objects and structures.

**Actions**

___Bash___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 15 (2d10 + 4) bludgeoning damage, and the ogre can push the target 5 feet away if the target is Huge or smaller.

___Block the Path___ Until the start of the ogre's next turn, attack rolls against the ogre have disadvantage, it has advantage on the attack roll it makes for an opportunity attack, and that attack deals an extra 16 (3d10) bludgeoning damage on a hit. Also, each enemy that tries to move out of the ogre's reach without teleporting must succeed on a DC 14 Strength saving throw or have its speed reduced to 0 until the start of the ogre's next turn.