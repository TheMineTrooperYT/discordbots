"Ravager Spawn (Flier Form)";;;_size_: Huge monstrosity
_alignment_: neutral
_challenge_: "22 (41,000 XP)"
_languages_: "--"
_skills_: "Perception +14"
_senses_: "darkvision 120 ft., tremorsense 60 ft., passive Perception 24"
_saving_throws_: "Dex +14, Con +13"
_damage_resistances_: "acid, cold, fire, lightning, thunder; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, poisoned"
_speed_: "20 ft., fly 100 ft."
_hit points_: "450 (36d12 + 216)"
_armor class_: "26 (natural armor)"
_stats_: | 27 (+8) | 25 (+7) | 23 (+6) | 5 (-3) | 24 (+7) | 18 (+4) |

___Charge.___ If the Ravager moves at least 20 feet straight toward a creature
and then hits it with a claw attack on the same turn, the target must succeed
on a DC 25 Strength saving throw or be knocked prone. If the target is
prone, the Ravager can make one claw attack against it as a bonus action.

___Flyby.___ The Ravager doesn’t provoke an opportunity attack when it flies
out of an enemy’s reach.

___Form-Shifting.___ The Ravager can physically alter its physiology to
take on one of the three listed forms: the crawler, the brawler, or the flier.
Doing so takes one minute, and during this period it cannot take any other
actions, though it is not considered incapacitated.

___Improved Critical.___ The Ravager’s bite and claws score a critical hit on
a roll of 19 or 20.

___Legendary Resistance (3/day).___ If the Ravager fails a saving throw, it
can choose to succeed instead.

___Magic Disruption.___ Every time the Ravager comes into contact with
magical effects, there is a chance that it disrupts it. This effect functions as
per the dispel magic spell cast using a 5th level spell slot with a +5 spell
casting ability modifier.

___Magic Weapons.___ The Ravager’s melee attacks are magical.

___Rampage.___ When the Ravager reduces a creature to 0 hit points with a
melee attack on its turn, the Ravager can take a bonus action to move up
to half its speed and make a bite attack.

___Regeneration.___ The Ravager regains 10 hit points at the start of its turn.
If the Ravager takes damage from an artifact or from a legendary action,
this trait doesn’t function at the start of the Ravager’s next turn. The
Ravager only dies if it starts its turn at 0 hit points and doesn’t regenerate.

___Vampiric Healing.___ Whenever the Ravager hits with a melee attack, it
regains hit points equal to half the damage it inflicts on its opponent, up to
its hit point maximum.

**Actions**

___Multiattack.___ The Ravager makes one bite and four claw attacks.

___Bite.___ Melee Weapon Attack: +15 to hit, reach 15 ft., one target. Hit: 29
(6d6 + 8) piercing damage.

___Claws.___ Melee Weapon Attack: +15 to hit, reach 15 ft., one target. Hit:
36 (8d6 + 8) slashing damage.
