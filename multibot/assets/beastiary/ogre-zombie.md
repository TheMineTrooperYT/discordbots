"Ogre Zombie";;;_size_: Large undead
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "understands Common and Giant but can't speak"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_saving_throws_: "Wis +0"
_speed_: "30 ft."
_hit points_: "85 (9d10+36)"
_armor class_: "8"
_condition_immunities_: "poisoned"
_stats_: | 19 (+4) | 6 (-2) | 18 (+4) | 3 (-4) | 6 (-2) | 5 (-3) |

___Undead Fortitude.___ If damage reduces the zombie to 0 hit points, it must make a Constitution saving throw with a DC of 5+the damage taken, unless the damage is radiant or from a critical hit. On a success, the zombie drops to 1 hit point instead.

**Actions**

___Morningstar.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) bludgeoning damage.