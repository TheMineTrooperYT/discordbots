"Spirit Weaver";;;_size_: Medium humanoid (any race)
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "any three languages"
_skills_: "Arcana +4, Insight +7, Nature +7"
_senses_: "passive Perception 14"
_saving_throws_: "Int +4, Wis +7"
_speed_: "30 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "16 (spiritual armor)"
_stats_: | 10 (+0) | 14 (+2) | 14 (+2) | 12 (+1) | 18 (+4) | 12 (+1) |

___Spiritual Armor.___ While the spirit weaver is not
wearing armor, its AC includes its Wisdom
modifier.

___Phase Walking.___ The spirit weaver has such a strong
connection to the Ethereal Plane that it can shift
between it and the Material Plane at will for short
bursts. Whenever the spirit weaver moves, it can
choose to step into the Ethereal Plane. While on
the Ethereal Plane, it can only affect and be affected
by creatures also on that plane. Creatures not on
the Ethereal Plane cannot perceive the spirit weaver
and cannot interact with it unless a special ability or
magic has given them the ability to do so.

___Stable Mind.___ The spirit weaver has advantage on all
Intelligence, Wisdom, and Charisma saving throws
against magic.

**Actions**

___Spirit Drain.___ Melee Weapon Attack: +7 to hit, reach
5 ft., one target. Hit: 21 (6d6) force damage and
the spirit weaver gains temporary hit points equal
to the damage dealt.

___Withering Burst.___ Target creature within 90 feet must
succeed on a DC 15 Constitution saving throw or
take 22 (5d8) force damage. If the creature fails
this saving throw by 5 or more, on its next turn it
can either move or take an action, but not both, as
its body is drained of strength.

___Spirit Lash (1/Day).___ The spirit weaver calls on its
ancestral spirits and unleashes them upon all
nearby enemies. Each hostile creature within 30
feet of the spirit weaver must make a DC 15
Wisdom saving throw, taking 28 (8d6) force
damage and become grappled by invisible spirits
on a failed save or half as much damage and not
grappled on a successful one. Creatures that failed
this save by 5 or more are restrained instead of
grappled. At the end of each turn, a grappled or
restrained creature can repeat this saving throw,
ending the effect on a success.
