"Girallon Zombie";;;_size_: Large undead
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_senses_: "darkvision 60 ft., passive Perception 8"
_condition_immunities_: "poisoned"
_damage_immunities_: "poison"
_speed_: "30 ft., climb 30 ft."
_hit points_: "59 (7d10 +21)"
_armor class_: "11 (natural armor)"
_stats_: | 18 (+4) | 12 (+1) | 16 (+3) | 3 (-4) | 7 (-2) | 5 (-3) |

___Aggressive.___ As a bonus action, the zombie can move up to its speed toward a hostile creature that it can see.

___Undead fortitude.___ If damage reduces the zombie to 0 hit points, it must make a Constitution saving throw with a DC of 5 + the damage taken, unless the damage is radiant or from a critical hit. On a success, the zombie drops to 1 hit point instead.

**Actions**

___Multiattack.___ The zombie makes five attacks: one with its bite and four with its claws.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (ld6 +4) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (ld4 +4) slashing damage.