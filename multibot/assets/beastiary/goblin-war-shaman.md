"Goblin War Shaman";;;_size_: Medium humanoid (goblinoid)
_alignment_: neutral evil
_challenge_: "2 (100 XP)"
_languages_: "Common, Goblin"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "30 ft."
_hit points_: "31 (9d6)"
_armor class_: "12"
_stats_: | 10 (+0) | 14 (+2) | 10 (+0) | 14 (+2) | 10 (+0) | 8 (-1) |

___Spellcasting.___ The shaman is a 3rd-level spellcaster.
His spellcasting ability is Intelligence (spell save DC
12, +4 to hit with spell attacks). He has the
following Wizard spells prepared:

* Cantrips (at will): _fire bolt, minor illusion_

* 1st level (4 slots): _color spray, grease, mage armor_

* 2nd level (2 slots): _blindness/deafness, enlarge/reduce_

**Actions**

___Multiattack.___ The goblin makes two attacks with its
scimitar.

___Scimitar.___ Melee Weapon Attack: +4 to hit, reach 5
ft., one target. Hit: 5 (1d6 + 2) slashing damage.
