"Ophidian";;;_size_: Medium monstrosity
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Ophidian"
_senses_: "darkvision 60 ft., passive Perception 13"
_skills_: "Perception +3, Stealth +5"
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "32 (5d8 + 10)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+2) | 17 (+3) | 15 (+2) | 12 (+1) | 13 (+1) | 12 (+1) |

___Keen Smell.___ The ophidian has advantage on Wisdom
(Perception) checks that rely on smell.

**Actions**

___Snake Hands.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one
target. Hit: 7 (1d8 + 3) piercing damage, and the target must succeed on a
DC 12 Constitution saving throw or be poisoned for 1 hour.

___Blinding Spray. (Recharge 5–6).___ The ophidian spews forth a 20-foot
cone of viscous liquid. All creatures in this area must succeed on a DC 12
Dexterity saving or be blinded for 1 minute. The blinded target can repeat
the saving throw at the end of each of its turns, ending the effect on itself
on a success.
