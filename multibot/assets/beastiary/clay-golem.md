"Clay Golem";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "9 (5,000 XP)"
_languages_: "understands the languages of its creator but can't speak"
_senses_: "darkvision 60 ft."
_damage_immunities_: "acid, poison, psychic, bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_speed_: "20 ft."
_hit points_: "133 (14d10+56)"
_armor class_: "14 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_stats_: | 20 (+5) | 9 (-1) | 18 (+4) | 3 (-4) | 8 (-1) | 1 (-5) |

___Acid Absorption.___ Whenever the golem is subjected to acid damage, it takes no damage and instead regains a number of hit points equal to the acid damage dealt.

___Berserk.___ Whenever the golem starts its turn with 60 hit points or fewer, roll a d6. On a 6, the golem goes berserk. On each of its turns while berserk, the golem attacks the nearest creature it can see. If no creature is near enough to move to and attack, the golem attacks an object, with preference for an object smaller than itself. Once the golem goes berserk, it continues to do so until it is destroyed or regains all its hit points.

___Immutable Form.___ The golem is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The golem has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The golem's weapon attacks are magical.

**Actions**

___Multiattack.___ The golem makes two slam attacks.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 16 (2d10 + 5) bludgeoning damage. If the target is a creature, it must succeed on a DC 15 Constitution saving throw or have its hit point maximum reduced by an amount equal to the damage taken. The target dies if this attack reduces its hit point maximum to 0. The reduction lasts until removed by the greater restoration spell or other magic.

___Haste (Recharge 5-6).___ Until the end of its next turn, the golem magically gains a +2 bonus to its AC, has advantage on Dexterity saving throws, and can use its slam attack as a bonus action.