"Ancient Gray Dragon";;;_size_: Gargantuan dragon
_alignment_: neutral evil
_challenge_: "21 (33,000 XP)"
_languages_: "Abyssal, Aquan, Common, Draconic, Giant"
_skills_: "Perception +18, Stealth +9"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 28"
_saving_throws_: "Dex +9, Con +14, Wis +11, Cha +11"
_damage_immunities_: "fire"
_speed_: "40 ft., fly 80 ft., swim 40 ft."
_hit points_: "455 (26d20 + 182)"
_armor class_: "21 (natural armor)"
_stats_: | 28 (+9) | 14 (+2) | 25 (+7) | 19 (+4) | 17 (+4) | 19 (+4) |

___Amphibious.___ The dragon can breathe air and water.

___Legendary Resistance (3/day).___ If the dragon fails a saving throw, it
can choose to succeed instead.

**Actions**

___Multiattack.___ The dragon uses its Frightful Presence and makes three
attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +16 to hit, reach 15 ft., one target. Hit: 20
(2d10 + 9) piercing damage plus 7 (2d6) fire damage.

___Claw.___ Melee Weapon Attack: +16 to hit, reach 10 ft., one target. Hit: 16
(2d6 + 9) slashing damage.

___Tail.___ Melee Weapon Attack: +16 to hit, reach 20 ft., one target. Hit: 18
(2d8 + 9) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon’s choice that is within
120 feet of the dragon and aware of it must succeed on a DC 20 Wisdom
saving throw or become frightened for 1 minute. A creature can repeat the
saving throw at the end of each of its turns, ending the effect on itself on
a success. If a creature’s saving throw is successful of the effect ends for
it, the creature is immune to the dragon’s Frightful Presence for the next
24 hours.

___Steam Breath (Recharge 5–6).___ The dragon exhales steam in a 90-foot
cone. Creatures in the area must make a DC 22 Constitution saving throw.
On a failed saving throw, the creature takes 80 (23d6) fire damage, or half
as much damage on a successful saving throw. Being underwater doesn’t
grant resistance to this damage.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. The dragon regains spent legendary
actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each
creature within 15 feet of the dragon must succeed on a DC 24 Dexterity
saving throw or take 16 (2d6 + 9) bludgeoning damage and be knocked
prone. The dragon can then fly up to half its flying speed.

**Lair** Actions

On initiative count 20 (losing initiative ties), the dragon takes a lair
action to cause one of the following effects; the dragon can’t use the same
effect two rounds in a row:

___Dead Water.___ A 30-foot cube of water the dragon can see within 120 feet
of it stagnates and becomes unable to support life of any kind. Aquatic
creatures or creatures benefiting from spells such as waterbreathing
cannot survive in the area and begin suffocating. The dragon can
end this effect at any time.

___Steam Cloud.___ The dragon causes an area of water within 120
feet of it to superheat, creating a cloud of fog in a 60-foot radius
sphere. The sphere spreads around corners and its area is heavily
obscured. The fog cloud remains for 1 hour or until a wind of
moderate or greater speed (at least 10 miles per hour) disperses it.

___Control Water.___ The dragon can cause an area of water to act as if
the spell _control water_ was cast on it. The dragon does not have to
concentrate on this effect, and it remains active for the duration or
until the dragon dismisses it (no action required).

**Regional** Effects

The region containing the ancient gray dragon’s lair is changed by its
presence, which creates one or more of the following effects:

___Obscuring Fog.___ Twenty-foot areas of fog and steam dot the landscape
within 6 miles of the lair. These areas are heavily obscured.

___Heavy Tides.___ Tides are supernaturally heavy and inundate coastal
areas, causing massive flooding.

If the dragon dies, these effects fade over 1d10 days.
