"Shellycoat";;;_size_: Medium fey
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Giant, Sylvan"
_skills_: "Perception +1, Stealth +4"
_senses_: "Darkvision 60 ft., passive Perception 11"
_condition_immunities_: "charmed, unconscious"
_speed_: "30 ft., swim 20 ft."
_hit points_: "75 (10d8 + 30)"
_armor class_: "14 (natural armor)"
_stats_: | 17 (+3) | 15 (+2) | 16 (+3) | 13 (+1) | 9 (-1) | 7 (-2) |

___Innate Spellcasting.___ The shellycoat can cast the following spells innately, requiring no components:

1/day each: _darkness, fog cloud_

1/day (if in possession of its coat): _water breathing_

___Regeneration.___ The shellycoat regains 3 hit points at the start of its turn. If the creature takes acid or fire damage, this trait doesn't function at the start of the monster's next turn. The shellycoat dies only if it starts its turn with 0 hit points and doesn't regenerate.

___Stealthy Observer.___ The shellycoat has advantage on Dexterity (Stealth) checks made to hide and any Perception checks that rely on hearing.

___Sunlight Sensitivity.___ The shellycoat becomes petrified after 5 (2d4) uninterrupted rounds of exposure to direct, natural sunlight.

**Actions**

___Multiattack.___ The shellycoat makes one bite attack and one claws attack.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Claws.___ Melee Weapon Attack: +5 to hit, reach 15 ft., one target. Hit: 12 (2d8 + 3) slashing damage and the target is grappled (escape DC 13), restrained, and poisoned (DC 13 Strength saving throw negates, lasts while grappled and 1 round after). The shellycoat can shift the position of a grappled creature by up to 15 feet as a bonus action. While it has a creature grappled, the shellycoat can use its claws attack only against the grappled creature.

