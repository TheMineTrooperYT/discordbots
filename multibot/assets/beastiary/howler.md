"Howler";;;_page_number_: 210
_size_: Large fiend
_alignment_: chaotic evil
_challenge_: "8 (3900 XP)"
_languages_: "understands Abyssal but can't speak"
_senses_: "darkvision 60 ft., passive Perception 15"
_skills_: "Perception +8"
_speed_: "40 ft."
_hit points_: "90  (12d10 + 24)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "frightened"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 17 (+3) | 16 (+3) | 15 (+2) | 5 (-2) | 20 (+5) | 6 (-2) |

___Pack Tactics.___ A howler has advantage on attack rolls against a creature if at least one of the howler's allies is within 5 feet of the creature and the ally isn't incapacitated.

**Actions**

___Multiattack___ The howler makes two bite attacks.

___Rending Bite___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) piercing damage. If the target is frightened it takes an additional 22 (4d10) psychic damage. This attack ignores damage resistance.

___Mind-Breaking Howl (Recharge 6)___ The howler emits a keening howl in a 60-foot cone. Each creature in that area that isn't deafened must succeed on a DC 16 Wisdom saving throw or be frightened until the end of the howler's next turn. While a creature is frightened in this way, its speed is halved, and it is incapacitated. A target that successfully saves is immune to the Mind-Breaking Howl of all howlers for the next 24 hours.