"Chwinga";;;_size_: Tiny elemental
_alignment_: neutral
_challenge_: "0 (10 XP)"
_senses_: "blindsight 60ft., passive Perception 17"
_skills_: "Acrobatics +7, Perception +7, Stealth +7"
_speed_: "20 ft., climb 20 ft., swim 20 ft."
_hit points_: "5 (2d4)"
_armor class_: "15"
_stats_: | 1 (-5) | 20 (+5) | 10 (0) | 14 (+2) | 16 (+3) | 16 (+3) |

___Evasion.___ When the chwinga is subjected to an effect that allows it to make a Dexterity saving throw to take only half damage, it instead takes no damage if it succeeds on the saving throw, and only half damage if it fails.

___Innate Spellcasting.___ The chwinga's innate spellcasting ability is Wisdom. It can innately cast the following spells, requiring no material or verbal components:

* At will: _druidcraft, guidance, pass without trace, resistance_

**Actions**

___Magical Gift (1/Day).___ The chwinga targets a humanoid it can see within S feet of it. The target gains a supernatural charm of the DM's choice. See chapter 7 of the Dungeon Master's Guide for more information on supernatural charms.

___Natural Shelter.___ The chwinga magically takes shelter inside a rock, a living plant, or a natural source of fresh water in its space. The chwinga can't be targeted by any attack, spell, or other effect while inside this shelter, and the shelter doesn't impair the chwinga's blindsight. The chwinga can use its action to emerge from a shelter. If its shelter is destroyed, the chwinga is forced out and appears in the shelter's space, but is otherwise unharmed.
