"Archer Bush";;;_size_: Small plant
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "--"
_skills_: "Stealth +4"
_senses_: "tremorsense 60 ft., passive Perception 10"
_speed_: "10 ft."
_hit points_: "78 (12d6 + 36)"
_armor class_: "13 (natural armor)"
_stats_: | 11 (+0) | 15 (+2) | 16 (+3) | 2 (-4) | 11 (+0) | 9 (-1) |

**Actions**

___Multiattack.___ The archer bush makes two attacks with its thorns.

___Thorns.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range
20/60 ft., one target. Hit: 6 (1d8 + 2) piercing damage.

___Bloodsuck.___ A creature within 5 ft. of the archer bush that has 0 hit
points is drained of blood. The creature automatically fails a death saving
throw.
