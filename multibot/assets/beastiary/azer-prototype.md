"Azer Prototype";;;_size_: Medium elemental
_alignment_: lawful neutral
_challenge_: "1 (200 XP)"
_languages_: "Ignan"
_senses_: "passive Perception 11"
_damage_immunities_: "fire, poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "27 (5d6 + 10)"
_armor class_: "15 (natural armor)"
_stats_: | 15 (+2) | 12 (+1) | 14 (+2) | 12 (+1) | 12 (+1) | 10 (+0) |

___Heated Body.___ A creature that touches the azer or
hits it with a melee attack while within 5 feet of it
takes 3 (1d6) fire damage.

___Heated Weapons.___ When the azer hits with a metal
melee weapon, it deals an extra 2 (1d4) fire damage
(included in the attack).

___Illumination.___ The azer sheds bright light in a 10-foot
radius and dim light for an additional 10 feet.

**Actions**

___Multiattack.___ The azer makes one attack with its
mace and one with its off-hand strike.

___Mace.___ Melee Weapon Attack: +4 to hit, reach 5ft.,
one target. Hit: 5 (1d6 + 2) bludgeoning damage
plus 2 (1d4) fire damage.

___Off-hand Strike.___ Melee Weapon Attack: +4 to hit,
reach 5ft., one target. Hit: 3 (1d6) bludgeoning
damage plus 2 (1d4) fire damage.
