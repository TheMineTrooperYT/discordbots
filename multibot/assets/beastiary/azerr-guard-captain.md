"Azer Guard Captain";;;_size_: Medium elemental
_alignment_: lawful neutral
_challenge_: "4 (1,100 XP)"
_languages_: "Ignan, Common"
_skills_: "Athletics +6, Arcana +3, Intimidation +2"
_senses_: "darkvision 90 ft., passive Perception 11"
_damage_immunities_: "fire, poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "85 (10d10 + 30)"
_armor class_: "17 (natural armor, shield)"
_stats_: | 18 (+4) | 10 (+0) | 16 (+3) | 12 (+1) | 12 (+1) | 10 (+0) |

___Heated Body.___ A creature that touches the azer or
hits it with a melee attack while within 5 feet of it
takes 5 (1d10) fire damage.

___Superheated Equipment.___ When the azer captain hits
with a metal melee weapon, it deals an extra 7
(2d6) fire damage (included in the attack).

___Illumination.___ The azer captain sheds bright light in a
15-foot radius and dim light for an additional 15
feet.

**Actions**

___Multiattack.___ The azer captain makes two attacks:
one with its warhammer and one shield bash.

___Warhammer.___ Melee Weapon Attack: +6 to hit, reach
5ft., one target. Hit: 8 (1d8 + 4) bludgeoning
damage plus 7 (2d6) fire damage.

___Shield Bash.___ Melee Weapon Attack: +6 to hit, reach
5ft., one target. Hit: 7 (1d6 + 4) bludgeoning
damage and the target must succeed on a DC 14
Constitution saving throw or be stunned until the
end of its next turn.

___Ignition (1/Day).___ The azer captain lets out a powerful
rallying cry that increases the internal flames within
all allies within 120 feet that can see it or hear it.
For the next minute, these azer have the damage of
their Heated Body increased by one dice category
to a maximum of 1d12 and gain 10 temporary hit
points.

**Reactions**

___Flame Barrier.___ The azer captain adds 3 to its AC
against a ranged spell attack that would hit it by
raising his shield and creating a protective barrier of
flame. If the attack is still successful, the attacker
takes 5 (1d10) fire damage as a spiral of flame
tracks the spell back to its origin. To use this ability,
the azer captain must see the attack and be
wielding a shield.
