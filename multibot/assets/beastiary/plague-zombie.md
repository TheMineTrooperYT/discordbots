"Plague Zombie";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "understands all languages it spoke in life but can't speak"
_senses_: "darkvision 60 ft., passive Perception 8"
_saving_throws_: "Wis +0"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "20 ft."
_hit points_: "22 (3d8 + 9)"
_armor class_: "8"
_stats_: | 13 (+1) | 6 (-2) | 16 (+3) | 3 (-4) | 6 (-2) | 5 (-3) |

___Death Burst.___ If the plague zombie is dropped to 0 hit points, it explodes
in a burst of decaying flesh. Any creature within 15 feet of the plague
zombie must make a saving throw against its Zombie Rot.

___Zombie Rot.___ A creature who takes a bite or claw attack, or who is
within 15 feet of the plague zombie when it drops to 0 hit points, must
make a DC 13 Constitution saving throw. On a failed save, the creature
contracts zombie rot.

While it has zombie rot, the creature cannot regain hit points except
via magical means, and it has vulnerability to slashing damage as its
flesh rots. At the end of each long rest after being infected, the creature’s
maximum hit points are reduced by 3 (1d6) and it can repeat the saving
throw, ending zombie rot on a success. Any reduction to the creature’s
hit point maximum is permanent until the zombie rot has been cured.
The reduction ends after the creature’s next long rest after being cured.
If this reduction drops the creature to 0 hit points, the creature dies and
rises as a plague zombie in 1d4 hours.

**Actions**

___Multiattack.___ The plague zombie makes one bite attack and one slam
attack.

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one creature. Hit:
4 (1d6 + 1) piercing damage, and the creature must make a DC 13
Constitution saving throw or contract zombie rot.

___Slam.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one creature. Hit:
4 (1d6 + 1) bludgeoning damage, and the creature must make a DC 13
Constitution saving throw or contract zombie rot.
