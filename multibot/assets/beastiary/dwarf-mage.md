"Dwarf Mage";;;_page_number_: 999
_size_: Medium humanoid (dwarf)
_alignment_: any alignment
_challenge_: "6 (2300 XP)"
_languages_: "any four languages"
_senses_: "darkvision, passive Perception 11"
_skills_: "Arcana +6, History +6"
_saving_throws_: "Int +6, Wis +4"
_speed_: "30 ft."
_hit points_: "40  (9d8)"
_armor class_: "12 (15 with mage armor)"
_damage_resistances_: "poison"
_stats_: | 9 (0) | 14 (+2) | 11 (0) | 17 (+3) | 12 (+1) | 11 (0) |

___Dwarven Resilience.___ The dwarf has advantage on saving throws against poison and has resistance to poison damage

___Spellcasting.___ The mage is a 9th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 15, +7 to hit with spell attacks). The mage has the following wizard spells prepared:

* Cantrips (at will): _fire bolt, light, mage hand, prestidigitation_

* 1st level (4 slots): _detect magic, mage armor, magic missile, shield_

* 2nd level (3 slots): _misty step, suggestion_

* 3rd level (3 slots): _counterspell, fireball, fly_

* 4th level (3 slots): _greater invisibility, ice storm_

* 5th level (1 slot): _cone of cold_

**Actions**

___Dagger___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.
