"Elemental Locus";;;_size_: Gargantuan elemental
_alignment_: neutral
_challenge_: "17 (18000 XP)"
_languages_: "Primordial"
_skills_: "Nature +6, Perception +6"
_senses_: "darkvision 120 ft., tremorsense 120 ft., passive Perception 16"
_saving_throws_: "Int +6, Wis +6, Cha +6"
_damage_immunities_: "acid, cold, fire, lightning, poison, thunder; bludgeoning, piercing, and slashing from nonmagical weapons"
_damage_resistances_: "bludgeoning, piercing, and slashing"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned, unconscious"
_speed_: "5 ft."
_hit points_: "290 (20d20 + 80)"
_armor class_: "16 (natural armor)"
_stats_: | 28 (+9) | 1 (-5) | 18 (+4) | 10 (+0) | 11 (+0) | 11 (+0) |

___Magic Resistance.___ The elemental locus has advantage on saving throws against spells and other magical effects.

___Immortal.___ The elemental locus does not age and does not die when it drops to 0 hit points. If the elemental locus drops to 0 hit points, it falls into a quiescent state for 25 weeks before returning to activity with full hit points. Its spawned elementals continue fighting whatever enemies attacked the elemental locus; if no enemies are present, they defend the locus's area.

___Massive.___ The elemental locus is larger than most Gargantuan creatures, occupying a space of 60 by 60 feet. Its movement is not affected by difficult terrain or by Huge or smaller creatures. Other creatures can enter and move through the elemental locus's space, but they must make a successful DC 20 Strength (Athletics) check after each 10 feet of movement. Failure indicates they fall prone and can move no farther that turn.

___Spawn Elementals.___ As a bonus action, the elemental locus loses 82 hit points and spawns an air, earth, fire, or water elemental to serve it. Spawned elementals answer to their creator's will and are not fully independent. The types of elementals the locus can spawn depend on the terrain it embodies; for example, an elemental locus of the desert can spawn earth, fire, and air elementals, but not water.

___Siege Monster.___ The elemental locus deals double damage to objects and structures.

**Actions**

___Multiattack.___ The elemental locus makes two slam attacks.

___Slam.___ Melee Weapon Attack: +15 to hit, reach 15 ft., one target. Hit: 36 (6d8 + 9) bludgeoning damage. If the target is a creature, it must succeed on a DC 23 Strength saving throw or be knocked prone.

