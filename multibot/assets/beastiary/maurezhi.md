"Maurezhi";;;_page_number_: 133
_size_: Medium fiend (demon)
_alignment_: chaotic evil
_challenge_: "7 (2900 XP)"
_languages_: "Abyssal, Elvish, telepathy 120 ft."
_senses_: "darkvision 120 ft., passive Perception 11"
_skills_: "Deception +5"
_damage_immunities_: "poison"
_speed_: "30 ft."
_hit points_: "88  (16d8 + 16)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "charmed, exhaustion, poisoned"
_damage_resistances_: "cold, fire, lightning, necrotic; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 14 (+2) | 17 (+3) | 12 (+1) | 11 (0) | 12 (+1) | 15 (+2) |

___Assume Form.___ The maurezhi can assume the appearance of any Medium humanoid it has eaten. It remains in this form for 1d6 days, during which time the form gradually decays until, when the effect ends, the form sloughs from the demon's body.

___Magic Resistance.___ The maurezhi has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack___ The maurezhi makes two attacks: one with its bite and one with its claws.

___Bite___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 14 (2d10 + 3) piercing damage. If the target is a humanoid, its Charisma score is reduced by 1d4. This reduction lasts until the target finishes a short or long rest. The target dies if this reduces its Charisma to 0. It rises 24 hours later as a ghoul, unless it has been revived or its corpse has been destroyed.

___Claws___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) slashing damage. If the target is a creature other than an undead, it must succeed on a DC 12 Constitution saving throw or be paralyzed for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Raise Ghoul (Recharge 5-6)___ The maurezhi targets one dead ghoul or ghast it can see within 30 feet of it. The target is revived with all its hit points.