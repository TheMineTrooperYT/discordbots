"Bearfolk";;;_size_: Medium humanoid
_alignment_: chaotic good
_challenge_: "3 (700 XP)"
_languages_: "Common, Giant"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "40 ft."
_hit points_: "45 (6d8 + 18)"
_armor class_: "14 (hide armor)"
_stats_: | 19 (+4) | 14 (+2) | 16 (+3) | 8 (-1) | 12 (+1) | 9 (-1) |

___Keen Smell.___ The bearfolk has advantage on Wisdom (Perception) checks that rely on smell.

**Actions**

___Multiattack.___ The bearfolk makes three attacks: one with its battleaxe, one with its warhammer, and one with its bite.

___Battleaxe.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage, or 9 (1d10 + 4) slashing damage if used two-handed.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) piercing damage.

___Warhammer.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) bludgeoning damage, or 9 (1d10 + 4) bludgeoning damage if used two-handed.

**Bonus** Actions

___Frenzy (1/short rest).___ The bearfolk triggers a berserk frenzy that lasts for 1 minute. While in frenzy, it gains resistance to bludgeoning, piercing, and slashing damage and has advantage on attack rolls. Attack rolls made against a frenzied bearfolk have advantage.
