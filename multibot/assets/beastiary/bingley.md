"Bingley";;;_size_: Small humanoid (halfling, shapechanger)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Halfling (can’t speak in wolf form)"
_senses_: "passive Perception 14"
_skills_: "Perception +4, Stealth +4"
_damage_immunities_: "Bludgeoning, Piercing, And Slashing From Nonmagical Attacks That Aren’t Silvered"
_speed_: "25 ft., 35 ft. in wolf form"
_hit points_: "49 (9d6 + 18)"
_armor class_: "12 in humanoid form, 13 in wolf or hybrid form"
_stats_: | 15 (+2) | 15 (+2) | 14 (+2) | 10 (+0) | 11 (+0) | 11 (+0) |

___Brave.___ Bingley has advantage on saving throws
against being frightened.

___Halfling Nimbleness.___ Bingley can move through
the space of a Medium or larger creature.
Keen Hearing and Smell. Bingley has advantage on
Wisdom (Perception) checks that rely on hearing or
smell.

___Lucky.___ When Bingley rolls a 1 on an attack roll, ability
check, or saving throw, he can reroll the die and
must use the new roll.

___Shapechanger.___ Bingley can use its action to polymorph
into a wolf-humanoid hybrid or into a wolf,
or back into his true form, which is humanoid. His
statistics, other than its AC, are the same in each
form. Any equipment he is wearing or carrying isn’t
transformed. It reverts to its true form if it dies.

**Actions**

___Multiattack (Humanoid or Hybrid Form Only).___
Bingley makes two attacks: two with his spear (humanoid
form) or one with his bite and one with his
claws (hybrid form).

___Bite (Wolf or Hybrid Form Only).___ Melee Weapon
Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage. If the target is a humanoid, it
must succeed on a DC 12 Constitution saving throw
or be cursed with werewolf lycanthropy.

___Claws (Hybrid Form Only).___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 7 (2d4 + 2)
slashing damage.

___Spear (Humanoid Form Only).___ Melee or Ranged
Weapon Attack: +4 to hit, reach 5 ft. or range 20/60
ft., one creature. Hit: 5 (1d6 + 2) piercing damage,
or 6 (1d8 + 2) piercing damage if used with two
hands to make a melee attack.
