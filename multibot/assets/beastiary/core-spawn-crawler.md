"Core Spawn Crawler";;;_size_: Small aberration
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: " understands Deep Speech but can't speak"
_skills_: "Perception +5"
_senses_: "blindsight 30 ft. (blind beyond this radius), tremorsense 60 ft., passive Perception 15"
_damage_immunities_: "psychic"
_condition_immunities_: "blinded"
_speed_: "30 ft."
_hit points_: "21 (6d6)"
_armor class_: "12"
_stats_: | 7 (-2) | 14 (+2) | 10 (+0) | 9 (-1) | 12 (+1) | 6 (-2) |

___Pack Tactics.___ The crawler has advantage on an attack roll against a creature if at least one of the crawler’s allies is within 5 feet of the creature and the ally isn’t incapacitated.

**Actions**

___Multiattack.___ The crawler makes four attacks: one with its bite, two with its claws, and one with its tail.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 4 (1d4 + 2) piercing damage and the target must succeed on a DC 11 Wisdom saving throw or become frightened until the start of the crawler’s next turn.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 15 ft., one target. Hit: 4 (1d4 + 2) slashing damage.

___Tail.___ Melee Weapon Attack: +4 to hit, reach 15 ft., one target. Hit: 5 (1d6 + 2) piercing damage.
