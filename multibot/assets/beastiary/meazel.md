"Meazel";;;_page_number_: 214
_size_: Medium humanoid (meazel)
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "Common"
_senses_: "darkvision 120 ft., passive Perception 13"
_skills_: "Perception +3, Stealth +5"
_speed_: "30 ft."
_hit points_: "35  (10d8 - 10)"
_armor class_: "13"
_stats_: | 8 (-1) | 17 (+3) | 9 (0) | 14 (+2) | 13 (+1) | 10 (0) |

___Shadow Stealth.___ While in dim light or darkness, the meazel can take the Hide action as a bonus action.

**Actions**

___Garrote___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target of the meazel's size or smaller. Hit: 6 (1d6 + 3) bludgeoning damage, and the target is grappled (escape DC 13 with disadvantage). Until the grapple ends, the target takes 10 (2d6 + 3) bludgeoning damage at the start of each of the meazel's turns. The meazel can't make weapon attacks while grappling a creature in this way.

___Shortsword___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage, plus 3 (1d6) necrotic damage.

___Shadow Teleport (Recharge 5-6)___ The meazel, any equipment it is wearing or carrying, and any creature it is grappling teleport to an unoccupied space within 500 feet of it, provided that the starting space and the destination are in dim light or darkness. The destination must be a place the meazel has seen before, but it need not be within line of sight. If the destination space is occupied, the teleportation leads to the nearest unoccupied space.
Any other creature the meazel teleports becomes cursed by shadow for 1 hour. Until this curse ends, every undead and every creature native to the Shadowfell within 300 feet of the cursed creature can sense it, which prevents that creature from hiding from them.