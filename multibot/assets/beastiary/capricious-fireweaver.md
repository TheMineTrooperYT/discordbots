"Capricious Fireweaver";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1 (200 XP)"
_languages_: "Any two languages"
_skills_: "Arcana +3, History +3, Persuasion +5"
_senses_: "passive Perception 11"
_saving_throws_: "Con +4, Cha +5"
_speed_: "30 ft."
_hit points_: "22 (4d6 + 8)"
_armor class_: "12 (15 with <i>mage armor</i>)"
_stats_: | 8 (-1) | 14 (+2) | 15 (+2) | 12 (+1) | 12 (+1) | 16 (+3) |

___Spellcasting.___ The flameweaver is a 3th-level
spellcaster. Its spellcasting ability is Charisma (spell
save DC 13, +5 to hit with spell attacks). The
flameweaver has the following sorcerer spells
prepared:

* Cantrips (at will): _firebolt, prestidigitation, shocking grasp_

* 1st level (4 slots): _burning hands, detect magic, mage armor, magic missile_

* 2nd level (2 slots): _flaming sphere, scorching ray, suggestion_

___Sorcery Points.___ The flameweaver has 3 sorcery
points. It can spend 1 or more sorcery points as a
bonus action to gain one of the following benefits:
* Quickened Spell. When the sorcerer casts a spell
that has a casting time of an action, it can spend
2 sorcery points to change the casting time to 1
bonus action instead.
* Subtle Spell. When the sorcerer casts a spell, it
can spend 1 sorcery point to cast the spell
without any somatic or verbal components.

___Wild Magic.___ When the fireweaver casts a spell of 1st
level or higher, roll a d10. On a result of 1, roll on the
Wild Magic Surge table (PHB Pg. 104) immediately
after the spell is cast to create a random effect.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +4 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 4
(1d4 + 2) piercing damage.
