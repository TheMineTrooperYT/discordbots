"Selang";;;_size_: Medium fey
_alignment_: chaotic evil
_challenge_: "4 (1100 XP)"
_languages_: "Common, Elvish, Sylvan, Void Speech"
_skills_: "Perception +6, Performance +8"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +4, Con +6, Cha +6"
_damage_immunities_: "acid, lightning"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "40 ft."
_hit points_: "76 (9d8 + 36)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 15 (+2) | 18 (+4) | 12 (+1) | 14 (+2) | 19 (+4) |

___Innate Spellcasting.___ The selang's innate spellcasting ability is Charisma (spell save DC 14). It can innately cast the following spells, requiring no material components:

At will: dancing lights, minor illusion

3/day each: alter self, fear, sleep, suggestion

**Actions**

___Multiattack.___ The selang makes two dagger attacks or two short bow attacks.

___Dagger.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 6 (1d4 + 4) piercing damage, plus sleep poison.

___Short Bow.___ Ranged Weapon Attack: +4 to hit, range 80/320, one target. Hit: 5 (1d6 + 2) piercing damage plus sleep poison.

___Sleep Poison.___ An injured creature must succeed on a DC 14 Constitution saving throw or be poisoned for 2d6 rounds. A creature poisoned in this way is unconscious. An unconscious creature wakes if it takes damage, or if a creature uses its action to shake it awake.

___Alien Piping.___ A selang can confuse and injure its enemies by playing weird, ear-bending harmonies on alien pipes, made from the beaks, cartilage, and throat sacs of a dorreq. When the selang plays a tune on these pipes, all creatures within 60 feet must make a successful DC 14 Wisdom saving throw or be affected by contagion, confusion, irresistible dance, or hideous laughter, depending on what alien and otherworldly music the dark satyr chooses to play. A creature that saves successfully against this psychic effect is immune to the piping for 24 hours. The selang can use each of these spell-like effects once per day.

