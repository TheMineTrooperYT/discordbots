"Young Silver Dragon";;;_size_: Large dragon
_alignment_: lawful good
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 30 ft., darkvision 120 ft."
_skills_: "Arcana +6, History +6, Perception +8, Stealth +4"
_damage_immunities_: "cold"
_saving_throws_: "Dex +4, Con +9, Wis +4, Cha +8"
_speed_: "40 ft., fly 80 ft."
_hit points_: "168 (16d10+80)"
_armor class_: "18 (natural armor)"
_stats_: | 23 (+6) | 10 (0) | 21 (+5) | 14 (+2) | 11 (0) | 19 (+4) |

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 17 (2d10 + 6) piercing damage.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) slashing damage.

___Breath Weapons (Recharge 5-6).___ The dragon uses one of the following breath weapons.

Cold Breath. The dragon exhales an icy blast in a 30-foot cone. Each creature in that area must make a DC 17 Constitution saving throw, taking 54 (12d8) cold damage on a failed save, or half as much damage on a successful one.

Paralyzing Breath. The dragon exhales paralyzing gas in a 30-foot cone. Each creature in that area must succeed on a DC 17 Constitution saving throw or be paralyzed for 1 minute. A creature can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.