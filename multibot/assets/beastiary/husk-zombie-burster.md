"Husk Zombie Burster";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "understands the languages it knew in life but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 8"
_saving_throws_: "Con +5, Wis +0"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "35 ft."
_hit points_: "37 (5d8 + 15)"
_armor class_: "10"
_stats_: | 16 (+3) | 10 (+0) | 16 (+3) | 3 (-4) | 6 (-2) | 5 (-3) |

___Curse of the Husk.___ A humanoid slain by a melee attack from the zombie revives as a husk zombie on its next turn.

___Undead Fortitude.___ If damage reduces the zombie to 0 hit points, it must make a Constitution saving throw with a DC of 5 + the damage taken, unless the damage is radiant or from a critical hit. On a success, the zombie drops to 1 hit point instead.

**Actions**

___Multiattack.___ The zombie makes two claw attacks. For each of these attacks that reduces a creature to 0 hit points, the zombie can make an additional claw attack.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Burst.___ The zombie explodes and is destroyed. Each creature within 5 feet of it must make a DC 12 Constitution saving throw, taking 14 (4d6) poison damage on a failed save, or half as much damage on a successful one. A humanoid creature killed by this damage rises as a husk zombie after 1 minute.
