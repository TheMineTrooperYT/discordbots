"Derro";;;_page_number_: 158
_size_: Small humanoid (derro)
_alignment_: chaotic evil
_challenge_: "1/4 (50 XP)"
_languages_: "Dwarvish, Undercommon"
_senses_: "darkvision 120 ft., passive Perception 7"
_skills_: "Stealth +4"
_speed_: "30 ft."
_hit points_: "13  (3d6 + 3)"
_armor class_: "13 (leather armor)"
_stats_: | 10 (0) | 14 (+2) | 12 (+1) | 11 (0) | 5 (-2) | 9 (0) |

___Magic Resistance.___ The derro has advantage on saving throws against spells and other magical effects.

___Sunlight Sensitivity.___ While in sunlight, the derro has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Hooked Spear___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 3 (1d6) piercing damage. If the target is Medium or smaller, the derro can choose to deal no damage and knock it prone.

___Light Crossbow___ Ranged Weapon Attack: +4 to hit, range 80/320 ft., one target. Hit: 6 (1d8 + 2) piercing damage.