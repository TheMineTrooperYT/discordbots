"Flutterflesh";;;_size_: Large undead
_alignment_: chaotic evil
_challenge_: "12 (8400 XP)"
_languages_: "Common, Darakhul"
_skills_: "Deception +4, Perception +5, Stealth +8"
_senses_: "darkvision 240 ft., passive Perception 15"
_saving_throws_: "Str +4, Dex +8"
_damage_vulnerabilities_: "radiant"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "cold, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, paralyzed, exhaustion, poison, stunned, unconscious"
_speed_: "10 ft., Fly 60 ft."
_hit points_: "187 (22d10 + 66)"
_armor class_: "16 (natural armor)"
_stats_: | 11 (+0) | 18 (+4) | 17 (+3) | 12 (+1) | 13 (+1) | 10 (+0) |

___Magic Weapons.___ The flutterflesh's attacks are magical.

___Turn Resistance.___ The flutterflesh has advantage on saving throws against any effect that turns undead.

___Creeping Death.___ A creature that starts its turn within 30 feet of the flutterflesh must make a successful DC 15 Constitution saving throw or take 14 (4d6) necrotic damage.

___Regeneration.___ The flutterflesh regains 10 hit points at the start of its turn. If the flutterflesh takes radiant or fire damage, this trait doesn't function at the start of its next turn. The flutterflesh dies only if it starts its turn with 0 hit points and doesn't regenerate.

**Actions**

___Multiattack.___ The flutterflesh makes two bone spur attacks or two tormenting gaze attacks.

___Bone Spur.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 17 (2d12 + 4) slashing damage plus 11 (2d10) necrotic damage. If both attacks hit a single creature in the same turn, it is grappled (escape DC 10). As a bonus action, the flutterflesh can choose whether this attack does bludgeoning, piercing, or slashing damage.

___Tormenting Gaze.___ A target creature within 120 feet and able to see the flutterflesh must make a DC 15 Wisdom saving throw.  The creature takes 18 (4d8) psychic damage and becomes paralyzed for 1d4 rounds on a failure, and half damage and isn't paralyzed with a success . Tormenting gaze can't be used against the same target twice in a single turn.

___Slash.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage. On a critical hit, the target takes an additional 27 (5d10) slashing damage and must make a DC 12 Constitution saving throw. On a failure, the flutterflesh lops off and absorbs one of the target's limbs (chosen randomly) and heals hit points equal to the additional slashing damage it inflicted.

