"Sibriex";;;_page_number_: 137
_size_: Huge fiend (demon)
_alignment_: chaotic evil
_challenge_: "18 (20,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft., passive Perception 23"
_skills_: "Arcana +13, History +13, Perception +13"
_damage_immunities_: "poison"
_saving_throws_: "Int +13, Cha +13"
_speed_: "0 ft., fly 20 ft. (hover)"
_hit points_: "150  (12d12 + 72)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 10 (0) | 3 (-3) | 23 (+6) | 25 (+7) | 24 (+7) | 25 (+7) |

___Contamination.___ The sibriex emits an aura of corruption 30 feet in every direction. Plants that aren't creatures wither in the aura, and the ground in it is difficult terrain for other creatures. Any creature that starts its turn in the aura must succeed on a DC 20 Constitution saving throw or take 14 (4d6) poison damage. A creature that succeeds on the save is immune to this sibriex's Contamination for 24 hours.

___Innate Spellcasting.___ The sibriex's innate spellcasting ability is Charisma (spell save DC 21). It can innately cast the following spells, requiring no material components:

* At will: _charm person, command, dispel magic, hold monster_

* 3/day: _feeblemind_

___Legendary Resistance (3/Day).___ If the sibriex fails a saving throw, it can choose to succeed instead.

___Magic Resistance.___ The sibriex has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack___ The sibriex uses Squirt Bile once and makes three attacks using its chain, bite, or both.

___Chain___ Melee Weapon Attack: +6 to hit, reach 15 ft., one target. Hit: 20 (2d12 + 7) piercing damage.

___Bite___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (2d8) piercing damage plus 9 (2d8) acid damage.

___Squirt Bile___ The sibriex targets one creature it can see within 120 feet of it. The target must succeed on a DC 20 Dexterity saving throw or take 35 (10d6) acid damage.

___Warp Creature___ The sibriex targets up to three creatures it can see within 120 feet of it. Each target must make a DC 20 Constitution saving throw. On a successful save, a creature becomes immune to this sibriex's Warp Creature. On a failed save, the target is poisoned, which causes it to also gain 1 level of exhaustion. While poisoned in this way, the target must repeat the saving throw at the start of each of its turns. Three successful saves against the poison end it, and ending the poison removes any levels of exhaustion caused by it. Each failed save causes the target to suffer another level of exhaustion. Once the target reaches 6 levels of exhaustion, it dies and instantly transforms into a living abyssal wretch under the sibriex's control. The transformation of the body can be undone only by a wish spell. (See 'Other' tab for variant)

**Legendary** Actions

The sibriex can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The sibriex regains spent legendary actions at the start of its turn.

___Cast a Spell___ The sibriex casts a spell.

___Spray Bile___ The sibriex uses Squirt Bile.

___Warp (Costs 2 Actions)___ The sibriex uses Warp Creature.
