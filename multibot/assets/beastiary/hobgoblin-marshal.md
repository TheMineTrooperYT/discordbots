"Hobgoblin Marshal";;;_size_: Medium humanoid (goblinoid)
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Goblin"
_senses_: "darkvision 60 ft., Passive Perception 10"
_speed_: "30 ft."
_hit points_: "39 (6d8 + 12)"
_armor class_: "17 (half plate)"
_stats_: | 15 (+2) | 14 (+2) | 14 (+2) | 12 (+1) | 10 (+0) | 13 (+1) |

___Martial Advantage.___ Once per turn, the hobgoblin
can deal an extra 10 (3d6) damage to a creature it hits
with a weapon attack if that creature is within 5 feet
of an ally of the hobgoblin that isn’t incapacitated.

**Actions**

___Multiattack.___ The hobgoblin makes two melee
weapon attacks.

___Spear.___ Melee or Ranged Weapon Attack: +4 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d6 + 2) piercing damage, or 6 (1d8 +2) piercing damage
is used with two hands to make a melee attack.

___Fiery Longbow.___ Ranged Weapon Attack: +4 to hit,
range 50/600 ft., one target. Hit: 6 (1d8 + 2) piercing
damage and 2 (1d4) fire damage. If the target is a creature
or a flammable object, it ignites and takes 2 (1d4)
fire damage at the start of each of its turns. A creature
can end this damage by using its action to make a DC
10 Dexterity check to extinguish the flames.

___Alchemist’s Fire.___ Improvised Weapon Attack: +2
to hit, range 20 ft., one target. Hit: 2 (1d4) fire
damage. The target takes 2 (1d4) fire damage at the
start of each of its turns. A creature can end this
damage by using its action to make a DC 10 Dexterity
check to extinguish the flames.

___Leadership (Recharges after a Short or Long Rest).___ For 1 minute, the hobgoblin can utter a special
command or warning whenever a nonhostile
creature that it can see within 30 feet of it makes an
attack roll or a saving throw. The creature can add
a d4 to its roll provided it can hear and understand
the hobgoblin. A creature can benefit from only
one Leadership die at a time. This effect ends if the
hobgoblin is incapacitated.
