"Elder Oblex";;;_page_number_: 219
_size_: Huge ooze
_alignment_: lawful evil
_challenge_: "10 (5900 XP)"
_languages_: "Common plus six more"
_senses_: "blindsight 60 ft. (blind beyond this distance), passive Perception 15"
_skills_: "Arcana +10, Deception +8, History +10, Nature +10, Perception +5, Religion +10"
_saving_throws_: "Int +10, Cha +8"
_speed_: "20 ft."
_hit points_: "115  (10d12 + 50)"
_armor class_: "16"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, prone"
_stats_: | 15 (+2) | 16 (+3) | 21 (+5) | 22 (+6) | 13 (+1) | 18 (+4) |

___Amorphous.___ The oblex can move through a space as narrow as 1 inch wide without squeezing.

___Aversion to Fire.___ If the oblex takes fire damage, it has disadvantage on attack rolls and ability checks until the end of its next turn.

___Innate Spellcasting.___ The oblex's innate spellcasting ability is Intelligence (spell save DC 18). It can innately cast the following spells, requiring no material components:

* At will: _charm person _(as 5th-level spell)_, detect thoughts, hold person_

* 3/day each: _confusion, dimension door, dominate person, fear, hallucinatory terrain, hold monster, hypnotic pattern, telekinesis_

___Sulfurous Impersonation.___ As a bonus action, the oblex can extrude a piece of itself that assumes the appearance of one Medium or smaller creature whose memories it has stolen. This simulacrum appears, feels, and sounds exactly like the creature it impersonates, though it smells faintly of sulfur. The oblex can impersonate 2d6 + 1 different creatures, each one tethered to its body by a strand of slime that can extend up to 120 feet away. For all practical purposes, the simulacrum is the oblex, meaning the oblex occupies its space and the simulacrum's space simultaneously. The slimy tether is immune to damage, but it is severed if there is no opening at least 1 inch wide between the oblex's main body and the simulacrum. The simulacrum disappears if the tether is severed.

**Actions**

___Multiattack___ The elder oblex makes two pseudopod attacks and uses Eat Memories.

___Pseudopod___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 17 (4d6 + 3) bludgeoning damage plus 7 (2d6) psychic damage.

___Eat Memories___ The oblex targets one creature it can see within 5 feet of it. The target must succeed on a DC 18 Wisdom saving throw or take 44 (8d10) psychic damage and become memory drained until it finishes a short or long rest or until it benefits from the greater restoration or heal spell. Constructs, oozes, plants, and undead succeed on the save automatically.
While memory drained, the target must roll a d4 and subtract the number rolled from any ability check or attack roll it makes. Each time the target is memory drained beyond the first, the die size increases by one: the d4 becomes a d6, the d6 becomes a d8, and so on until the die becomes a d20, at which point the target becomes unconscious for 1 hour. The effect then ends.
When an oblex causes a target to become memory drained, the oblex learns all the languages the target knows and gains all its proficiencies, except any saving throw proficiencies.
