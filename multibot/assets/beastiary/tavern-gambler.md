"Tavern Gambler";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "Any two languages and Thieves' Cant"
_skills_: "Acrobatics +6, Insight +9, Persuasion +6, Sleight of Hand +9, Stealth +6"
_senses_: "passive Perception 13"
_saving_throws_: "Dex +6, Int +3"
_speed_: "30 ft."
_hit points_: "55 (10d8 + 10)"
_armor class_: "15 (studded leather)"
_stats_: | 10 (+0) | 17 (+3) | 12 (+1) | 10 (+0) | 14 (+2) | 16 (+3) |

___Cunning Action.___ As a bonus action, the gambler can
take the Dash, Disengage, or Hide action.

___Lucky Charm.___ When the gambler rolls a natural 1 for
an attack roll or skill check, it can reroll and take the
second result instead. The gambler also has
advantage on skill checks for dice and card games.

___Sneak Attack (1/Turn).___ The gambler deals an extra
14 (4d6) damage when it hits a target with a
weapon attack and has advantage on the attack roll,
or when the target is within 5 feet of an ally of the
gambler that isn't incapacitated and the gambler
doesn't have disadvantage on the attack roll.

**Actions**

___Multiattack.___ The gambler makes two attacks with its
dagger.

___Dagger.___ Melee or Ranged Weapon Attack: +6 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 5
(1d4 + 3) piercing damage.

**Reactions**

___Uncanny Dodge (1/Turn).___ The gambler halves the
damage that it takes from an attack that hits it. The
gambler must be able to see the attacker.
