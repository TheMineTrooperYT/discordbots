"Bowing Bird";;;_size_: Medium monstrosity
_alignment_: lawful neutral
_challenge_: "1/8 (25 XP)"
_languages_: "understands simple words in Common, but can't speak"
_skills_: "Perception +4"
_senses_: "passive Perception 14"
_speed_: "20 ft., fly 50 ft."
_hit points_: "22 (4d8 + 4)"
_armor class_: "12"
_stats_: | 13 (+1) | 14 (+2) | 12 (+1) | 5 (-3) | 15 (+2) | 9 (-1) |

___Keen Sight.___ The bird has advantage on Wisdom
(Perception) checks that rely on sight.

___Natural Decorum.___ Creatures of Small size or larger
that the bird can see that move to within 5 feet of
the bird without first bowing to it (or performing
some analogous gesture, if unable to bow) provoke
an attack of opportunity from the bird.

**Actions**

___Bill.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one
target. Hit: 3 (1d4 + 1) bludgeoning damage.
