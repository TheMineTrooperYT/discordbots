"Oregg Steeltwister";;;_size_: Medium humanoid (orc)
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "Orc, Common"
_skills_: "Arcana +5, Athletics +5"
_senses_: "darkvision 60 ft., passive Perception 10"
_saving_throws_: "Str +5, Wis +3"
_speed_: "30 ft."
_hit points_: "82 (11d8 + 33)"
_armor class_: "16 (chainmail)"
_stats_: | 15 (+2) | 12 (+1) | 16 (+3) | 9 (-1) | 11 (+0) | 18 (+4) |

___Aggressive.___ As a bonus action, the orc can
move up to its speed toward a hostile creature it
can see.

___Rune-Tattooed.___ Whenever Oregg casts a spell of
1st level or higher, he gains a number of temporary hit points equal to Xd10, where X is the level
of the spell.

___Spellcasting.___ Oregg Steeltwister is a 5th-level
spellcaster. His spellcasting ability is Charisma
(spell save DC 15, +7 to hit with spell attacks). He
has the following sorcerer spells prepared:

* Cantrips (at will): _firebolt, message, poison spray, true strike_

* 1st level (4 slots): _magic missile, shield_

* 2nd level (3 slots): _heat metal_

* 3rd level (2 slots): _fly, fireball_


**Actions**

___Multiattack.___ Oregg makes two longsword attacks
and can cast a cantrip as a bonus action.

___Longsword.___ Melee Weapon Attack: +5 to hit,
reach 5 ft., one target. Hit: 7 (1d10 + 2) slashing damage.

___Firebolt.___ Ranged Spell Attack: +7 to hit, range
120 ft., one target. Hit: 11 (2d10) fire damage.

___Poison Spray.___ One creature within 10 feet of
Oregg must make a successful DC 15 Constitution
saving throw or take 13 (2d12) poison damage.
