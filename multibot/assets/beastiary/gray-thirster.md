"Gray Thirster";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "understands all languages it knew in life but can't speak"
_skills_: "Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "fire, poison"
_damage_resistances_: "bludgeoning, necrotic"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_speed_: "30 ft."
_hit points_: "39 (6d8 + 12)"
_armor class_: "13"
_stats_: | 12 (+1) | 16 (+3) | 15 (+2) | 6 (-2) | 12 (+1) | 14 (+2) |

___Thirst.___ The gray thirster projects a 30-foot aura of desiccating thirst. The first time a creature enters the aura on its turn, or when it starts its turn in the aura, it must make a successful DC 12 Constitution saving throw or gain one level of exhaustion. If the saving throw is successful, the creature is immune to the gray thirster's Thirst for the next 24 hours.

**Actions**

___Multiattack.___ The gray thirster makes two claw attacks and one Withering Turban attack

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 6 (1d6 + 3) slashing damage.

___Withering Turban.___ Melee Weapon Attack: +5 to hit, reach 10 ft., one creature. Hit: 5 (1d4 + 3) necrotic damage. If the target failed a saving throw against the Thirst trait at any point in this encounter, its hit point maximum is reduced by an amount equal to the damage it took from this attack. This reduction lasts until the target has no exhaustion levels.

___Drought (1/Day).___ The gray thirster draws the moisture from a 20-foot radius area centered on itself. Nonmagical water and other liquids in this area turn to dust. Each creature that is neither undead nor a construct in the area must make a DC 13 Constitution saving throw, taking 9 (2d8) necrotic damage on a failure, and half damage on a success. Plants, oozes, and creatures with the Amphibious, Water Breathing, or Water Form traits have disadvantage on this saving throw. Liquids carried by a creature that makes a successful saving throw are not destroyed.

