"Death";;;_size_: Medium construct
_alignment_: lawful neutral
_challenge_: "6 (2,300 XP)"
_languages_: "all those of the creature who summoned it"
_senses_: "truesight 60 ft., passive Perception 15"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "112 (15d8 + 45)"
_armor class_: "17 (natural armor)"
_stats_: | 16 (+3) | 15 (+2) | 16 (+3) | 13 (+1) | 14 (+2) | 15 (+2) |

___The Law of Death.___ At the start of each of
Death’s turns, undead within 10 feet of it take 10
force damage.

___Chaos Vulnerability.___ The Inexorables have disadvantage
on all saving throws against spells.

___Inexorable.___ The Inexorables are immune to any
effects that would slow them or deny them
actions or movement.

___Innate Spellcasting.___ Death’s innate spellcasting
ability is Charisma (spell save DC 13). It can
innately cast the following spells, requiring no
material components:

* 3/day: _death ward_

**Actions**

___Multiattack.___ Death makes three slam attacks
or makes one slam attack and one Death to
Undeath attack.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5 ft.,
one target. Hit: 12 (2d8 + 3) bludgeoning damage.

___Death to Undeath.___ Death points at an undead
creature within 60 feet. The target must make
a DC 13 Wisdom saving throw or take 33 (6d10)
force damage. On a successful save, the target
takes half that damage.
