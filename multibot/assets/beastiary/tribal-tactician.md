"Tribal Tactician";;;_size_: Medium humanoid (any race)
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "any two languages"
_skills_: "Athletics +5, Survival +3"
_senses_: "passive Perception 11"
_saving_throws_: "Str +5, Con +5"
_speed_: "30 ft."
_hit points_: "60 (8d8 + 24)"
_armor class_: "14 (hide)"
_stats_: | 16 (+3) | 14 (+2) | 16 (+3) | 14 (+2) | 12 (+1) | 10 (+0) |

___Combat Maneuvers (3/Short Rest).___ Tacticians have
trained all their life to excel in combat and as a
result have mastered special combat maneuvers
that they can use in combat. Whenever the
tactician makes a melee weapon attack, it can
choose to execute one of these maneuvers to add
additional effects to the attack. In addition to these
other effects, all maneuvers cause the attacks to
deal an additional 1d8 damage. Each tactician has
two random maneuvers from the list below
available for use:

* Crippling Strike – The target's speed is 0 on its next turn.
* Dizzying Strike – Concentration saving throws made as a result of this attack's damage are made at disadvantage.
* Precise Strike – This attack is made with advantage.
* Weakening Strike – The next weapon attack made by the target deals half damage (rounded down).

**Actions**

___Multiattack.___ The tactician makes two attacks with its
battleaxe and one with its unarmed strike..

___Battleaxe.___ Melee Weapon Attack: +5 to hit, reach 5
ft., one target. Hit: 7 (1d8 + 3) slashing damage, or
8 (1d10 + 3) slashing damage if used with two
hands.

___Unarmed Strike.___ Melee Weapon Attack: +4 to hit,
reach 5 ft., one target. Hit: 5 (1d6 + 2) bludgeoning
damage.

___Handaxe.___ Ranged Weapon Attack: +5 to hit, range
20/60 ft., one target. Hit: 5 (1d6 + 3) slashing
damage.
