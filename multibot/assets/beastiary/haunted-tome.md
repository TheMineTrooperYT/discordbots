"Haunted Tome";;;_size_: Tiny construct
_alignment_: chaotic evil
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_senses_: "blindsight 30 ft., passive Perception 8"
_speed_: "0 ft., fly 15 ft. (hover)"
_hit points_: "5 (2d4)"
_armor class_: "11"
_stats_: | 3 (-4) | 12 (+1) | 10 (+0) | 14 (+2) | 7 (-2) | 4 (-3) |

___Cursed Contents.___ Any creature that writes its name
within this tome becomes affected by a streak of
terrible luck. When any of those creatures are
making a critical skill check (DM's discretion), that
creature makes the roll with disadvantage as some
random misfortune befalls them. These misfortunes
automatically become documented within the
pages of the tome along with humorous and
degrading remarks regarding the outcome. When
the tome is destroyed or someone else writes their
name in the book, the curse is lifted.

___False Appearance.___ While the tome remains
motionless, it is indistinguishable from a normal
book. It will always brandish a name such as _Power
and Wealth_ or _Rags to Riches_ and will appear in
pristine condition. The contents of the book
encourage a reader to write its name within the
pages with promises of power.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft.,
one target. Hit: 3 (1d4 + 2) piercing damage.
