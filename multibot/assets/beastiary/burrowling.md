"Burrowling";;;_size_: Small humanoid
_alignment_: lawful neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_skills_: "Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "30 ft., burrow 10 ft."
_hit points_: "27 (6d6 + 6)"
_armor class_: "13"
_stats_: | 10 (+0) | 16 (+3) | 12 (+1) | 9 (-1) | 12 (+1) | 13 (+1) |

___Burrow Awareness.___ A burrowling gets advantage on Perception checks if at least one other burrowling is awake within 10 feet.

___Pack Tactics.___ The burrowling has advantage on attack rolls when its target is adjacent to at least one other burrowling that's capable of attacking.

**Actions**

___Multiattack.___ The burrowling makes one bite attack and one claw attack.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Sling.___ Ranged Weapon Attack: +5 to hit, range 30/120 ft., one target. Hit: 5 (1d4 + 3) bludgeoning damage.

