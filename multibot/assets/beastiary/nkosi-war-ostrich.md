"Nkosi War Ostrich";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_senses_: ", passive Perception 10"
_speed_: "60 ft."
_hit points_: "42 (5d10 + 15)"
_armor class_: "11"
_stats_: | 15 (+2) | 12 (+1) | 16 (+3) | 2 (-4) | 10 (+0) | 5 (-3) |

___Standing Leap.___ The ostrich can jump horizontally up to 20 feet and vertically up to 10 feet, with or without a running start.

___Battle Leaper.___ If a riderless ostrich jumps at least 10 feet and lands within 5 feet of a creature, it has advantage on attacks against that creature this turn.

**Actions**

___Multiattack.___ The ostrich makes two kicking claw attacks.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) bludgeoning damage.

