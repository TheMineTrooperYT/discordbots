"Dretch";;;_size_: Small fiend (demon)
_alignment_: chaotic evil
_challenge_: "1/4 (50 XP)"
_languages_: "Abyssal, telepathy 60 ft. (works only with creatures that understand Abyssal)"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "20 ft."
_hit points_: "18 (4d6+4)"
_armor class_: "11 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 11 (0) | 11 (0) | 12 (+1) | 5 (-3) | 8 (-1) | 3 (-4) |

**Actions**

___Multiattack.___ The dretch makes two attacks: one with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 3 (1d6) piercing damage.

___Claws.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 5 (2d4) slashing damage.

___Fetid Cloud (1/Day).___ A 10-foot radius of disgusting green gas extends out from the dretch. The gas spreads around corners, and its area is lightly obscured. It lasts for 1 minute or until a strong wind disperses it. Any creature that starts its turn in that area must succeed on a DC 11 Constitution saving throw or be poisoned until the start of its next turn. While poisoned in this way, the target can take either an action or a bonus action on its turn, not both, and can't take reactions.