"Mbielu";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Perception +3"
_senses_: ", passive Perception 13"
_speed_: "30 ft., swim 20 ft."
_hit points_: "95 (10d12 + 30)"
_armor class_: "15 (natural armor)"
_stats_: | 19 (+4) | 14 (+2) | 16 (+3) | 2 (-4) | 12 (+1) | 6 (-2) |

___Toxic Skin.___ A creature that touches the mbielu or hits it with a melee attack exposes itself to the mbielu's poisonous skin. The creature must succeed on a DC 13 Constitution saving throw or be poisoned for 1 minute. While poisoned in this way, a creature also suffers disadvantage on Intelligence, Wisdom, and Charisma saving throws.

**Actions**

___Tail.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 20 (3d10 + 4) bludgeoning damage. If the target is a creature, it must succeed on a DC 14 Strength saving throw or be knocked prone.

**Reactions**

___Rollover.___ If the mbielu is grappled by a Large creature, it rolls on top of the grappler and crushes it. The mbielu automatically escapes from the grapple and the grappler takes 20 (3d10 + 4) bludgeoning damage.

