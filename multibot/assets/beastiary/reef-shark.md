"Reef Shark";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_senses_: "blindsight 30 ft."
_skills_: "Perception +2"
_speed_: "swim 40 ft."
_hit points_: "22 (4d8+4)"
_armor class_: "12 (natural armor)"
_stats_: | 14 (+2) | 13 (+1) | 13 (+1) | 1 (-5) | 10 (0) | 4 (-3) |

___Pack Tactics.___ The shark has advantage on an attack roll against a creature if at least one of the shark's allies is within 5 ft. of the creature and the ally isn't incapacitated.

___Water Breathing.___ The shark can breathe only underwater.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage.