"Virtue";;;_size_: Medium celestial
_alignment_: any good alignment
_challenge_: "7 (2,900 XP)"
_languages_: "all, telepathy 60 ft."
_senses_: "truesight 60 ft., passive Perception 19"
_saving_throws_: "Int +6, Wis +6, Cha +8"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "radiant; bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: " charmed, exhaustion, frightened, poisoned"
_speed_: "0 ft., fly 40 ft. (hover)"
_hit points_: "97 (13d8 + 39)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+2) | 13 (+1) | 16 (+3) | 16 (+3) | 17 (+3) | 20 (+5) |

___Magic Resistance.___ The Virtue has advantage on
saving throws against spells and other magical
effects from evil characters and sources.

___Aura of Protection Against Evil.___ Evil creatures
have disadvantage on attack rolls against all allies
within 5 feet of the Virtue. Allies in this area can’t
be charmed, frightened, or possessed by evil creatures. If an ally is already charmed, frightened, or
possessed by evil magic, the ally has advantage on
any new saving throw against the relevant effect.

**Actions**

___Sing.___ At the start of each of its turns, the Virtue
chooses a chorus below and sings it. Only creatures who can hear the Virtue are affected by
its chorus. It can only sing one chorus at a time.
Any damage it takes forces it to make a Constitution saving throw to maintain the chorus. The
DC equals 10 or half the damage taken, whichever
number is higher.

* ___Chorus of Succor.___ While the Virtue sings, each ally
may choose one condition they’re suffering from at
the start of their turn and end it.

* ___Chorus of Inspiration.___ While the Virtue sings, each
ally may add a d6 to the result of any attack roll
or saving throw, once per round. The ally can wait
until after they roll the d20 before deciding to use
the inspiration die, but must decide before the GM
says whether the roll succeeds or fails.

* ___Chorus of Retribution.___ While the Virtue sings, any
enemy who deals damage to one of the Virtue’s
allies immediately takes 5 thunder damage.

* ___Chorus of Damnation.___ While the Virtue sings, any
extraplanar evil creatures have disadvantage on
saving throws against spells that would send them
to another plane.
