"Dwarf Veteran";;;_page_number_: 999
_size_: Medium humanoid (dwarf)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "any one language (usually Common)"
_senses_: "darkvision, passive Perception 12"
_skills_: "Athletics +5, Perception +2"
_speed_: "30 ft."
_hit points_: "58  (9d8 + 18)"
_armor class_: "17 (splint)"
_damage_resistances_: "poison"
_stats_: | 16 (+3) | 13 (+1) | 14 (+2) | 10 (0) | 11 (0) | 10 (0) |

___Dwarven Resilience.___ The dwarf has advantage on saving throws against poison and has resistance to poison damage

**Actions**

___Multiattack___ The veteran makes two longsword attacks. If it has a shortsword drawn, it can also make a shortsword attack.

___Longsword___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage, or 8 (1d10 + 3) slashing damage if used with two hands.

___Shortsword___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Heavy Crossbow___ Ranged Weapon Attack: +3 to hit, range 100/400 ft., one target. Hit: 6 (1d10 + 1) piercing damage.
