"Axe Beak";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_speed_: "50 ft."
_hit points_: "19 (3d10+3)"
_armor class_: "11"
_stats_: | 14 (+2) | 12 (+1) | 12 (+1) | 2 (-4) | 10 (0) | 5 (-3) |

**Actions**

___Beak.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) slashing damage.