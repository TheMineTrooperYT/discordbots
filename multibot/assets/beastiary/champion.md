"Champion";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "9 (5,000 XP)"
_languages_: "any one language (usually Common)"
_skills_: "Athletics +9, Intimidation +5, Perception +6"
_saving_throws_: "Str +9, Con +6"
_speed_: "30 ft."
_hit points_: "143 (22d8+44)"
_armor class_: "18 (plate)"
_stats_: | 20 (+5) | 15 (+2) | 14 (+2) | 10 (0) | 14 (+2) | 12 (+1) |

___Indomitable (2/Day).___ The champion rerolls a failed saving throw.

___Second Wind (Recharges after a Short or Long Rest).___ As a bonus action, the champion can regain 20 hit points.

**Actions**

___Multiattack.___ The champion makes three attacks with its greatsword or its light crossbow.

___Greatsword.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 12 (2d6+5) slashing damage, plus 7 (2d6) slashing damage if the champion has more than half of its total hit points remaining.

___Light Crossbow.___ Ranged Weapon Attack: +6 to hit, range 80/320 ft., one target. Hit: 6 (1d8+2) piercing damage, plus 7 (2d6) piercing damage if the champion has more than half of its total hit points remaining.