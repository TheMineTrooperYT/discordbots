"Blood Hunter";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "any one language (usually Common)"
_skills_: "Acrobatics +4, Insight +6, Perception +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Str +7, Wis +6"
_speed_: "30 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "16 (half plate)"
_stats_: | 18 (+4) | 12 (+1) | 15 (+2) | 9 (-1) | 16 (+3) | 11 (+0) |

___Blood Curse of Binding (1/Day).___ As a bonus action, the blood hunter targets one creature it can see within 30 feet of it. The target must succeed on a DC 14 Strength saving throw or have its speed reduced to 0 and be unable to take reactions. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Blood Frenzy.___ The blood hunter has advantage on melee attack rolls against any creature that doesn’t have all its hit points.

___Innate Spellcasting (1/Day).___ The blood hunter can innately cast _hex_. Its innate spellcasting ability is Intelligence.

___Magic Resistance.___ The blood hunter has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The blood hunter attacks twice with a weapon.

___Greatsword.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) slashing damage plus 3 (1d6) fire damage.

___Heavy Crossbow.___ Ranged Weapon Attack: +4 to hit, range 100/400 ft., one target. Hit: 6 (1d10 + 1) piercing damage.
