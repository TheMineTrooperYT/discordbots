"Magma Ooze";;;_size_: Large ooze
_alignment_: unaligned
_challenge_: "6 (2,300 XP)"
_languages_: "--"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 8"
_damage_immunities_: "acid, cold, fire, lightning, poison, psychic, slashing"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, prone"
_speed_: "20 ft., climb 20 ft."
_hit points_: "85 (9d10 + 36)"
_armor class_: "7"
_stats_: | 16 (+3) | 4 (-3) | 18 (+4) | 1 (-5) | 6 (-2) | 1 (-5) |

___Amorphous.___ The magma ooze can move through a space as narrow as
1 inch wide without squeezing.

___Superheated.___ Creatures that touch the magma ooze take 7 (2d6)
fire damage. Any nonmagical weapon used to attack the magma ooze
melts and warps. After dealing damage, the weapon takes a permanent
and cumulative –1 penalty to damage rolls. If its penalty drops to –5,
the weapon is destroyed. Nonmagical ammunition that hits the ooze is
destroyed after dealing damage.

The ooze can melt through 2-inch-thick, nonmagical wood or metal in
1 round.

___Spider Climb.___ The magma ooze can climb difficult surfaces, including
upside down on ceilings, without needing to make an ability check.

**Actions**

___Pseudopod.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit:
13 (3d6 + 3) bludgeoning damage plus 28 (8d6) fire damage. In addition,
any nonmagical armor worn by the target is partially burned and melted
and takes a permanent and cumulative –1 penalty to the AC it offers. The
armor is destroyed if the penalty reduces its AC to 10.

**Reactions**

___Split.___ When a magma ooze that is Medium or larger is subjected to
slashing damage, it splits into two new magma oozes if it has at least 10
hit points. Each new magma ooze has hit points equal to half the original
ooze’s, rounded down. New oozes are one size smaller than the original
magma ooze.
