"Giant Leech";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "--"
_senses_: "blindsight 30 ft., passive Perception 10"
_speed_: "5 ft., swim 20 ft."
_hit points_: "26 (4d8 + 8)"
_armor class_: "11"
_stats_: | 11 (+0) | 12 (+1) | 14 (+2) | 2 (-4) | 10 (+0) | 1 (-5) |

___Vulnerability to Salt.___ A handful of salt burns a giant leech
as if it were a flask of acid, causing 1d6 acid damage per use.

**Actions**

___Blood Drain.___ Melee Weapon Attack: +3 to hit, reach 5 ft. one
creature. Hit: 4 (1d6 + 1) piercing damage, and the leech attaches
to the target. While attached, the leech doesn’t attack. Instead, at the
start of the leech’s turns, the target loses 5 (1d8 + 1) hit points due
to blood loss.

The leech can detach itself by spending 5 feet of its movement.
It does so after it drains 25 hit points of blood from the target or
the target dies. A creature, including the target, can use its action to
make a DC 10 Strength check to rip the leech off and make it detach.
