"Fate";;;_size_: Medium construct
_alignment_: lawful neutral
_challenge_: "8 (3,900 XP)"
_languages_: "all those of the creature who summoned it"
_senses_: "truesight 60 ft., passive Perception 15"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft., fly 30 ft. (hover)"
_hit points_: "136 (16d8 + 64)"
_armor class_: "19 (natural armor)"
_stats_: | 16 (+3) | 12 (+1) | 18 (+4) | 15 (+2) | 14 (+2) | 10 (+0) |

___The Law of Fate.___ Fate does not make attack rolls,
always using a 12 as its result (before applying
bonuses) and cannot be affected by advantage
or disadvantage. Fate does not roll damage,
instead always taking the average result, though
damage dice are listed so the average can be calculated.

___Chaos Vulnerability.___ The Inexorables have disadvantage
on all saving throws against spells.

___Inexorable.___ The Inexorables are immune to any
effects that would slow them or deny them
actions or movement.

**Actions**

___Multiattack.___ Fate makes three slam attacks.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5 ft.,
one target. Hit: 12 (2d8 + 3) bludgeoning damage.

___The Destiny Light (Recharge 5–6).___ Fate fires
a beam of Destiny Light at a target it can see
within 60 feet. The target must make a DC 13
Constitution saving throw. On a failed save, it
takes 36 (8d8) necrotic damage, and it only deals
average damage and uses 10 for its attack rolls
(before bonuses) for the next 10 minutes. On a
successful save, it takes half as much damage,
and its attack and damage rolls are not affected.
