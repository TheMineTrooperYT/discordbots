"Mage Hunter";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "Any two languages"
_skills_: "Arcana +4, Athletics +6, Deception +6, Insight +3"
_senses_: "passive Perception 10"
_saving_throws_: "Str +3, Dex +2, Con +5, Int +4, Wis +6, Cha +9"
_speed_: "30 ft."
_hit points_: "52 (7d10 + 14)"
_armor class_: "17 (splint mail)"
_stats_: | 16 (+3) | 8 (-1) | 14 (+2) | 12 (+1) | 10 (+0) | 16 (+3) |

___Divine Smite.___ When the hunter hits with a melee
weapon attack, it can expend a spell slot to deal an
addition 9 (2d8) radiant damage, in addition to the
weapon's damage. This damage increases by 1d8
radiant damage per spell slot level above 1st.

___Great Weapon Fighting Style.___ When the hunter rolls a
1 or 2 on a damage die for an attack with its pike, it
can reroll that die and take the second result.

___Spellcasting.___ The hunter is a 6th-level spellcaster. Its
spellcasting ability is Charisma (spell save DC 14, +6 to hit with spell attacks). The hunter has the
following paladin spells prepared:

* 1st level (4 slots): _bane, detect magic, hunter's mark, shield of faith_

* 2nd level (2 slots): _hold person, magic weapon, misty step_

___Vow of Enmity (1/Day).___ As a bonus action, the
hunter utters a vow against a creature it can see
within 10 feet. The hunter has advantage on attack
rolls against that creature for 1 minute or until the
creature falls to 0 hit points or falls unconscious.

**Actions**

___Multiattack.___ The hunter makes two attacks with its
pike.

___Pike.___ Melee Weapon Attack: +6 to hit, reach 10 ft.,
one target. Hit: 8 (1d10 + 3) piercing damage.

___Lay on Hands (2/Day).___ The hunter touches a creature
and restores 15 hit points to it.

**Reactions**

___Spell Slash.___ When a creature within 10 feet of the
hunter casts a spell, it can use its reaction to make
a weapon attack against that creature. If that
creature is concentrating on a spell, it has
disadvantage on the saving throw to maintain its
concentration.
