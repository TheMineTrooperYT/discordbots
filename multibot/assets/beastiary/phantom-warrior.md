"Phantom Warrior";;;_page_number_: 235
_size_: Medium undead
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "any languages it knew in life"
_senses_: "darkvision 60 ft."
_skills_: "Perception +2, Stealth +4"
_damage_immunities_: "cold, necrotic, poison"
_speed_: "30 ft."
_hit points_: "45 (6d8+18)"
_armor class_: "16"
_condition_immunities_: "charmed, exhaustion, frightened, grappled, paralyzed, petrified, poisoned, prone, restrained"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 16 (+3) | 11 (0) | 16 (+3) | 8 (-1) | 10 (0) | 15 (+2) |

___Ethereal Sight.___ The phantom warrior can see 60 feet into the Ethereal Plane when it is on the Material Plane, and vice versa.

___Incorporeal Movement.___ The phantom warrior can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Spectral Armor and Shield.___ The phantom warrior's AC accounts for its spectral armor and shield.

**Actions**

___Multiattack.___ The phantom warrior makes two attacks with its spectral longsword.

___Spectral Longsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8+3) force damage.

___Etherealness.___ The phantom warrior enters the Ethereal Plane from the Material Plane, or vice versa. It is visible on the Material Plane while it is in the Border Ethereal, and vice versa, yet it can't affect or be affected by anything on the other plane.
