"Giant Sea Horse";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_speed_: "0 ft., swim 40 ft."
_hit points_: "16 (3d10)"
_armor class_: "13 (natural armor)"
_stats_: | 12 (+1) | 15 (+2) | 11 (0) | 2 (-4) | 12 (+1) | 5 (-3) |

___Charge.___ If the sea horse moves at least 20 ft. straight toward a target and then hits it with a ram attack on the same turn, the target takes an extra 7 (2d6) bludgeoning damage. If the target is a creature, it must succeed on a DC 11 Strength saving throw or be knocked prone.

___Water Breathing.___ The sea horse can breathe only underwater.

**Actions**

___Ram.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6 + 1) bludgeoning damage.