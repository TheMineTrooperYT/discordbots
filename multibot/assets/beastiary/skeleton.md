"Skeleton";;;_size_: Medium undead
_alignment_: lawful evil
_challenge_: "1/4 (50 XP)"
_languages_: "understands all languages it spoke in life but can't speak"
_senses_: "darkvision 60 ft."
_speed_: "30 ft."
_hit points_: "13 (2d8+4)"
_armor class_: "13 (armor scraps)"
_damage_vulnerabilities_: "bludgeoning"
_condition_immunities_: "poisoned"
_stats_: | 10 (0) | 14 (+2) | 15 (+2) | 6 (-2) | 8 (-1) | 5 (-3) |

**Actions**

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Shortbow.___ Ranged Weapon Attack: +4 to hit, range 80/320 ft., one target. Hit: 5 (1d6 + 2) piercing damage.