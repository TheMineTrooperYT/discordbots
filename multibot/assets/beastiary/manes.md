"Manes";;;_size_: Small fiend (demon)
_alignment_: chaotic evil
_challenge_: "1/8 (25 XP)"
_languages_: "understands Abyssal but can't speak"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "20 ft."
_hit points_: "9 (2d6+2)"
_armor class_: "9 (natural armor)"
_condition_immunities_: "charmed, frightened, poisoned"
_damage_resistances_: "cold, fire, lightning"
_stats_: | 10 (0) | 9 (-1) | 13 (+1) | 3 (-4) | 8 (-1) | 4 (-3) |

**Actions**

___Claws.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 5 (2d4) slashing damage.