"Spectral Guardian";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "6 (2300 XP)"
_languages_: "understands the languages it knew in life but can't speak"
_senses_: "blindsight 10 ft., darkvision 60 ft., passive Perception 13"
_saving_throws_: "Dex +7, Wis +6"
_damage_immunities_: "cold, necrotic, poison"
_damage_resistances_: "acid, fire, lightning, thunder; bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, grappled, paralyzed, petrified, poisoned, prone, restrained"
_speed_: "0 ft., fly 60 ft. (hover)"
_hit points_: "110 (13d8 + 52)"
_armor class_: "14"
_stats_: | 6 (-2) | 18 (+4) | 18 (+4) | 11 (+0) | 16 (+3) | 18 (+4) |

___Incorporeal Movement.___ The spectral guardian can move through other creatures and objects as if they were difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Tomb Bound.___ The spectral guardian is bound to the area it defends. It can't move more than 100 feet from the place it is bound to protect.

___Withering Miasma.___ A creature that starts its turn in the spectral guardian's space must make a successful DC 15 Constitution saving throw or take 18 (4d8) necrotic damage and its hit point maximum is reduced by an amount equal to the necrotic damage taken. This reduction lasts until the creature finishes a long rest.

___Variant: Arcane Guardian.___ Some spectral guardians were not warriors in life, but powerful magic users. An arcane guardian has a challenge rating of 8 (3,900 XP) and the following added trait: Spellcasting. The arcane guardian is a 9th level spellcaster. Its spellcasting ability is Charisma (spell save DC 15, +7 to hit with spell attacks). The guardian knows the following sorcerer spells, which do not require material components:

Cantrips (at will): acid splash, chill touch, dancing lights,minor illusion, ray of frost

1st level (4 slots): mage armor, ray of sickness

2nd level (3 slots): darkness, scorching ray

3rd level (3 slots): fear, slow, stinking cloud

4th level (3 slots): blight, ice storm

5th level (1 slot): cone of cold

**Actions**

___Multiattack.___ The spectral guardian makes two spectral rend attacks.

___Spectral Rend.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) necrotic damage. If the target is a creature, it must succeed on a DC 14 Wisdom saving throw or be frightened and have its speed reduced to 0; both effects last until the end of its next turn.

