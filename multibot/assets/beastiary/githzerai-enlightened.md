"Githzerai Enlightened";;;_page_number_: 208
_size_: Medium humanoid (gith)
_alignment_: lawful neutral
_challenge_: "10 (5900 XP)"
_languages_: "Gith"
_senses_: "passive Perception 18"
_skills_: "Arcana +7, Insight +8, Perception +8"
_saving_throws_: "Str +6, Dex +8, Int +7, Wis +8"
_speed_: "30 ft."
_hit points_: "112  (15d8 + 45)"
_armor class_: "18"
_stats_: | 14 (+2) | 19 (+4) | 16 (+3) | 17 (+3) | 19 (+4) | 13 (+1) |

___Innate Spellcasting (Psionics).___ The githzerai's innate spellcasting ability is Wisdom (spell save DC 16, +8 to hit with spell attacks). It can innately cast the following spells, requiring no components:

* At will: _mage hand _(the hand is invisible)

* 3/day each: _blur, expeditious retreat, feather fall, jump, see invisibility, shield_

* 1/day each: _haste, plane shift, teleport_

___Psychic Defense.___ While the githzerai is wearing no armor and wielding no shield, its AC includes its Wisdom modifier.

**Actions**

___Multiattack___ The githzerai makes three unarmed strikes.

___Unarmed Strike___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) bludgeoning damage plus 13 (3d8) psychic damage.

___Temporal Strike (Recharge 6)___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 13 (2d8 + 4) bludgeoning damage plus 52 (8d12) psychic damage. The target must succeed on a DC 16 Wisdom saving throw or move 1 round forward in time. A target moved forward in time vanishes for the duration. When the effect ends, the target reappears in the space it left or in an unoccupied space nearest to that space if it's occupied.
