"Deep Speaker";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "Any two languages, telepathy 30 ft."
_skills_: "Arcana +4, History +4, Perception +5, Religion +4"
_senses_: "darkvision 120 ft., passive Perception 14"
_saving_throws_: "Wis +5, Cha +6"
_speed_: "30 ft."
_hit points_: "58 (9d8 + 18)"
_armor class_: "12 (15 with <i>mage armor</i>)"
_stats_: | 10 (+0) | 14 (+2) | 15 (+2) | 12 (+1) | 14 (+2) | 16 (+3) |

___Deep Knowledge.___ The speaker can read all writing.

___Spellcasting.___ The speaker is a 6th-level spellcaster.
Its spellcasting ability is Charisma (spell save DC
14, +6 to hit with spell attacks). It regains its
expended spellslots when it finishes a short or long
rest. The speaker has the following warlock spells
prepared:

* Cantrips (at will): _chill touch, eldritch blast, mage hand_

* 1st-3rd level (2 3rd-level slots): _clairvoyance, detect thoughts, dissonant whispers, mage armor, phantasmal force, sending, Tasha's hideous laughter_

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +4 to hit,
reach 5 ft. or range 20/60 ft., one target. Hit: 4
(1d4 + 2) piercing damage.

**Reactions**

___Entropic Ward (1/Short Rest).___ When an enemy
makes an attack roll against the speaker, it can use
its reaction to impose disadvantage on that roll and
the next attack roll the speaker makes against that
creature is made with advantage.
