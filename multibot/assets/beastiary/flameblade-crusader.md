"Flameblade Crusader";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "Any four languages"
_skills_: "Arcana +8, Athletics +7, Deception +6, Religion +8"
_senses_: "passive Perception 11"
_saving_throws_: "Int +8, Wis +5"
_speed_: "30 ft."
_hit points_: "62 (8d6 + 4d10 + 12)"
_armor class_: "17 (splint mail)"
_stats_: | 17 (+1) | 10 (+3) | 12 (+2) | 19 (+0) | 12 (+2) | 14 (+4) |

___Arcane Recovery (1/Day).___ When the crusader finishes a
short rest, it can regain up to 4 total expended spell
slots.

___Burning Strike.___ When the crusader hits with a melee
weapon attack, it can expend a spell slot to deal an
addition 9 (2d8) fire damage, in addition to the
weapon's damage. This damage increases by 1d8 fire
damage per spell slot level above 1st.

___Combat Caster.___ The crusader can perform the somatic
components of spells even when it has weapons or a
shield in one or both hands.

___Endless Assault.___ When the crusader takes the attack
action, it can expend a spell slot up to level 2 to gain
additional attacks this turn. If a level 1 slot is expended,
the crusader gains one additional attack. If the level 2
spell slot is expended, the crusader gains 2 additional
attacks.

___Transmutation Mastery.___ The crusader has a deep
knowledge of transmutation magics and can use that
to adapt its fighting style to the needs at hand. As a
bonus action, the crusader can transform its weapon
into a pike, a greatsword, or back to a maul.

___Spellcasting.___ The crusader is a 9th-level spellcaster. Its
spellcasting ability is intelligence (spell save DC 16, +8
to hit with spell attacks). The crusader has the
following wizard and paladin spells prepared:
* Cantrips (at will): _dancing lights, fire bolt_

* 1st level (4 slots): _burning hands, searing smite, thunderous smite_

* 2nd level (3 slots): _flaming sphere, magic weapon_

* 3rd level (3 slots): _fireball, phantom steed_

* 4th level (2 slots): _fire shield, wall of fire_

**Actions**

___Multiattack.___ The crusader makes two melee attacks.

___Maul.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one
target. Hit: 10 (2d6 + 3) bludgeoning damage.

___Pike.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one
target. Hit: 8 (1d10 + 3) piercing damage.

___Greatsword.___ Melee Weapon Attack: +7 to hit, reach 5 ft.,
one target. Hit: 10 (2d6 + 3) slashing damage.
