"Small Earth Elemental";;;_size_: Small elemental
_alignment_: neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Terran"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 10"
_damage_vulnerabilities_: "thunder"
_damage_immunities_: "poison"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "40 ft."
_hit points_: "25 (3d10 + 9)"
_armor class_: "16 (natural armor)"
_stats_: | 14 (+2) | 8 (-1) | 16 (+3) | 4 (-3) | 10 (+0) | 6 (-2) |

___Earth Glide.___ The elemental can burrow through non
magical, unworked earth and stone. While doing so,
the elemental doesn't disturb the material it moves
through.

**Actions**

___Slam.___ Melee Weapon Attack: +4 to hit, reach 5 ft.,
one target. Hit: 6 (1d8 + 2) bludgeoning damage.

___Fling Pebble.___ Ranged Weapon Attack: +4 to hit,
range 20/60 ft., one target. Hit: 4 (1d4 +2)
bludgeoning damage.
