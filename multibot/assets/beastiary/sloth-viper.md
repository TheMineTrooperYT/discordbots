"Sloth Viper";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "2 (450 XP)"
_languages_: "--"
_skills_: "Perception +3, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "30 ft., climb 30 ft., swim 30 ft."
_hit points_: "38 (7d10)"
_armor class_: "16 (natural armor)"
_stats_: | 13 (+1) | 16 (+3) | 11 (+0) | 2 (-4) | 12 (+1) | 2 (-4) |

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage, and the target must succeed on a DC 13 Constitution
saving throw. On a failed saving throw, the target takes 14 (4d6) poison
damage and the target is poisoned for 1 minute. On a successful saving
throw, the target takes half damage and isn’t poisoned.

While the target is poisoned, its speed is halved, it can’t use reactions,
and it can take only one action or one bonus action on each of its turns, and
regardless of abilities or magic items, it can’t make more than one melee
or ranged attack during its turn. If the creature attempts to cast a spell with
a casting time of 1 action, roll a d20. On an 11 or higher, the spell doesn’t
take effect until the creature’s next turn, and the creature must use its
action on that turn to complete the spell. If it can’t, the spell is wasted.
