"Change";;;_size_: Medium construct
_alignment_: lawful neutral
_challenge_: "7 (2,900 XP)"
_languages_: "all those of the creature who summoned it"
_senses_: "truesight 60 ft., passive Perception 14"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft."
_hit points_: "120 (16d8 + 48)"
_armor class_: "18 (natural armor)"
_stats_: | 15 (+2) | 17 (+3) | 16 (+3) | 13 (+1) | 12 (+1) | 10 (+0) |

___The Law of Change.___ Enemies with 10 feet cannot
benefit from magical healing.

___Chaos Vulnerability.___ The Inexorables have disadvantage
on all saving throws against spells.

___Inexorable.___ The Inexorables are immune to any
effects that would slow them or deny them
actions or movement.

**Actions**

___Multiattack.___ Change makes three javelin attacks.

___Javelin.___ Melee or Ranged Weapon Attack: +5 to
hit, reach 5 ft. or range 30/120 ft., one target. Hit:
12 (2d8 + 3) piercing damage.
