"Air Elemental";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "Auran"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "fly 90 ft. (hover)"
_hit points_: "90 (12d10+24)"
_armor class_: "15"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_damage_resistances_: "lightning, thunder, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 14 (+2) | 20 (+5) | 14 (+2) | 6 (-2) | 10 (0) | 6 (-2) |

___Air Form.___ The elemental can enter a hostile creature's space and stop there. It can move through a space as narrow as 1 inch wide without squeezing.

**Actions**

___Multiattack.___ The elemental makes two slam attacks.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) bludgeoning damage.

___Whirlwind (Recharge 4-6).___ Each creature in the elemental's space must make a DC 13 Strength saving throw. On a failure, a target takes 15 (3d8 + 2) bludgeoning damage and is flung up 20 feet away from the elemental in a random direction and knocked prone. If a thrown target strikes an object, such as a wall or floor, the target takes 3 (1d6) bludgeoning damage for every 10 feet it was thrown. If the target is thrown at another creature, that creature must succeed on a DC 13 Dexterity saving throw or take the same damage and be knocked prone.

If the saving throw is successful, the target takes half the bludgeoning damage and isn't flung away or knocked prone.