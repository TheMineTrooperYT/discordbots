"Ooze Golem";;;_size_: Large construct
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "understands the languages of its creator but can’t speak"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 10"
_damage_immunities_: "acid, cold, poison, lightning, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned, prone"
_speed_: "20 ft., climb 20 ft."
_hit points_: "94 (9d10 + 45)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 5 (-3) | 20 (+5) | 6 (-2) | 10 (+0) | 5 (-3) |

___Acid.___ A creature that touches the golem or hits it with a melee attack
while within 5 feet of it takes 9 (2d8) acid damage.

___Amorphous.___ The golem can move through a space as narrow as 1 inch
wide without squeezing.

___Death Throes.___ When the ooze golem drops to 0 hit points, its body
explodes in a 10-foot sphere. Creatures in that area must make a DC 15
Dexterity saving throw, taking 10 (3d6) acid damage on a failed saving
throw, or half as much damage on a successful saving throw. The area of
the golem’s death throes remains acidic for 1 minute after death, and any
creature that begins its turn in the area or enters it is affected as well.

___Immutable Form.___ The golem is immune to any spell or effect that
would alter its form.

___Magic Resistance.___ The golem has advantage on saving throws against
spells and other magical effects.

___Magic Weapon.___ The golem’s weapon attacks are magical.

___Regeneration.___ The ooze golem regains 10 hit points at the beginning of
each of its turns as long as it has at least 1 hit point.

___Spider Climb.___ The golem can climb difficult surfaces, including upside
down on ceilings, without needing to make an ability check.

**Actions**

___Multiattack.___ The ooze golem makes two slam attacks.

___Slam.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13
(2d8 + 4) bludgeoning damage plus 9 (2d8) acid damage.
