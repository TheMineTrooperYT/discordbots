"Walrutte";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_resistances_: "cold"
_condition_immunities_: "prone"
_speed_: "20 ft., swim 50 ft."
_hit points_: "92 (8d10 + 48)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 10 (+0) | 22 (+6) | 2 (-4) | 10 (+0) | 5 (-3) |

___Blubber.___ The walrutte has advantage on any saving
throw it makes against an effect that would deal cold
damage.

___Hold Breath.___ The walrutte can hold its breath for 1 hour.

___Ice Walk.___ The walrutte can move across icy surfaces
without needing to make an ability check. Additionally,
difficult terrain due to ice or snow doesn't cost it extra
movement.

___Swimming Leap.___ If the walrutte swims at least 10 feet in
a straight line before breaching the surface of the
water, its long jump is up to 30 feet and its high jump
is up to 15 feet. As part of this movement, the walrutte
can break through up to 5 feet of solid ice.

**Actions**

___Multiattack.___ The walrutte makes two gore attacks.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one
target. Hit: 30 (4d12 + 4) piercing damage.

___Gore.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one
target. Hit: 13 (2d8 + 4) piercing damage.

___Deadly Breach.___ If the walrutte jumps at least 15 feet as
part of its movement, it can then use this action to land
upright in a space that contains one or more other
creatures. Each of those creatures must succeed on a
DC 15 Strength or Dexterity saving throw (target's
choice) or be knocked prone and take 14 (3d6 + 4)
bludgeoning damage plus 14 (4d6) cold damage. On a
successful save, the creature takes only half the
damage, isn't knocked prone, and is pushed 5 feet out
of the walrutte's space into an unoccupied space of the
creature's choice. If no unoccupied space is within
range, the creature instead falls prone in the walrutte's
space.
