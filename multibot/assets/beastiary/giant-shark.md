"Giant Shark";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_senses_: "blindsight 60 ft."
_skills_: "Perception +3"
_speed_: "swim 50 ft."
_hit points_: "126 (11d12+55)"
_armor class_: "13 (natural armor)"
_stats_: | 23 (+6) | 11 (0) | 21 (+5) | 1 (-5) | 10 (0) | 5 (-3) |

___Blood Frenzy.___ The shark has advantage on melee attack rolls against any creature that doesn't have all its hit points.

___Water Breathing.___ The shark can breathe only underwater.

**Actions**

___Bite.___ Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 22 (3d10 + 6) piercing damage.