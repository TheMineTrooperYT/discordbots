"Firebird";;;_size_: Small celestial
_alignment_: neutral good
_challenge_: "4 (1100 XP)"
_languages_: "Celestial, Common, Elvish, Primordial, Sylvan"
_skills_: "Acrobatics +6, Arcana +5, Insight +4, Medicine +4, Nature +5, Perception +7, Religion +5"
_senses_: "truesight 60 ft., passive Perception 17"
_saving_throws_: "Dex +6, Con +4, Int +5, Wis +4, Cha +7"
_damage_immunities_: "fire"
_damage_resistances_: "lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, frightened, invisible"
_speed_: "20 ft., fly 100 ft."
_hit points_: "99 (18d6 + 36)"
_armor class_: "16 (natural armor)"
_stats_: | 12 (+1) | 19 (+4) | 14 (+2) | 16 (+3) | 15 (+2) | 21 (+5) |

___Innate Spellcasting.___ The firebird's innate spellcasting ability is Charisma (spell save DC 15). It can innately cast the following spells, requiring no material components:

At will: guidance, purify food and drink, speak with animals

3/day each: charm person, cure wounds (2d8 + 5), daylight, faerie fire, heat metal, hypnotic pattern, tongues

1/day each: geas, heal, reincarnate

___Light of the World.___ The firebird's feathers glow with a warm light. The creature sheds light as dim as a candle or as bright as a lantern. It always sheds light, and any feathers plucked from the creature continue to shed light as a torch.

___Warming Presence.___ The firebird and any creatures within a 5-foot radius are immune to the effects of natural, environmental cold. Invited into a home or building, a firebird can expand this warming presence to its inhabitants no matter how close they are to the creature.

**Actions**

___Multiattack.___ The firebird makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 5 (1d8 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (2d8 + 4) slashing damage.

___Blinding Ray (Recharge 5-6).___ The firebird can fire a burning ray of light from its tail feathers in a line 5 feet wide and up to 50 feet long. Targets in the line must succeed on a DC 15 Dexterity saving throw or take 24 (7d6) fire damage and become blinded for 1d4 rounds. A successful saving throw negates the blindness and reduces the damage by half.

