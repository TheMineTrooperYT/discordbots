"Treant";;;_size_: Huge plant
_alignment_: chaotic good
_challenge_: "9 (5,000 XP)"
_languages_: "Common, Druidic, Elvish, Sylvan"
_speed_: "30 ft."
_hit points_: "138 (12d12+60)"
_armor class_: "16 (natural armor)"
_damage_vulnerabilities_: "fire"
_damage_resistances_: "bludgeoning, piercing"
_stats_: | 23 (+6) | 8 (-1) | 21 (+5) | 12 (+1) | 16 (+3) | 12 (+1) |

___False Appearance.___ While the treant remains motionless, it is indistinguishable from a normal tree.

___Siege Monster.___ The treant deals double damage to objects and structures.

**Actions**

___Multiattack.___ The treant makes two slam attacks.

___Slam.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 16 (3d6 + 6) bludgeoning damage.

___Rock.___ Ranged Weapon Attack: +10 to hit, range 60/180 ft., one target. Hit: 28 (4d10 + 6) bludgeoning damage.

___Animate Trees (1/Day).___ The treant magically animates one or two trees it can see within 60 feet of it. These trees have the same statistics as a treant, except they have Intelligence and Charisma scores of 1, they can't speak, and they have only the Slam action option. An animated tree acts as an ally of the treant. The tree remains animate for 1 day or until it dies; until the treant dies or is more than 120 feet from the tree; or until the treant takes a bonus action to turn it back into an inanimate tree. The tree then takes root if possible.