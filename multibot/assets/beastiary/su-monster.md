"Su-Monster";;;_size_: Medium monstrosity
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_senses_: "passive Perception 13"
_skills_: "Athletics +6, Perception +3"
_speed_: "30 ft., climb 30 ft."
_hit points_: "27 (5d8 +5)"
_armor class_: "12"
_stats_: | 14 (+2) | 15 (+2) | 12 (+1) | 9 (-1) | 13 (+1) | 9 (-1) |

**Actions**

___Multiattack.___ The Su-monster makes two attacks: one with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (ld4 +2) piercing damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4 +2) slashing damage, or 12 (4d4 +2) slashing damage if the su-monster is hanging by its tail and all four of its limbs ace free.

___Psychic Crush (Recharge 5-6).___ The Su-monster targets one creature it can see within 30 feet of it. The target must succeed on a DC 11 Wisdom saving throw or take 17 (5d6) psychic damage and be stunned for 1 minute. The stunned target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.