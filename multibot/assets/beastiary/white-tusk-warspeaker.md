"White Tusk Warspeaker";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Orc"
_skills_: "Persuasion +3, Religion +4"
_senses_: "darkvision 60 ft., passive Perception 13"
_saving_throws_: "Wis +5"
_speed_: "30 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "14 (chainmail scraps)"
_stats_: | 16 (+3) | 8 (-1) | 15 (+2) | 10 (+0) | 16 (+3) | 12 (+1) |

___Aggressive.___ As a bonus action, the orc can
move up to its speed toward a hostile creature it
can see.

___Minion: Savage Horde.___ After moving at least 20
feet in a straight line toward a creature, the next
attack the orc makes against that creature scores
a critical hit on a roll of 18–20.

___Spellcasting.___ The orc is a 5th-level spellcaster.
Its spellcasting ability is Wisdom (spell save DC
13, +5 to hit with spell attacks). The orc has the
following cleric spells prepared:

* Cantrips (at will): _resistance, sacred flame, thaumaturgy_

* 1st level (4 slots): _cure wounds, guiding bolt, inflict wounds_

* 2nd level (3 slots): _blindness/deafness, spiritual weapon_ (greataxe)

* 3rd level (2 slots): _bestow curse, spirit guardians_


**Actions**

___Multiattack.___ The White Tusk warspeaker makes a
goading lash attack and two spiked club attacks.

___Goading Lash.___ Melee Weapon Attack: +5 to hit,
reach 5 ft., one allied target. Hit: 5 (1d4 + 3) slashing damage. The next melee weapon attack the
target makes before the end of its next turn has
advantage and deals an extra 10 (3d6) damage.

___Spiked Club.___ Melee Weapon Attack: +5 to hit,
reach 5 ft., one target. Hit: 10 (2d6 + 3) piercing
damage. 
