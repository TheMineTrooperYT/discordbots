"Riptide Horror";;;_size_: Medium monstrosity
_alignment_: lawful evil
_challenge_: "7 (2,900 XP)"
_languages_: "--"
_skills_: "Perception +2, Stealth +5"
_senses_: "blindsight 60 ft. (blind beyond this radius), passive Perception 12"
_damage_vulnerabilities_: "lightning"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "blinded"
_speed_: "20 ft., swim 40 ft."
_hit points_: "95 (10d8 + 50)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 15 (+2) | 20 (+5) | 8 (-1) | 9 (-1) | 8 (-1) |

___Amphibious.___ The riptide horror can breathe air and water.

___Spider Climb.___ The riptide horror can climb difficult surfaces, including
upside down on ceilings, without needing to make an ability check.

**Actions**

___Multiattack.___ The riptide horror makes six attacks: three with its
tentacles plus three bites.

___Tentacles.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit:
7 (1d8 + 3) bludgeoning damage and the target is grappled (escape DC
14). Until the grapple ends, the target is restrained, and the target must
succeed on a DC 16 Constitution saving throw or become poisoned for 1
hour. While the target is poisoned, it is paralyzed. The riptide horror can
have up to 6 creatures grappled with its six tentacles.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10
(2d6 + 3) piercing damage.
