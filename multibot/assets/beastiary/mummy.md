"Mummy";;;_size_: Medium undead
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 60 ft."
_damage_immunities_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_saving_throws_: "Wis +2"
_speed_: "20 ft."
_hit points_: "58 (9d8+18)"
_armor class_: "11 (natural armor)"
_damage_vulnerabilities_: "fire"
_condition_immunities_: "necrotic, poisoned"
_stats_: | 16 (+3) | 8 (-1) | 15 (+2) | 6 (-2) | 10 (0) | 12 (+1) |

**Actions**

___Multiattack.___ The mummy can use its Dreadful Glare and makes one attack with its rotting fist.

___Rotting Fist.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) bludgeoning damage plus 10 (3d6) necrotic damage. If the target is a creature, it must succeed on a DC 12 Constitution saving throw or be cursed with mummy rot. The cursed target can't regain hit points, and its hit point maximum decreases by 10 (3d6) for every 24 hours that elapse. If the curse reduces the target's hit point maximum to 0, the target dies, and its body turns to dust. The curse lasts until removed by the remove curse spell or other magic.

___Dreadful Glare.___ The mummy targets one creature it can see within 60 ft. of it. If the target can see the mummy, it must succeed on a DC 11 Wisdom saving throw against this magic or become frightened until the end of the mummy's next turn. If the target fails the saving throw by 5 or more, it is also paralyzed for the same duration. A target that succeeds on the saving throw is immune to the Dreadful Glare of all mummies (but not mummy lords) for the next 24 hours.