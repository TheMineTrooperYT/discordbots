"Oreioth";;;_size_: Medium humanoid (human)
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Abyssal, Common"
_skills_: "Arcana +5, Investigation +5, Medicine +1"
_saving_throws_: "Wis +1"
_speed_: "30 ft."
_hit points_: "39 (6d8+12)"
_armor class_: "11 (14 with mage armor)"
_stats_: | 8 (-1) | 13 (+1) | 14 (+2) | 16 (+3) | 9 (-1) | 11 (0) |

___Grim Harvest.___ Once per turn when Oreioth kills one or more creatures with a spell of 1st level or higher, he regains hit points equal to twice the spell's level.

___Spellcasting.___ Oreioth is a 6th-level spellcaster. His spellcasting ability is Intelligence (spell save DC 13, +5 to hit with spell attacks). He has the following wizard spells prepared:

* Cantrips (at will): _chill touch, minor illusion, prestidigitation, shocking grasp_

* 1st level (4 slots): _false life, mage armor, magic missile, ray of sickness_

* 2nd level (3 slots): _crown of madness, misty step_

* 3rd level (3 slots): _animate dead, vampiric touch_

___Swift Animation (Recharges after a Long Rest).___ When a living Medium or Small humanoid within 30 feet of Oreioth dies, he can use an action on his next turn to cast animate dead on that humanoid's corpse, instead of using the spell's normal casting time.

**Actions**

___Dagger.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) piercing damage.
