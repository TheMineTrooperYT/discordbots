"Duergar Spore Servant";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_languages_: "-"
_senses_: "blindsight 30 ft. (blind beyond this radius)"
_speed_: "15 ft."
_hit points_: "26 (4d8+8)"
_armor class_: "16 (scale armor, shield)"
_condition_immunities_: "blinded, charmed, frightened, paralyzed"
_damage_resistances_: "poison"
_stats_: | 14 (+2) | 11 (0) | 14 (+2) | 2 (-4) | 6 (-2) | 1 (-5) |

**Actions**

___War Pick.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage.