"Drow House Captain";;;_page_number_: 184
_size_: Medium humanoid (elf)
_alignment_: neutral evil
_challenge_: "9 (5,000 XP)"
_languages_: "Elvish, Undercommon"
_senses_: "darkvision 120 ft., passive Perception 16"
_skills_: "Perception +6, Stealth +8"
_saving_throws_: "Dex +8, Con +6, Wis +6"
_speed_: "30 ft."
_hit points_: "162  (25d8 + 50)"
_armor class_: "16 (chain mail)"
_stats_: | 14 (+2) | 19 (+4) | 15 (+2) | 12 (+1) | 14 (+2) | 13 (+1) |

___Battle Command.___ As a bonus action, the drow targets one ally he can see within 30 feet of him. If the target can see or hear the drow, the target can use its reaction to make one melee attack or to take the Dodge or Hide action.

___Fey Ancestry.___ The drow has advantage on saving throws against being charmed, and magic can't put the drow to sleep.

___Innate Spellcasting.___ The drow's innate spellcasting ability is Charisma (spell save DC 13). He can innately cast the following spells, requiring no material components:

* At will: _dancing lights_

* 1/day each: _darkness, faerie fire , levitate _(self only)

___Sunlight Sensitivity.___ While in sunlight, the drow has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack___ The drow makes three attacks: two with his scimitar and one with his whip or his hand crossbow.

___Scimitar___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) slashing damage plus 14 (4d6) poison damage.

___Whip___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 6 (1d4 + 4) slashing damage. If the target is an ally, it has advantage on attack rolls until the end of its next turn.

___Hand Crossbow___ Ranged Weapon Attack: +8 to hit, range 30/120 ft., one target. Hit: 7 (1d6 + 4) piercing damage, and the target must succeed on a DC 13 Constitution saving throw or be poisoned for 1 hour. If the saving throw fails by 5 or more, the target is also unconscious while poisoned in this way. The target regains consciousness if it takes damage or if another creature takes an action to shake it.
