"Lord of the Azer";;;_size_: Large elemental
_alignment_: lawful evil
_challenge_: "8 (3,900 XP)"
_languages_: "Ignan, Common"
_skills_: "Athletics +8, Arcana +4, Intimidation +5"
_senses_: "darkvision 90 ft., passive Perception 10"
_saving_throws_: "Str +8, Con +6"
_damage_immunities_: "fire, poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "130 (16d10 + 42)"
_armor class_: "18 (plate mail)"
_stats_: | 20 (+5) | 10 (+0) | 16 (+3) | 13 (+1) | 10 (+0) | 14 (+2) |

___Legendary Resistance (1/Day).___ If the azer fails a
saving throw, it can choose to succeed instead.
This effect is granted by the Azer’s crown, so it if it
somehow lost, this ability cannot be used.

___Heated Body.___ A creature that touches the azer or
hits it with a melee attack while within 5 feet of it
takes 6 (1d12) fire damage.

___Heated Weapons.___ When the azer hits with a metal
melee or ranged weapon, it deals an extra 7 (2d6)
fire damage (included in the attack).

___Illumination.___ The azer sheds bright light in a 15-foot
radius and dim light for an additional 15 feet.

**Actions**

___Multiattack.___ The azer makes two attacks with its
maul or two with its throwing hammers.

___Maul.___ Melee Weapon Attack: +8 to hit, reach 10 ft.,
one target. Hit: 12 (2d6 + 5) bludgeoning damage,
plus 7 (2d6) fire damage.

___Throwing Hammer.___ Ranged Weapon Attack: +8 to
hit, range 20/60 ft., one target. Hit: 8 (1d6 + 5)
bludgeoning damage plus 7 (2d6) fire damage.

___Molten Slam (Recharge 5-6).___ The azer slams his maul
down with such devastating force that the ground
itself cracks open in a 30-foot cone and releases
lava. Each creature in that area must make a DC 15
Dexterity saving throw, taking 44 (8d10) fire
damage on a failed save, or half as much damage on
a successful one.
