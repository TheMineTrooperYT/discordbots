"Qwyllion";;;_size_: Medium aberration
_alignment_: neutral evil
_challenge_: "8 (3900 XP)"
_languages_: "Common, Goblin, Infernal, Sylvan, Void Speech"
_skills_: "Acrobatics +11, Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_saving_throws_: "Dex +8, Cha +6"
_damage_resistances_: "acid, cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "30 ft."
_hit points_: "110 (13d8 + 52)"
_armor class_: "16 (natural armor)"
_stats_: | 12 (+1) | 20 (+5) | 19 (+4) | 12 (+1) | 13 (+1) | 16 (+3) |

___Disruptive.___ Because of the qwyllion's nauseating nature, spellcasters have disadvantage on concentration checks while within 40 feet of the qwyllion.

___Nauseating Aura.___ The qwyllion is nauseatingly corrupt. Any creature that starts its turn within 20 feet of the qwyllion must succeed on a DC 14 Constitution saving throw or be poisoned for 1d8 rounds. If a creature that's already poisoned by this effect fails the saving throw again, it becomes incapacitated instead, and a creature already incapacitated by the qwyllion drops to 0 hit points if it fails the saving throw. A successful saving throw renders a creature immune to the effect for 24 hours. Creatures dominated by the qwyllion are immune to this effect.

**Actions**

___Multiattack.___ The qwyllion uses its deadly gaze if it can, and makes two claw attacks.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 24 (3d12 + 5) slashing damage.

___Deadly Gaze (recharge 5-6).___ The qwyllion turns its gaze against a single creature within 20 feet of the qwyllion. The target must succeed on a DC 14 Constitution saving throw or take 16 (3d8 + 3) necrotic damage and be incapacitated until the start of the qwyllion's next turn. A humanoid slain by a qwyllion's death gaze rises 2d4 hours later as a specter under the qwyllion's control.

___Innate Spellcasting.___ The qwyllion's innate casting ability is Charisma (spell save DC 14). She can innately cast the following spells, requiring no material components:

3/day each: dominate person (range 20 feet), shatter

