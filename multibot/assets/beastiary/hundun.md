"Hundun";;;_size_: Large celestial
_alignment_: chaotic good
_challenge_: "10 (5900 XP)"
_languages_: "understands Celestial and Primordial, but cannot speak intelligibly"
_skills_: "Athletics +9, Insight +9, Perception +9"
_senses_: "blindsight 60 ft., passive Perception 20"
_saving_throws_: "Con +7, Wis +9, Cha +8"
_damage_immunities_: "acid, psychic"
_damage_resistances_: "lightning, thunder; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, stunned, unconscious"
_speed_: "40 ft., fly 40 ft."
_hit points_: "153 (18d10 + 54)"
_armor class_: "18 (natural armor)"
_stats_: | 20 (+5) | 14 (+2) | 16 (+3) | 4 (-3) | 20 (+5) | 18 (+4) |

___Brainless.___ Hunduns are immune to any spell or effect that allows an Intelligence, Wisdom, or Charisma saving throw. Trying to contact or read a Hundun's mind confuses the caster as the spell for 1 round.

___Dance of Creation.___ Hunduns can perform an act of magical creation almost unlimited in scope every 1d8 days. The effect is equivalent to a wish spell, but it must create something.

___Enlightening Befuddlement.___ When a hundun's confusion spell affects a target, it can elect to use the following table rather than the standard one:

1d100 Result

___01-10 Inspired:.___ Advantage on attack rolls, ability checks, and saving throws

___11-20 Distracted:.___ Disadvantage on attack rolls, ability checks, and saving throws

___21-50 Incoherent:.___ The target does nothing but babble or scribble incoherent notes on a new idea

___51-75 Obsessed:.___ Target is recipient of geas to create a quality magical object

___76-100 Suggestible:.___ Target receives a suggestion from the hundun

___Innate Spellcasting.___ The hundun's innate spellcasting ability is Wisdom (spell save DC 17). It can cast the following spells, requiring no material components:

Constant: confusion (always centered on the hundun), detect thoughts

At will: create or destroy water, dancing lights, mending, prestidigitation

3/day each: compulsion, dimension door, black tentacles, irresistible dance

1/day each: awaken, creation, heroes' feast, magnificent mansion, plant growth, reincarnate, stone shape

___Magic Weapons.___ The hundun's weapon attacks are magical.

**Actions**

___Multiattack.___ The hundun makes four slam attacks.

___Slam.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 15 (3d6 + 5) bludgeoning damage.

