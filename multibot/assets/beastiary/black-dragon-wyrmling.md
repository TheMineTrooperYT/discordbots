"Black Dragon Wyrmling";;;_size_: Medium dragon
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Draconic"
_senses_: "blindsight 10 ft., darkvision 60 ft."
_skills_: "Perception +4, Stealth +4"
_damage_immunities_: "acid"
_saving_throws_: "Dex +4, Con +3, Wis +2, Cha +3"
_speed_: "30 ft., fly 60 ft., swim 30 ft."
_hit points_: "33 (6d8+6)"
_armor class_: "17 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 13 (+1) | 10 (0) | 11 (0) | 13 (+1) |

___Amphibious.___ The dragon can breathe air and water.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (1d10 + 2) piercing damage plus 2 (1d4) acid damage.

___Acid Breath (Recharge 5-6).___ The dragon exhales acid in a 15-foot line that is 5 feet wide. Each creature in that line must make a DC 11 Dexterity saving throw, taking 22 (Sd8) acid damage on a failed save, or half as much damage on a successful one.