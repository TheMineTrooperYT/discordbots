"Caustic Alchemist";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common, any other language"
_senses_: "passive Perception 10"
_damage_immunities_: "acid"
_speed_: "30 ft."
_hit points_: "52 (8d8 + 16)"
_armor class_: "13"
_stats_: | 11 (+0) | 16 (+3) | 14 (+2) | 14 (+2) | 10 (+0) | 9 (-1) |

___Acidic Flesh.___ Ranged projectiles that hit the alchemist
are dissolved and cannot be recovered.

**Actions**

___Multiattack.___ The alchemist makes two attacks with its
claws or one with its caustic burst.

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5ft., one
target. Hit: 6 (1d6 + 3) slashing damage plus 3 (1d6) acid damage.

___Caustic Bile.___ Ranged Weapon Attack: +5 to hit, range
15/30 ft., one target. Hit: The target must make a DC
13 Dexterity saving throw, taking 17 (5d6) acid
damage on a failed save, or half as much damage on a
successful one.
