"Blindheim";;;_size_: Small aberration
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Primordial"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "30 ft."
_hit points_: "17 (5d6)"
_armor class_: "13 (natural armor)"
_stats_: | 10 (+0) | 14 (+2) | 11 (+0) | 2 (-4) | 12 (+1) | 6 (-2) |

___Eye Beams.___ When a blindheim’s eyes are open, it emits a 30-
foot cone of light. It can see normally in this light and functions
normally in areas of magical darkness. A creature looking at a
blindheim when its eye beams are “on” must succeed on a DC
12 Constitution saving throw or be blinded for 1 hour. A blinded
creature can repeat the saving throw at the end of each of its turns,
ending the effect on a success.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) piercing damage.
