"Arboreal Grappler";;;_size_: Medium aberration
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Acrobatics +5, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "10 ft., climb 40 ft."
_hit points_: "90 (12d8 + 36)"
_armor class_: "14 (natural armor)"
_stats_: | 16 (+3) | 16 (+3) | 16 (+3) | 6 (-2) | 10 (+0) | 6 (-2) |

___Spider Climb.___ The arboreal grappler can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Boscage Brachiation.___ The arboreal grappler doesn't provoke opportunity attacks when it moves out of an enemy's reach by climbing.

**Actions**

___Multiattack.___ The arboreal grappler makes one bite attack and two tentacle attacks.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 6 (1d6 + 3) piercing damage.

___Tentacle.___ Melee Weapon Attack: +5 to hit, reach 10 ft., one target. Hit: 10 (2d6 + 3) bludgeoning damage, and the target is grappled (escape DC 13). Until this grapple ends, the target is restrained and the tentacle can't be used to attack a different target. The arboreal grappler has two tentacles, each of which can grapple one target. When the arboreal grappler moves, it can drag a Medium or smaller target it is grappling at full speed.

