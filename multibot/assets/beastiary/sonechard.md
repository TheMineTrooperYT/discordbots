"Sonechard, General of Orcus";;;_size_: Large fiend (demon lord)
_alignment_: chaotic evil
_challenge_: "20 (25,000 XP)"
_languages_: "Abyssal, Celestial, Common, Draconic, Giant, Goblin, Ignan, Infernal, Terran; telepathy 120 ft."
_skills_: "Arcana +10, Athletics +12, Nature +10, Perception +15, Religion +10"
_senses_: "truesight 120 ft., passive Perception 25"
_saving_throws_: "Str +12, Dex +10, Con +11, Wis +9"
_damage_immunities_: "lightning, poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_damage_resistances_: "acid, cold, fire"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_speed_: "40 ft., fly 80 ft."
_hit points_: "336 (32d10 + 160)"
_armor class_: "19 (natural armor)"
_stats_: | 22 (+6) | 18 (+4) | 21 (+5) | 19 (+4) | 16 (+3) | 20 (+5) |

___Special Equipment.___ Sonechard carries the war pick Fool’s Errand.

___Aggressive.___ As a bonus action, Sonechard can move up to its speed
toward a hostile creature that it can see.

___Innate Spellcasting.___ Sonechard’s innate spellcasting ability is Charisma
(spell save DC 19, +11 to hit with spell attacks). It can cast the following
spells, requiring no material components.

* At will: _animate dead, chill touch _(17th level)_, inflict wounds _(5th level)

* 3/day each: _circle of death, create undead, fireball, finger of death, harm_

* 1/day: _create undead _(9th level)

___Legendary Resistance (3/day).___ If Sonechard fails a saving throw, it can
choose to succeed instead.

___Magic Resistance.___ Sonechard has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ Sonechard’s weapon attacks are magical.

___Stench.___ Any creature that starts its turn within 10 feet of Sonechard
must succeed on a DC 19 Constitution saving throw or be poisoned until
the start of its next turn. On a successful saving throw, the creature is
immune to Sonechard’s Stench for 24 hours.

___Unholy Aura.___ An unholy aura surrounds Sonechard out to a radius of
40 feet. A creature who enters or begins its turn in the area must make
a DC 19 Wisdom saving throw. On a failed saving throw, the target is
frightened for 1 minute. While frightened, it is paralyzed. A frightened
target can repeat the saving throw at the end of each of its turns, ending
the effect on a success.

___Undead Master.___ When Sonechard casts animate dead or create undead,
it creates twice the amount of undead with each casting.

**Actions**

___Multiattack.___ Sonechard makes two attacks with Fool’s Errand, one
claw attack, and one head butt, or two claw attacks and one head butt.

___Fool’s Errand.___ Melee Weapon Attack: +15 to hit, reach 5 ft., one target. Hit: 18 (2d8 + 9) piercing damage plus 10 (3d6) necrotic damage.

___Claw.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) slashing damage.

___Head Butt.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) bludgeoning damage, and the target must succeed on a DC 20 Constitution saving throw or be stunned until the end of its
next turn.

___Control Undead.___ Sonechard chooses one undead creature not already
under its control that it can see within 120 feet of it. That creature, or the
creature that controls it, must make a DC 19 Wisdom saving throw. On a
failed saving throw, the undead falls under Sonechard’s control and obeys
its every command.

**Bonus** Actions

___Command Undead.___ Sonechard gives telepathic commands to any
undead it controls unless they are on another plane of existence. It can
command them to take specific actions, such as “attack those creatures”
or general instructions “guard this passageway.”

**Legendary** Actions

Sonechard can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. Sonechard regains spent legendary
actions at the start of its turn.

___Head butt.___ Sonechard uses its head butt attack.

___Command Undead.___ Sonechard can command one undead Sonechard
controls to use its reaction to move its speed, or make a melee or ranged attack.

___Animate Dead (Costs 2 Actions).___ Sonechard animates one corpse
within 120 feet of it as a zombie.

**Lair** Actions

On initiative count 20 (losing initiative ties), Sonechard can take a lair
action to cause one of the following effects; Sonechard can’t use the same
effect two rounds in a row:

___Heal Undead.___ Sonechard sends necrotic energy coursing through his
lair. Up to six undead under his command regain 19 (4d6 + 5) hit points
each. Sonechard does not need to see these undead in order to grant them
this boon.

___Raise Dead.___ Sonechard chooses one slain creature and causes the
creature’s soul to rise as a specter under its control.

___Fearsome Show.___ Sonechard causes the many corpses within its lair
to animate briefly and babble the horrific tales of their deaths. Creatures
within its lair that are not constructs or undead must make a DC 20 Wisdom
saving throw. On a failed saving throw, those creatures are frightened for
1 minute. While frightened, those creatures must attempt to flee the lair,
Dashing where possible. If a creature is trapped or cannot move, they can
take the Dodge action instead. A frightened creature can repeat the saving
throw at the end of each of its turns, ending the effect on a success. If
the saving throw is successful, or if the effect ends on it, that creature is
immune to this effect for 24 hours.

___Undead Awareness.___ Whenever an undead is created or enters within
1 mile of Sonechard’s lair, Sonechard is aware of the creature’s presence
and location.

**Regional** Effects

The region containing Sonechard’s lair is warped by its magic, creating
one or more of the following effects:

___Undead Walking.___ Slain creatures sometimes rise as skeletons or
zombies, abhorred mockeries of their former states.

___Nightmares.___ Unpleasant dreams wrack those who rest within 6 miles
of Sonechard’s lair.

If Sonechard dies, the effects fade immediately, but animated undead
remain until destroyed.

# Fool's Errand

_Weapon (war pick), artifact (requires attunement)_

Fool’s Errand is a macabre spectacle, wrought of a dark,
unknown wood as hard as steel. The spike of the war pick is
similarly unfinished and unpolished and glows dimly with dark
energies. Blood and ichor constantly drip from the pick and seems
to freeze and thaw continuously no matter what attempts are made
to clean the weapon.

___Attunement.___ To attune to Fools’ Errand, you must slay a celestial
being of lawful good alignment.

___Magic Weapon.___ You have a +3 bonus to attack and damage rolls
made with Fool’s Errand. It also functions as a sword of wounding.

___Unholy Smite.___ When you hit with Fools’ Errand, you deal an
additional 3d6 necrotic damage on a hit, and a creature who takes
this damage is poisoned for 1 minute. While poisoned, the creature
has disadvantage on saving throws against your spells and other
effects, and you have advantage on attack rolls with Fool’s Errand.

___The Smell of Blood.___ While holding Fool’s Errand, you have
advantage on Wisdom (Perception) checks to notice hidden
creatures.

___Spellcasting.___ Fool’s Errand has 20 charges. You can use an action
to cast one of the following spells using those charges: _bestow
curse _(3 charges)_, blight _(4 charges)_, circle of death _(6 charges), or
_finger of death _(7 charges). If you expend the last charge, you take
4d6 necrotic damage and regain a number of charges equal to the
damage dealt, up to a maximum of 20. This damage cannot drop
you below 1 hit point. Fool’s Errand recovers all lost charges each
day at midnight.

___Destruction.___ Fool’s Errand can only be destroyed by the ancient
celestial Sonechard first killed to attune to the weapon. This creature
must bath it in a holy water font specially crafted and blessed for
the purpose.
