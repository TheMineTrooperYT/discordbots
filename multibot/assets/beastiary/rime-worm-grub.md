"Rime Worm Grub";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "--"
_senses_: "darkvision 200 ft., passive Perception 11"
_saving_throws_: "Str +5, Con +5"
_damage_resistances_: "cold"
_speed_: "30 ft., swim 30 ft, burrow (snow, ice) 20 ft."
_hit points_: "45 (6d8 + 18)"
_armor class_: "13 (natural armor)"
_stats_: | 16 (+3) | 12 (+1) | 16 (+3) | 4 (-3) | 12 (+1) | 3 (-4) |

___Born of Rime.___ A rime worm grub can breathe air or water with equal ease.

___Ravenous.___ At the grub stage, the worm is painfully hungry. Rime worm grubs can make opportunity attacks against enemies who disengage.

**Actions**

___Multiattack.___ The rime worm makes one tendril attack and one gnash attack.

___Tendril.___ Melee Weapon Attack. +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Gnash.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) slashing damage.

