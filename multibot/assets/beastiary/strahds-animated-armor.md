"Strahd's Animated Armor";;;_page_number_: 227
_size_: Medium construct
_alignment_: lawful evil
_challenge_: "6 (2,300 XP)"
_languages_: "understands Common but can't speak"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_skills_: "Perception +3"
_damage_immunities_: "lightning, poison"
_speed_: "30 ft."
_hit points_: "112 (15d8+45)"
_armor class_: "21 (natural armor)"
_condition_immunities_: "blinded, charmed, deafened, exhaustion, frightened, paralyzed, petrified, poisoned"
_damage_resistances_: "cold, fire"
_stats_: | 17 (+3) | 13 (+1) | 16 (+3) | 9 (-1) | 10 (0) | 9 (-1) |

___Constructed Nature.___ An animated object doesn't require air, food, drink, or sleep.

The magic that animates an object is dispelled when the construct drops to 0 hit points. An animated object reduced to 0 hit points becomes inanimate and is too damaged to be of much use or value to anyone.

___Antimagic Susceptibility.___ The armor is incapacitated while in the area of an antimagic field. If targeted by dispel magic, the armor must succeed on a Constitution saving throw against the caster's spell save DC or fall unconscious for 1 minute.

___False Appearance.___ While the armor remains motionless, it is indistinguishable from a normal suit of armor.

**Actions**

___Multiattack.___ The armor makes two melee attacks or uses Shocking Bolt twice.

___Greatsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (2d6+3) slashing damage plus 3 (1d6) lightning damage.

___Shocking Bolt.___ Ranged Spell Attack: +4 to hit (with advantage on the attack roll if the target is wearing armor made of metal), range 60 ft., one target. Hit: 10 (3d6) lightning damage.
