"Clockwork Myrmidon";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "6 (2300 XP)"
_languages_: "understands Common"
_skills_: "Athletics +8, Perception +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Str +11, Dex +5"
_damage_immunities_: "poison, psychic"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "153 (18d10+54)"
_armor class_: "16 (natural armor)"
_stats_: | 20 (+5) | 14 (+2) | 16 (+3) | 10 (+0) | 10 (+0) | 1 (-5) |

___Immutable Form.___ The clockwork myrmidon is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The clockwork myrmidon has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The clockwork myrmidon makes two attacks: two pick attacks or two slam attacks, or one of each.

___Heavy Pick.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 14 (2d8 + 5) piercing damage.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 11 (1d12 + 5) bludgeoning damage.

___Alchemical Flame Jet (Recharge 5-6).___ The clockwork myrmidon can spew a jet of alchemical fire in a line 20 feet long and 5 feet wide. Any creature in the path of the jet takes 26 (4d12) fire damage, or half damage with a successful DC 15 Dexterity saving throw. The clockwork myrmidon can use this attack four times before its internal reservoir is emptied.

___Grease Spray (Recharge 5-6).___ As a bonus action, the clockwork myrmidon's chest can fire a spray of alchemical grease with a range of 30 feet, covering a 10-by-10 foot square area and turning it into difficult terrain. Each creature standing in the affected area must succeed on a DC 15 Dexterity saving throw or fall prone. A creature that enters the area or ends its turn there must also succeed on a DC 15 Dexterity saving throw or fall prone. The clockwork myrmidon can use this attack four times before its internal reservoir is emptied.

___Alchemical Fireball.___ The clockwork myrmidon's alchemical flame reservoir explodes when the construct is destroyed, spraying nearby creatures with burning fuel. A creature within 5 feet of the myrmidon takes 19 (3d12) fire damage, or half damage with a successful DC 15 Dexterity saving throw. This explosion doesn't occur if the clockwork myrmidon has already fired its alchemical flame jet four times.

