"Wereraven";;;_page_number_: 242
_size_: Medium humanoid (human shapechanger)
_challenge_: "2 (450 XP)"
_languages_: "Common (can't speak in raven form)"
_skills_: "Insight +4, Perception +6"
_damage_immunities_: "bludgeoning, piercing, and slashing from nonmagical attacks not made with silvered weapons"
_speed_: "30 ft., fly 50 ft. in raven and hybrid forms"
_hit points_: "31 (7d8)"
_armor class_: "12"
_stats_: | 10 (0) | 15 (+2) | 11 (0) | 13 (+1) | 15 (+2) | 14 (+2) |

___Shapechanger.___ The wereraven can use its action to polymorph into a raven-humanoid hybrid or into a raven, or back into its human form. Any equipment it is wearing or carrying isn't transformed. It reverts to its human form if it dies.

___Mimicry.___ The wereraven can mimic simple sounds it has heard, such as a person whispering, a baby crying, or an animal chittering. A creature that hears the sounds can tell they are imitations with a successful DC 10 Wisdom (Insight) check.

**Actions**

___Multiattack (Human or Hybrid Form Only).___ The wereraven makes two weapon attacks, one of which can be with its hand crossbow.

___Beak (Raven or Hybrid Form Only).___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 1 piercing damage in raven form, or 4 (1d4+2) piercing damage in hybrid form. If the target is humanoid, it must succeed on a DC 10 Constitution saving throw or be cursed with wereraven lycanthropy.

___Shortsword (Humanoid or Hybrid Form Only).___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6+2) piercing damage.

___Hand Crossbow (Humanoid or Hybrid Form Only).___ Ranged Weapon Attack: +4 to hit, range 30/120 ft., one target. Hit: 5 (1d6+2) piercing damage.