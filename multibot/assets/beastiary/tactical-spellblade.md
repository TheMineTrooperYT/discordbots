"Tactical Spellblade";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "Any two languages"
_skills_: "Arcana +5, Athletics +5, History +5, Perception +4"
_senses_: "passive Perception 14"
_saving_throws_: "Str +6, Dex +3"
_speed_: "30 ft."
_hit points_: "60 (8d10 + 16)"
_armor class_: "19 (splint mail, shield)"
_stats_: | 16 (+3) | 11 (+0) | 14 (+2) | 15 (+2) | 12 (+1) | 10 (+2) |

___Bonded Weapon.___ The spellblade has a magical bond
with its weapon. Unless incapacitated, the
spellblade cannot be disarmed and can summon its
weapon as a bonus action as long as it is on the
same plane of existence.

___Second Wind (1/Short Rest).___ As a bonus action, the
spellblade can regain 1d10 + 6 hit points.

___Spellcasting.___ The spellblade is a 6th-level spellcaster.
Its spellcasting ability is Intelligence (spell save DC
13, +5 to hit with spell attacks). The spellblade has
the following wizard spells prepared:

* Cantrips (at will): _fire bolt, shocking grasp_

* 1st level (3 slots): _detect magic, feather fall, jump, shield_

**Actions**

___Multiattack.___ The spellblade makes two attacks with
its longsword, or casts a cantrip and makes a
longsword attack.

___Longsword.___ Melee Weapon Attack: +6 to hit, reach
5 ft., one target. Hit: 7 (1d8 + 3) slashing damage,
or 8 (1d10 + 3) slashing damage if used with two
hands.

___Action Surge (1/Short Rest).___ The spellblade makes
four longsword attacks.

**Reactions**

___Protective Fighting.___ When a creature the spellblade
can see attacks an ally within 5 feet of the
spellblade, it can use its reaction to impose
disadvantage on that attack roll as long as the
spellblade is wielding a shield.
