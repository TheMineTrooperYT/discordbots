"Tree Blight";;;_page_number_: 230
_size_: Huge plant
_alignment_: neutral evil
_challenge_: "7 (2,900 XP)"
_languages_: "understands Common and Druidic but doesn't speak"
_senses_: "blindsight 60 ft. (blind beyond this radius)"
_speed_: "30 ft."
_hit points_: "149 (13d12+65)"
_armor class_: "15 (natural armor)"
_condition_immunities_: "blinded, deafened"
_stats_: | 23 (+6) | 10 (0) | 20 (+5) | 6 (-2) | 10 (0) | 3 (-4) |

___False Appearance.___ While the blight remains motionless, it is indistinguishable from a dead tree.

___Siege Monster.___ The blight deals double damage to objects and structures.

**Actions**

___Multiattack.___ The blight makes four attacks: two with its branches and two with its grasping roots, If it has a target grappled, the blight can also make a bite attack against the target as a bonus action.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 19 (3d8+6) piercing damage.

___Branch.___ Melee Weapon Attack: +9 to hit, reach 15 ft., one target. Hit 16 (3d6+6) bludgeoning damage.

___Grasping Root.___ Melee Weapon Attack: +9 to hit, reach 15 ft., one creature not grappled by the blight. Hit: the target is grappled (escape DC 15). Until the grapple ends, the target takes 9 (1d6+6) bludgeoning damage at the start of each of its turns. The root has AC 15 and can be severed by dealing 6 slashing damage or more to it at once. Cutting the root doesn't hurt the blight but ends the grapple.
