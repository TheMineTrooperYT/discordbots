"Darz Helgar";;;_size_: Medium humanoid (illuskan human)
_alignment_: neutral
_challenge_: "0 (10 XP)"
_languages_: "Common"
_skills_: "Intimidation +2, Sleight of Hand +4, Stealth +4"
_speed_: "30 ft."
_hit points_: "27 (5d8+5)"
_armor class_: "12"
_senses_: " passive Perception 10"
_stats_: | 15 (+2) | 15 (+2) | 12 (+1) | 10 (0) | 11 (0) | 11 (0) |

___Sneak Attack (1/turn).___ Darz deals an extra 7 (2d6) damage when he hits a target with a weapon attack and has advantage on the attack roll, or when the target is within 5 feet of an ally of Darz that isn't incapacitated and Darz doesn't have disadvantage on the attack roll.

**Actions**

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6+2) piercing damage.

___Sling.___ Ranged Weapon Attack: +4 to hit, range 30/120 ft., one target. Hit: 4 (1d4+2) piercing damage. Darz carries twenty sling stones.

**Roleplaying** Information

In his youth, Darz was a member of the Xanathar Thieves' Guild in Waterdeep. After serving ten years in prison for his crimes, he cut all ties to the city and moved north to be a campground caretaker.

**Ideal:** "You csan run from your past, but you can't hide from it."

**Bond:** "I've made a new life in Triboar. I'm not gonna run away this time. "

**Flaw:** "I have no regrets. I do whatever it takes to survive."