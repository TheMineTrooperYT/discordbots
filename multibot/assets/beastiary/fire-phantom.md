"Fire Phantom";;;_size_: Medium undead
_alignment_: chaotic neutral
_challenge_: "6 (2,300 XP)"
_languages_: "Ignan"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "fire"
_damage_vulnerabilities_: "cold"
_condition_immunities_: "charmed, frightened, paralyzed, poisoned, unconscious"
_speed_: "30 ft."
_hit points_: "49 (9d8 + 9)"
_armor class_: "14 (natural armor)"
_stats_: | 19 (+4) | 14 (+2) | 13 (+1) | 5 (-3) | 12 (+1) | 14 (+2) |

___Brutal Critical.___ The fire phantom can roll one additional weapon
damage when determining the extra damage for a critical hit with a melee
attack.

___Death Throes.___ When the fire phantom dies, it explodes, and each
creature within 30 feet of it must make a DC 14 Dexterity saving throw,
taking 28 (8d6) fire damage on a failed save, or half as much damage on
a successful one. The explosion ignites flammable objects in that area that
aren’t being worn or carried.

___Fire Form.___ A creature that touches the fire phantom or hits it with a
melee attack while within 5 feet of it takes 7 (2d6) fire damage.

___Reckless.___ At the start of its turn, the fire phantom can gain advantage
on all melee weapon attack rolls that turn but attack rolls against it have
advantage until the start of its next turn.

**Actions**

___Multiattack.___ The fire phantom makes two fist of fire attacks.

___Fist of Fire.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit:
11 (2d6 + 4) bludgeoning damage plus 7 (2d6) fire damage.

___Fire Blast.___ Ranged Weapon Attack: +7 to hit, range 60 ft., one target.
Hit: 10 (3d6) fire damage.
