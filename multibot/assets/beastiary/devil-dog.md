"Devil Dog";;;_size_: Large fiend (devil)
_alignment_: lawful evil
_challenge_: "5 (1,800 XP)"
_languages_: "Infernal, telepathy 120 ft."
_saving_throws_: "Con +5, Wis +6, Cha +5"
_skills_: "Insight +9, Perception +9, Stealth +6, Survival +9"
_senses_: "darkvision 120 ft., passive Perception 19"
_damage_immunities_: "fire, poison"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t silvered"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_speed_: "50 ft., climb 30 ft."
_hit points_: "67 (9d10 + 18)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 17 (+3) | 15 (+2) | 11 (+0) | 17 (+3) | 14 (+2) |

___Devil’s Sight.___ Magical darkness doesn’t impede the devil’s darkvision.

___Keen Smell.___ The devil dog has advantage on Wisdom (Perception)
checks that rely on smell.

___Magic Resistance.___ The devil has advantage on saving throws against
spells and other magical effects.

___Scent of Prey.___ The devil dog knows the exact location of any soul
whose true name it knows, as long as that soul resides in a vessel or body
within 1 mile. If the soul is beyond that distance, it knows the direction
the soul lies in. If the soul is on a different plane of existence, it knows the
plane that the soul resides on.

**Actions**

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13
(2d8 + 4) piercing damage. If the target is a creature, it must succeed on a
DC 15 Strength saving throw or be knocked prone.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11
(2d6 + 4) slashing damage.
