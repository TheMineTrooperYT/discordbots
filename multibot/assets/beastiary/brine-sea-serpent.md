"Brine Sea Serpent";;;_size_: Huge dragon
_alignment_: chaotic evil
_challenge_: "13 (10,000 XP)"
_languages_: "Aquan, Draconic"
_skills_: "Athletics +10, Perception +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Str +10, Dex +7, Con +9"
_damage_immunities_: "acid"
_condition_immunities_: "prone"
_speed_: "0 ft., swim 60 ft."
_hit points_: "195 (17d12 + 85)"
_armor class_: "18 (natural armor)"
_stats_: | 21 (+5) | 15 (+2) | 20 (+5) | 7 (-2) | 13 (+1) | 14 (+2) |

___Amphibious.___ The brine sea serpent can breathe air and water.

**Actions**

___Multiattack.___ A brine sea serpent attacks once with its bite and once
with its tail.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 21
(3d10 + 5) piercing damage plus 10 (3d6) acid damage, and the target is
grappled (escape DC 18). If the brine sea serpent already has a creature
grappled with its bite, it can only bite that creature, and it has advantage
on that attack.

___Tail Slap.___ Melee Weapon Attack: +10 to hit, reach 15 ft., one target. Hit:
18 (3d8 + 5) bludgeoning damage and the target must succeed at a DC
18 Strength saving throw or be pushed 10 feet away and knocked prone.

___Brine Blast (Recharge 6).___ The brine serpent releases a 50-foot cone
of briny acid. Creatures in the area must make a DC 18 Dexterity saving
throw. On a failed saving throw, the target takes 72 (16d8) acid damage.

**Legendary** Actions

The brine sea serpent can take up to 3 legendary actions, choosing from
the options below. Only one legendary action option can be used at a time
and only at the end of another creature’s turn. The serpent regains spent
legendary actions at the start of its turn.

___Tail Attack.___ The brine serpent uses its tail attack.

___Coiling Maneuver (Costs 2 Actions).___ The brine sea serpent attempts to
grapple a Large or smaller target. The target must make a DC 18 Strength
saving throw or be grappled and restrained. The sea serpent can’t use its
other legendary actions or its Tail Slap if it’s grappling a target. At the
beginning of each of the brine serpent’s turns, the grappled target takes 18
(3d8 + 5) bludgeoning damage.
