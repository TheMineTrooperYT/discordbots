"Naergoth Bladelord";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "11 (7,200 XP)"
_languages_: "Common, Draconic"
_senses_: "darkvision 60 ft."
_skills_: "Perception +6, Stealth +5"
_damage_immunities_: "poison"
_saving_throws_: "Dex +5, Wis +4"
_speed_: "30 ft."
_hit points_: "135 (18d8+54)"
_armor class_: "18 (plate)"
_condition_immunities_: "exhaustion, poisoned"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_stats_: | 20 (+5) | 12 (+1) | 16 (+3) | 12 (+1) | 14 (+2) | 16 (+3) |

___Sunlight Sensitivity.___ While in sunlight, Naergoth has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ Naergoth makes three attacks, either with his longsword or longbow. He can use Life Drain in place of one longsword attack.

___Life Drain.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 20 (5d6 + 3) necrotic damage. The target must succeed on a DC 15 Constitution saving throw or its hit point maximum is reduced by an amount equal to the damage taken. This reduction lasts until the target finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.

___Longsword.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 9 (1d8 + 5) slashing damage, or 10 (1d10 + 5) if used with two hands, plus 10 (3d6) necrotic damage.

___Longbow.___ Ranged Weapon Attack: +5 to hit, range 150/600 ft., one target. Hit: 5 (1d8 + 1) piercing damage plus 10 (3d6) necrotic damage.