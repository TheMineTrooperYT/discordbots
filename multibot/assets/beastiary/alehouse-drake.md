"Alehouse Drake";;;_size_: Tiny dragon
_alignment_: chaotic neutral
_challenge_: "1/2 (100 XP)"
_languages_: "Common, Draconic"
_skills_: "Deception +5, Insight +3, Persuasion +5"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Dex +5"
_condition_immunities_: "paralyzed, unconscious"
_speed_: "40 ft., fly 80 ft."
_hit points_: "65 (10d4 + 40)"
_armor class_: "13"
_stats_: | 7 (-2) | 16 (+3) | 19 (+4) | 11 (+0) | 12 (+1) | 16 (+3) |

___Innate Spellcasting.___ The drake's innate casting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

* At will: _friends, vicious mockery_

* 5/day each: _calm emotions, dissonant whispers, ray of sickness, hideous laughter_

* 3/day each: _confusion, invisibility_

___Forgetful Spellcasting.___ When a creature fails an Intelligence, Wisdom, or Charisma saving throw against a spell cast by an alehouse drake, the creature immediately forgets the source of the spellcasting.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (2d4 + 3) slashing damage.

___Breath Weapon (Recharge 5-6).___ An alehouse drake can burp a cloud of intoxicating gas in a 15-foot cone. A creature caught in the cloud becomes poisoned for 1 minute and must make a successful DC 14 Constitution saving throw or become stunned for 1d6 rounds.

___Discombobulating Touch.___ An alehouse drake can make a touch attack that grants its target +3 to Dexterity-based skill checks and melee attacks but also induces confusion as per the spell. This effect lasts for 1d4 rounds. A successful DC 13 Charisma saving throw negates this effect.

