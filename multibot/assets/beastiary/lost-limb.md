"Lost Limb";;;_size_: Small monstrosity
_alignment_: neutral
_challenge_: "2 (450 XP)"
_languages_: "--"
_skills_: "Perception +1"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "poison"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft., climb 30 ft."
_hit points_: "16 (3d6 + 6)"
_armor class_: "14 (natural armor)"
_stats_: | 16 (+3) | 16 (+3) | 14 (+2) | 4 (-3) | 4 (-3) | 4 (-3) |

___Adhesive.___ The lost limb adheres to the limb of its victim. A
Huge or smaller creature adhered to the limb is also grappled
(escape DC 13). Ability checks made to escape the grapple have
disadvantage.

___Compact.___ The lost limb may stay in the same space as another creature
or character.

___Fuse.___ Once the limb has consumed the limb it is replacing, the creature
that it is attached to slowly begins to heal, regaining 5 hit points at the
start of each of its turns. After 1 minute, the creature finally can control the
limb and gains a +2 bonus to Strength as the limb becomes a permanent
part of its body.

___Grappler.___ The lost limb has advantage on attack rolls against any
creature grappled by it.

**Actions**

___Slam.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5
(1d8 +1) bludgeoning damage and the target is grappled (escape DC 13)
and restrained.

___Consume.___ A creature grappled by the lost limb must make a DC 15
Constitution saving throw. On a failed save, the target takes 10 (2d6 + 3)
necrotic damage as the lost limb starts to consume the limb of the target.
The target takes an additional 10 (2d6) + 3) necrotic damage at the start of
each of its turns but can repeat the saving throw at the end of each of its
turns, ending the effect on a success. If the limb consumes more than onequarter
of the target’s hit points, the target’s limb is permanently replaced
by the lost limb as it attaches to the joint (shoulder or hip) of the target.
