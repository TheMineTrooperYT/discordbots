"Rock Troll";;;_size_: Large giant
_alignment_: chaotic evil
_challenge_: "11 (7,200 XP)"
_languages_: "Giant"
_skills_: "Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_resistances_: "force, poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "30 ft., burrow 20 ft."
_hit points_: "150 (12d10 + 84)"
_armor class_: "15 (natural armor)"
_stats_: | 24 (+7) | 10 (+0) | 24 (+7) | 6 (-2) | 9 (-1) | 6 (-2) |

___Keen Smell.___ The troll has advantage on Wisdom (Perception) checks
that rely on smell.

___Regeneration.___ The rock troll regains 10 hit points at the start of its turn
if it has at least 1 hit point and is underground touching earth or rock. If
the rock troll takes acid or fire damage, this trait doesn’t function at the
start of the troll’s next turn.

___Rend.___ If the troll hits the target with both claws on the same turn, the
troll rends the target, dealing an additional 22 (4d10) slashing damage to
the target.

___Sunlight Vulnerability.___ The rock troll begins to turn to stone and its
speed is halved when it starts its turn in true sunlight (magical light does
not have the same effect). The rock troll must make a DC 17 Constitution
saving throw at the end of its next turn. If it fails this saving throw, it
completely turns to stone and is petrified. The troll must repeat the saving
throw for each turn it remains in sunlight.

The petrified effect is permanent unless dispelled (but only if done out
of direct sunlight).

___Tough Hide.___ The rock troll has extremely thick, tough skin that protects
it from nonmagical weapons.

**Actions**

___Multiattack.___ The rock troll makes three melee attacks: one with bite and
two claws. If both claw attacks hit the same target on the same turn, the
troll rends the creature with its claws, inflicting additional slashing damage.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 16
(2d8 + 7) piercing damage.

___Claws.___ Melee Weapon Attack: +11 to hit, reach 5 ft., one target. Hit: 18
(2d10 + 7) slashing damage.
