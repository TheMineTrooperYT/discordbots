"Rasa Cutthroat";;;_size_: Medium humanoid
_alignment_: any evil alignment
_challenge_: "2 (450 XP)"
_languages_: "Common"
_skills_: "Deception +5, Stealth +7"
_senses_: "passive Perception 10"
_speed_: "30 ft."
_hit points_: "52 (8d8 + 16)"
_armor class_: "15 (studded leather)"
_stats_: | 12 (+1) | 17 (+3) | 14 (+2) | 14 (+2) | 10 (+0) | 13 (+1) |

___Innate Spellcasting.___ The rasa's innate spellcasting
ability is Intelligence (spell save DC 12). It can
innately cast the following spells, requiring no
components:

* At will: _mage hand, message_

* 1/day each: _disguise self, silent image_

* 2/day each: _misty step_

___Inscrutable Intentions.___ The rasa is immune to any
magical effects to determine if it is lying.

___Mask of Debilitation.___ While wearing this mask, the
cutthroat's weapon attacks inflict a delayed paralysis on
the target. On hit, the target must succeed on a DC 12
Constitution saving throw against paralysis. On a failed
save, the target's body begins to stiffen and is
restrained. A restrained creature must repeat this
saving throw at the end of its next turn, becoming
paralyzed for 1 minute on a failed save. On a success,
this effect ends and the creature is immune to the
Mask of Debilitation effect for 24 hours.

___Mask of Subtlety.___ While wearing this mask, a target hit
by the cutthroat's dagger cannot speak until the start
of the cutthroat's next turn. In addition, the cutthroat
can Hide or Disengage as a bonus action on each of its
turns.

___Sight of the Rasa.___ The rasa share a special sight that
is invisible to all non-rasa creatures. As an action, a
rasa can touch a creature and place a secret mark
on that creature's chest. This mark lets other rasa
know whether or not this creature should be
regarded as a friend or a foe.

**Actions**

___Multiattack.___ The rasa makes two melee attacks.

___Poisoned Dagger.___ Melee or Ranged Weapon Attack: +5
to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5
(1d4 + 3) piercing damage plus 2 (1d4) poison
damage.

___Explosive Coin (1/Day).___ The cutthroat throws a gold
coin on the ground and sets it to either immediately
detonate or to detonate when another creature
attempts to pick it up. When the coin is triggered, each
creature within 5 feet of the coin must make a DC 12
Dexterity saving throw, taking 14 (4d6) fire damage on
a failed save, or half as much damage on a successful
one. If the coin is not triggered within 24 hours, it
becomes a mundane gold coin.
