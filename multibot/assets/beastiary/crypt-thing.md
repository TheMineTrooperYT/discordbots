"Crypt Thing";;;_size_: Medium undead
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "Common"
_skills_: "Deception +6, Perception +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_speed_: "30 ft."
_hit points_: "84 (13d8 + 26)"
_armor class_: "15 (natural armor)"
_stats_: | 12 (+1) | 16 (+3) | 15 (+2) | 12 (+1) | 14 (+2) | 16 (+3) |

___Magic Weapons.___ The crypt thing’s weapon attacks are magical.

**Actions**

___Multiattack.___ The crypt thing makes two attacks with its claws.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13
(3d6 + 3) slashing damage and 10 (3d6) necrotic damage.

___Teleport Other (1/day).___ As an action, the crypt thing can teleport all
creatures within 50 feet of it to a randomly determined location. A creature
affected by the crypt thing’s Teleport Other must make a DC 15 Wisdom
saving throw to avoid being teleported.
An affected creature is teleported in a random direction and a random
distance (1d10 x 100 feet) away from the crypt thing. Roll randomly for
each creature that fails its saving throw.
If the affected creature would arrive in a place already occupied by
an object or another creature, the affected creature takes 14 (4d6) force
damage and is not teleported.
