"Noble";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1/8 (25 XP)"
_languages_: "any two languages"
_skills_: "Deception +5, Insight +4, Persuasion +5"
_speed_: "30 ft."
_hit points_: "9 (2d8)"
_armor class_: "15 (breastplate)"
_stats_: | 11 (0) | 12 (+1) | 11 (0) | 12 (+1) | 14 (+2) | 16 (+3) |

**Actions**

___Rapier.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 5 (1d8 + 1) piercing damage.

**Reactions**

___Parry.___ The noble adds 2 to its AC against one melee attack that would hit it. To do so, the noble must see the attacker and be wielding a melee weapon.