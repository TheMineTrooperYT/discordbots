"Beldora";;;_size_: Medium humanoid (illuskan human)
_alignment_: chaotic good
_challenge_: "0 (10 XP)"
_languages_: "Common, Draconic, Dwarvish, Halfling"
_skills_: "Deception +5, Insight +3, Investigation +5, Perception +5, Persuasion +5"
_speed_: "30 ft."
_hit points_: "18 (4d8)"
_armor class_: "12"
_senses_: "qpassive Perception 13"
_stats_: | 10 (0) | 14 (+2) | 10 (0) | 16 (+3) | 12 (+1) | 16 (+3) |

**Actions**

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6+2) slashing damage, .

___Hand Crossbow.___ Ranged Weapon Attack: +4 to hit, range 30/120 ft., one target. Hit: 5 (1d6+2) piercing damage. Beldora carries ten crossbow bolts.

**Reactions**

___Duck and Cover.___ Beldora adds 2 to her AC against on melee attack that would hit her. To do so, Duvessa must see the attacker and can't be grappled or restrained.

**Roleplaying** Information

Beldora is a member of the harpers who survives using her wits and wiles. She looks like a homeless waif, but she's a survivor who shies away from material wealth.

**Ideal:** "We should all strive to help one another"

**Bond:** "I'll risk my life to protect the powerless."

**Flaw:** "I like lying to people. Makes life more interesting, no?"