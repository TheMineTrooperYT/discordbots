"Veteran";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "any one language (usually Common)"
_skills_: "Athletics +5, Perception +2"
_speed_: "30 ft."
_hit points_: "58 (9d8+18)"
_armor class_: "17 (splint)"
_stats_: | 16 (+3) | 13 (+1) | 14 (+2) | 10 (0) | 11 (0) | 10 (0) |

**Actions**

___Multiattack.___ The veteran makes two longsword attacks. If it has a shortsword drawn, it can also make a shortsword attack.

___Longsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage, or 8 (1d10 + 3) slashing damage if used with two hands.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Heavy Crossbow.___ Ranged Weapon Attack: +3 to hit, range 100/400 ft., one target. Hit: 6 (1d10 + 1) piercing damage.