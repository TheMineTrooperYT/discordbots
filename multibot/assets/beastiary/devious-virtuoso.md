"Devious Virtuoso";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "Any three languages"
_skills_: "Acrobatics +8, Deception +11, Insight +9, Perception +5, Performance +11, Sleight of Hand +12"
_senses_: "passive Perception 15"
_saving_throws_: "Dex +8, Cha +7"
_speed_: "30 ft."
_hit points_: "77 (14d8 + 14)"
_armor class_: "17 (half plate)"
_stats_: | 10 (+0) | 18 (+4) | 13 (+1) | 16 (+3) | 12 (+1) | 17 (+3) |

___Combat Caster.___ The virtuoso can perform the somatic
components of spells even when it has weapons or a
shield in one or both hands.

___Cunning Action.___ As a bonus action, the virtuoso can
take the Dash, Disengage, or Hide action.

___Inspire (3/Short Rest).___ Target uninspired creature within
60 feet of the virtuoso that can hear it gains a d8
inspiration die. Once within the next 10 minutes, that
creature can roll that die and add the number rolled to
one ability check, attack roll, or saving throw it makes.
This die must be rolled before it is decided if the roll
succeeds or fails.

___Sneak Attack (1/Turn).___ The virtuoso deals an extra 7
(2d6) damage when it hits a target with a weapon
attack and has advantage on the attack roll, or when the
target is within 5 feet of an ally of the virtuoso that
isn't incapacitated and the virtuoso doesn't have
disadvantage on the attack roll.

___Spellcasting.___ The virtuoso is a 6th-level spellcaster. Its
spellcasting ability is Charisma (spell save DC 15, +7 to
hit with spell attacks). The virtuoso has the following
bard spells prepared:

* Cantrips (at will): _friends, minor illusion, vicious mockery_

* 1st level (4 slots): _charm person, disguise self, illusory script_

* 2nd level (3 slots): _detect thoughts, suggestion_

* 3rd level (3 slots): _feign death, major image_

**Actions**

___Multiattack.___ The virtuoso makes three attacks with its
rapier or two attacks with its longbow.

___Rapier.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one
target. Hit: 8 (1d8 + 4) piercing damage.

___Longbow.___ Ranged Weapon Attack: +8 to hit, range
150/600 ft., one target. Hit: 8 (1d8 + 4) piercing
damage.
