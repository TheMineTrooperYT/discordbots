"Giant Fire Beetle";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "blindsight 30 ft."
_speed_: "30 ft."
_hit points_: "4 (1d6+1)"
_armor class_: "13 (natural armor)"
_stats_: | 8 (-1) | 10 (0) | 12 (+1) | 1 (-5) | 7 (-2) | 3 (-4) |

___Illumination.___ The beetle sheds bright light in a 10-foot radius and dim light for an additional 10 ft..

**Actions**

___Bite.___ Melee Weapon Attack: +1 to hit, reach 5 ft., one target. Hit: 2 (1d6 - 1) slashing damage.