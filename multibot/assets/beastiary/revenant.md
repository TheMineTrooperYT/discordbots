"Revenant";;;_size_: Medium undead
_alignment_: neutral
_challenge_: "5 (1,800 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_saving_throws_: "Str +7, Con +7, Wis +6, Cha +7"
_speed_: "30 ft."
_hit points_: "136 (16d8+64)"
_armor class_: "13 (leather armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned, stunned"
_damage_resistances_: "necrotic, psychic"
_stats_: | 18 (+4) | 14 (+2) | 18 (+4) | 13 (+1) | 16 (+3) | 18 (+4) |

___Regeneration.___ The revenant regains 10 hit points at the start of its turn. If the revenant takes fire or radiant damage, this trait doesn't function at the start of the revenant's next turn. The revenant's body is destroyed only if it starts its turn with 0 hit points and doesn't regenerate.

___Rejuvenation.___ When the revenant's body is destroyed, its soul lingers. After 24 hours, the soul inhabits and animates another corpse on the same plane of existence and regains all its hit points. While the soul is bodiless, a wish spell can be used to force the soul to go to the afterlife and not return.

___Turn Immunity.___ The revenant is immune to effects that turn undead.

___Vengeful Tracker.___ The revenant knows the distance to and direction of any creature against which it seeks revenge, even if the creature and the revenant are on different planes of existence. If the creature being tracked by the revenant dies, the revenant knows.

**Actions**

___Multiattack.___ The revenant makes two fist attacks.

___Fist.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage. If the target is a creature against which the revenant has sworn vengeance, the target takes an extra 14 (4d6) bludgeoning damage. Instead of dealing damage, the revenant can grapple the target (escape DC 14) provided the target is Large or smaller.

___Vengeful Glare.___ The revenant targets one creature it can see within 30 feet of it and against which it has sworn vengeance. The target must make a DC 15 Wisdom saving throw. On a failure, the target is paralyzed until the revenant deals damage to it, or until the end of the revenant's next turn. When the paralysis ends, the target is frightened of the revenant for 1 minute. The frightened target can repeat the saving throw at the end of each of its turns, with disadvantage if it can see the revenant, ending the frightened condition on itself on a success.