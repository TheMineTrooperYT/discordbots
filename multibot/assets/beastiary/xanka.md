"Xanka";;;_size_: Small construct
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "Understands the languages of its creator but can't"
_senses_: "blindsight 120 ft., passive Perception 10"
_condition_immunities_: "charmed, exhaustion, frightened,"
_speed_: "25 ft., climb 15 ft."
_hit points_: "18 (4d6 + 4)"
_armor class_: "15 (natural armor)"
_stats_: | 10 (+0) | 15 (+2) | 12 (+1) | 4 (-3) | 10 (+0) | 7 (-2) |

___Ingest Weapons.___ When the xanka is hit by a melee weapon and the final, adjusted attack roll is 19 or less, the weapon gains a permanent -1 penalty to damage rolls, after inflicting damage for this attack. If the penalty reaches -5, the weapon is destroyed. Even magic weapons are subject to this effect.

___Magic Weapons.___ The xanka's weapon attacks are magical.

___Constructed Nature.___ A xanka doesn't require air, food, drink, or sleep.

**Actions**

___Absorb.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 2) force damage, and the xanka regains hit points equal to the damage caused by its attack. In addition, a living creature hit by this attack must make a successful DC 12 Dexterity saving throw or suffer a gaping wound that causes 2 (1d4) necrotic damage at the end of each of the creature's turns until the wound is treated with magical healing or with a successful DC 10 Intelligence (Medicine) check. If a creature who fails this saving throw is wearing armor or using a shield, the creature can choose to prevent the necrotic damage by permanently reducing the AC of its armor or shield by 1 instead.

