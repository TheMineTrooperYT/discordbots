"Shadowghast";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_skills_: "Perception +3, Stealth +8"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "poison"
_damage_resistances_: "necrotic"
_condition_immunities_: "charmed, exhaustion, poisoned"
_speed_: "35 ft."
_hit points_: "49 (9d8 + 9)"
_armor class_: "15"
_stats_: | 14 (+2) | 20 (+5) | 12 (+1) | 12 (+1) | 11 (+0) | 8 (-1) |

___Stench.___ Any creature that starts its turn within 5 feet of the shadowghast must succeed on a DC 12 Constitution saving throw or be poisoned until the start of its next turn. On a successful saving throw, the creature is immune to this Stench for 24 hours.

___Shadow Stealth.___ While in dim light or darkness, the shadowghast can take the Hide action as a bonus action.

**Actions**

___Multiattack.___ The shadowghast makes two attacks: one with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 11 (2d8 + 2) slashing damage plus 5 (1d10) necrotic damage.

___Claws.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) slashing damage. If the target is a creature other than an undead, it must succeed on a DC 12 Constitution saving throw or be paralyzed for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.
