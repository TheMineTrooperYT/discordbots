"Myconid Sprout";;;_size_: Small plant
_alignment_: lawful neutral
_challenge_: "0 (10 XP)"
_senses_: "darkvision 120 ft."
_speed_: "10 ft."
_hit points_: "7 (2d6)"
_armor class_: "10"
_stats_: | 8 (-1) | 10 (0) | 10 (0) | 8 (-1) | 11 (0) | 5 (-3) |

___Distress Spores.___ When the myconid takes damage, all other myconids within 240 feet of it can sense its pain.

___Sun Sickness.___ While in sunlight, the myconid has disadvantage on ability checks, attack rolls, and saving throws. The myconid dies if it spends more than 1 hour in direct sunlight.

**Actions**

___Fist.___ Melee Weapon Attack: +1 to hit, reach 5 ft., one target. Hit: 1 (1d4- 1) bludgeoning damage plus 2 (1d4) poison damage.

___Rapport Spores (3/Day).___ A 10-foot radius of spores extends from the myconid. These spores can go around corners and affect only creatures with an Intelligence of 2 or higher that aren't undead, constructs, or elementals. Affected creatures can communicate telepathically with one another while they are within 30 feet of each other. The effect lasts for 1 hour.