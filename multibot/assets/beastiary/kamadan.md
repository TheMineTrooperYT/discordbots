"Kamadan";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "4 (1,100 XP)"
_senses_: "passive Perception 14"
_skills_: "Perception +4, Stealth +7"
_speed_: "30 ft."
_hit points_: "67 (9d10 +18)"
_armor class_: "13"
_stats_: | 16 (+3) | 16 (+3) | 14 (+2) | 3 (-4) | 14 (+2) | 10 (0) |

___Keen Smell.___ The kamadan has advantage on Wisdom (Perception) checks that rely on smell.

___Pounce.___ If the kamadan moves at least 20 feet straight toward a creature and then hits it with a claw attack on the same turn, that target must succeed on a DC 13 Strength saving throw or be knocked prone. If the target is knocked prone, the kamadan can make two attacks -one with its bite and one with its snakes- against it as a bonus action.

**Actions**

___Multiattack.___ The kamadan makes two attacks: one with its bite or claw and one with its snakes.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (ld6 +3) piercing damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target.Hit: 6 (ld6 +3) slashing damage.

___Snakes.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 6 (ld6 +3) piercing damage, and the target must make a DC 12 Constitution saving throw, taking 21 (6d6) poison damage on a failed save, or half as much damage on a successful one.

___Sleep Breath (Recharges after a Short or Long Rest).___ The kamadan exhales sleep gas in a 30-foot cone. Each creature in that area must succeed on a DC 12 Constitution saving throw or fall unconscious for 10 minutes. This effect ends for a creature if it takes damage or someone uses an action to wake it.