"Nightgarm";;;_size_: Large monstrosity
_alignment_: chaotic evil
_challenge_: "6 (2300 XP)"
_languages_: "Common, Giant, Goblin, telepathy 200 ft. (with falsemen only)"
_skills_: "Perception +5, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_damage_vulnerabilities_: "radiant; silvered weapons"
_damage_resistances_: "lightning, thunder; bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_speed_: "20 ft. (bipedal), 40 ft. (quadrupedal)"
_hit points_: "114 (12d10 + 48)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 14 (+2) | 18 (+4) | 10 (+0) | 15 (+2) | 16 (+3) |

___Spawn Falseman.___ If a nightgarm spends an entire round consuming a humanoid corpse, it immediately becomes pregnant. Nine hours later, it gives birth to a duplicate of the devoured creature. Known as a "falseman," this duplicate has all the memories and characteristics of the original but serves its mother loyally, somewhat similar to a familiar's relationship to a wizard. A nightgarm can have up to 14 falsemen under her control at a time. A nightgarm can communicate telepathically with its falsemen at ranges up to 200 feet.

___Distending Maw.___ Like snakes, nightgarms can open their mouths far wider than other creatures of similar size. This ability grants it a formidable bite and allows it to swallow creatures up to Medium size.

___Superstitious.___ A nightgarm must stay at least 5 feet away from a brandished holy symbol or a burning sprig of wolf's bane, and it cannot touch or make melee attacks against a creature holding one of these items. After 1 round, the nightgarm can make a DC 15 Charisma saving throw at the start of each of its turns; if the save succeeds, the nightgarm temporarily overcomes its superstition and these restrictions are lifted until the start of the nightgarm's next turn.

___Innate Spellcasting.___ The nightgarm's innate spellcasting ability is Charisma (spell save DC 14). It can innately cast the following spells, requiring no material components:

3/day each: darkness, dissonant whispers, hold person

1/day each: conjure woodland beings (wolves only), dimension door, scrying (targets falsemen only)

**Actions**

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 27 (4d10 + 5) piercing damage, and a Medium or smaller target must succeed on a DC 15 Strength saving throw or be swallowed whole. A swallowed creature is blinded and restrained and has total cover against attacks and other effects outside the nightgarm. It takes 21 (6d6) acid damage at the start of each of the nightgarm's turns. A nightgarm can have only one creature swallowed at a time. If the nightgarm takes 25 damage or more on a single turn from the swallowed creature, the nightgarm must succeed on a DC 14 Constitution saving throw at the end of that turn or regurgitate the creature, which falls prone within 5 feet of the nightgarm. If the nightgarm dies, a swallowed creature is no longer restrained by it and can escape from the corpse by using 10 feet of movement, exiting prone.

