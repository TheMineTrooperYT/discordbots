"Cikavak";;;_size_: Tiny fey
_alignment_: neutral
_challenge_: "1/8 (25 XP)"
_languages_: "understands Common; telepathy (touch)"
_skills_: "Perception +5, Stealth +9"
_senses_: "passive Perception 15"
_damage_resistances_: "acid, fire, poison"
_speed_: "10 ft., fly 40 ft."
_hit points_: "17 (7d4)"
_armor class_: "12"
_stats_: | 4 (-3) | 15 (+2) | 10 (+0) | 12 (+1) | 12 (+1) | 4 (-3) |

___Innate Spellcasting.___ The cikavak's innate spellcasting ability is Wisdom (spell save DC 11). It can innately cast the following spells, requiring no material components:

* At will: _speak with animals_

* 1/day: _silence_

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, range 5 ft., one target. Hit: 7 (1d4 + 2) piercing damage.

