"Yusdrayl";;;_size_: Small humanoid (kobold)
_alignment_: lawful evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Draconic"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +2, Insight +2, Stealth +4"
_speed_: "30 ft."
_hit points_: "16 (3d6+6)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 8 (-1) | 15 (+2) | 14 (+2) | 10 (0) | 10 (0) | 16 (+3) |

___Source.___ tales from the yawning portal,  page 248

___Spellcasting.___ Yusdrayl is a 2nd-level spellcaster. Her spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). She knows the following sorcerer spells:

Cantrips (at will): mage hand, prestidigitation, ray of frost, shocking grasp

1st level (4 slots): burning hands, chromatic orb, mage armor

___Sunlight Sensitivity.___ While in sunlight, Yusdrayl has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Pack Tactics.___ Yusdrayl has advantage on an attack roll against a creature if at least one of her allies is within 5 feet of the creature and the ally isn't incapacitated.

**Actions**

___Dagger.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.