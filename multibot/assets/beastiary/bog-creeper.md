"Bog Creeper";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "9 (5,000 XP)"
_languages_: "--"
_skills_: "Perception +5"
_senses_: "tremorsense 60 ft., passive Perception 15"
_speed_: "10 ft., swim 20 ft."
_hit points_: "104 (11d8 + 55)"
_armor class_: "12 (natural armor)"
_stats_: | 18 (+4) | 10 (+0) | 20 (+5) | 3 (-4) | 14 (+2) | 6 (-2) |

___False Appearance.___ While the bog creeper remains
motionless, it is indistinguishable from normal plants.

___Marsh Move.___ A bog creeper doesn’t treat marshy,
swampy terrain as difficult terrain.

**Actions**

___Multiattack.___ The bog creeper makes up to four attacks:
one with its bite, one slam, and two with its tendrils.

___Bite.___ Melee Weapon Attack: +7 to hit,
reach 5 ft., one target. Hit: 13 (2d8 + 4)
piercing damage.

___Slam.___ Melee Weapon Attack: +7 to hit,
reach 5 ft., one target. Hit: 15 (2d10 + 4)
bludgeoning damage.

___Tendrils.___ Melee Weapon Attack: +7 to hit,
reach 10 ft., one target. Hit: 11 (2d6 + 4)
bludgeoning damage. The target is grappled
(escape DC 14) if the bog creeper isn’t
already grappling a creature, and the target
is restrained until the grapple ends. The bog
creeper can only grapple one target.

___Acid Spray (3/day).___ The bog creeper sprays stomach acid in a 30-foot
cone. Each creature in that area must make a DC 15 Dexterity saving
throw, taking 14 (3d8) acid damage on a failed save, or half as much
damage on a successful one.
