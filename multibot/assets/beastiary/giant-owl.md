"Giant Owl";;;_size_: Large beast
_alignment_: neutral
_challenge_: "1/4 (50 XP)"
_languages_: "Giant Owl, understands Common, Elvish, and Sylvan but can't speak"
_senses_: "darkvision 120 ft."
_skills_: "Perception +5, Stealth +4"
_speed_: "5 ft., fly 60 ft."
_hit points_: "19 (3d10+3)"
_armor class_: "12"
_stats_: | 13 (+1) | 15 (+2) | 12 (+1) | 8 (-1) | 13 (+1) | 10 (0) |

___Flyby.___ The owl doesn't provoke opportunity attacks when it flies out of an enemy's reach.

___Keen Hearing and Sight.___ The owl has advantage on Wisdom (Perception) checks that rely on hearing or sight.

**Actions**

___Talons.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 8 (2d6 + 1) slashing damage.