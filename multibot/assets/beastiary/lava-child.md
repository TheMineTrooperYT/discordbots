"Lava Child";;;_page_number_: 313
_size_: Medium humanoid (lava child)
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "Common, Ignan"
_senses_: "darkvision 60 ft., passive Perception 10"
_skills_: "Athletics +6, Survival +2"
_speed_: "25 ft, climb 20 ft."
_hit points_: "60 (8d8+24)"
_armor class_: "11"
_stats_: | 18 (+4) | 13 (+1) | 16 (+3) | 11 (0) | 10 (0) | 10 (0) |

___Metal Immunity.___ The lava child can move through metal without hindrance, and it has advantage on attack rolls against any creature wearing metal armor or using a metal shield.

**Actions**

___Multiattack.___ The lava child makes two attacks: one with its bite and one with its claws.