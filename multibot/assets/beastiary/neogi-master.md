"Neogi Master";;;_size_: Medium aberration
_alignment_: lawful evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common, Deep Speech, Undercommon, telepathy 30 ft."
_senses_: "darkvision 120 ft. (penetrates magical darkness)"
_skills_: "Arcana +5, Deception +6, Intimidation +6, Perception +3, Persuasion +6"
_saving_throws_: "Wis +3"
_speed_: "30 ft., climb 30 ft."
_hit points_: "71 (13d6+26)"
_armor class_: "15 (natural armor)"
_stats_: | 6 (-2) | 16 (+3) | 14 (+2) | 16 (+3) | 12 (+1) | 18 (+4) |

___Mental Fortitude.___ The neogi has advantage on saving throws against being charmed or frightened, and magic can't put the neogi to sleep.

___Spellcasting.___ The neogi is a 7th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 14, +6 to hit with spell attacks). It regains its expended spell slots when it finishes a short or long rest. It knows the following warlock spells:

* Cantrips (at will): _eldritch blast _(range 300 ft., +4 bonus to each damage roll) _, guidance, mage hand, minor illusion, prestidigitation, vicious mockery_

* 1st-4th level (2 4th-level slots): _arms of Hadar, counterspell, dimension door, fear, hold person, hunger of Hadar, invisibility, unseen servant_

___Spider Climb.___ The neogi can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

**Actions**

___Multiattack.___ The neogi makes two attacks: one with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 it, one target. Hit: 6 (1d6+3) piercing damage plus 14 (4d6) poison damage, and the target must succeed on a DC 12 Constitution saving throw or become poisoned for 1 minute. A target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Claws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (2d4+3) slashing damage.

___Enslave (Recharges after a Short or Long Rest).___ The neogi targets one creature it can see within 30 feet of it. The target must succeed on a DC 14 Wisdom saving throw or be magically  charmed by the neogi for 1 day, or until the neogi dies or is more than 1 mile from the target. The charmed target obeys the neogi's commands and can't take reactions, and the neogi and the target can communicate telepathically with each other at a distance of up to 1 mile. Whenever the charmed target takes damage, it can repeat the saving throw, ending the effect on itself on a success.
