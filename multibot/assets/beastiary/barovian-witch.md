"Barovian Witch";;;_page_number_: 229
_size_: Medium humanoid (human)
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +4, Perception +2"
_speed_: "30 ft."
_hit points_: "16 (3d8+3)"
_armor class_: "10"
_stats_: | 7 (-2) | 11 (0) | 13 (+1) | 14 (+2) | 11 (0) | 12 (+1) |

___Spellcasting.___ The witch is a 3rd-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 12, +4 to hit with spell attacks). The witch has the following wizard spells prepared:

* Cantrips (at will): _mage hand, prestidigitation, ray of frost_

* 1st level (4 slots): _ray of sickness, sleep, Tasha's hideous laughter_

* 2nd level (2 slots): _alter self, invisibility_

**Actions**

___Claws (Requires Alter Self).___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d6+1) slashing damage. This attack is magical.

___Dagger.___ Melee or Ranged Weapon Attack: +2 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 2 (1d4) piercing damage.
