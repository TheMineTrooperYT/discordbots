"Yakfolk Warrior";;;_size_: Large monstrosity
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Yikaria"
_skills_: "Deception +4, Survival +4"
_speed_: "30 ft."
_hit points_: "60 (8d10+16)"
_armor class_: "11 (leather)"
_stats_: | 18 (+4) | 11 (0) | 15 (+2) | 14 (+2) | 15 (+2) | 14 (+2) |

___Possession (Recharges after a rest.___ The yakfolk attempts to magically possess a humanoid or giant. The yakfolk must touch the target throughout a short rest or the attempt fails. At the end of the rest, the target must succeed a DC 12 Con saving throw or be possessed by the yakfolk, which disappears with everything its carrying and wearing. Until the possession ends, the target is incapacitated, loses control of its body, and it unaware of its surroundings. The yakfolk now controls the body and cannot be targeted by any attack, spell, or other effect, and it retains its alignment, its Intelligence, Wisdom, and Charisma scores; and its proficiencies. It otherwise uses the target's statistics, except the target's knowledge, class features, feats, and proficiencies.

The possession lats until either the body drops to 0 hit points, the yakfolk ends the possession as an action, or the yakfolk is forced out of the body by an effect such as dispel evil and good spell. When the possession ends, the yakfolk appears in an unoccupied space within 5 feet of the body and is stunned until the end of its next turn. If the host body dies while it is possessed by the yakfolk, the yakfolk dies as well and its body does not reappear.

**Actions**

___Multiattack.___ The yakfolk makes two attacks, either with it's greatsword or its longbow

___Greatsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 18 (4d6 + 4) slashing damage.

___Longbow.___ Ranged Weapon Attack: +2 to hit, range 150/600 ft., one target. Hit: 9 (2d8) piercing damage.