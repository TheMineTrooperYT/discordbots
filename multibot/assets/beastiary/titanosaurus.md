"Titanosaurus";;;_size_: Gargantuan beast
_alignment_: unaligned
_challenge_: "7 (2,900 XP)"
_languages_: "--"
_senses_: "passive Perception 8"
_saving_throws_: "Con +8"
_speed_: "30 ft."
_hit points_: "201 (13d20 + 65)"
_armor class_: "17 (natural armor)"
_stats_: | 30 (+10) | 7 (-2) | 20 (+5) | 2 (-4) | 10 (+0) | 7 (-2) |

**Actions**

___Stomp.___ Melee Weapon Attack: +13 to hit, reach 20
ft., one target. Hit: 40 (6d10 + 7) bludgeoning
damage, and the target must succeed on a DC 16
Strength saving throw or be knocked prone.

___Tail.___ Melee Weapon Attack: +13 to hit, reach 20 ft.,
one target. Hit: 45 (7d10 + 7) bludgeoning damage.

**Legendary** Actions

The titanosaurus can take 3 legendary actions,
choosing from the options below. Only one
legendary action can be used at a time and only at
the end of another creature’s turn. The
titanosaurus regains spent legendary actions at the
start of its turn.

___Momentum.___ The titanosaurus moves half its speed
without provoking opportunity attacks.

___Tough Defense (2 actions).___ The titanosaurus picks
one of the following damage types and is
resistant to that type of damage until the start of
its next turn: bludgeoning, piercing or slashing.

___Tail Thunder (Costs 3 Actions).___ The titanosaurus
makes a tail attack with disadvantage against
each creature in a 20 foot cone.
