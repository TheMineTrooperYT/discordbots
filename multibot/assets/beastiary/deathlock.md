"Deathlock";;;_page_number_: 128
_size_: Medium undead
_alignment_: neutral evil
_challenge_: "4 (1100 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 60 ft., passive Perception 11"
_skills_: "Arcana +4, History +4"
_damage_immunities_: "poison"
_saving_throws_: "Int +4, Cha +5"
_speed_: "30 ft."
_hit points_: "36  (8d8)"
_armor class_: "12 (15 with mage armor)"
_condition_immunities_: "exhaustion, poisoned"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 11 (0) | 15 (+2) | 10 (0) | 14 (+2) | 12 (+1) | 16 (+3) |

___Innate Spellcasting.___ The deathlock's innate spellcasting ability is Charisma (spell save DC 13). It can innately cast the following spells, requiring no material components:

* At will: _detect magic, disguise self, mage armor_

___Spellcasting.___ The deathlock is a 5th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). It regains its expended spell slots when it finishes a short or long rest. It knows the following warlock spells:

* Cantrips (at will): _chill touch, eldritch blast, mage hand_

* 3rd level (2 slots): _arms of Hadar, dispel magic, hold person, hunger of Hadar, invisibility, spider climb_

___Turn Resistance.___ The deathlock has advantage on saving throws against any effect that turns undead.

**Actions**

___Deathly Claw___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) necrotic damage.
