"Spark";;;_size_: Tiny elemental
_alignment_: chaotic neutral
_challenge_: "7 (2900 XP)"
_languages_: "Common, Primordial"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Dex +8"
_damage_immunities_: "lightning"
_damage_resistances_: "acid, fire, force, thunder; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, poisoned, prone, restrained, unconscious"
_speed_: "10 ft., fly 60 ft. (hover)"
_hit points_: "84 (13d4 + 52)"
_armor class_: "16 (natural armor)"
_stats_: | 4 (-3) | 20 (+5) | 18 (+4) | 10 (+0) | 12 (+1) | 17 (+3) |

___Innate Spellcasting.___ The spark's innate casting ability is Charisma (spell save DC 14, +6 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

At will: shocking grasp

3/day: lightning bolt

1/day: call lightning

**Actions**

___Inhabit.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: The target must succeed on a DC 14 Charisma saving throw or become dominated by the spark, as the dominate person spell. The spark instantly enters the target's space and merges into the target's physical form. While inhabiting a creature, a spark takes no damage from physical attacks. The target creature receives a +4 bonus to its Dexterity and Charisma scores while it's inhabited. The speech and actions of an inhabited creature are noticeably jerky and erratic to any creature with passive Perception 14 or higher. Each time the spark uses innate spellcasting, the host can attempt another DC 14 Charisma saving throw. A successful save expels the spark, which appears in an unoccupied space within 5 feet of the former host. The inhabiting spark slowly burns out its host's nervous system. The inhabited creature must make a successful DC 15 Constitution saving throw at the end of each 24 hour-period or take 2d6 lightning damage and have its maximum hit points reduced by the same amount. The creature dies if this damage reduces its hit point maximum to 0. The reduction lasts until the inhabited creature completes a long rest after the spark is expelled.

