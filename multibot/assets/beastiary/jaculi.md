"Jaculi";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_senses_: "blindsight 30 ft., passive Perception 11"
_skills_: "Athleitcs +4, Perception +1, Stealth +4"
_speed_: "30 ft., climb 20 ft."
_hit points_: "16 (3d10)"
_armor class_: "14 (natural armor)"
_stats_: | 15 (+2) | 14 (+2) | 11 (0) | 2 (-4) | 8 (-1) | 3 (-4) |

___Camouflage.___ The jaculi has advantage on Dexterity (Stealth) checks made to hide.

___Keen Smell.___ The jaculi has advantage on Wisdom (Perception) checks that rely on smell.

**Actions**

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 +2) piercing damage.

___Spring.___ The jaculi springs up to 30 feet in a straight line and makes a bite attack against a target within its reach. This attack has advantage if the jaculi springs at least 10 feet. If the attack hits, the bite deals an extra 7 (2d6) piercing damage.