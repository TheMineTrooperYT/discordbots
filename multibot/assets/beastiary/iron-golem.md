"Iron Golem";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "16 (15,000 XP)"
_languages_: "understands the languages of its creator but can't speak"
_senses_: "darkvision 120 ft."
_damage_immunities_: "fire, poison, psychic, bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_speed_: "30 ft."
_hit points_: "210 (20d10+100)"
_armor class_: "20 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_stats_: | 24 (+7) | 9 (-1) | 20 (+5) | 3 (-4) | 11 (0) | 1 (-5) |

___Fire Absorption.___ Whenever the golem is subjected to fire damage, it takes no damage and instead regains a number of hit points equal to the fire damage dealt.

___Immutable Form.___ The golem is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The golem has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The golem's weapon attacks are magical.

**Actions**

___Multiattack.___ The golem makes two melee attacks.

___Slam.___ Melee Weapon Attack: +13 to hit, reach 5 ft., one target. Hit: 20 (3d8 + 7) bludgeoning damage.

___Sword.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 23 (3d10 + 7) slashing damage.

___Poison Breath (Recharge 5-6).___ The golem exhales poisonous gas in a 15-foot cone. Each creature in that area must make a DC 19 Constitution saving throw, taking 45 (l0d8) poison damage on a failed save, or half as much damage on a successful one.