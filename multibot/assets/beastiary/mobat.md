"Mobat";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "--"
_skills_: "Perception +3"
_senses_: "blindsight 60 ft., passive Perception 15"
_speed_: "20 ft., fly 40 ft."
_hit points_: "51 (6d10 + 18)"
_armor class_: "14 (natural armor)"
_stats_: | 17 (+3) | 15 (+2) | 16 (+3) | 6 (-2) | 13 (+1) | 6 (-2) |

___Echolocation.___ The mobat can’t use its blindsight while deafened.

___Flyby.___ The mobat doesn’t provoke an opportunity attack when it flies out
of an enemy’s reach.

___Keen Hearing.___ The mobat has advantage on Wisdom (Perception)
checks that rely on hearing.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 13
(3d6 + 3) piercing damage.

___Stunning Screech (Recharge 5–6).___ The mobat emits a piercing
screech. All creatures within 30 feet of it that can hear it must make a DC
12 Constitution saving throw or be stunned for 1 minute. A stunned target
can repeat the saving throw at the end of each of its turns, ending the effect
on itself on a success.
