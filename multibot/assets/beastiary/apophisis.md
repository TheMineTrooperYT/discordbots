"Apophisis";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "6 (2,300 XP)"
_languages_: "Common, Draconic, Elven"
_senses_: "Darkvision 60 ft., passive Perception 13"
_skills_: "Perception +5, Stealth +6"
_saving_throws_: "Dex +6, Wis +5"
_damage_resistances_: "Necrotic; Bludgeoning, Piercing, And Slashing From Nonmagical Attacks"
_speed_: "30 ft."
_hit points_: "82 (11d8 + 33)"
_armor class_: "13 (16 with <i>Mage Armor</i>)"
_stats_: | 16 (+3) | 16 (+3) | 16 (+3) | 15 (+2) | 14 (+2) | 16 (+3) |

___Spellcasting.___ Apophisis is a 5th-level spellcaster.
Her spellcasting ability is Charisma (spell save DC
14, +6 to hit with spell attacks). Apophisis has the
following sorcerer spells prepared:

* Cantrips (at will): _ray of frost, chill touch, mage hand, prestidigitation_

* 1st level (4 slots): _charm person, detect magic, mage armor, shield_

* 2nd level (3 slots): _darkness, suggestion_

* 3rd level (2 slots): _counterspell, fly_

___Regeneration.___ Apophisis regains 10 hit points at the
start of her turn if she has at least 1 hit point and
isn’t in sunlight or running water. If Apophisis takes
radiant damage or damage from holy water, this trait
doesn’t function at the start of Apophisis’ next turn.

___Spider Climb.___ Apophisis can climb difficult surfaces,
including upside down on ceilings, without
needing to make an ability check.

___Vampire Weaknesses.___ Apophisis has the following
flaws:
* **Forbiddance.** Apophisis can’t enter a residence
without an invitation from one of the occupants.
* **Harmed by Running Water.** Apophisis takes 20 acid
damage when she ends her turn in running water.
* **Stake to the Heart.** Apophisis is destroyed if a
piercing weapon made of wood is driven into her
heart while she is incapacitated in her resting place.
* **Sunlight Hypersensitivity.** Apophisis takes 20 radiant
damage when she starts her turn in sunlight.
While in sunlight, she has disadvantage on attack
rolls and ability checks.

**Actions**

___Multiattack.___ Apophisis makes two attacks, one
with her bite and one with her claws.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft.,
one creature. Hit: 8 (2d4 + 3) slashing damage. Instead
of dealing damage, Apophisis can grapple the
target (escape DC 13).

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one
willing creature, or a creature that is grappled by
Apophisis, incapacitated, or restrained. Hit: 6 (1d6 +
3) piercing damage plus 7 (2d6) necrotic damage. The
target’s hit point maximum is reduced by an amount
equal to the necrotic damage taken, and Apophisis
regains hit points equal to that amount. The reduction
lasts until the target finishes a long rest. The target
dies if this effect reduces its hit point maximum to 0.

**Lair** Actions

On initiative count 20 (losing initiative ties), Apophisis
takes a lair action to cause one of the following effects:
* Apophisis creates fog as though she had cast the
_fog cloud_ spell. The fog lasts until initiative count
20 on the next round.
* The barge lets out a long low gong sound. All
creatures within 100 feet who can hear that are
not undead must make a DC 13 Constitution saving
throw, taking 10 (3d6) necrotic damage on a failed
save and half as much damage on a successful one.

**Regional** Effects

The region surrounding Apophisis’ barge is warped
by her unnatural presence, creating any of the following
effects:
* There’s a noticeable increase in the populations of
bats, rats, and wolves in the region.
* Plants wilt within 500 feet of the boat. This effect
lasts for the next 24 hours.
* Shadows cast within 500 feet of the lair seem abnormally
gaunt and sometimes move and flit about
as though alive.
* A creeping fog clings to the ground and water
within 500 feet of Apophisis’s lair. The fog occasionally
takes eerie forms, such as grasping claws
and writhing serpents.
* Anyone traversing the water within 500 feet of
her lair feels things pull and snag on their watercraft,
garments, and feet while they are in or near
the water. While this does not impede their movement,
it is disconcerting

If Apophisis is destroyed, these effects end after 2d6
days.
