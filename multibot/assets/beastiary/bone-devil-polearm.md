"Bone Devil Polearm";;;_size_: Large fiend (devil)
_alignment_: lawful evil
_challenge_: "12 (8,400 XP)"
_languages_: "Infernal, telepathy 120 ft."
_senses_: "darkvision 120 ft."
_skills_: "Deception +7, Insight +6"
_damage_immunities_: "fire, poison"
_saving_throws_: "Int +5, Wis +6, Cha +7"
_speed_: "40 ft., fly 40 ft."
_hit points_: "142 (15d10+60)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_stats_: | 18 (+4) | 16 (+3) | 18 (+4) | 13 (+1) | 14 (+2) | 16 (+3) |

___Devil's Sight.___ Magical darkness doesn't impede the devil's darkvision.

___Magic Resistance.___ The devil has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The devil makes three attacks: two with its hooked polearm and one with its sting.

___Hooked Polearm.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 17 (2d12 + 4) piercing damage. If the target is a huge or smaller creature, it is grappled (escape DC 14). Until the grapple ends, the devil can't use its polearm on another target.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 8 (1d8 + 4) slashing damage.

___Sting.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 13 (2d8 + 4) piercing damage plus 17 (5d6) poison damage, and the target must succeed on a DC 14 Constitution saving throw or become poisoned for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success .