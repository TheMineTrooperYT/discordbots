"Ancient Mist Dragon";;;_size_: Gargantuan dragon
_alignment_: neutral
_challenge_: "20 (25,000 XP)"
_languages_: "Auran, Aquan, Celestial, Common, Draconic, Elvish, Gnome, Ignan, Sylvan"
_skills_: "Perception +14, Stealth +8, Survival +8"
_senses_: "blindsight 120 ft., darkvision 120 ft., passive Perception 24"
_saving_throws_: "Dex +8, Con +14, Wis +8, Cha +8"
_damage_immunities_: "fire"
_speed_: "40 ft., fly 80 ft., swim 40 ft."
_hit points_: "333 (18d20 + 144)"
_armor class_: "19 (natural armor)"
_stats_: | 26 (+8) | 14 (+2) | 26 (+8) | 12 (+1) | 15 (+2) | 14 (+2) |

___Amphibious.___ The mist dragon can breathe both water and air.

___Legendary Resistance (3/day).___ If the dragon fails a saving throw, it can
choose to succeed instead.

**Actions**

___Multiattack.___ The dragon uses its Frightful Presence and makes three
attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +14 to hit, reach 20 ft., one target. Hit:
19 (2d10 + 8) piercing damage plus 14 (4d6) fire damage.

___Claw.___ Melee Weapon Attack: +14 to hit, reach 10 ft., one target.
Hit: 22 (4d6 + 8) slashing damage.

___Tail.___ Melee Weapon Attack: +14 to hit, reach 20 ft., one target.
Hit: 17 (2d8 + 8) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon’s choice that
is within 120 feet of the dragon and aware of it must succeed
on a DC 16 Wisdom saving throw or become frightened for 1
minute. A creature can repeat the saving throw at the end of
each of its turns, ending the effect on itself on a success. If a
creature’s saving throw is successful or the effect ends for it,
the creature is immune to the dragon’s Frightful Presence for the
next 24 hours.

___Breath Weapon (Recharge 5–6).___ The mist dragon uses one of the
following breath weapons.

1. _Obscuring Mist._ The dragon exhales a murky opaque mist in a 90-foot
cube that spreads around corners. This mist heavily obscures the area and
remains for 1 minute, or until dispersed with a moderate or stronger wind
(at least 10 miles per hour).

2. _Scalding Mist._ The dragon exhales a fiery blast of lingering mist in
a 90-foot cone that spreads around corners. The mist lightly obscures
the area and remains for 1 minute, or until dispersed with a moderate or
stronger wind (at least 10 miles per hour). Each creature that enters the
area or begins its turn there must make a DC 22 Constitution saving throw,
taking 77 (22d6) fire damage on a failed saving throw, or half as much
damage on a successful one. Being underwater doesn’t grant resistance
to this damage.

___Mist Form.___ The mist dragon polymorphs into a Gargantuan cloud of
mist, or back into its true form. While in mist form, the dragon can’t take
any actions, speak, or manipulate objects. It is weightless, has a flying
speed of 20 feet, can hover, and can enter a hostile creature’s space and
stop there. In addition, if air can pass through a space, the mist can do so
without squeezing, and it can’t pass through water. It has advantage on
Strength, Dexterity, and Constitution saving throws, it is immune to all
nonmagical damage.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. The dragon regains spent legendary
actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each
creature within 15 feet of the dragon must succeed on a DC 22 Dexterity
saving throw or take 15 (2d6 + 8) bludgeoning damage and be knocked
prone. The dragon can then fly up to half its flying speed.

**Lair** Actions

On initiative count 20 (losing initiative ties), the dragon takes a lair
action to cause one of the following effects; the dragon can’t use the same
effect two rounds in a row:

___Steam Cloud.___ Scalding steam condenses into an area in a 20-foot sphere
centered on a point within 120 feet of the mist dragon. Creatures who
enter the area or begin their turn within it must make a DC 16 Constitution
saving throw, taking 16 (4d6 + 2) fire damage on a failed saving throw, or
half as much damage on a success. The mist cloud heavily obscures the
area and remains until the next initiative count 20.

___Rain.___ The dragon causes rain to fall as if it had cast the create or destroy
water spell as a 9th level spell (creating rainfall within a 70-foot cube) on
an area within 120 feet of it that it can see.

___Wind Barrier.___ The dragon causes strong winds to form barriers, as if
it had cast the wind wall spell at a point it can see within 120 feet of it.
The effects last until the dragon uses this lair action again, or until the
dragon dies.

**Regional** Effects

The region containing the ancient mist dragon’s lair is changed by its
presence, which creates one or more of the following effects:

___Tropical Atmosphere.___ The dragon’s presence causes extreme moisture
and humidity, creating a tropical atmosphere within 6 miles of the dragon’s
lair.

___Difficult Plants.___ Plant life of all kinds flourishes, and sentient plant life
flocks to the area, and makes trekking to the dragon’s lair difficult.
If the dragon dies, these effects fade over 1d10 days.
