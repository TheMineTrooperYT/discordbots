"Sir Braford";;;_size_: Medium humanoid (human)
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "Common"
_skills_: "Athletics +S, Perception +3"
_speed_: "30 ft."
_hit points_: "19 (3d8+6)"
_armor class_: "18 (chain mail, shield)"
_stats_: | 16 (+3) | 9 (-1) | 14 (+2) | 10 (0) | 13 (+1) | 14 (+2) |

___Source.___ tales from the yawning portal,  page 243

___Barkskin.___ Sir Braford's AC can't be lower than 16.

___Special Equipment.___ Sir Braford wields Shatterspike, a magic longsword that grants a + 1 bonus to attack and damage rolls made with it (included in his attack). See appendix B for the item's other properties.

___Tree Thrall.___ If the Gulthias Tree dies, Sir Braford dies 24 hours later.

**Actions**

___Longsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) slashing damage, or 9 (1dlO + 4) slashing damage if used with two hands.

**Reactions**

___Protection.___ When a creature Sir Braford can see attacks a target other than him that is within 5 feet of him, he can use a reaction to use his shield to impose disadvantage on the attack roll.