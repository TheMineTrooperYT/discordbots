"Scout";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "1/2 (100 XP)"
_languages_: "any one language (usually Common)"
_skills_: "Nature +4, Perception +5, Stealth +6, Survival +5"
_speed_: "30 ft."
_hit points_: "16 (3d8+3)"
_armor class_: "13 (leather armor)"
_stats_: | 11 (0) | 14 (+2) | 12 (+1) | 11 (0) | 13 (+1) | 11 (0) |

___Keen Hearing and Sight.___ The scout has advantage on Wisdom (Perception) checks that rely on hearing or sight.

**Actions**

___Multiattack.___ The scout makes two melee attacks or two ranged attacks.

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Longbow.___ Ranged Weapon Attack: +4 to hit, ranged 150/600 ft., one target. Hit: 6 (1d8 + 2) piercing damage.