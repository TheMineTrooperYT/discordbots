"Faldric the Archer";;;_size_: Medium humanoid
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Common"
_skills_: "Acrobatics +5, Perception +3"
_senses_: "passive Perception 13"
_saving_throws_: "Dex +4 Wis +2"
_speed_: "30 ft."
_hit points_: "44 (8d8 + 8)"
_armor class_: "15 (studded leather)"
_stats_: | 11 (+0) | 16 (+3) | 13 (+1) | 11 (+0) | 14 (+2) | 13 (+1) |

___Acute Sight.___ Faldric has advantage on Wisdom
(Perception) checks that rely on sight.


**Actions**

___Multiattack.___ Faldric makes two attacks with either his longbow or his shortsword.

___Longbow.___ Ranged Weapon Attack: +5 to hit,
range 150/600 ft., one target. Hit: 7 (1d8 + 3)
piercing damage.

___Shortsword.___ Melee Weapon Attack: +5 to
hit, reach 5 ft., one target. Hit: 6 (1d6 + 3)
piercing damage.

**Reactions**

___Pelliton’s Archer.___ When a creature Faldric can
see enters a space adjacent to Sir Pelliton, Faldric
may use his reaction to make a longbow attack
against that creature. If this attack is successful,
the target is also knocked prone.
