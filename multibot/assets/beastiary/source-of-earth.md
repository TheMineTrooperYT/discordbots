"Source of Earth";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "6 (2,300 XP)"
_languages_: "Aquan, Auran, Common, Ignan, Terran"
_senses_: "darkvision 60 ft., passive Perception 17"
_saving_throws_: "Str +3, Con +8"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned"
_speed_: "10 ft., fly 10 ft. (hover)"
_hit points_: "124 (13d8 + 65)"
_armor class_: "17 (natural armor)"
_stats_: | 11 (+0) | 9 (-1) | 20 (+5) | 10 (+0) | 19 (+4) | 10 (+0) |

___Foundation of All the World.___ Allies within 60 feet
of the Source cannot be moved against their will or
knocked prone.

**Actions**

___Diamondskin.___ As a bonus action, the Source
focuses its gaze on an ally within 30 feet. Diamond
crystals emerge from the target’s skin, increasing
their AC by 2 for 10 minutes, after which the
diamonds turn to worthless dust, restoring the
target’s skin to its previous state.

___Back to Earth.___ The Source focuses its gaze on an
enemy within 30 feet. If the target’s body is made
of flesh, the target must make a DC 15 Constitution
saving throw. On a failed save, the creature magically
begins to turn to stone and is restrained. At
the end of its next turn, it must repeat the saving
throw. On a success, the effect ends. On a failure,
the creature is petrified until freed by the _greater
restoration_ spell or other magic.

___Wall of Stone (1/Day).___ The Source causes a
30-foot-long wall of solid stone to thrust up
through the floor. The wall has six 5-foot-long
panels to it, which can be arranged however the
summoner of the Source wishes, but each segment
must form a line or a right angle with the previous
segment. Each panel is 6 inches thick and 10 feet
high. Each panel has AC 15 and 180 hit points. If
reduced to 0 hit points, a panel crumbles, leaving
loose scree that counts as difficult terrain. The wall
lasts for 10 minutes, then turns to dust.

If the wall is summoned in a creature’s space,
the creature is pushed to one side of the wall,
chosen by the summoner. If a creature would be
surrounded on all sides by the wall (or the wall
and another solid surface), that creature can make
a Dexterity saving throw. On a success, it can use
its reaction to move up to its speed so that it is no
longer enclosed by the wall.
