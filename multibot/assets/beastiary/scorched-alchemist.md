"Scorched Alchemist";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "--"
_senses_: "passive Perception 10"
_damage_immunities_: "fire"
_speed_: "30 ft."
_hit points_: "42 (5d10 + 15)"
_armor class_: "11"
_stats_: | 18 (+4) | 12 (+1) | 16 (+3) | 15 (+2) | 10 (+0) | 7 (-2) |

**Actions**

___Multiattack.___ The alchemist makes two attacks: one with
its punch and one with its bite.

___Flaming Punch.___ Melee Weapon Attack: +6 to hit, reach
5ft., one target. Hit: 6 (1d4 + 4) bludgeoning damage
plus 3 (1d6) fire damage.

___Scorching Bite.___ Melee Weapon Attack: +6 to hit, reach
5ft., one target.Hit: 8 (1d8 + 4) piercing damage plus 3
(1d6) fire damage.

___Flicker of Flames (1/Day).___ The alchemist launches a
stream of fire at a target within 30 feet. This flame
leaps to up to two other targets, each of which must be
within 10 feet of the first target. A target can be a
creature or an object and can only be targeted by one
leap of flame.
Each target must make a DC 12 Dexterity saving throw,
taking 10 (3d6) fire damage on a failed save, or half as
much damage on a successful one.
