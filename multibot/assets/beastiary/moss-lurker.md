"Moss Lurker";;;_size_: Small humanoid
_alignment_: chaotic neutral
_challenge_: "1 (200 XP)"
_languages_: "Giant, Sylvan, Trollkin"
_skills_: "Perception +2, Stealth +4"
_senses_: "blindsight 60 ft., passive Perception 12"
_saving_throws_: "Str +4, Dex +4"
_damage_immunities_: "fire, poison"
_condition_immunities_: "blind, poisoned"
_speed_: "30 ft."
_hit points_: "45 (10d6 + 10)"
_armor class_: "15 (natural armor)"
_stats_: | 14 (+2) | 14 (+2) | 12 (+1) | 12 (+1) | 10 (+0) | 10 (+0) |

___Camouflage.___ A moss lurker has advantage on Dexterity (Stealth) checks to hide in forested or swampy terrain.

___Love of Heavy Weapons.___ While moss lurkers can use heavy weapons, they have disadvantage while wielding them.

___Keen Hearing and Smell.___ The moss lurker has advantage on Wisdom (Perception) checks that rely on hearing or smell.

___Poisoned Gifts.___ A moss lurker can contaminate liquids or food with poison. Someone who consumes the contaminated substance must make a successful DC 11 Constitution saving throw or become poisoned for 1 hour. When the poison is introduced, the moss lurker can choose a poison that also causes the victim to fall unconscious, or to become paralyzed while poisoned in this way. An unconscious creature wakes if it takes damage, or if a creature uses an action to shake it awake.

**Actions**

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6+2) slashing damage.

___Great Sword or Maul.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) slashing or bludgeoning damage.

___Mushroom-Poisoned Javelin.___ Ranged Weapon Attack: +4 to hit, range 30 ft., one target. Hit: 5 (1d6 + 2) piercing damage plus 6 (1d12) poison damage and the target is poisoned until the start of the moss lurker's next turn. A successful DC 11 Constitution save halves the poison damage and prevents poisoning.

___Dropped Boulder.___ Ranged Weapon Attack: +4 to hit, range 100 ft. (vertically), one target. Hit: 10 (3d6) bludgeoning damage.

