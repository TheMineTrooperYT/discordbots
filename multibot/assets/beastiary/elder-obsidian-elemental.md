"Elder Obsidian Elemental";;;_size_: Huge elemental
_alignment_: neutral
_challenge_: "17 (18,000 XP)"
_languages_: "Terran"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire; bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "exhaustion, paralyzed, petrified, poisoned, unconscious"
_speed_: "20 ft."
_hit points_: "225 (18d12 + 108)"
_armor class_: "19 (natural armor)"
_stats_: | 23 (+6) | 8 (-1) | 22 (+6) | 4 (-3) | 11 (+0) | 11 (+0) |

___Brute.___ A melee weapon attack deals one extra die of its damage when
the obsidian elemental hits with it (included in the attack).

___Death Throes.___ When the obsidian elemental dies, it explodes, and each
creature within 30 feet of it must make a DC 20 Dexterity saving throw,
taking 52 (15d6) slashing damage and 52 (15d6) fire damage on a failed
saving throw, or half as much damage on a successful one.

___Molten Glass.___ A creature that hits the obsidian elemental with a melee
attack while within 5 feet of it takes 7 (2d6) fire damage.

**Actions**

___Multiattack.___ The obsidian elemental makes two claw attacks.

___Claw.___ Melee Weapon Attack: +12 to hit, reach 5 ft., one target. Hit: 33
(6d8 + 6) slashing damage plus 22 (5d8) fire damage.
