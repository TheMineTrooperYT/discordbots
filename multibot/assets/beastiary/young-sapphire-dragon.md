"Young Sapphire Dragon";;;_size_: Large dragon
_alignment_: neutral
_challenge_: "10 (18,000 XP)"
_languages_: "Common, Draconic, telepathy 120 ft."
_skills_: "Arcana +8, Insight +7, Perception +7, Religion +8"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 21"
_saving_throws_: "Dex +7, Int +8, Wis +7, Cha +7"
_damage_vulnerabilities_: "psychic"
_speed_: "40 ft., fly 80 ft. (hover)"
_hit points_: "127 (17d10 + 34)"
_armor class_: "19 (natural armor)"
_stats_: | 18 (+4) | 17 (+3) | 14 (+2) | 18 (+4) | 16 (+3) | 17 (+3) |

___Awe Aura.___ All creatures within 30 feet must
make a DC 16 Charisma saving throw in order to
attack this dragon. On a failed save, the attacking
creature’s turn ends immediately. On a success,
that creature is immune to the Awe Aura of all
gemstone dragons for 1 week.

**Psionics**

___Charges:___ 17 | ___Recharge:___ 1d6 | ___Fracture:___ 17

**Actions**

___Multiattack.___ The dragon makes three attacks: one
with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 10 ft.,
one target. Hit: 15 (2d10 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft.,
one target. Hit: 11 (2d6 + 4) slashing damage.
