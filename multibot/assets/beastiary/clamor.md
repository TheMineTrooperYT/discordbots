"Clamor";;;_size_: Medium aberration
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "--"
_skills_: "Deception +7, Performance +7"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "thunder"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "0 ft., fly 60 ft."
_hit points_: "37 (5d8 + 15)"
_armor class_: "13 (natural armor)"
_stats_: | 1 (-5) | 16 (+3) | 16 (+3) | 5 (-2) | 12 (+1) | 17 (+3) |

___Incorporeal Movement.___ The clamor can move through other creatures
and objects as if they were difficult terrain. It takes 5 (1d10) force damage
if it ends its turn inside an object.

___Mimicry.___ The clamor can mimic any sound it has ever heard, including
humanoid voices. A creature that hears the sounds can tell they are
imitations with a successful DC 17 Wisdom (Insight) check.

___Natural Invisibility.___ The clamor is invisible.

___Silence Vulnerability.___ If the clamor starts its turn in an area under
the effect of silence, it takes 13 (3d8) force damage, and it must make a
DC 15 Wisdom saving throw or become frightened for 1 minute. While
frightened, it must use its action to Dash away from the area of silence. If
it begins its turn further than 60 feet away from the area of silence, it can
repeat the saving throw at the end of each of its turns, ending the effect
on a success.

**Actions**

___Thunder Touch.___ Melee Spell Attack: +5 to hit, reach 5 ft., one target.
Hit: 12 (2d8 + 3) thunder damage, and the target is pushed 5 feet directly
away from the clamor.
