"Bone Crab";;;_size_: Small beast
_alignment_: neutral
_challenge_: "1/2 (100 XP)"
_languages_: "--"
_skills_: "Perception +3, Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_resistances_: "bludgeoning"
_speed_: "20 ft., swim 10 ft."
_hit points_: "33 (6d6 + 12)"
_armor class_: "13 (natural armor)"
_stats_: | 10 (+0) | 14 (+2) | 14 (+2) | 1 (-5) | 12 (+1) | 4 (-3) |

___Amphibious.___ The bone crab can breathe air and water.

___Bone Camouflage.___ A bone crab has advantage on Dexterity (Stealth) checks while it's among bones.

___Hive Mind.___ A bone crab can communicate perfectly with all other bone crabs within 100 feet of it. If one is aware of danger, they all are.

___Leap.___ Bone crabs have incredibly powerful legs and can leap up to 10 feet straight ahead or backward as part of its movement; this counts as withdraw action when moving away from a foe.

**Actions**

___Multiattack.___ The bone crab makes two claw attacks.

___Claw.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) slashing damage.

___White Ghost Shivers.___ A living creature that is injured by or makes physical contact with a creature carrying the white ghost shivers must succeed on a DC 11 Constitution saving throw at the end of the encounter to avoid becoming infected. This disease manifests after 24 hours, beginning as a mild chill, but increasingly severe after a day, accompanied by a fever. Hallucinations are common, and the fright they induce lends the disease its name. At onset, the infected creature gains two levels of exhaustion that cannot be removed until the disease is cured by lesser restoration, comparable magic, or rest. The infected creature makes another DC 11 Constitution saving throw at the end of each long rest; a successful save removes one level of exhaustion. If the saving throw fails, the disease persists. If both levels of exhaustion are removed by successful saving throws, the victim has recovered naturally.

