"Nezznar the Black Spider";;;_size_: Medium humanoid (elf)
_alignment_: neutral evil
_challenge_: "2 (450 XP)"
_languages_: "Elvish, Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Arcana +5, Perception +4, Stealth +3"
_saving_throws_: "Int +5, Wis +4"
_speed_: "30 ft."
_hit points_: "27 (6d8)"
_armor class_: "11 (14 with mage armor)"
_stats_: | 9 (-1) | 13 (+1) | 10 (0) | 16 (+3) | 14 (+2) | 13 (+1) |

___Special Equipment.___ Nezznar has a spider staff.

___Fey Ancestry.___ Nezznar has advantage on saving throws against being charmed, and magic can't put him to sleep.

___Sunlight Sensitivity.___ Nezznar has disadvantage on attack rolls when he or his target is in sunlight.

___Innate Spellcasting.___ Nezznar can innately cast the following spells, requiring no material components:

* At will: _dancing lights_

* 1/day each: _darkness, faerie fire _(save DC 12)

___Spellcasting.___ Nezznar is a 4th-level spellcaster that uses Intelligence as his spellcasting ability (spell save DC 13; +5 to hit with spell attacks). Nezznar has the following spells prepared from the wizard's spell list:

* Cantrips (at will): _mage hand, ray of frost, shocking grasp_

* 1st Level (4 slots): _mage armor, magic missile, shield_

* 2nd Level (3 slots): _invisibility, suggestion_

**Actions**

___Spider Staff.___ Melee Weapon Attack: +1 to hit, reach 5 ft., one target. Hit: 2 (1d6-1) bludgeoning damage plus 3 (1d6) poison damage.
