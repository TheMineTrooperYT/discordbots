"Algorith";;;_size_: Medium construct
_alignment_: lawful neutral
_challenge_: "10 (5900 XP)"
_languages_: "Common, Celestial, Draconic, Infernal"
_skills_: "Athletics +9, Insight +7, Investigation +5, Perception +7"
_senses_: "darkvision 60 ft., passive Perception 17"
_saving_throws_: "Dex +6, Con +8, Wis +7, Cha +8"
_damage_immunities_: "poison"
_damage_resistances_: "acid, cold, lightning"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "40 ft., fly 40 ft."
_hit points_: "136 (16d8 + 64)"
_armor class_: "18 (natural armor)"
_stats_: | 21 (+5) | 14 (+2) | 19 (+4) | 13 (+1) | 16 (+3) | 18 (+4) |

___Immutable Form.___ The algorith is immune to any spell or effect that would alter its form.

___Innate Spellcasting.___ The algorith's innate spellcasting ability is Charisma (spell save DC 16). It can innately cast the following spells, requiring no material components:

* At will: _aid, blur, detect magic, dimension door_

* 5/day each: _dispel magic_

* 1/day: _commune _(5 questions)_, wall of force_

**Actions**

___Multiattack.___ The algorith makes two logic razor attacks.

___Logic Razor.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 31 (4d12 + 5) force damage.

___Cone of Negation (Recharge 5-6).___ An algorith can project a cone of null energy. Targets inside the 30 foot cone take 42 (12d6) force damage and suffer the effect of a dispel magic spell. A successful DC 16 Dexterity saving throw reduces the damage to half and negates the dispel magic effect on that target.

___Reality Bomb (5/Day).___ The algorith can summon forth a tiny rune of law and throw it as a weapon. Any creature within 30 feet of the square where the reality bomb lands takes 21 (6d6) force damage and is stunned until the start of the algorith's next turn. A target that makes a successful DC 16 Dexterity saving throw takes half damage and isn't stunned.

