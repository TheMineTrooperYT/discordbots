"Liosalfar";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "8 (3900 XP)"
_languages_: "Common, Celestial, Elemental, Elvish, Giant"
_skills_: "Arcana +7, Insight +7, Perception +7"
_senses_: "blindsight 120 ft., truesight 60 ft., passive Perception 17"
_saving_throws_: "Dex +10, Con +3, Int +7, Wis +7, Cha +4"
_damage_immunities_: "poison, psychic, radiant"
_damage_resistances_: "acid, cold, fire, lightning, thunder; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "blinded, charmed, exhaustion (see Lightform special ability), grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "fly 60 ft. (hover)"
_hit points_: "110 (20d10)"
_armor class_: "17"
_stats_: | 10 (+0) | 25 (+7) | 10 (+0) | 18 (+4) | 18 (+4) | 12 (+1) |

___Alien Mentality.___ A liosalfar's exotic consciousness renders it immune to psychic effects, and any attempt to read their thoughts leaves the reader confused for 1 round.

___Darkness Vulnerability.___ Magical darkness is harmful to a liosalfar: They take 2d10 necrotic damage, or half damage with a successful DC 14 Constitution saving throw, each time they start their turn inside magical darkness. Natural darkness is unpleasant to them but not harmful.

___Incorporeal Movement.___ The liosalfar can move through other creatures and objects as difficult terrain. It takes 5 (1d10) force damage if it ends its turn inside an object.

___Innate Spellcasting.___ The liosalfar's innate spellcasting ability is Intelligence (spell save DC 15, +7 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

At Will: augury, color spray, dancing lights, detect magic, flare, light, silent image, spare the dying

2/day each: alter self, blur, divination, hypnotic pattern, prismatic spray, scorching ray

1/day each: hallucinatory terrain, plane shift, sunbeam

___Lightform.___ Liosalfar are composed entirely of light. They are incorporeal and not subject to ability damage, polymorph, petrification, or attacks that alter their form.

___Prismatic Glow.___ Liosalfar shed rainbow illumination equal to a daylight spell. They cannot extinguish this glow without perishing but can reduce it to the level of torchlight at will. Even when using alter self they have a faint, diffused glow that's visible in dim light or darkness.

**Actions**

___Multiattack.___ The liosalfar makes two Disrupting Touch attacks.

___Disrupting Touch.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 26 (4d12) radiant damage, and the target must succeed on a DC 15 Wisdom saving throw or become stunned for 1 round.

