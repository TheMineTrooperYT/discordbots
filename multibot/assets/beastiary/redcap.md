"Redcap";;;_size_: Small fey
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Sylvan"
_senses_: "darkvision 60 ft."
_skills_: "Athletics +6, Perception +3"
_speed_: "25 ft."
_hit points_: "45 (6d6+24)"
_armor class_: "13 (natural armor)"
_stats_: | 18 (+4) | 13 (+1) | 18 (+4) | 10 (0) | 12 (+1) | 9 (-1) |

___Iron Boots.___ While moving, the redcap has disadvantage on Dexterity (Stealth) checks.

___Outsize Strength.___ While grappling, the redcap is considered to be Medium. Also, wielding a heavy weapon doesn't impose disadvantage on its attack rolls.

**Actions**

___Multiattack.___ The redcap makes three attacks with its wicked sickle.

___Wicked Sickle.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 9 (2d4+4) slashing damage.

___Ironbound Pursuit.___ The redcap moves up to its speed to a creature it can see and kicks with its iron boots. The target must succeed on a DC 14 Dexterity saving throw or take 20 (3d10+4) bludgeoning damage and be knocked prone.