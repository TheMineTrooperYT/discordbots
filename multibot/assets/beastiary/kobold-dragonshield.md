"Kobold Dragonshield";;;_size_: Small humanoid (kobold)
_alignment_: lawful evil
_challenge_: "1 (200 XP)"
_languages_: "Common, Draconic"
_senses_: "darkvision 60 ft."
_skills_: "Perception +1"
_speed_: "20 ft."
_hit points_: "44 (8d6+16)"
_armor class_: "15 (leather armor, shield)"
_stats_: | 12 (+1) | 15 (+2) | 14 (+2) | 8 (-1) | 9 (-1) | 10 (0) |

___Dragon's Resistance.___ The kobold has resistance to a type of damage based on the color of dragon that invested it with power (choose or roll a d10): 1-2, acid (black); 3-4, cold (white); 5-6, fire (red); 7-8, lightning (blue); 9-10, poison (green).

___Heart of the Dragon.___ If the kobold is frightened or paralyzed by an effect that allows a saving throw, it can repeat the save at the start of its turn to end the effect on itself and all kobolds within 30 feet of it. Any kobold that benefits from this trait (including the dragonshield) has advantage on its next attack roll.

___Pack Tactics.___ The kobold has advantage on an attack roll against a creature if at least one of the kobold's allies is within 5 feet of the creature and the ally isn't incapacitated.

___Sunlight Sensitivity.___ While in sunlight, the kobold has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The kobold makes two melee attacks.

___Spear.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range 20/60 it, one target. Hit: 4 (ld6+l) piercing damage, or 5 (ld8+l) piercing damage if used with two hands to make a melee attack.