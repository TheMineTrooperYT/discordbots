"Flatwoods Monster";;;_size_: Large aberration
_alignment_: lawful evil
_challenge_: "7 (2,900 XP)"
_languages_: "Deep Speech"
_skills_: "Investigation +7, Perception +3, Stealth +8"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 13"
_saving_throws_: "Con +6, Int +7"
_damage_resistances_: "psychic; bludgeoning, piercing, and slashing from nonmagical attacks"
_condition_immunities_: "charmed, frightened"
_speed_: "30 ft., fly 40 ft. (hover)"
_hit points_: "170 (20d10 + 60)"
_armor class_: "17 (natural armor)"
_stats_: | 10 (+0) | 20 (+5) | 16 (+3) | 18 (+4) | 11 (+0) | 17 (+3) |

___Keen Sight.___ The Flatwoods Monster has advantage on Wisdom (Perception) checks that rely on sight.

___Alien Mind.___ The Flatwoods Monster's thoughts are unreadable, and any creature attempting to mentally interact 
with it must make a DC 15 Intelligence saving throw or take 10 (2d6 + 3) psychic damage.

**Actions**

___Multiattack.___ The Flatwoods Monster makes two claw and one eye ray attack, or two eye ray attacks.

___Claws.___ Melee weapon attack, +8 to hit, reach 5 ft., one target. Hit: 18 (2d10 + 7) piercing damage, plus 7 (2d6) poison damage.

___Eye Ray.___ Ranged weapon attack, +8 to hit, reach 30 ft., one target. Hit: 25 (5d8 + 3) radiant damage.

___Shriek (Recharge 5-6).___ All other creatures within 30 feet of the Flatwoods Monster that can hear it must
 make a DC 15 Wisdom saving throw, becoming frightened on a failure.  At the end of each of its turns, a creature 
 frightened in this way can repeat this saving throw, ending the effect on a success.
