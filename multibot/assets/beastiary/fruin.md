"Fruin";;;_size_: Small elemental
_alignment_: neutral evil
_challenge_: "1/2 (100 XP)"
_languages_: "understands Aquan and Auran but can't speak"
_senses_: "darkvision 60 ft., passive Perception 8"
_skills_: "Stealth +5"
_saving_throws_: "Con +4"
_damage_immunities_: "cold, poison; bludgeoning, piercing, and slashing from nonmagical attacks"
_damage_resistances_: "piercing from magical attacks"
_damage_vulnerabilites_: "fire"
_condition_immunities_: "Poisoned"
_speed_: "25 ft."
_hit points_: "22 (4d6 + 8)"
_armor class_: "10"
_stats_: | 14 (+2) | 12 (+1) | 14 (+2) | 2 (-4) | 6 (-2) | 7 (-2) |

___Snow Body.___ The fruin's body is comprised of snow, and
it can reform any severed parts of its body at the start
of each of its turns (no action required). If the fruin
spends more than 8 consecutive hours in temperatures
above 40 degrees Fahrenheit, it melts, killing it.

___Snow Camouflage.___ The fruin has advantage on Dexterity
(Stealth) checks made to hide in snowy terrain.

___Snow Gathering.___ The fruin inherently collects loose
snow with its body as it moves. The fruin regains 1 hit
point for every 5 feet it moves through loosely-packed
and undisturbed snow that is 2 feet deep or deeper. At
the GM's discretion, continued usage of this ability in
one area may disturb the snow there, preventing this
ability from being used there again.

**Actions**

___Slam.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one
target. Hit: 1 bludgeoning damage plus 3 (1d6) cold
damage.

___Glom.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one
creature. Hit: The fruin attaches to the target. When the
target moves, the fruin moves with it. Each fruin
attached to a creature reduces its speed by a
cumulative 10 feet. If the number of fruins attached to
a creature exceeds it Strength modifier (minimum 2
fruins), the creature is buried under a mound of snow.
A buried creature is restrained and can't breathe. A
buried creature is freed when it or another creature
uses an action to make a successful DC 13 Strength
(Athletics) check to excavate it, ending the effect and
causing all attached fruins to appear in the nearest
unoccupied spaces.
