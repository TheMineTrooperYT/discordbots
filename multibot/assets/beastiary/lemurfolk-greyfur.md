"Lemurfolk Greyfur";;;_size_: Small humanoid
_alignment_: neutral
_challenge_: "4 (1100 XP)"
_languages_: "Common, Lemurfolk"
_skills_: "Acrobatics +5, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "20 ft., climb 10 ft., fly 40 ft."
_hit points_: "67 (15d6 + 15)"
_armor class_: "13 (16 with mage armor)"
_stats_: | 9 (-1) | 16 (+3) | 12 (+1) | 16 (+3) | 12 (+1) | 10 (+0) |

___Silent Glide.___ The lemurfolk can glide for 1 minute, making almost no sound. It gains a fly speed of 40 feet, and it must move at least 20 feet on its turn to keep flying. A gliding lemurfolk has advantage on Dexterity (Stealth) checks.

___Sneak Attack (1/Turn).___ The greyfur deals an extra 7 (2d6) damage when it hits with a weapon attack and it has advantage, or when the target is within 5 feet of an ally of the greyfur that isn't incapacitated and the greyfur doesn't have disadvantage on the attack roll.

___Spellcasting.___ The greyfur is a 5th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 13, +5 to hit with spell attacks). The greyfur has the following wizard spells prepared:

Cantrips (at will): light, mage hand, minor illusion, poison spray, resistance

1st Level (4 slots): mage armor, sleep

2nd Level (3 slots): detect thoughts, misty step

3rd Level (2 slots): lightning bolt

**Actions**

___Kukri Dagger.___ Melee Weapon Attack: +5 to hit, reach 5 ft., range 20/60, one target. Hit: 5 (1d4 + 3) piercing damage.

___Blowgun.___ Ranged Weapon Attack: +5 to hit, range 25/100 ft., one creature. Hit: 5 (1d4 + 3) piercing damage, and the target must succeed on a DC 13 Constitution saving throw or be poisoned and unconscious for 1d4 hours. Another creature can use an action to shake the target awake and end its unconsciousness but not the poisoning.

