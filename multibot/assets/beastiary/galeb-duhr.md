"Galeb Duhr";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "6 (2,300 XP)"
_languages_: "Terran"
_senses_: "darkvision 60 ft., tremorsense 60 ft."
_damage_immunities_: "poison"
_speed_: "15 ft. (30 ft. when rolling, 60 ft. rolling downhill)"
_hit points_: "85 (9d8+45)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "exhaustion, paralyzed, poisoned, petrified"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 20 (+5) | 14 (+2) | 20 (+5) | 11 (0) | 12 (+1) | 11 (0) |

___False Appearance.___ While the galeb duhr remains motionless, it is indistinguishable from a normal boulder.

___Rolling Charge.___ If the galeb duhr rolls at least 20 ft. straight toward a target and then hits it with a slam attack on the same turn, the target takes an extra 7 (2d6) bludgeoning damage. If the target is a creature, it must succeed on a DC 16 Strength saving throw or be knocked prone.

**Actions**

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5 ft ., one target. Hit: 12 (2d6 + 5) bludgeoning damage.

___Animate Boulders (1/Day).___ The galeb duhr magically animates up to two boulders it can see within 60 ft. of it. A boulder has statistics like those of a galeb duhr, except it has Intelligence 1 and Charisma 1, it can't be charmed or frightened, and it lacks this action option. A boulder remains animated as long as the galeb duhr maintains concentration, up to 1 minute (as if concentrating on a spell) .