"Werebat";;;_page_number_: 317
_size_: Small humanoid (goblin shapechanger)
_challenge_: "2 (450 XP)"
_languages_: "Goblin (can’t speak in bat form)"
_senses_: "darkvision 60 ft., passive Perception 13"
_skills_: "Perception +3, Stealth +5"
_damage_immunities_: "bludgeoning, piercing, and slashing from nonmagical attacks not made with silvered weapons"
_speed_: "30 ft. (climb 30 ft., fly 60 ft. in bat or hybrid form)"
_hit points_: "24 (7d6)"
_armor class_: "13"
_stats_: | 8 (-1) | 17 (+3) | 10 (0) | 10 (0) | 12 (+1) | 8 (-1) |

___Shapechanger.___ The werebat can use its action to polymorph into a Medium bat-humanoid hybrid, or into a Large giant bat, or back into its true form, which is humanoid. Its statistics, other than its size, are the same in each form. Any equipment it is wearing or carrying isn’t transformed. It reverts to its true form if it dies.

___Echolocation (Bat or Hybrid Form Only).___ The werebat has blindsight out to a range of 60 feet as long as it’s not deafened.

___Keen Hearing.___ The werebat has advantage on Wisdom (Perception) checks that rely on hearing.

___Nimble Escape (Humanoid Form Only).___ The werebat can take the Disengage or Hide action as a bonus action on each of its turns.

___Sunlight Sensitivity.___ While in sunlight, the werebat has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack (Humanoid or Hybrid Form Only).___ In humanoid form, the werebat makes two scimitar attacks or two shortbow attacks. In hybrid form, it can make one bite attack and one scimitar attack.

___Bite (Bat or Hybrid Form Only).___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 6 (1d6 + 3) piercing damage, and the werebat gains temporary hit points equal to the damage dealt. If the target is a humanoid, it must succeed on a DC 10 Constitution saving throw or be cursed with werebat lycanthropy.

___Scimitar (Humanoid or Hybrid Form Only).___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Shortbow (Humanoid or Hybrid Form Only).___ Ranged Weapon Attack: +5 to hit, range 80/320 ft., one target. Hit: 6 (1d6 + 3) piercing damage.