"Priest";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "2 (450 XP)"
_languages_: "any two languages"
_skills_: "Medicine +7, Persuasion +3, Religion +4"
_speed_: "25 ft."
_hit points_: "27 (5d8+5)"
_armor class_: "13 (chain shirt)"
_stats_: | 10 (0) | 10 (0) | 12 (+1) | 13 (+1) | 16 (+3) | 13 (+1) |

___Divine Eminence.___ As a bonus action, the priest can expend a spell slot to cause its melee weapon attacks to magically deal an extra 10 (3d6) radiant damage to a target on a hit. This benefit lasts until the end of the turn. If the priest expends a spell slot of 2nd level or higher, the extra damage increases by 1d6 for each level above 1st.

___Spellcasting.___ The priest is a 5th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 13, +5 to hit with spell attacks). The priest has the following cleric spells prepared:

* Cantrips (at will): _light, sacred flame, thaumaturgy_

* 1st level (4 slots): _cure wounds, guiding bolt, sanctuary_

* 2nd level (3 slots): _lesser restoration, spiritual weapon_

* 3rd level (2 slots): _dispel magic, spirit guardians_

**Actions**

___Mace.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 3 (1d6) bludgeoning damage.
