"Giant Ant Queen";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "4 (1100 XP)"
_languages_: "--"
_senses_: "blindsight 60 ft., passive Perception 10"
_speed_: "40 ft."
_hit points_: "85 (10d10 + 30)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 13 (+1) | 16 (+3) | 2 (-4) | 11 (+0) | 4 (-3) |

___Keen Smell.___ The giant ant queen has advantage on Wisdom (Perception) checks that rely on smell.

___Queen's Scent.___ Giant ants defending a queen gain advantage on all attack rolls.

**Actions**

___Multiattack.___ The giant ant queen makes two bite attacks and one sting attack.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) bludgeoning damage and the target is grappled (escape DC 13). Until this grapple ends, the target is restrained, and the giant ant can't bite a different target.

___Sting.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) piercing damage plus 22 (4d10) poison damage, or half as much poison damage with a successful DC 14 Constitution saving throw.

