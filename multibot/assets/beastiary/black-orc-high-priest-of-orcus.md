"Black Orc High Priest of Orcus";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "9 (5,000 XP)"
_languages_: "Abyssal, Common, Orc"
_skills_: "Arcana +5, Deception +6, Insight +9, Intimidation +6, Perception +9"
_senses_: "truesight 120 ft., passive Perception 19"
_saving_throws_: "Wis +9, Cha +6"
_speed_: "30 ft."
_hit points_: "127 (15d8 + 60)"
_armor class_: "16 (scale mail)"
_stats_: | 16 (+3) | 14 (+2) | 18 (+4) | 12 (+1) | 20 (+5) | 14 (+2) |

___Abyssal Blessing.___ The priest of Orcus gains 10 temporary hit points
when it reduces a hostile creature that is not an undead to 0 hit points.

___Blessing of Orcus.___ Black orcs have advantage on saving throws against
the spells and effects of undead creatures.

___Deadsight.___ The high priest of Orcus has truesight out to a range of 120
feet.

___Spellcasting.___ The high priest of Orcus is a 10 level spellcaster. Its
spellcasting ability is Wisdom (spell save DC 17, +9 to hit with spell
attacks). It has the following cleric spells prepared:

* Cantrips (at will): _chill touch, guidance, mending, resistance_

* 1st level (4 slots): _bane, cure wounds, false life, inflict wounds_

* 2nd level (3 slots): _aid, blindness/deafness, hold person, silence_

* 3rd level (3 slots): _animate dead, bestow curse, dispel magic, spirit guardians_

* 4th level (3 slots): _banishment, death ward, guardian of faith_

* 5th level (2 slots): _dispel evil and good, insect plague_

___Unholy Strike.___ Once on each of the high priest’s turns when it hits a
creature with a melee weapon attack, the high priest can cause the attack to deal
an extra 18 (4d8) necrotic damage to the target.

**Actions**

___Multiattack.___ The high priest makes two melee attacks.

___Mace.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 12
(2d8 + 3) bludgeoning damage.

___Caress of Orcus (Recharge 5–6).___ Melee Weapon Attack: +7 to
hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) necrotic damage, and the
target’s Strength score is reduced by 1d4. The target dies if this reduces
its Strength to 0. Otherwise, the reduction lasts until the target finishes a
short or long rest.

If a non-evil humanoid dies from this attack, a shadow rises from the
corpse in 24 hours under the priest’s control, unless the humanoid is
restored to life or its body is destroyed. The priest can have no more than
three shadows under its control at one time.
