"Ezmerelda d'Avenir";;;_page_number_: 230
_size_: Medium humanoid (human)
_alignment_: chaotic good
_challenge_: "8 (3,900 XP)"
_languages_: "Common, Elvish"
_skills_: "Acrobatics +7, Arcana +6, Deception +9, Insight +3, Medicine +3, Perception +6, Performance +6, Sleight of Hand +7, Stealth +7, Survival +6"
_saving_throws_: "Wis +3"
_speed_: "30 ft."
_hit points_: "82 (11d8+33)"
_armor class_: "17 (studded leather armor +1)"
_stats_: | 14 (+2) | 19 (+4) | 16 (+3) | 16 (+3) | 11 (0) | 17 (+3) |

___Special Equipment.___ In addition to her magic armor and weapons, Ezmerelda has two potions of greater healing, six vials of holy water, and three wooden stakes.

___Spellcasting.___ Ezmerelda is a 7th-level spellcaster. Her spellcasting ability is Intelligence (spell save DC 14, +6 to hit with spell attacks). Ezmerelda has the following wizard spells prepared:

* Cantrips (at will): _fire bolt, light, mage hand, prestidigitation_

* 1st level (4 slots): _protection from good and evil, magic missile, shield_

* 2nd level (3 slots): _darkvision, knock, mirror image_

* 3rd level (3 slots): _clairvoyance, lightning bolt, magic circle_

* 4th level (1 slot): _greater invisibility_

**Actions**

___Multiattack.___ Ezmerelda makes three attacks: two with her +1 rapier and one with her +1 handaxe or her silvered shortsword.

___Rapier +1.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 9 (1d8+5) piercing damage.

___Handaxe +1.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 20/60 ft., one creature. Hit: 6 (1d6+3) slashing damage.

___Silvered Shortsword.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 7 (1d6+4) piercing damage.

___Curse (Recharges after a Long Rest).___ Ezmerelda targets one creature that she can see within 30 feet of her. The target must succeed on a DC 14 Wisdom saving throw or be cursed. While cursed, the target has vulnerability to one type of damage of Ezmerelda's choice. The curse lasts until ended with a greater restoration spell, a remove curse spell, or similar magic. When the curse ends, Ezmerelda takes 3d6 psychic damage.

___Evil Eye (Recharges after a Short or Long Rest).___ Ezmerelda targets one creature that she can see within 10 feet of her and casts one of the following spells on the target (save DC 14), requiring neither somatic nor material components to do so: animal friendship, charm person, or hold person. If the target succeeds on the initial saving throw, Ezmerelda is blinded until the end of her next turn. Once a target succeeds on a saving throw against this effect, it is immune to the Evil Eye power of all Vistani for 24 hours.
