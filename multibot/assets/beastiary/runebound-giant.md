"Runebound Giant";;;_size_: Huge giant
_alignment_: chaotic evil
_challenge_: "8 (3,900 XP)"
_languages_: "Giant"
_senses_: "passive Perception 9"
_condition_immunities_: "charmed"
_speed_: "40 ft."
_hit points_: "130 (12d12 + 52)"
_armor class_: "13 (natural armor)"
_stats_: | 22 (+6) | 8 (-1) | 18 (+4) | 5 (-3) | 8 (-1) | 6 (-2) |

___Runebound.___ The giant's body is coated in magical
runes granted by a runespeaker that provide it with
additional strength. The giant gains one of the
following runic bonuses:
* Rune of Destruction – The giant's greatclub
attacks deal double damage to objects and
structures. In addition, when the giant hits a
creature with its greatclub, that creature must
succeed on a DC 17 Strength saving throw or be
pushed up to 15 feet in a straight line and
knocked prone.
* Rune of Immortality – The giant's maximum hit
points is increased by 20. In addition, if damage
reduces the giant to 0 hit points, it must make a
Constitution saving throw with a DC of 5 + the
damage taken, unless the damage was from a
critical hit. On a success, the giant drops to 1 hit
point instead.
* Rune of Warding – The giant has advantage on
saving throws against spells and other magical
effects.

___Runic Weapon.___ The giant's greatclub is imbued with
powerful arcane runes, causing attacks with that
weapon to deal an additional 4 (1d8) force damage
(included in the attack).

**Actions**

___Multiattack.___ The giant makes two greatclub attacks.

___Runic Greatclub.___ Melee Weapon Attack: +9 to hit,
reach 10 ft., one target. Hit: 19 (3d8 + 6)
bludgeoning damage plus 4 (1d8) force damage.

___Rock.___ Ranged Weapon Attack: +9 to hit, range
60/240 ft., one target. Hit: 22 (3d10) + 6)
bludgeoning damage.
