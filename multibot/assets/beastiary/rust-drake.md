"Rust Drake";;;_size_: Medium dragon
_alignment_: chaotic evil
_challenge_: "8 (3900 XP)"
_languages_: "Common, Draconic"
_skills_: "Perception +3, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_vulnerabilities_: "acid"
_damage_immunities_: "poison"
_condition_immunities_: "paralyzed, poisoned"
_speed_: "30 ft., burrow 5 ft., fly 100 ft."
_hit points_: "161 (19d8 + 76)"
_armor class_: "17 (natural armor)"
_stats_: | 20 (+5) | 15 (+2) | 19 (+4) | 12 (+1) | 8 (-1) | 8 (-1) |

**Actions**

___Multiattack.___ The drake makes one bite attack and one tail swipe attack.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 18 (3d8 + 5) piercing damage, and the target must succeed on a DC 16 Constitution save or contract Rust Drake Lockjaw.

___Tail Swipe.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) bludgeoning damage.

___Vomits Scrap (Recharge 5-6).___ A rust drake can vomit forth a 15-foot cone of rusted metal. Targets in the affected area take 55 (10d10) slashing damage, or half damage with a successful DC 15 Dexterity saving throw. In addition, affected creatures must also make a successful DC 15 Constitution saving throw or contract Rust Drake Tetanus.

___Rust Drake Lockjaw.___ This disease manifests symptoms in 1d4 days, when the affected creature experiences painful muscle spasms, particularly in the jaw. After each long rest, the creature must repeat the saving throw. If it fails, the victim takes 1d3 Dexterity damage and is paralyzed for 24 hours; if the saving throw succeeds, the creature takes no damage and feels well enough to act normally for the day. This continues until the creature dies from Dexterity loss, recovers naturally by making successful saving throws after two consecutive long rests, or is cured with lesser restoration or comparable magic. After the disease ends, the victim recovers 1d3 lost Dexterity with each long rest; greater restoration or comparable magic can restore it all at once.

