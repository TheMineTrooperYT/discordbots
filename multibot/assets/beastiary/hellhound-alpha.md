"Hellhound Alpha";;;_size_: Medium fiend
_alignment_: lawful evil
_challenge_: "4 (1,100 XP)"
_languages_: "Understands Infernal and Ignan, but cannot speak"
_skills_: "Perception +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_damage_immunities_: "fire"
_speed_: "50 ft."
_hit points_: "75 (10d10 + 20)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 12 (+1) | 14 (+2) | 8 (-1) | 14 (+2) | 6 (-2) |

___Keen Hearing and Smell.___ The hound has advantage
on Wisdom (Perception) checks that rely on
hearing or smell.

___Pack Tactics.___ The hound has advantage on an attack
roll against a creature if at least one of the hound’s
allies is within 5 feet of the creature and the ally
isn’t incapacitated.

___Flame Charge.___ If the hound moves at least 10 feet
straight toward a target and then hits it with a bite
attack on the same turn, the target takes an extra
13 (3d8) fire damage and must succeed on a DC
13 Strength saving throw or be knocked prone.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5ft.,
one target. Hit: 9 (1d10 + 4) piercing damage plus
13 (3d8) fire damage.

___Horrifying Howl (1/Day).___ Each creature of the
hound’s choice that is within 60 feet and is not
deafened must succeed on a DC 12 Wisdom saving
throw or become frightened for 1 minute. A
creature can repeat the saving throw at the end of
each of its turns, ending the fear on a success.
