"Lead Skeleton";;;_size_: Medium undead
_alignment_: neutral
_challenge_: "6 (2,300 XP)"
_languages_: "the languages it knew in life but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "acid, cold, fire, lightning, poison; bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "66 (12d8 + 12)"
_armor class_: "18 (natural armor)"
_stats_: | 21 (+6) | 18 (+4) | 13 (+1) | 2 (-4) | 10 (+0) | 1 (-5) |

___Immutable Form.___ The skeleton is immune to any spell or effect that
would alter its form.

___Magic Resistance.___ The skeleton has advantage on saving throws
against spells and other magical effects.

___Magic Weapons.___ The skeleton’s weapon attacks are magical.

**Actions**

___Multiattack.___ The lead skeleton makes two slam attacks.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14
(2d8 + 5) bludgeoning damage.
