"Giant Dire Abyssal Frog";;;_size_: Large elemental
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "--"
_skills_: "Stealth +7, Perception +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +7, Con +8"
_damage_immunities_: "poison"
_damage_resistances_: "acid, cold; bludgeoning, piercing and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "30 ft., swim 30 ft."
_hit points_: "105 (10d10 + 50)"
_armor class_: "16 (natural armor)"
_stats_: | 20 (+5) | 18 (+4) | 20 (+5) | 5 (-3) | 10 (+0) | 10 (+0) |

___Amphibious.___ The frog can breathe air and water.

___Keen Smell.___ The frog has advantage on Wisdom (Perception)
checks that rely on smell.

___Poison Hide.___ A creature that touches the giant dire Abyssal
frog or hits it with an unarmed or natural weapon attack takes 10 (3d6)
poison damage from the milky, poisonous slime that oozes from its hide.

___Standing Leap.___ The frog’s long jump is up to 30 feet and its high jump
is up to 20 feet, with or without a running start.

**Actions**

___Multiattack.___ The giant dire Abyssal frog makes three attacks: one with
its bite, one with its claws, and one with its tongue.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14
(2d8 + 5) piercing damage, and the target is grappled (escape DC 16).
Until this grapple ends, the target is restrained and the frog can’t bite
another target.

___Claws.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 15
(3d6 + 5) slashing damage.

___Tongue.___ Melee Weapon Attack: +8 to hit, reach 15 ft., one target. Hit:
18 (2d12 + 5) slashing damage, and the target must succeed on a DC 16
Strength saving throw or be pulled up to 10 feet toward the giant dire
Abyssal frog.

___Swallow.___ The giant dire Abyssal frog makes one bite attack against
a Large or smaller target it is grappling. If the attack hits, the target is
swallowed, and the grapple ends. The swallowed target is blinded and
restrained, it has total cover against attacks and other effects outside the
giant dire Abyssal frog, and it takes 14 (4d6) acid damage at the start of
each of the giant dire Abyssal frog’s turns. The giant dire Abyssal frog can
only swallow one target at a time.
If the giant dire Abyssal frog dies, a swallowed creature is no longer
restrained by it and can escape from the corpse using 5 feet of movement,
exiting prone.
