"Ngobou";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "5 (1800 XP)"
_languages_: "--"
_skills_: "Perception +5"
_senses_: ", passive Perception 15"
_speed_: "40 ft."
_hit points_: "85 (10d10 + 30)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 9 (-1) | 16 (+3) | 2 (-4) | 9 (-1) | 6 (-2) |

___Trampling Charge.___ If the ngobou moves at least 20 feet straight toward a creature and then hits it with a gore attack on the same turn, that target must succeed on a DC 13 Strength saving throw or be knocked prone. If the target is prone, the ngobou can make one stomp attack against it as a bonus action.

___Elephants' Bane.___ The ngobou has advantage on attacks against elephants. It can detect by scent whether an elephant has been within 180 feet of its location anytime in the last 48 hours.

___Spikes.___ A creature that grapples an ngobou takes 9 (2d8) piercing damage.

**Actions**

___Gore.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 38 (6d10 + 5) piercing damage.

___Stomp.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one prone creature. Hit: 18 (3d8 + 5) bludgeoning damage.

