"Ancient Titan";;;_size_: Gargantuan celestial
_alignment_: neutral good
_challenge_: "12 (8400 XP)"
_languages_: "Common, Giant, Primordial, Titan, telepathy 120 ft."
_skills_: "Athletics +14, Intimidation +9, Perception +7"
_senses_: "darkvision 120 ft., passive Perception 17"
_saving_throws_: "Con +10, Wis +7, Cha +9"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "50 ft."
_hit points_: "198 (12d20 + 72)"
_armor class_: "15 (breastplate)"
_stats_: | 27 (+8) | 13 (+1) | 22 (+6) | 16 (+3) | 16 (+3) | 20 (+5) |

___Magic Resistance.___ The ancient titan has advantage on saving throws against spells and other magical effects.

___Innate Spellcasting.___ The ancient titan's spellcasting ability is Charisma (spell save DC 17). The ancient titan can innately cast the following spells, requiring no material components:

3/day: power word stun

1/day: power word kill

**Actions**

___Multiattack.___ The ancient titan makes two greatsword attacks or two longbow attacks

___Greatsword.___ Melee Weapon Attack: +12 to hit, reach 10 ft., one target. Hit: 38 (8d6 + 8) slashing damage.

___Longbow.___ Ranged Weapon Attack: +5 to hit, range 150/640 ft., one target. Hit: 19 (4d8 + 1) piercing damage.

___Eldritch Singularity (Recharge 5-6).___ The ancient titan opens a momentary rupture in the eldritch source that fuels its words of power. This rupture appears at a spot designated by the titan within 100 feet. Any creature within 60 feet of the spot must make a DC 17 Constitution saving throw. On a failure, the creature takes 28 (8d6) force damage, falls prone, and is pulled 1d6 x 10 feet toward the eldritch singularity, taking an additional 3 (1d6) bludgeoning damage per 10 feet they were dragged. If the saving throw succeeds, the target takes half as much force damage and isn't knocked prone or pulled. The spot where the rupture occurs becomes the center of a 60-foot-radius antimagic field until the end of the ancient titan's next turn. The titan's spells are not affected by this antimagic field.

