"Barghest";;;_size_: Large fiend (shapechanger)
_alignment_: neutral evil
_challenge_: "4 (1,100 XP)"
_languages_: "Abyssal, Common, Goblin, Infernal, telepathy 60 ft."
_senses_: "blindsight 60 ft., darkvision 60 ft."
_skills_: "Deception +4, Intimidation +4, Perception +5, Stealth +4"
_damage_immunities_: "acid, poison"
_speed_: "60 ft. (30 ft.in goblin form)"
_hit points_: "90 (12d10+24)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 19 (+4) | 15 (+2) | 14 (+2) | 13 (+1) | 12 (+1) | 14 (+2) |

___Shapechanger.___ The barghest can use its action to polymorph into a Small goblin or back into its true form. Other than its size and speed, its statistics are the same in each form. Any equipment it is wearing or carrying isn't transformed. The barghest reverts to its true form if it dies.

___Fire Banishment.___ When the barghest starts its turn engulfed in flames that are at least 10 feet high or wide, it must succeed on a DC 15 Charisma saving throw or be instantly banished to Gehenna. Instantaneous bursts of flame (such as a red dragon's breath or a fireball spell) don't have this effect on the barghest.

___Keen Smell.___ The barghest has advantage on Wisdom (Perception) checks that rely on smell.

___Innate Spellcasting.___ The barghest's innate spellcasting ability is Charisma (spell save DC 12). It can innately cast the following spells, requiring no material components::

* At will: _levitate, minor illusion, pass without trace_

* 1/day each: _charm person, dimension door, suggestion_

___Soul Feeding.___ A barghest can feed on the corpse of a humanoid that it killed that has been dead for less than 10 minutes, devouring both flesh and soul in doing so. This feeding takes at least 1 minute, and it destroys the victim's body. The victim's soul is trapped in the barghest for 24 hours, after which time it is digested. If the barghest dies before the soul is digested, the soul is released.

While a humanoid's soul is trapped in a barghest, any form of revival that could work has only a 50 percent chance of doing so, freeing the soul from the barghest if it is successful. Once a creature's soul is digested, however, no mortal magic can return that humanoid to life.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 13 (2d8+4) piercing damage.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8+4) slashing damage.
