"Lesser Thaumaturmite";;;_size_: Small monstrosity
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_languages_: "--"
_senses_: "darkvision 30 ft., passive Perception 7"
_condition_immunities_: "prone"
_speed_: "30 ft., climb 30 ft."
_hit points_: "10 (3d6)"
_armor class_: "12"
_stats_: | 10 (+0) | 15 (+2) | 10 (+0) | 3 (-4) | 5 (-3) | 5 (-3) |

___Magic Scent.___ The thaumaturmite can pinpoint, by scent, the location of
any magical cloth within 30 feet of it.

___Consume Magic.___ Any magical cloth that the thaumaturmite hits starts
to corrode. Each time the thaumaturmite bites into
a magical cloth which is worn or carried,
the bearer of the item must make
a successful DC 12 Dexterity
saving throw or the item starts
corroding. After two failed
saving throws the item loses
its magical qualities.

___Spider Climb.___ The thaumaturmite can climb difficult surfaces, including
upside down on ceilings, without needing to make an ability check.

___Magic Resistance.___ The thaumaturmite has advantage on saving throws
against spells and other magical effects.

**Actions**

___Bite.___ Melee weapon attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) piercing damage.
