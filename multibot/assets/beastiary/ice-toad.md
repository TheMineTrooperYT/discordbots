"Ice Toad";;;_size_: Medium monstrosity
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "Ice Toad"
_senses_: "darkvision 60 ft."
_skills_: "Perception +2"
_damage_immunities_: "cold"
_speed_: "30 ft., swim 30 ft."
_hit points_: "32 (5d8+10)"
_armor class_: "12 (natural armor)"
_stats_: | 13 (+1) | 10 (0) | 14 (+2) | 8 (-1) | 10 (0) | 6 (-2) |

___Amphibious.___ The toad can breathe air or water.

___Cold Aura.___ Any creature that starts its turn within 5 feet of the toad takes 3 (1d6) cold damage.

___Standing Leap.___ The toad's long jump is up to 20 feet and its high jump is up to 10 feet, with or without a running start.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4 (1d8) cold damage. If the target is a Medium or smaller creature it is grappled (escape DC 11). Until this grapple ends, the toad can't bite another target.