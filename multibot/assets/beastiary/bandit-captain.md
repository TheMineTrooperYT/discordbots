"Bandit Captain";;;_size_: Medium humanoid (any race)
_alignment_: any non-lawful alignment
_challenge_: "2 (450 XP)"
_languages_: "any two languages"
_skills_: "Athletics +4, Deception +4"
_saving_throws_: "Str +4, Dex +5, Wis +2"
_speed_: "30 ft."
_hit points_: "65 (10d8+20)"
_armor class_: "15 (studded leather)"
_stats_: | 15 (+2) | 16 (+3) | 14 (+2) | 14 (+2) | 11 (0) | 14 (+2) |

**Actions**

___Multiattack.___ The captain makes three melee attacks: two with its scimitar and one with its dagger. Or the captain makes two ranged attacks with its daggers.

___Scimitar.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Dagger.___ Melee or Ranged Weapon Attack: +5 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 5 (1d4 + 3) piercing damage.

**Reactions**

___Parry.___ The captain adds 2 to its AC against one melee attack that would hit it. To do so, the captain must see the attacker and be wielding a melee weapon.