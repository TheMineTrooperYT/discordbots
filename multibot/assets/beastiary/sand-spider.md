"Sand Spider";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "7 (2900 XP)"
_languages_: "--"
_skills_: "Perception +4, Stealth +6"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 14"
_speed_: "30 ft., burrow 20 ft."
_hit points_: "105 (14d10 + 28)"
_armor class_: "15 (natural armor)"
_stats_: | 20 (+5) | 17 (+3) | 14 (+2) | 4 (-3) | 12 (+1) | 4 (-3) |

___Sand Stealth.___ The sand spider gains an additional +3 to Stealth (+9 in total) in sand terrain.

___Spider Climb.___ The sand spider can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

___Ambusher.___ The sand spider has advantage on attack rolls against surprised targets.

**Actions**

___Multiattack.___ The sand spider makes two attacks with its impaling legs and one bite attack.

___Impaling Leg.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 11 (1d12 + 5) piercing damage. If the sand spider scores a critical hit with this attack, it rolls damage dice three times instead of twice. If both impaling leg attacks hit the same target, the second hit does an extra 11 (1d12 + 5) piercing damage.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one creature. Hit: 16 (2d10 + 5) piercing damage plus 13 (3d8) poison damage, or half as much poison damage with a successful DC 13 Constitution saving throw.

**Reactions**

___Trapdoor Ambush.___ When a creature walks over a sand spider's hidden burrow, the spider can use its reaction to attack that creature with two impaling leg attacks. The creature is considered a surprised target for both attacks. If one or both attacks hit and the target is a Medium or smaller creature, then the sand spider and the target engage in a Strength contest. If the creature wins, it can immediately move 5 feet away from the sand spider. If the contest results in a tie, the creature is grappled (escape DC 15). If the sand spider wins, the creature is grappled and dragged by the sand spider 30 feet into its lair. If the creature is still grappled at the start of the sand spider's next turn, it becomes restrained instead. The restrained creature can escape by using an action to make a successful DC 15 Strength (Athletics) check.

