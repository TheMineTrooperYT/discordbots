"Master Thief";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "any one language (usually Common) plus thieves' cant"
_skills_: "Acrobatics +7, Athletics +3, Perception +3, Sleight of Hand +7, Stealth +7"
_saving_throws_: "Dex +7, Int +3"
_speed_: "30 ft."
_hit points_: "84 (13d8+26)"
_armor class_: "16 (studded leather armor)"
_stats_: | 11 (0) | 18 (+4) | 14 (+2) | 11 (0) | 11 (0) | 12 (+1) |

___Cunning Action.___ On each of its turns, the thief can use a bonus action to take the Dash, Disengage, or Hide action.

___Evasion.___ If the thief is subjected to an effect that allows it to make a Dexterity saving throw to take only half damage, the thief instead takes no damage if it succeeds on the saving throw, and only half damage if it fails.

___Sneak Attack (1/Turn).___ The thief deals an extra 14 (4d6) damage when it hits a target with a weapon attack and has advantage on the attack roll, or when the target is within 5 feet of an ally of the thief that isn't incapacitated and the thief doesn't have disadvantage on the attack roll.

**Actions**

___Multiattack.___ The thief makes three attacks with its shortsword.

___Shortsword.___ Melee Weapon Attack: +7 to hit, reach 5ft., one target. Hit: 7 (1d6+4) piercing damage.

___Light Crossbow.___ Ranged Weapon Attack: +7 to hit, range 80/320 ft., one target. Hit: 8 (1d8+4) piercing damage.

**Reactions**

___Uncanny Dodge.___ The thief halves the damage that it takes from an attack that hits it. The thief must be able to see the attacker.