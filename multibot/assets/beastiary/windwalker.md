"Windwalker";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "Any three languages"
_skills_: "Arcana +7, Nature +7, Perception +7, Survival +7"
_senses_: "passive Perception 17"
_saving_throws_: "Int +7, Wis +7"
_speed_: "30 ft."
_hit points_: "74 (8d8 + 4d6 + 24)"
_armor class_: "13 (leather)"
_stats_: | 8 (-1) | 14 (+2) | 14 (+2) | 16 (+3) | 16 (+3) | 10 (+0) |

___Arcane Insights.___ The windwalker has advantage on
saving throws against spells and magical effects.

___Natural Recovery (2/Day).___ When the windwalker
finishes a short rest, it can regain up to 4 total
expended spell slots.

___Spellcasting.___ The windwalker is a 9th-level
spellcaster. Its spellcasting ability is wisdom (spell
save DC 15, +7 to hit with spell attacks). The
windwalker has the following druid spells prepared:

* Cantrips (at will): _druidcraft, gust, thunderclap_

* 1st level (4 slots): _entangle, faerie fire, fog cloud_

* 2nd level (3 slots): _barkskin, gust of wind, levitate, misty step_

* 3rd level (3 slots): _call lightning, fly, sleet storm, wind wall_

* 4th level (3 slots): no spells learned for this level

* 5th level (1 slot): no spells learned for this level

___Wild Shape Giant Eagle (2/Day).___ As a bonus action,
the windwalker transforms into a giant eagle for up
to four hours. When the eagle is reduced to 0 hit
points, the windwalker reverts to its normal form,
with any excess damage carrying over to the
windwalker's hit points.

___Wind Shield.___ The area around the windwalker is
enveloped in chaotic winds. The area within 15 feet
of the windwalker is considered difficult terrain for
enemy creatures. Additionally, ranged attacks
against the windwalker are made with disadvantage.

**Actions**

___Quarterstaff.___ Melee Weapon Attack: +3 to hit, reach
5 ft., one target. Hit: 3 (1d8 - 1) bludgeoning
damage, or 4 (1d10 - 1) bludgeoning damage if
used with two hands.
