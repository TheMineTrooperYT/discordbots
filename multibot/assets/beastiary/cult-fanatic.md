"Cult Fanatic";;;_size_: Medium humanoid (any race)
_alignment_: any non-good alignment
_challenge_: "2 (450 XP)"
_languages_: "any one language (usually Common)"
_skills_: "Deception +4, Persuasion +4, Religion +2"
_speed_: "30 ft."
_hit points_: "33 (6d8+6)"
_armor class_: "13 (leather armor)"
_stats_: | 11 (0) | 14 (+2) | 12 (+1) | 10 (0) | 13 (+1) | 14 (+2) |

___Dark Devotion.___ The fanatic has advantage on saving throws against being charmed or frightened.

___Spellcasting.___ The fanatic is a 4th-level spellcaster. Its spell casting ability is Wisdom (spell save DC 11, +3 to hit with spell attacks). The fanatic has the following cleric spells prepared:

* Cantrips (at will): _light, sacred flame, thaumaturgy_

* 1st level (4 slots): _command, inflict wounds, shield of faith_

* 2nd level (3 slots): _hold person, spiritual weapon_

**Actions**

___Multiattack.___ The fanatic makes two melee attacks.

___Dagger.___ Melee or Ranged Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one creature. Hit: 4 (1d4 + 2) piercing damage.
