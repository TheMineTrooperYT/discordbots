"Umbral Eye Killer";;;_size_: Medium monstrosity
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_skills_: "Perception +6, Stealth +6"
_senses_: "darkvision 120 ft., passive Perception 16"
_speed_: "30 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "13"
_stats_: | 17 (+3) | 16 (+3) | 14 (+2) | 2 (-4) | 13 (+1) | 12 (+1) |

___Sunlight Vulnerability.___ If a source of sunlight is brought within 5 feet
of the eye killer, the eye killer becomes frightened for 1 minute. While
frightened, the eye killer must end any grapple it is maintaining and use
its action to Dash away from the source of sunlight.

___Innate Spellcasting.___ The umbral eye killer’s innate spellcasting ability
is Charisma (spell save DC 12, +4 to hit with spell attacks). It can cast
the following without material components.

* 3/day: _darkness_

___See in Darkness.___ The umbral eye killer can see in magical and
nonmagical darkness.

**Actions**

___Tail Slap.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit:
14 (2d10 + 3) bludgeoning damage and the target is grappled (escape DC
13). At the beginning of each of the eye killer’s turns, a grappled creature
takes 14 (2d10 + 3) bludgeoning damage.

___Death Ray (1/day).___ If the eye killer begins its turn within an area of
bright light or dim light, it can immediately douse that source of light if it
is created by a spell of 3rd level or lower. When it does so, it absorbs and
redirects the light in a 60-foot line that is 5 feet across. Creatures in the
area must make a DC 13 Dexterity saving throw. On a failed saving throw,
the target drops to 0 hit points and begins dying. On a successful saving
throw, the target takes 10 (3d6) radiant damage.
