"Yagnoloth";;;_page_number_: 252
_size_: Large fiend (yugoloth)
_alignment_: neutral evil
_challenge_: "11 (7200 XP)"
_languages_: "Abyssal, Infernal, telepathy 60 ft."
_senses_: "blindsight 60 ft., darkvision 60 ft., passive Perception 16"
_skills_: "Deception +8, Insight +6, Perception +6, Persuasion +8"
_damage_immunities_: "acid, poison"
_saving_throws_: "Dex +6, Int +7, Wis +6, Cha +8"
_speed_: "40 ft."
_hit points_: "147  (14d10 + 70)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 19 (+4) | 14 (+2) | 21 (+5) | 16 (+3) | 15 (+2) | 18 (+4) |

___Innate Spellcasting.___ The yagnoloth's innate spellcasting ability is Charisma (spell save DC 16). It can innately cast the following spells, requiring no material components:

* At will: _darkness, detect magic, dispel magic, invisibility _(self only)_, suggestion_

* 3/day: _lightning bolt_

___Magic Resistance.___ The yagnoloth has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The yagnoloth's weapon attacks are magical.

**Actions**

___Multiattack___ The yagnoloth makes one massive arm attack and one electrified touch attack, or it makes one massive arm attack and teleports before or after the attack.

___Electrified Touch___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 27 (6d8) lightning damage.

___Massive Arm___ Melee Weapon Attack: +8 to hit, reach 15 ft., one target. Hit: 23 (3d12 + 4) bludgeoning damage. If the target is a creature, it must succeed on a DC 16 Constitution saving throw or is stunned until the end of the yagnoloth's next turn.

___Life Leech___ The yagnoloth touches one incapacitated creature within 15 feet of it. The target takes 36 (7d8 + 4) necrotic damage, and the yagnoloth gains temporary hit points equal to half the damage dealt. The target must succeed on a DC 16 Constitution saving throw, or its hit point maximum is reduced by an amount equal to the damage taken. This reduction lasts until the target finishes a long rest, and the target dies if its hit point maximum is reduced to 0.

___Battlefield Cunning (Recharge 4-6)___ Up to two allied yugoloths within 60 feet of the yagnoloth that can hear it can use their reactions to make one melee attack each.

___Teleport___ The yagnoloth magically teleports, along with any equipment it is wearing or carrying, up to 60 feet to an unoccupied space it can see.
