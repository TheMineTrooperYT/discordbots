"Young Mist Dragon";;;_size_: Large dragon
_alignment_: neutral
_challenge_: "10 (5,900 XP)"
_languages_: "Auran, Aquan, Common, Draconic, Ignan"
_skills_: "Perception +9, Stealth +6, Survival +5"
_senses_: "blindsight 120 ft., darkvision 120 ft., passive Perception 19"
_saving_throws_: "Dex +6, Con +9, Wis +5, Cha +8"
_damage_immunities_: "fire"
_speed_: "40 ft., fly 80 ft., swim 40 ft."
_hit points_: "147 (14d10 + 70)"
_armor class_: "17 (natural armor)"
_stats_: | 23 (+7) | 14 (+2) | 21 (+7) | 14 (+3) | 13 (+2) | 19 (+5) |

___Amphibious.___ The mist dragon can breathe both water and air.

**Actions**

___Multiattack.___ The dragon uses its Frightful Presence and makes three
attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 17
(2d10 + 6) piercing damage plus 7 (2d6) fire damage.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 13
(2d6 + 6) slashing damage.

___Breath Weapon (Recharge 5–6).___ The mist dragon uses one of the
following breath weapons.

1. _Obscuring Mist._ The dragon exhales a murky opaque
mist in a 30-foot cube that spreads around corners. This mist
heavily obscures the area and remains for 1 minute, or until
dispersed with a moderate or stronger wind (at least 10 miles
per hour).

2. _Scalding Mist._ The dragon exhales a fiery blast of
lingering mist in a 30-foot cone that spreads around
corners. The mist lightly obscures the area and remains for 1
minute, or until dispersed with a moderate or stronger wind (at
least 10 miles per hour). Each creature that enters the area or
begins their turn there must make a DC 17 Constitution saving
throw, taking 49 (14d6) fire damage on a failed saving throw, or
half as much damage on a successful one. Being underwater doesn’t
grant resistance to this damage.

___Mist Form.___ The mist dragon polymorphs into a Large cloud of mist,
or back into its true form. While in mist form, the dragon can’t take any
actions, speak, or manipulate objects. It is weightless, has a flying speed
of 20 feet, can hover, and can enter a hostile creature’s space and stop
there. In addition, if air can pass through a space, the mist can do so
without squeezing, and it can’t pass through water. It has advantage on
Strength, Dexterity, and Constitution saving throws, it is immune to all
nonmagical damage.
