"Serpopard";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "4 (1100 XP)"
_languages_: "--"
_skills_: "Perception +3, Stealth +5"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_resistances_: "poison"
_condition_immunities_: "poisoned"
_speed_: "40 ft., swim 30 ft."
_hit points_: "85 (10d10 + 30)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 16 (+3) | 16 (+3) | 2 (-4) | 12 (+1) | 6 (-2) |

___Swamp Stealth.___ The serpopard gains an additional +2 to Stealth (+7 in total) in sand or swamp terrain.

___Sinuous Strikeback.___ The serpopard can take any number of reactions in a round, but it can react only once to each trigger.

**Actions**

___Multiattack.___ The serpopard makes two bite attacks and two claw attacks.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 10 ft., one target. Hit: 10 (2d6 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) slashing damage.

___Musk (Recharges after a Short or Long Rest).___ The serpopard releases a jet of foul-smelling musk in a 15-foot cone that lasts for 2d4 rounds. Creatures in the cone must make DC 13 Dexterity saving throws. If the save succeeds, the creature moves to the nearest empty space outside the cone; if the saving throw fails, the creature becomes drenched in musk. A creature that enters the area of the cone while the musk persists is saturated automatically. A creature saturated in musk is poisoned. In addition, every creature that starts its turn within 5 feet of a saturated creature must make a successful DC 15 Constitution saving throw or be poisoned until the start of its next turn. Serpopard musk (and the poisoning) wear off naturally in 1d4 hours. A saturated creature can end the effect early by spending 20 minutes thoroughly washing itself, its clothes, and its equipment with water and soap.

