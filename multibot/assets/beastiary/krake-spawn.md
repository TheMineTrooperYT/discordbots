"Krake Spawn";;;_size_: Huge monstrosity
_alignment_: neutral evil
_challenge_: "9 (5000 XP)"
_languages_: "Common, Infernal, Primordial, Void Speech"
_senses_: "darkvision 60 ft., passive Perception 12"
_saving_throws_: "Str +11, Con +10, Int +7, Cha +8"
_damage_immunities_: "cold, poison, psychic"
_condition_immunities_: "charmed, poisoned"
_speed_: "20 ft., swim 30 ft."
_hit points_: "150 (12d12 + 72)"
_armor class_: "16 (natural armor)"
_stats_: | 24 (+7) | 12 (+1) | 22 (+6) | 17 (+3) | 15 (+2) | 18 (+4) |

___Amphibious.___ The krake spawn can breathe air and water.

___Jet.___ While underwater, the krake spawn can take the withdraw action to jet backward at a speed of 140 feet. It must move in a straight line while using this ability.

___Innate Spellcasting.___ The krake spawn's innate spellcasting ability is Charisma (spell save DC 16). It can innately cast the following spells, requiring no material components:

At will: protection from energy, ray of frost

1/day each: ice storm, wall of ice

**Actions**

___Multiattack.___ The krake spawn makes eight tentacle attacks and one bite attack. It can subsitute one constrict attack for two tentacle attacks if it has a creature grappled at the start of the krake spawn's turn, but it never constricts more than once per turn.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 10 ft, one target. Hit: 12 (1d10 + 7) slashing damage.

___Tentacle.___ Melee Weapon Attack: +11 to hit, reach 15 ft., one target. Hit: 10 (1d6 + 7) necrotic damage. If two tentacle attacks hit the same target in one turn, the target is also grappled (escape DC 17).

___Constrict.___ The constricted creature takes 26 (3d12 + 7) bludgeoning damage and is grappled (escape DC 17) and restrained.

___Ink Cloud (Recharge 6).___ The krake spawn emits black, venomous ink in a 30-foot cloud as a bonus action while underwater. The cloud affects vision as the darkness spell, and any creature that starts its turn inside the cloud takes 10 (3d6) poison damage, or half damage with a successful DC 18 Constitution saving throw. The krake spawn's darkvision is not impaired by this cloud. The cloud persists for 1 minute, then disperses.

___Vomit Forth the Deeps (1/Day).___ The krake spawn sprays halfdigested food from its maw over a 15-foot cone. This acidic slurry causes 3 (1d6) acid damage and targets must make a successful DC 18 Constitution saving throw or be incapacitated until the end of their next turn.

