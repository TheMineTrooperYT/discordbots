"Green Guard Drake";;;_size_: Medium dragon
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "understands Draconic but can't speak it"
_senses_: "darkvision 60 ft."
_skills_: "Perception +2"
_speed_: "30 ft., swim 30 ft."
_hit points_: "52 (7d8+21)"
_armor class_: "14 (natural armor)"
_damage_resistances_: "poison"
_stats_: | 16 (+3) | 11 (0) | 16 (+3) | 4 (-3) | 10 (0) | 7 (-2) |

___Amphibious.___ The guard drake can breathe air and water.

**Actions**

___Multiattack.___ The drake attacks twice, once with its bite and once with its tail.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8+3) piercing damage.

___Tail.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6+3) bludgeoning damage.