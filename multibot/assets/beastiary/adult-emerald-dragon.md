"Adult Emerald Dragon";;;_size_: Huge dragon
_alignment_: chaotic neutral
_challenge_: "15 (13,000 XP)"
_languages_: "Common, Draconic, telepathy 120 ft."
_skills_: "Arcana +10, Insight +8, Perception +8, Religion +10"
_senses_: "blindsight 60 ft., darkvision 120 ft., passive Perception 23"
_saving_throws_: "Dex +10, Int +10, Wis +8, Cha +9"
_damage_vulnerabilities_: "psychic"
_damage_immunities_: "fire, lightning"
_damage_resistances_: "bludgeoning, slashing, and piercing from nonmagical attacks"
_speed_: "40 ft., fly 80 ft. (hover), swim 40 ft."
_hit points_: "127 (17d12 + 17)"
_armor class_: "20 (natural armor)"
_stats_: | 19 (+4) | 20 (+5) | 12 (+1) | 20 (+5) | 16 (+3) | 18 (+4) |

___Legendary Resistance (3/Day).___ If the dragon fails
a saving throw, it can choose to succeed instead.

___Interference Aura.___ Enemies within 30 feet must
make a DC 18 Intelligence saving throw every
round to maintain spells that require concentration.

**Psionics**

___Charges:___ 17 | ___Recharge:___ 1d8 | ___Fracture:___ 19

**Actions**

___Multiattack.___ The dragon makes three attacks: one
with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 10 ft.,
one target. Hit: 15 (2d10 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 5 ft.,
one target. Hit: 11 (2d6 + 4) slashing damage.

___Tail.___ Melee Weapon Attack: +9 to hit; reach 15 ft.,
one target. Hit: 13 (2d8 + 4) bludgeoning damage.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing
from the options below. Only one legendary action
option can be used at a time and only at the end of
another creature’s turn. The dragon regains spent
legendary actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception)
Check.

___Psionics.___ The dragon uses a psionic ability.

___Psionic Shift (Costs 2 Actions).___ The dragon
releases a wave of telekinetic energy from its mind.
Every creature within 15 feet must make a DC 24
Intelligence saving throw or take 12 (2d6 + 5) force
damage and be knocked prone. The dragon then
can move up to half its movement speed.
