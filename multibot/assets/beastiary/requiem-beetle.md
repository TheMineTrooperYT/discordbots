"Requiem Beetle";;;_size_: Gargantuan beast
_alignment_: unaligned
_challenge_: "15 (13,000 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., tremorsense 100 ft., passive Perception 10"
_damage_immunities_: "acid"
_speed_: "50 ft."
_hit points_: "495 (30d20 + 180)"
_armor class_: "19 (natural armor)"
_stats_: | 27 (+8) | 12 (+1) | 23 (+6) | 1 (-5) | 11 (+0) | 2 (-4) |

___Charge.___ If the requiem beetle moves at least 20 feet straight toward a creature and then hits it with a bite attack on the same turn, that target must succeed on a DC 20 Strength saving throw or be knocked prone. If the target is prone, the requiem beetle can make one claw attack against it as a bonus action.

___Earthshaker.___ When the requiem beetle moves more than 10 feet in a single turn, creatures within 20 feet of it must succeed on a DC 12 Dexterity saving throw or be knocked prone as the ground shakes.

**Actions**

___Multiattack.___ The requiem beetle makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +13 to hit, reach 10 ft., one target. Hit: 24 (3d10 + 8) piercing damage.

___Claws.___ Melee Weapon Attack: +13 to hit, reach 15 ft., one target. Hit: 21 (3d8 + 8) slashing damage.
