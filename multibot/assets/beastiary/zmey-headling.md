"Zmey Headling";;;_size_: Medium dragon
_alignment_: chaotic evil
_challenge_: "5 (1800 XP)"
_languages_: "Common, Draconic, Sylvan"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_resistances_: "cold, fire"
_condition_immunities_: "paralyzed, unconscious"
_speed_: "30 ft., swim 20 ft."
_hit points_: "105 (14d8 + 42), regeneration 5/round"
_armor class_: "16 (natural armor)"
_stats_: | 16 (+3) | 10 (+0) | 1 (-5) | 8 (-1) | 16 (+3) | 8 (-1) |

___Amphibious.___ The zmey headling can breathe air and water.

___Regeneration.___ The zmey headling reaver regains 10 hit points at the start of its turn. This trait doesn't function if the zmey headling took acid or fire damage since the end of its previous turn. It dies if it starts its turn with 0 hit points and doesn't regenerate.

**Actions**

___Multiattack.___ The zmey headline makes one bite attack and one claws attack.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 16 (2d12 + 3) piercing damage.

___Claws.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 12 (2d8 + 3) slashing damage.

___Tail.___ Melee Weapon Attack: +11 to hit, reach 20 ft., one target. Hit: 15 (2d8 + 6) bludgeoning damage.

___Fire Breath (Recharge 5-6).___ The zmey headling exhales fire in a 30-foot cone. Each creature in that area takes 16 (3d10) fire damage, or half damage with a successful DC 16 Dexterity saving throw.

