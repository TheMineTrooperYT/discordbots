"Halaster Blackcloak";;;_page_number_: 310
_size_: Medium humanoid (human)
_alignment_: chaotic evil
_challenge_: "23 (50,000 XP)"
_languages_: "Abyssal, Celestial, Common, Draconic, Dwarvish, Elvish, Infernal, Undercommon"
_senses_: "darkvision 120 ft., passive Perception 21"
_skills_: "Arcana +21, HIstory +21, Perception +11"
_saving_throws_: "Int +14, Wis +11"
_speed_: "30 ft."
_hit points_: "246 (29d8+116)"
_armor class_: "14 (17 with *mage armor*)"
_damage_resistances_: "fire and lightning (granted by the *blast scepter*; see “Special Equipment” below)"
_stats_: | 10 (+0) | 18 (+4) | 18 (+4) | 24 (+7) | 18 (+4) | 18 (+4) |

___Special Equipment.___ Halaster wears a *robe of eyes* that lets him see in all directions, gives him darkvision out to a range of 120 feet, grants advantage on Wisdom (Perception) checks that rely on sight, and allows him to see invisible creatures and objects, as well as into the Ethereal Plane, out to a range of 120 feet.

Halaster wields a *blast scepter* (a very rare magic item that requires attunement). It can be used as an arcane focus. Whoever is attuned to the *blast scepter* gains resistance to fire and lightning damage and can, as an action, use it to cast *thunderwave* as a 4th-level spell (save DC 16) without expending a spell slot.

Halaster also wears a *horned ring* (a very rare magic item that requires attunement), which allows an attuned wearer to ignore Undermountain’s magical restrictions (see “Alterations to Magic”).

___Arcane Recovery (1/Day).___ When he finishes a short rest, Halaster recovers all his spell slots of 5th level and lower.

___Legendary Resistance (3/Day).___ If Halaster fails a saving throw, he can choose to succeed instead.

___Rejuvenation.___ If Halaster dies in Undermountain, he revives after 1d10 days, with all his hit points and any missing body parts restored. His new body appears in a random safe location in Undermountain.

___Spellcasting.___  Halaster is a 20th-level spellcaster. His spellcasting ability is Intelligence (spell save DC 22, +14 to hit with spell attacks). He can cast *disguise self* and *invisibility* at will. He can cast *fly* and *lightning bolt* once each without expending a spell slot, but can’t do so again until he finishes a short or long rest. Halaster has the following wizard spells prepared:

* Cantrips (at will): _dancing lights, fire bolt, light, mage hand, prestidigitation_

* 1st level (4 slots): _mage armor, magic missile, shield, silent image_

* 2nd level (3 slots): _arcane lock, cloud of daggers, darkvision, knock_

* 3rd level (3 slots): _counterspell, dispel magic, fireball_

* 4th level (3 slots): _confusion, hallucinatory terrain, polymorph_

* 5th level (3 slots): _Bigby’s hand, geas, wall of force_

* 6th level (2 slots): _chain lightning, globe of invulnerability, programmed illusion_

* 7th level (2 slots): _finger of death, symbol, teleport_

* 8th level (1 slot): _maze, mind blank_

* 9th level (1 slot): _meteor swarm, wish_

**Actions**

___Blast Scepter.___ Halaster uses his *blast scepter* to cast *thunderwave* as a 4th-level spell (save DC 16).


**Legendary** Actions

Halaster can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature’s turn. Halaster regains spent legendary actions at the start of his turn.

___Cast Spell.___ Halaster casts a spell of 3rd level or lower.

___Spell Ward (Costs 2 Actions).___ Halaster expends a spell slot of 4th level or lower and gains 5 temporary hit points per level of the slot.
