"Spider Thief";;;_size_: Small construct
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_languages_: "understands Common but can't speak"
_skills_: "Stealth +3"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison, psychic"
_damage_resistances_: "fire"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft., climb 20 ft."
_hit points_: "54 (12d6 + 12)"
_armor class_: "13 (natural armor)"
_stats_: | 10 (+0) | 12 (+1) | 12 (+1) | 3 (-4) | 10 (+0) | 1 (-5) |

___Immutable Form.___ The spider thief is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The spider thief has advantage on saving throws against spells and other magical effects.

___Wire-Assisted Jump.___ If its razor line attack is available, a spider thief can use its movement to leap 20 feet in any direction by launching the wire like a spider's web so that it spears or snags an object, then immediately reeling it back in. It can carry up to 25 lb. of additional weight while moving this way. Moving this way doesn't expend its razor line attack.

**Actions**

___Multiattack.___ The spider thief makes two sickle claw attacks.

___Sickle Claw.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 10 (2d8 + 1) slashing damage.

___Razor Line (Recharge 5-6).___ Melee Weapon Attack: +3 to hit, reach 15 ft., one target. Hit: 3 (1d4 + 1) slashing damage, and the target is grappled (escape DC 10). Instead of moving, the spider thief can retract the razor line and pull itself onto the grappled creature (the spider thief enters and remains in the target's space). The spider thief's sickle claw attacks have advantage against a grappled creature in the same space. If the grappled creature escapes, the spider thief immediately displaces into an unoccupied space within 5 feet.

