"Flameskull";;;_size_: Tiny undead
_alignment_: neutral evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft."
_skills_: "Arcana +5, Perception +2"
_damage_immunities_: "cold, fire, poison"
_speed_: "0 ft., fly 40 ft. It can hover."
_hit points_: "40 (9d4+18)"
_armor class_: "13"
_condition_immunities_: "charmed, frightened, paralyzed, poisoned, prone"
_damage_resistances_: "lightning, necrotic, piercing"
_stats_: | 1 (-5) | 17 (+3) | 14 (+2) | 16 (+3) | 10 (0) | 11 (0) |

___Illumination.___ The flameskull sheds either dim light in a 15- foot radius, or bright light in a 15-foot radius and dim light for an additional 15 ft.. It can switch between the options as an action.

___Magic Resistance.___ The flameskull has advantage on saving throws against spells and other magical effects.

___Rejuvenation.___ If the flameskull is destroyed, it regains all its hit points in 1 hour unless holy water is sprinkled on its remains or a dispel magic or remove curse spell is cast on them.

___Spellcasting.___ The flameskull is a 5th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 13, +5 to hit with spell attacks). It requires no somatic or material components to cast its spells. The flameskull has the following wizard spells prepared:

* Cantrips (at will): _mage hand_

* 1st level (3 slots): _magic missile, shield_

* 2nd level (2 slots): _blur, flaming sphere_

* 3rd level (1 slot): _fireball_

**Actions**

___Multiattack.___ The flameskull uses Fire Ray twice.

___Fire Ray.___ Ranged Spell Attack: +5 to hit, range 30 ft., one target. Hit: 10 (3d6) fire damage.
