"Salt Golem";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "10 (5900 XP)"
_languages_: "understands the languages of its creator but can't speak"
_skills_: "Athletics +9"
_senses_: "darkvision 120 ft., passive Perception 10"
_damage_immunities_: "fire, poison, psychic; bludgeoning, piercing, and slashing from nonmagical weapons that aren't adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "20 ft."
_hit points_: "110 (11d10 + 55)"
_armor class_: "17 (natural armor)"
_stats_: | 20 (+5) | 9 (-1) | 20 (+5) | 3 (-4) | 11 (+0) | 1 (-5) |

___Blinding Salt Spray.___ Any time the golem is hit in combat, thousands of tiny salt crystals erupt from its body. All creatures within 5 feet of the golem must succeed on a DC 17 Dexterity saving throw or become blinded for 1d3 rounds.

___Immutable Form.___ The golem is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The golem has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The golem's weapon attacks are magical.

**Actions**

___Multiattack.___ The golem makes two slam attacks.

___Slam.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 27 (5d8 + 5) bludgeoning damage and the target must make a successful DC 17 Constitution saving throw or gain one level of exhaustion.

