"Warhorse Skeleton";;;_size_: Large undead
_alignment_: lawful evil
_challenge_: "1/2 (100 XP)"
_senses_: "darkvision 60 ft."
_damage_immunities_: "poison"
_speed_: "60 ft."
_hit points_: "22 (3d10+6)"
_armor class_: "13 (barding scraps)"
_damage_vulnerabilities_: "bludgeoning"
_condition_immunities_: "exhaustion, poisoned"
_stats_: | 18 (+4) | 12 (+1) | 15 (+2) | 2 (-4) | 8 (-1) | 5 (-3) |

**Actions**

___Hooves.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage.