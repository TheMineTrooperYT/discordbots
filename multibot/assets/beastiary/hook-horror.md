"Hook Horror";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "Hook Horror"
_senses_: "blindsight 60 ft., darkvision 10 ft."
_skills_: "Perception +3"
_speed_: "30 ft., climb 30 ft."
_hit points_: "75 (10d10+20)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 10 (0) | 15 (+2) | 6 (-2) | 12 (+1) | 7 (-2) |

___Echolocation.___ The hook horror can't use its blindsight while deafened.

___Keen Hearing.___ The hook horror has advantage on Wisdom (Perception) checks that rely on hearing.

**Actions**

___Multiattack.___ The hook horror makes two hook attacks.

___Hook.___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 11 (2d6 + 4) piercing damage.