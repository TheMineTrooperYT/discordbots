"Dogmole Juggernaut";;;_size_: Large monstrosity
_alignment_: neutral
_challenge_: "5 (1800 XP)"
_languages_: "--"
_senses_: "blindsight 30 ft., passive Perception 10"
_saving_throws_: "Con +11"
_speed_: "30 ft., burrow 10 ft., swim 10 ft."
_hit points_: "126 (12d10 + 60)"
_armor class_: "15 (chain armor)"
_stats_: | 21 (+5) | 14 (+2) | 20 (+5) | 2 (-4) | 10 (+0) | 2 (-4) |

___Burrow.___ Dogmole juggernauts cannot burrow into solid rock, but they can move through softer material like soil or loose rubble, leaving a usable tunnel 10 ft. in diameter.

___Ferocity (1/Day).___ When the dogmole juggernaut is reduced to 0 hit points, it doesn't die until the end of its next turn.

___Powerful Build.___ A dogmole juggernaut is treated as one size larger if doing so is advantageous to it (such as during grapple checks, pushing attempts, and tripping attempts, but not for the purposes of squeezing or AC). It gains advantage against magical pushing attempts such as gust of wind or Repelling Blast.

___Wormkiller Rage.___ Wild dogmole juggernaut packs are famed for their battles against the monsters of the dark caverns of the world. If a dogmole juggernaut draws blood against vermin, purple worms, or other underground invertebrate, it gains a +4 bonus to Strength and Constitution but suffers a -2 penalty to AC. The wormkiller rage lasts for a number of rounds equal to 1+ its Constitution modifier (minimum 1 round). It cannot end the rage voluntarily while the creatures that sent it into a rage still live.

**Actions**

___Multiattack.___ The dogmole juggernaut makes one claw attack and one bite attack.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 11 (1d12 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 19 (4d6 + 5) slashing damage.

