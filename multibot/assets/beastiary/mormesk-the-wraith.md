"Mormesk the Wraith";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Infernal"
_senses_: "darkvision 60 ft."
_damage_immunities_: "necrotic, poison"
_speed_: "0 ft., fly 60 ft."
_hit points_: "45 (6d8+18)"
_armor class_: "13"
_condition_immunities_: "charmed, grappled, paralyzed, petrified, poisoned, prone, restrained"
_damage_resistances_: "acid, cold, fire, lightning, thunder, bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_stats_: | 6 (-2) | 16 (+3) | 16 (+3) | 12 (+1) | 14 (+2) | 15 (+2) |

___Incorporeal Movement.___ The wraith can move through an object or another creature, but can't stop there.

___Sunlight Sensitivity.___ While in sunlight, the wraith has disadvantage on attack rolls and on Wisdom (Perception) checks that rely on sight.

**Actions**

___Life Drain.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 16 (3d8 + 3) necrotic damage, and the target must succeed on a DC 13 Constitution saving throw or its hit point maximum is reduced by an amount equal to the damage taken. If this attack reduces the target's hit point maximum to 0, the target dies. This reduction to the target's hit point maximum lasts until the target finishes a long rest.