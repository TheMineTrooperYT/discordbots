"Bulette";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_senses_: "darkvision 60 ft., tremorsense 60 ft."
_skills_: "Perception +6"
_speed_: "40 ft., burrow 40 ft."
_hit points_: "94 (9d10+45)"
_armor class_: "17 (natural armor)"
_stats_: | 19 (+4) | 11 (0) | 21 (+5) | 2 (-4) | 10 (0) | 5 (-3) |

___Standing Leap.___ The bulette's long jump is up to 30 ft. and its high jump is up to 15 ft., with or without a running start.

**Actions**

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 30 (4d12 + 4) piercing damage.

___Deadly Leap.___ If the bulette jumps at least 15 ft. as part of its movement, it can then use this action to land on its ft. in a space that contains one or more other creatures. Each of those creatures must succeed on a DC 16 Strength or Dexterity saving throw (target's choice) or be knocked prone and take 14 (3d6 + 4) bludgeoning damage plus 14 (3d6 + 4) slashing damage. On a successful save, the creature takes only half the damage, isn't knocked prone, and is pushed 5 ft. out of the bulette's space into an unoccupied space of the creature's choice. If no unoccupied space is within range, the creature instead falls prone in the bulette's space.