"Vrock Zombie";;;_size_: Large fiend (undead)
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "understands Abyssal but can’t speak"
_senses_: "darkvision 120 ft., passive Perception 11"
_saving_throws_: "Dex +5, Wis +4, Cha +2"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "40 ft., fly 60 ft."
_hit points_: "104 (11d10 + 44)"
_armor class_: "15 (natural armor)"
_stats_: | 17 (+3) | 15 (+2) | 18 (+4) | 8 (-1) | 13 (+1) | 8 (-1) |

___Undead Fortitude.___ If damage reduces the zombie to 0 hit points, it
makes a Constitution saving throw with a DC of 5+ the damage taken,
unless the damage is radiant or from a critical hit. On a success, the
zombie drops to 1 hit point instead.

___Magic Resistance.___ The vrock has advantage on saving throws against
spells and other magical effects.

**Actions**

___Multiattack.___ The vrock makes two attacks: one with its beak and one
with its talons.

___Beak.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10
(2d6 + 3) piercing damage.

___Talons.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 14
(2d10 + 3) slashing damage.

___Spores (Recharge 6).___ A 15-foot radius cloud of toxic spores extends
out from the vrock. The spores spread around corners. Each creature in
that area must succeed on a DC 14 Constitution saving throw or become
poisoned. While poisoned in this way, a target takes 5 (1d10) poison
damage at the start of each of its turns. A target can repeat the saving throw
at the end of each of its turns, ending the effect on itself on a success.
Emptying a vial of holy water on the target also ends the effect on it.

___Stunning Screech (1/day).___ The vrock emits a horrific screech. Each
creature within 20 feet of it that can hear it and that isn’t a demon must
succeed on a DC 14 Constitution saving throw or be stunned until the end
of the vrock’s next turn.
