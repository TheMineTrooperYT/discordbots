"Lizardfolk Shaman";;;_size_: Medium humanoid (lizardfolk)
_alignment_: neutral
_challenge_: "2 (450 XP)"
_languages_: "Draconic"
_skills_: "Perception +4, Stealth +4, Survival +6"
_speed_: "30 ft., swim 30 ft."
_hit points_: "27 (5d8+5)"
_armor class_: "13 (natural armor)"
_stats_: | 15 (+2) | 10 (0) | 13 (+1) | 10 (0) | 15 (+2) | 8 (-1) |

___Hold Breath.___ The lizardfolk can hold its breath for 15 minutes.

___Spellcasting (Lizardfolk Form Only).___ The lizardfolk is a 5th-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 12, +4 to hit with spell attacks). The lizardfolk has the following druid spells prepared:

* Cantrips (at will): _druidcraft, produce flame, thorn whip_

* 1st Level (4 slots): _entangle, fog cloud_

* 2nd Level (3 slots): _heat metal, spike growth_

* 3rd Level (2 slots): _conjure animals _(reptiles only)_, plant growth_

**Actions**

___Multiattack (Lizardfolk Form Only).___ The lizardfolk makes two attacks: one with its bite and one with its claws.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage, or 7 (1d10 + 2) piercing damage in crocodile form. If the lizardfolk is in crocodile form and the target is a Large or smaller creature, the target is grappled (escape DC 12). Until this grapple ends, the target is restrained, and the lizardfolk can't bite another target. If the lizardfolk reverts to its true form, the grapple ends.

___Claws (Lizardfolk Form Only).___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) slashing damage.

___Change Shape (Recharges after a Short or Long Rest).___ The lizardfolk magically polymorphs into a crocodile, remaining in that form for up to 1 hour. It can revert to its true form as a bonus action. Its statistics, other than its size, are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.
