"Giant Boar";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_speed_: "40 ft."
_hit points_: "42 (5d10+15)"
_armor class_: "12 (natural armor)"
_stats_: | 17 (+3) | 10 (0) | 16 (+3) | 2 (-4) | 7 (-2) | 5 (-3) |

___Charge.___ If the boar moves at least 20 ft. straight toward a target and then hits it with a tusk attack on the same turn, the target takes an extra 7 (2d6) slashing damage. If the target is a creature, it must succeed on a DC 13 Strength saving throw or be knocked prone.

___Relentless (Recharges after a Short or Long Rest).___ If the boar takes 10 damage or less that would reduce it to 0 hit points, it is reduced to 1 hit point instead.

**Actions**

___Tusk.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) slashing damage.