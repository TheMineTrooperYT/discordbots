"Gerridae";;;_size_: Large fey
_alignment_: neutral
_challenge_: "1 (200 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "10 ft., climb 10 ft., swim 80 ft."
_hit points_: "77 (9d10 + 27)"
_armor class_: "14"
_stats_: | 16 (+3) | 15 (+2) | 17 (+3) | 2 (-4) | 13 (+1) | 7 (-2) |

___Bred to the Saddle.___ Gerridae do not take any penalties to their movement or speed due to encumbrance or carrying a single rider.

___Waterborne.___ Any gerridae can run while on the surface of water, but not while on land or climbing. They treat stormy water as normal rather than difficult terrain. A gerridae takes one point of damage for every hour spent on dry land.

**Actions**

___Multiattack.___ The gerridae makes one bite attack and one claw attack.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) slashing damage.

