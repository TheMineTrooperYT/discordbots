"Witchlight";;;_size_: Tiny construct
_alignment_: neutral
_challenge_: "1/4 (50 XP)"
_languages_: "understands the language of its creator but can't speak"
_skills_: "Perception +3"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "poison, radiant"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "fly 50 ft."
_hit points_: "10 (4d4)"
_armor class_: "14"
_stats_: | 1 (-5) | 18 (+4) | 10 (+0) | 10 (+0) | 13 (+1) | 7 (-2) |

___Dispel Magic Weakness.___ Casting dispel magic on a witchlight paralyzes it for 1d10 rounds.

___Luminance.___ A witchlight normally glows as brightly as a torch. The creature can dim itself to the luminosity of a candle, but it cannot extinguish its light. Because of its glow, the witchlight has disadvantage on Dexterity (Stealth) checks.

___Thin As Light.___ While a witchlight is not incorporeal, it can pass through any opening that light can.

**Actions**

___Light Ray.___ Ranged Weapon Attack: +6 to hit, range 30 ft., one target. Hit: 6 (1d4 + 4) radiant damage.

___Flash (Recharge 5-6).___ The witchlight emits a bright burst of light that blinds all sighted creatures within 30 feet for 1d4 rounds unless they succeed on a DC 10 Constitution saving throw.

