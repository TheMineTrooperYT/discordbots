"Mimi";;;_size_: Tiny fey
_alignment_: neutral good
_challenge_: "2 (450 XP)"
_languages_: "Common, Sylvan"
_skills_: "Perception +3, Stealth +6 (+8 in snowy terrain)"
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "cold"
_speed_: "10 ft., fly 50 ft."
_hit points_: "9 (2d4 + 4)"
_armor class_: "14"
_stats_: | 4 (-3) | 19 (+4) | 14 (+2) | 15 (+2) | 13 (+1) | 14 (+2) |

___Group Casting (1/day).___ A group of three or more mimis together can
use _freezing sphere_.

___Magic Resistance.___ The mimi has advantage on saving throws against
spells and other magic effects.

___Innate Spellcasting.___ The mimi’s spellcasting ability is Charisma (spell
save DC 12, +4 to hit with spell attacks). It can innately cast the following
spells, requiring no material components:

* At will: _invisibility, ray of frost_

* 2/day: _frost fingers_ (as _burning hands_, but cold damage)

* 1/day: _cone of cold_

**Actions**

___Shortsword.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target.
Hit: 7 (1d6 + 4) piercing damage.

___Lower Temperature.___ The mimi lowers the temperature in an area within
30 feet of it that is no larger than a 15-foot cube. Creatures who enter or
begin their turn within the area must succeed on a DC 12 Constitution
saving throw or take 11 (2d8 + 2) cold damage. If a creature takes cold
damage, it has disadvantage on all attack rolls and ability checks for 1
minute.
