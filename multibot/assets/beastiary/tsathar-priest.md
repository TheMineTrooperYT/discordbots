"Tsathar Priest";;;_size_: Medium monstrosity
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Abyssal, Tsathar"
_skills_: "Nature +5, Perception +5, Religion +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_speed_: "30 ft., swim 30 ft."
_hit points_: "52 (8d8 + 16)"
_armor class_: "14 (frog hide armor)"
_stats_: | 10 (+0) | 14 (+2) | 14 (+2) | 12 (+1) | 17 (+3) | 11 (+0) |

___Amphibious.___ The tsathar can breathe air and water.

___Keen Smell.___ The tsathar has advantage on Wisdom (Perception) checks
that rely on smell.

___Slimy.___ Tsathar continuously cover themselves with muck and slime.
Creatures attempting to grapple a tsathar do so with disadvantage.

___Standing Leap.___ The tsathar’s long jump is up to 20 feet and its high
jump is up to 10 feet, with or without a running start.

___Fetid Shroud of the Frog God.___ The priest of Tsathogga is surrounded
by a fetid, swirling shroud of foul corruption. At the start of each of the
priest’s turns, each creature within 5 feet of it takes 7 (2d6) poison damage.
A creature that touches the priest or hits it with a melee attack while within
5 feet of it takes 7 (2d6) poison damage.

___Fetid Strike.___ Once on each of the priest’s turns when it hits a creature
with a weapon attack, it can cause the attack to deal an extra 9 (2d8)
poison damage to the target.

___Spellcasting.___ The tsathar priest is a 5th level spellcaster. Its spellcasting
ability is Wisdom (spell save DC 13, +5 to hit with spell attacks). It has
the following cleric spells prepared:

* Cantrips (at will): _guidance, poison spray, resistance, thaumaturgy_

* 1st level (4 slots): _bane, bless, cure wounds, detect magic, inflict wounds_

* 2nd level (3 slots): _enhance ability, hold person, silence_

* 3rd level (3 slots): _bestow curse, dispel magic, stinking cloud_

**Actions**

___Kukri.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5
(1d6 + 2) slashing damage.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 5 (1d6 + 2) piercing damage.

___Claws.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit:
6 (1d8 + 2) slashing damage, and the target must succeed on a DC 13
Constitution saving throw or become the living host to a tsathar egg,
which over the course of the egg maturing, migrates to the chest cavity of
the host. The host creature must make another DC 13 Constitution saving
throw after 24 hours of the egg having been implanted. A failed saving
throw results in the host becoming violently ill, followed by a deep comalike state that lasts 2d6 + 2 days. At the end of each day, the host can
attempt another saving throw with a success indicating that its body has
managed to destroy the egg through normal immune response. At the end
of the incubation period, the host awakes to excruciating pain as the young
tsathar, freed from its egg, tears its way out of the host, who is reduced to
0 hit points in the process.

A DC 16 Wisdom (Medicine) check can be attempted to surgically
extract an egg from the host. A lesser restoration spell will also cure the
condition and purge the host of the egg.
