"Kuo-Toa Whip";;;_size_: Medium humanoid (kuo-toa)
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "Undercommon"
_senses_: "darkvision 120 ft."
_skills_: "Perception +6, Religion +4"
_speed_: "32 ft., swim 30 ft."
_hit points_: "65 (10d8+20)"
_armor class_: "11 (natural armor)"
_stats_: | 14 (+2) | 10 (0) | 14 (+2) | 12 (+1) | 14 (+2) | 11 (0) |

___Amphibious.___ The kuo-toa can breathe air and water.

___Otherwordly Perception.___ The kuo-toa can sense the presence of any creature within 30 feet of it that is invisible or on the Ethereal Plane. It can pinpoint such a creature that is moving.

___Slippery.___ The kuo-toa has advantage on ability checks and saving throws made to escape a grapple.

___Sunlight Sensitivity.___ While in sunlight, the kuo-toa has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

___Spellcasting.___ The kuo-toa is a 2nd-level spellcaster. Its spellcasting ability is Wisdom (spell save DC 12, +4 to hit with spell attacks). The kuo-toa has the following cleric spells prepared:

* Cantrips (at will): _sacred flame, thaumaturgy_

* 1st level (3 slots): _bane, shield of faith_

**Actions**

___Multiattack.___ The kuo-toa makes two attacks: one with its bite and one with its pincer staff.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Pincer Staff.___ Melee Weapon Attack: +4 to hit, reach 10 ft., one target. Hit: 5 (1d6 + 2) piercing damage. If the target is a Medium or smaller creature, it is grappled (escape DC 14). Until this grapple ends, the kuo-toa can't use its pincer staff on another target.
