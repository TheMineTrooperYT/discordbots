"Chaos Blade";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "Any two languages"
_skills_: "Athletics +7, Deception +8, Perception +5, Persuasion +8"
_senses_: "passive Perception 11"
_saving_throws_: "Con +7, Cha +8"
_speed_: "30 ft."
_hit points_: "73 (6d6 + 4d10 + 30)"
_armor class_: "17 (breastplate)"
_stats_: | 10 (+0) | 16 (+3) | 16 (+3) | 9 (-1) | 13 (+1) | 18 (+4) |

___Chaotic Strikes.___ When the chaos blade rolls a natural
1 or a critical hit with a weapon attack, it can
immediately cast a spell with a casting time of 1
action as a bonus action.

___Improved Criticals.___ The chaos blade scores a critical
hit with its weapon attacks on a roll of 19 or 20.

___Infused Strikes.___ The chaos blades weapons are
infused with elemental energy and deal 2d6
additional cold, fire, or lightning damage on hit
(chosen at random and included in the attack).

___Spellcasting.___ The chaos blade is a 6th-level
spellcaster. Its spellcasting ability is charisma (spell
save DC 16, +8 to hit with spell attacks). The chaos
blade has the following sorcerer spells prepared:

* Cantrips (at will): _acid splash, blade ward, true strike_

* 1st level (4 slots): _burning hands, color spray, shield_

* 2nd level (3 slots): _blur, hold person, misty step_

* 3rd level (3 slots): _counterspell, dispel magic, haste_

___War Caster.___ The chaos blade has advantage on
Constitution saving throws made to maintain
concentration on spells and can cast spells while
wielding a weapon in each hand.

**Actions**

___Multiattack.___ The chaos blade makes two shortsword
attacks.

___Shortsword.___ Melee Weapon Attack: +7 to hit, reach
5 ft., one target. Hit: 6 (1d6 + 3) piercing damage
plus 7 (2d6) cold, fire, or lightning damage (chosen
randomly).

___Action Surge (1/Short Rest).___ The chaos blade makes
3 shortsword attacks.
