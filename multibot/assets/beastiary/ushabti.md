"Ushabti";;;_size_: Large construct
_alignment_: unaligned
_challenge_: "9 (5000 XP)"
_languages_: "Ancient language of DM's choice"
_skills_: "Arcana +4, History +4, Perception +8"
_senses_: "darkvision 60 ft., passive Perception 18"
_saving_throws_: "Dex +7, Cha +3"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft."
_hit points_: "105 (10d10 + 50)"
_armor class_: "18 (natural armor)"
_stats_: | 21 (+5) | 17 (+3) | 20 (+5) | 11 (+0) | 19 (+4) | 9 (-1) |

___Dynastic Aura.___ A creature that starts its turn within 15 feet of the ushabti must make a DC 17 Constitution saving throw, unless the ushabti is incapacitated. On a failed save, the creature has its breath stolen; it takes 9 (2d8) necrotic damage, and until the end of the ushabti's next turn, can't cast spells that require a verbal component or speak louder than a whisper. If a creature's saving throw is successful, the creature is immune to this ushabti's Dynastic Aura for the next 24 hours.

___Healing Leech.___ If a creature within 30 feet of the ushabti regains hit points from a spell or a magical effect, the creature gains only half the normal number of hit points and the ushabti gains the other half.

___Immutable Form.___ The ushabti is immune to any spell or effect that would alter its form.

___Magic Resistance.___ The ushabti has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ The ushabti's weapon attacks are magical.

**Actions**

___Multiattack.___ The ushabti makes one attack with Medjai's scepter and one with its khopesh.

___Medjai's Scepter.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) bludgeoning damage plus 10 (3d6) poison damage.

___Khopesh.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 12 (2d6 + 5) slashing damage.

