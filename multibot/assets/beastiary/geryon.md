"Geryon";;;_page_number_: 173
_size_: Huge fiend (devil)
_alignment_: lawful evil
_challenge_: "22 (41,000 XP)"
_languages_: "all, telepathy 120 ft."
_senses_: "truesight 120 ft., passive Perception 20"
_skills_: "Deception +13, Intimidation +13, Perception +10"
_damage_immunities_: "cold, fire, poison"
_saving_throws_: "Dex +10, Con +13, Wis +10, Cha +13"
_speed_: "30 ft., fly 50 ft."
_hit points_: "300  (24d12 + 144)"
_armor class_: "19 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, poisoned"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 29 (+9) | 17 (+3) | 22 (+6) | 19 (+4) | 16 (+3) | 23 (+6) |

___Innate Spellcasting.___ Geryon's innate spellcasting ability is Charisma (spell save DC 21). He can innately cast the following spells, requiring no material components:

* At will: _alter self _(can become Medium when changing his appearance)_, detect magic, geas, ice storm, invisibility _(self only)_, locate object, suggestion, wall of ice_

* 1/day each: _divine word, symbol _(pain only)

___Legendary Resistance (3/Day).___ If Geryon fails a saving throw, he can choose to succeed instead.

___Magic Resistance.___ Geryon has advantage on saving throws against spells and other magical effects.

___Magic Weapons.___ Geryon's weapon attacks are magical.

___Regeneration.___ Geryon regains 20 hit points at the start of his turn. If he takes radiant damage, this trait doesn't function at the start of his next turn. Geryon dies only if he starts his turn with 0 hit points and doesn't regenerate.

**Actions**

___Multiattack___ Geryon makes two attacks: one with his claws and one with his stinger.

___Claws___ Melee Weapon Attack: +16 to hit, reach 15 ft., one target. Hit: 23 (4d6 + 9) slashing damage. If the target is Large or smaller, it is grappled (DC 24) and is restrained until the grapple ends. Geryon can grapple one creature at a time. If the target is already grappled by Geryon, the target takes an extra 27 (6d8) slashing damage.

___Stinger___ Melee Weapon Attack: +16 to hit, reach 20 ft., one creature. Hit: 14 (2d4 + 9) piercing damage, and the target must succeed on a DC 21 Constitution saving throw or take 13 (2d12) poison damage and become poisoned until it finishes a short or long rest. The target's hit point maximum is reduced by an amount equal to half the poison damage it takes. If its hit point maximum drops to 0, it dies. This reduction lasts until the poisoned condition is removed.

___Teleport___ Geryon magically teleports, along with any equipment he is wearing and carrying, up to 120 feet to an unoccupied space he can see.

**Legendary** Actions

Geryon can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. Geryon regains spent legendary actions at the start of his turn.

___Infernal Glare___ Geryon targets one creature he can see within 60 feet of him. If the target can see Geryon, the target must succeed on a DC 23 Wisdom saving throw or become frightened of Geryon until the end of its next turn.

___Swift Sting (Costs 2 Actions)___ Geryon attacks with his stinger.

___Teleport___ Geryon uses his Teleport action.
