"Hsagrath";;;_size_: Small construct
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "--"
_senses_: "blindsight 60 ft., Passive Perception 10"
_saving_throws_: "Dex +4"
_damage_immunities_: "poison, psychic"
_condition_immunities_: "Blinded, Charmed, Deafened, Frightened, Paralyzed, Poisoned"
_speed_: "0 ft., fly 50 ft. (hover)"
_hit points_: "45 (10d8)"
_armor class_: "17 (natural armor)"
_stats_: | 17 (+3) | 12 (+1) | 11 (+0) | 9 (-1) | 5 (-3) | 1 (-5) |

___Antimagic Susceptibility.___ The hsagrath is incapacitated while in the
area of an antimagic field. If targeted by dispel magic, the hsagrath must
succeed on a Constitution saving throw against the caster’s spell save DC
or fall unconscious for 1 minute.

___False Appearance.___ While the hsagrath remains motionless and isn’t
flying, it is indistinguishable from a normal chain.

___Fiendish Humor.___ When a target reaches 0 hit points, a hsgarath will
release and seek a new target.

**Actions**

___Whip.___ Melee Weapon Attack: +6 to hit, reach 30 ft., all targets within
range. Hit: 7 (1d8+3) bludgeoning damage.

___Constrict.___ Melee Weapon Attack: +6 to hit, reach 30 ft., one target. Hit:
12 (2d8+3) bludgeoning damage and the target is grappled (Escape DC
16). A grappled opponent takes 12 (2d8+3) bludgeoning damage at the
beginning of its turn. The hsagrath can’t make any other attacks while it
has a creature grappled.
