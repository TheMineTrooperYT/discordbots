"Chalkydri Angel";;;_size_: Medium celestial
_alignment_: neutral good
_challenge_: "12 (8,400 XP)"
_languages_: "all, telepathy 120 ft."
_skills_: "Intimidation +8, Perception +8"
_senses_: "darkvision 120 ft., passive Perception 18"
_saving_throws_: "Con +9, Wis +8, Cha +8"
_damage_resistances_: "radiant; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, exhaustion, frightened"
_speed_: "40 ft., fly 90 ft."
_hit points_: "123 (13d8 + 65)"
_armor class_: "18 (natural armor)"
_stats_: | 23 (+6) | 16 (+3) | 20 (+5) | 18 (+4) | 18 (+4) | 19 (+4) |

___Angelic Weapons.___ The chalkydri’s weapon attacks are magical. When
the chalkydri hits with any weapon, the weapon deals an extra 4d8 radiant
damage (included in the attack).

___Divine Awareness.___ The chalkydri knows if it hears a lie.

___Innate Spellcasting.___ The chalkydri’s spellcasting ability is Charisma
(spell save DC 16, +8 to hit with spell attacks). It can innately cast the
following spells, requiring no material components:

* At will: _detect evil and good_

* 3/day: _dispel evil and good, flame strike_

* 1/day: _commune, raise dead_

___Magic Resistance.___ The chalkydri has advantage on saving throws
against spells and other magic effects.

**Actions**

___Multiattack.___ The chalkydri attacks twice with its glaive.

___Glaive.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit:
22 (3d10 + 6) slashing damage plus 18 (4d8) radiant damage.

___Javelin.___ Ranged Weapon Attack: +10 to hit, range 30/120 ft., target. Hit:
13 (2d6 + 6) piercing damage plus 18 (4d8) radiant damage.
