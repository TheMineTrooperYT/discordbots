"One-Eyed Shiver";;;_size_: Medium humanoid (human)
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Common"
_skills_: "Arcana +3, Perception +3, Intimidation +5"
_damage_immunities_: "cold"
_speed_: "30 ft."
_hit points_: "49 (9d8+9)"
_armor class_: "12 (15 with mage armor)"
_stats_: | 10 (0) | 14 (+2) | 12 (+1) | 13 (+1) | 13 (+1) | 17 (+3) |

___Chilling Mist.___ While it is alive, the one-eyed shiver projects an aura of cold mist within 10 feet of itself. If the one-eyed shiver deals damage to a creature in this area, the creature also takes 5 (1d10) cold damage.

___Spellcasting.___ The one-eyed shiver is a 5th-level spellcaster. Its spellcasting ability is Charisma (spell save DC 13, +5 to hit with spell attacks). It knows the following sorcerer spells:

* Cantrips (at will): _chill touch, mage hand_

* 1st level (4 slots): _fog cloud, mage armor, thunderwave_

* 2nd level (3 slots): _mirror image, misty step_

* 3rd level (2 slots): _fear_

**Actions**

___Dagger.___ Melee Weapon Attack: +4 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.

___Eye of Frost.___ The one-eyed shiver casts ray of frost from its missing eye. If it hits, the target is also restrained. A target restrained in this way can end the condition by using an action, succeeding on a DC 13 Strength check.
