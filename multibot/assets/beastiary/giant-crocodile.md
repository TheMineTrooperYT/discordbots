"Giant Crocodile";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "5 (1,800 XP)"
_skills_: "Stealth +5"
_speed_: "30 ft., swim 50 ft."
_hit points_: "85 (9d12+27)"
_armor class_: "14 (natural armor)"
_stats_: | 21 (+5) | 9 (-1) | 17 (+3) | 2 (-4) | 10 (0) | 7 (-2) |

___Hold Breath.___ The crocodile can hold its breath for 30 minutes.

**Actions**

___Multiattack.___ The crocodile makes two attacks: one with its bite and one with its tail.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 21 (3d10 + 5) piercing damage, and the target is grappled (escape DC 16). Until this grapple ends, the target is restrained, and the crocodile can't bite another target.

___Tail.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target not grappled by the crocodile. Hit: 14 (2d8 + 5) bludgeoning damage. If the target is a creature, it must succeed on a DC 16 Strength saving throw or be knocked prone.