"Pteranodon";;;_size_: Medium beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_skills_: "Perception +1"
_speed_: "10 ft., fly 60 ft."
_hit points_: "13 (3d8)"
_armor class_: "13 (natural armor)"
_stats_: | 12 (+1) | 15 (+2) | 10 (0) | 2 (-4) | 9 (-1) | 5 (-3) |

___Flyby.___ The pteranodon doesn't provoke opportunity attacks when it flies out of an enemy's reach.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 6 (2d4 + 1) piercing damage