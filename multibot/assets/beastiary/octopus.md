"Octopus";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "0 (10 XP)"
_senses_: "darkvision 30 ft."
_skills_: "Perception +2, Stealth +4"
_speed_: "5 ft., swim 30 ft."
_hit points_: "3 (1d6)"
_armor class_: "12"
_stats_: | 4 (-3) | 15 (+2) | 11 (0) | 3 (-4) | 10 (0) | 4 (-3) |

___Hold Breath.___ While out of water, the octopus can hold its breath for 30 minutes.

___Underwater Camouflage.___ The octopus has advantage on Dexterity (Stealth) checks made while underwater.

___Water Breathing.___ The octopus can breathe only underwater.

**Actions**

___Tentacles.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 1 bludgeoning damage, and the target is grappled (escape DC 10). Until this grapple ends, the octopus can't use its tentacles on another target.

___Ink Cloud (Recharges after a Short or Long Rest).___ A 5-foot-radius cloud of ink extends all around the octopus if it is underwater. The area is heavily obscured for 1 minute, although a significant current can disperse the ink. After releasing the ink, the octopus can use the Dash action as a bonus action.