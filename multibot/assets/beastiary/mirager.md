"Mirager";;;_size_: Medium fey
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "Common, Sylvan"
_skills_: "Deception +7, Performance +9, Perception +4"
_senses_: "darkvision 60 ft., passive Perception 14"
_speed_: "30 ft."
_hit points_: "78 (12d8 + 24)"
_armor class_: "13"
_stats_: | 12 (+1) | 16 (+3) | 14 (+2) | 10 (+0) | 14 (+2) | 20 (+5) |

___Shapechanger.___ The mirager can use its action to polymorph into a Small or Medium humanoid, or back into its true form. Its statistics, other than its size, are the same in each form. Any equipment it is wearing or carrying isn't transformed. It reverts to its true form if it dies.

___Innate Spellcasting.___ The mirager's innate spellcasting ability is Charisma (spell save DC 15). It can innately cast the following spells, requiring no material components:

* 3/day: _charm person_

* 1/day each: _hallucinatory terrain, suggestion_

___Enthralling Mirage.___ When the mirager casts _hallucinatory terrain_, the area appears so lush and inviting that those who view it feel compelled to visit. Any creature that approaches within 120 feet of the terrain must make a DC 15 Wisdom saving throw. Those that fail are affected as by the _enthrall_ spell with the mirager as the caster; they give the mirage their undivided attention, wanting only to explore it, marvel at its beauty, and rest there for an hour. The mirager can choose to have creatures focus their attention on itself instead of the _hallucinatory terrain_. Creatures affected by the _enthrall_ effect automatically fail saving throws to disbelieve the _hallucinatory terrain_. This effect ends if the _hallucinatory terrain_ is dispelled.

**Actions**

___Multiattack..___ The mirager makes two slam attacks.

___Slam.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) bludgeoning damage.

___Thirst Kiss.___ Melee Weapon Attack: reach 5 ft., one charmed, grappled, or incapacitated target. The mirager feeds on the body moisture of creatures it lures into kissing it. A kiss hits automatically, does 21 (6d6) necrotic damage, and fills the mirager with an exultant rush of euphoria that has the same effect as a _heroism_ spell lasting 1 minute. The creature that was kissed doesn't notice that it took damage from the kiss unless it makes a successful DC 16 Wisdom (Perception) check.

___Captivating Dance (Recharges after a Short or Long Rest, Humanoid Form Only).___ The mirager performs a sinuously swaying dance. Humanoids within 20 feet that view this dance must make a successful DC 16 Wisdom saving throw or be stunned for 1d4 rounds and charmed by the mirager for 1 minute. Humanoids of all races and genders have disadvantage on this saving throw. A creature that saves successfully is immune to this mirager's dance for the next 24 hours.

