"Glabrezu";;;_size_: Large fiend (demon)
_alignment_: chaotic evil
_challenge_: "9 (5,000 XP)"
_languages_: "Abyssal, telepathy 120 ft."
_senses_: "truesight 120 ft."
_damage_immunities_: "poison"
_saving_throws_: "Str +9, Con +9, Wis +7, Cha +7"
_speed_: "40 ft."
_hit points_: "157 (15d10+75)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "poisoned"
_damage_resistances_: "cold, fire, lightning, bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 20 (+5) | 15 (+2) | 21 (+5) | 19 (+4) | 17 (+3) | 16 (+3) |

___Innate Spellcasting.___ The glabrezu's spellcasting ability is Intelligence (spell save DC 16). The glabrezu can innately cast the following spells, requiring no material components:

* At will: _darkness, detect magic, dispel magic_

* 1/day each: _confusion, fly, power word stun_

___Magic Resistance.___ The glabrezu has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ The glabrezu makes four attacks: two with its pincers and two with its fists. Alternatively, it makes two attacks with its pincers and casts one spell.

___Pincer.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 16 (2d10 + 5) bludgeoning damage. If the target is a Medium or smaller creature, it is grappled (escape DC 15). The glabrezu has two pincers, each of which can grapple only one target.

___Fist.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 7 (2d4 + 2) bludgeoning damage.

___Variant: Summon Demon (1/Day).___ The demon chooses what to summon and attempts a magical summoning.

A glabrezu has a 30 percent chance of summoning 1d3 vrocks, 1d2 hezrous, or one glabrezu.

A summoned demon appears in an unoccupied space within 60 feet of its summoner, acts as an ally of its summoner, and can't summon other demons. It remains for 1 minute, until it or its summoner dies, or until its summoner dismisses it as an action.
