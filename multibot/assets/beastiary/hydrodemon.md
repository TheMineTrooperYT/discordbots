"Hydrodemon";;;_size_: Large fiend (demon)
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "Abyssal"
_skills_: "Perception +6"
_senses_: "darkvision 60 ft., passive Perception 16"
_saving_throws_: "Dex +5, Con +7"
_damage_immunities_: "poison"
_damage_resistances_: "cold, fire, lightning"
_condition_immunities_: "poisoned"
_speed_: "30 ft., fly 40 ft., swim 60 ft."
_hit points_: "85 (9d10 + 36)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 14 (+2) | 18 (+4) | 8 (-1) | 11 (+0) | 14 (+2) |

___Amphibious.___ The hydrodemon can breathe air and water.

___Magic Resistance.___ The demon has advantage on saving throws against
spells and other magical effects.

___Magic Weapons.___ The demon’s weapon attacks are magical.

___Innate Spellcasting.___ The hydrodemon’s spellcasting ability is Charisma
(spell save DC 13, +5 to hit with spell attacks). It can innately cast the
following spells, requiring no material components:

* At will: _darkness, detect magic, water walk_

* 2/day each: _dimension door, teleport_

* 1/day each: _hallow_

**Actions**

___Multiattack.___ The hydrodemon makes three attacks: one with its bite
and two with its claws. The demon can use its Sleep Spittle instead of
using its bite.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13
(2d8 + 4) piercing damage, and the target is grappled (escape DC 14).
Until this grapple ends, the target is restrained and the demon can’t bite
another target.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 10 ft., one target. Hit: 11
(2d6 + 4) slashing damage.

___Sleep Spittle.___ One target within 60 ft. must succeed on a DC 15 Wisdom
saving throw or fall unconscious for 1 minute. The sleeping target can be
awakened if someone uses an action to shake or slap the sleeper awake,
and the target will wake if it takes damage.

___Summon (1/day).___ The demon chooses what to summon and attempts a
magical summoning.
A hydrodemon has a 30% chance of summoning one hydrodemon.
A summoned demon appears in an unoccupied space within 60 feet of
its summoner, acts as an ally of its summoner, and can’t summon other
demons. It remains for 1 minute, until it or its summoner dies, or until its
summoner dismisses it as an action.
