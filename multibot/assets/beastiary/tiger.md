"Tiger";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "1 (200 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +3, Stealth +6"
_speed_: "40 ft."
_hit points_: "37 (5d10+10)"
_armor class_: "12"
_stats_: | 17 (+3) | 15 (+2) | 14 (+2) | 3 (-4) | 12 (+1) | 8 (-1) |

___Keen Smell.___ The tiger has advantage on Wisdom (Perception) checks that rely on smell.

___Pounce.___ If the tiger moves at least 20 ft. straight toward a creature and then hits it with a claw attack on the same turn, that target must succeed on a DC 13 Strength saving throw or be knocked prone. If the target is prone, the tiger can make one bite attack against it as a bonus action.

**Actions**

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 8 (1d10 + 3) piercing damage.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage.