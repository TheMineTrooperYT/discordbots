"Gnoll Berserker";;;_size_: Medium humanoid
_alignment_: chaotic evil
_challenge_: "3 (700 XP)"
_languages_: "Gnoll"
_saving_throws_: "Str +5, Con +4"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "40 ft."
_hit points_: "65 (10d8 + 20)"
_armor class_: "15 (bone and hide)"
_stats_: | 17 (+3) | 14 (+2) | 15 (+2) | 8 (-1) | 11 (+0) | 9 (-1) |

___Rage (Recharges after a Short or Long Rest).___ As a bonus action, the gnoll can enter a rage at the start of its turn. The rage lasts for 1 minute or until the gnoll is incapacitated. While raging, the gnoll gains the following benefits:
* The gnoll has advantage on Strength checks and Strength saving throws.
* When it makes a melee weapon attack, the gnoll gains a +2 bonus to the damage roll.
* The gnoll has resistance to bludgeoning, piercing, and slashing damage.
* The gnoll gains an additional action on each of its turns. That action can be used only to make one weapon attack or move up to its speed towards a hostile target it can see.

___Rampage.___ When the gnoll reduces a creature to 0 hit points with a melee attack on its turn, the gnoll can take a bonus action to move up to half its speed and make a bite attack.

**Actions**

___Multiattack.___ The gnoll makes three attacks: one with its bite and two with its boneclaws.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 5 (1d4 + 3) piercing damage, or 7 (1d4 + 5) piercing damage while raging.

___Boneclaws.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 7 (1d8 + 3) slashing damage, or 9 (1d8 + 5) slashing damage while raging.
