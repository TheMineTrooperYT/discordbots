"Githzerai Zerth";;;_size_: Medium humanoid (gith)
_alignment_: lawful neutral
_challenge_: "6 (2,300 XP)"
_languages_: "Gith"
_skills_: "Arcana +6, Insight +6, Perception +6"
_saving_throws_: "Str +4, Dex +7, Int +6, Wis +6"
_speed_: "30 ft."
_hit points_: "84 (13d8+26)"
_armor class_: "17"
_stats_: | 13 (+1) | 18 (+4) | 15 (+2) | 16 (+3) | 17 (+3) | 12 (+1) |

___Innate Spellcasting (Psionics).___ The githzerai's innate spellcasting ability is Wisdom. It can innately cast the following spells, requiring no components:

* At will: _mage hand _(the hand is invisible)

* 3/day each: _feather fall, jump, see invisibility, shield_

___Psychic Defense.___ While the githzerai is wearing no armor and wielding no shield, its AC includes its Wisdom modifier.

**Actions**

___Multiattack.___ The githzerai makes two unarmed strikes.

___Unarmed Strike.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11 (2d6 + 4) bludgeoning damage plus 13 (3d8) psychic damage. This is a magic weapon attack.
