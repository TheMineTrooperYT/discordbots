"Maegera the Dawn Titan";;;_size_: Gargantuan elemental
_alignment_: chaotic evil
_challenge_: "23 (50,000 XP)"
_languages_: "Ignan"
_senses_: "blindsight 120 ft."
_damage_immunities_: "fire, poison"
_saving_throws_: "Con +12, Wis +7, Cha +11"
_speed_: "50 ft.."
_hit points_: "341 (22d20+110)"
_armor class_: "16"
_condition_immunities_: "charmed, frightened, grappled, paralyzed, petrified, poisoned, prone, restrained"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_stats_: | 21 (+5) | 22 (+6) | 20 (+5) | 10 (0) | 10 (0) | 19 (+4) |

___Empowered Attacks.___ Maegera's slam attacks are treated as magical for the purpose of overcoming resistance and immunity to damage from nonmagical attacks.

___Fire Aura.___ At the start of each of Maegera's turns, each creature within 30 feet of it takes 35 (10d6) fire damage, and flammable objects in the aura that aren't being worn or carried ignite. A creature also takes 35 (10d6) fire damage from touching Maegera or from hitting it with a melee attack while within 10 feet of it, and a creature takes that damage the first time on a turn that Maegera moves into its space. Nonmagical weapons that hit Maegera are destroyed by fire immediately after dealing damage to it.

___Fire Form.___ Maegera can enter a hostile creature's space and stop there. It can move through a space as narrow as 1 inch wide without squeezing if fire could pass through that space.

___Illumination.___ Maegera sheds bright light in a 120-foot radius and dim light in an additional 120 ft..

___Innate Spellcasting.___ Maegera's can innately cast fireball (spell save DC 19) at will, requiring no material components. Maegera's spell casting ability is Charisma.

___Magic Resistance.___ Maegera has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack.___ Maegera makes three slam attacks.

___Slam.___ Melee Weapon Attack: +12 to hit, reach 15 ft., one target. Hit: 15 (3d6 + 5) bludgeoning damage plus 35 (10d6) fire damage

**Legendary** Actions

Maegera the Dawn Titan can take 3 legendary actions, choosing from the options below. Only one legendary action option can be used at a time, and only at the end of another creature's turn. Maegera regains spent legendary actions at the start of its turn.

___Quench Magic.___ Maegera targets one creature that it can see within 60 feet of it. Any resistence or immunity to fire damage that the target gains from a spell or magic item is suppressed.  The effect lasts until the end of Maegera's next turn.

___Smoke Cloud (Costs 2 Actions).___ Maegera exhales a billowing cloud of hot smoke and embers that fills a 60 foot cube. Each creature in the area takes 11 (2d10) fire damage. The cloud lasts until the end of Maegera's next turn. Creatures completely in the cloud are blinded and can't be seen.

___Create Fire Elemental (Costs 3 Actions).___ Maegera's hit points are reduced by 50 as part of it separates and becomes a fire elemental with 102 hit points. The fire element appears in an unoccupied space within 15 feet of Maegera and acts on Maegera's initiative count. Maegera can't use this action if it has 50 hit points or fewer. The fire elemental obeys Maegera's commands and fights until destroyed.
