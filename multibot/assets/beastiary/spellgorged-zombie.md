"Spellgorged Zombie";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "1 (200 XP)"
_languages_: "understands the languages it knew in life but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 8"
_saving_throws_: "Wis +0"
_damage_immunities_: "poison"
_condition_immunities_: "poisoned"
_speed_: "20 ft."
_hit points_: "75 (10d8 + 30)"
_armor class_: "10"
_stats_: | 13 (+1) | 10 (+0) | 16 (+3) | 3 (-4) | 6 (-2) | 5 (-3) |

___Undead Fortitude.___ If damage reduces the zombie to 0 hit points, it
makes a Constitution saving throw with a DC of 5+ the damage taken,
unless the damage is radiant or from a critical hit. On a success, the
zombie drops to 1 hit point instead.

___Spell Storing.___ The zombie can store any spells cast into its mouth as if
it were a ring of spell storing. The zombie can store up to 5 levels worth
of spells at a time. The spells stored in the zombie uses the slot level, spell
save DC, spell attack bonus, and the spellcasting ability of the original
caster. Once the spell is released by the zombie it is no longer stored in it,
freeing up space for additional spells.

**Actions**

___Slam.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 4
(1d6 + 1) bludgeoning damage.
