"Anubian";;;_size_: Medium elemental
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Primordial"
_skills_: "Stealth +5"
_senses_: "darkvision 60 ft., tremorsense 30 ft., passive Perception 11"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "30 ft."
_hit points_: "44 (8d8 + 8)"
_armor class_: "13"
_stats_: | 12 (+1) | 16 (+3) | 12 (+1) | 10 (+0) | 12 (+1) | 10 (+0) |

___Sand Stealth.___ The anubian gains an additional +2 (+7 in total) to Stealth in sand terrain

___Sand Step.___ Instead of moving, the anubian's humanoid form collapses into loose sand and immediately reforms at another unoccupied space within 10 feet. This movement doesn't provoke opportunity attacks. After using this trait in sand terrain, the anubian can Hide as part of this movement even if under direct observation. Anubians can sand step under doors or through similar obstacles, provided there's a gap large enough for sand to sift through.

___Vulnerability to Water.___ For every 5 feet the anubian moves while touching water or for every gallon of water splashed on it, it takes 2 (1d4) cold damage. An anubian completely immersed in water takes 10 (4d4) cold damage at the start of its turn.

**Actions**

___Multiattack.___ The anubian makes two claw attacks.

___Claw.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one target. Hit: 6 (1d6 + 3) slashing damage.

___Haboob (1/Day).___ The anubian creates a sandstorm in a cylinder 30-feet high, that reaches to within 5 feet of it. The storm moves with the anubian. The area is heavily obscured, and each creature other than an anubian that enters the sandstorm or ends its turn there must make a successful DC 13 Strength saving throw or be restrained by it. Also, each creature other than an anubian that ends its turn inside the sandstorm takes 3 (1d6) slashing damage. The anubian can maintain the haboob for up to 10 minutes as if concentrating on a spell. While maintaining the haboob, the anubian's speed is reduced to 5 feet and it can't sand step. Creatures restrained by the sandstorm move with the anubian. A creature can free itself or an adjacent creature from the sandstorm by using its action and making a DC 13 Strength check. A successful check ends the restraint on the target creature.

