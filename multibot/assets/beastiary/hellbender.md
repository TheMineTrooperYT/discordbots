"Hellbender";;;_size_: Large aberration
_alignment_: chaotic evil
_challenge_: "5 (1,800 XP)"
_languages_: "Hellbender"
_senses_: "darkvision 60 ft., passive Perception 9"
_damage_immunities_: "fire"
_damage_resistances_: "acid, poison"
_speed_: "20 ft."
_hit points_: "76 (8d10 + 32)"
_armor class_: "18 (natural armor)"
_stats_: | 20 (+5) | 12 (+1) | 19 (+4) | 5 (-3) | 9 (-1) | 5 (-3) |

**Actions**

___Multiattack.___ The hellbender makes two attacks with its claws.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 10
(1d10+5) slashing damage plus 4 (1d8) fire damage.

___Fiery Pulse (Recharge 5–6).___ The hellbender sends a burst of fire from
all small pores in its chitin-like hide. Any creature within 5 feet of the
hellbender must make a DC 15 Dexterity saving throw, taking 24 (7d6)
fire damage on a failed save, or half as much damage on a successful one.
