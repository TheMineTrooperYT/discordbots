"Giant Rhinoceros Beetle";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "7 (2,900 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "20 ft."
_hit points_: "114 (12d10 + 48)"
_armor class_: "15 (natural armor)"
_stats_: | 23 (+6) | 10 (+0) | 18 (+4) | 1 (-5) | 10 (+0) | 9 (-1) |

___Charge.___ If the giant rhinoceros beetle moves at least 20 feet straight toward a creature and then hits it with a gore attack on the same turn, that target must succeed on a DC 16 Strength saving throw or be knocked prone. If the target is prone, the giant rhinoceros beetle can make one slam attack against it as a bonus action.

**Actions**

___Multiattack.___ The giant rhinoceros beetle makes three attacks: one bite, one gore, and one slam.

___Bite.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 15 (2d8 + 6) piercing damage.

___Gore.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 17 (2d10 + 6) piercing damage.

___Slam.___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) bludgeoning damage.
