"Deathlock Wight (B)";;;_page_number_: 129
_size_: Medium undead
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "the languages it knew in life"
_senses_: "darkvision 60 ft., passive Perception 14"
_skills_: "Arcana +3, Perception +4"
_damage_immunities_: "poison"
_saving_throws_: "Wis +4"
_speed_: "30 ft."
_hit points_: "37  (5d8 + 15)"
_armor class_: "12 (15 with mage armor)"
_condition_immunities_: "exhaustion, poisoned"
_damage_resistances_: "necrotic; bludgeoning, piercing, and slashing from nonmagical attacks"
_stats_: | 11 (0) | 14 (+2) | 16 (+3) | 12 (+1) | 14 (+2) | 16 (+3) |

___Innate Spellcasting.___ The wight's innate spellcasting ability is Charisma (spell save DC 13). It can innately cast the following spells, requiring no verbal or material components:

* At will: _detect magic, disguise self, mage armor_

* 1/day each: _fear, hold person, misty step_

___Sunlight Sensitivity.___ While in sunlight, the wight has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack___ The wight attacks twice with Grave Bolt.

___Grave Bolt___ Ranged Spell Attack: +5 to hit, range 120 ft., one target. Hit: 7 (1d8 + 3) necrotic damage.

___Life Drain___ Melee Weapon Attack: +4 to hit, reach 5 ft., one creature. Hit: 9 (2d6 + 2) necrotic damage. The target must succeed on a DC 13 Constitution saving throw or its hit point maximum is reduced by an amount equal to the damage taken. This reduction lasts until the target finishes a long rest. The target dies if this effect reduces its hit point maximum to 0.

A humanoid slain by this attack rises 24 hours later as a zombie under the wight's control, unless the humanoid is restored to life or its body is destroyed. The wight can have no more than twelve zombies under its control at one time.
