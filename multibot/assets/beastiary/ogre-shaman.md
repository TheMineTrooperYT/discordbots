"Ogre Shaman";;;_size_: Large giant
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Common, Giant"
_senses_: "darkvision 60 ft., passive Perception 9"
_speed_: "40 ft."
_hit points_: "59 (7d10 + 21)"
_armor class_: "9"
_stats_: | 16 (+3) | 8 (-1) | 16 (+3) | 5 (-3) | 8 (-1) | 14 (+2) |

___Thick Hide.___ The ogre has advantage on saving
throws against spells and other magical effects.

___Innate Spellcasting.___ The ogre’s innate spellcasting
ability is Charisma (spell save DC 12). The ogre can
innately cast the following spells, requiring no
material components:

* At will: _light, mold earth, create bonfire_

* 1/day each: _earth tremor, enhance ability, fog cloud, color spray_

**Actions**

___Club.___ Melee Weapon Attack: +5 to hit, reach 5ft.,
one target. Hit: 10 (2d6 + 3) bludgeoning damage.
