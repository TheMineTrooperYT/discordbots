"Gravity Elemental";;;_size_: Large elemental
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "--"
_skills_: "Perception +3, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 13"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "0 ft., fly 40 ft."
_hit points_: "95 (10d10 + 40)"
_armor class_: "14 (natural armor)"
_stats_: | 10 (+0) | 10 (+0) | 18 (+4) | 4 (-3) | 11 (+0) | 11 (+0) |

___Distortion.___ The gravity elemental’s Armor Class includes its
Constitution modifier. In addition, the gravity elemental can hide when
only lightly obscured by bending light around it.

___Singularity.___ The gravity elemental is immune to spells and effects that
would push, pull, or move it.

**Actions**

___Gravity Field.___ The gravity elemental manipulates the very forces of
nature and chooses one of the effects below.

1. ___Lift.___ The gravity elemental uses an action to manipulate the surrounding
gravity to lift up to two objects weighing no more than 150 pounds that
are within 20 feet of it. It can then use a bonus action to hurl both objects
at a creature it can detect within 20 feet. The targets must make a DC
15 Dexterity saving throw, taking 17 (3d8 + 4) bludgeoning damage on
a failed saving throw, or half as much damage on a success. A gravity
elemental can drop an object it is lifting without taking an action.

2. ___Hold.___ The gravity elemental increases local gravity. Creatures within 20
feet of the elemental must succeed on a DC 15 Strength saving throw or be
restrained until the beginning of the elemental’s next turn.

3. ___Crush (1/short or long rest).___ The gravity elemental greatly increases local gravity
within a 20-foot radius of itself. All creatures, other than the elemental,
within that area make a DC 15 Constitution saving throw, taking 48 (8d10 + 4) force damage on a failed saving throw, or half as much damage on a success.

___Engulf.___ The gravity elemental attempts to engulf one target it can see
within 5 feet of it that is at least one size smaller than it. That target makes
a DC 15 Dexterity saving throw. On a successful saving throw, the target is
pushed 5 feet in a random direction. On a failed save, the gravity elemental
enters that target’s space, and the target is grappled and restrained. While
restrained, a target has total cover from attacks coming from outside the
elemental, and at the beginning of each of the elemental’s turns, a target
takes 26 (4d10 + 4) force damage. If this damage drops a target to 0 hit
points, it is rendered down to its constituent atoms and cannot be brought
back to life by any magic short of true resurrection or wish.

A grappled target can attempt to break free with a DC 15 Strength
saving throw at the end of each of its turns, ending the effect on a success
and moving 5 feet in a random direction.

**Reactions**

___Deflection.___ The gravity elemental uses its reaction to deflect missile
weapons that are at least one size smaller than it when it is hit with a
ranged weapon attack. The damage the elemental takes from the attack
is reduced by 10 (1d6 + 7). If the damage is reduced to 0, the gravity
elemental can destroy the missile entirely, or cause the missile to orbit
around it, and the elemental can make a ranged attack with the weapon
or piece of ammunition it just caught, as part of the same reaction (+7 to hit, range 20/60 ft.).
