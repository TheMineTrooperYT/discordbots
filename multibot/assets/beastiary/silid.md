"Silid";;;_size_: Small humanoid
_alignment_: chaotic evil
_challenge_: "1 (200 XP)"
_languages_: "Goblin, Undercommon"
_skills_: "Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 9"
_speed_: "30 ft."
_hit points_: "16 (3d6 + 6)"
_armor class_: "13 (natural armor)"
_stats_: | 13 (+1) | 15 (+2) | 14 (+2) | 10 (+0) | 9 (-1) | 9 (-1) |

___Innate Spellcasting.___ The silid’s innate spellcasting ability is Charisma
(spell save DC 9, +1 to hit with spell attacks). It can innately cast _blur_ once per
day, requiring no material components.

___Surprise Attack.___ If the silid surprises a creature and hits it with an
attack during the first round of combat, the target takes an extra 10 (3d6)
damage from the attack.

**Actions**

___Shortsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target.
Hit: 5 (1d6 + 2) piercing damage.

___Spear.___ Melee or Ranged Weapon Attack: +3 to hit, reach 5 ft. or range
20/60 ft., one target. Hit: 4 (1d6 + 1) piercing damage.
