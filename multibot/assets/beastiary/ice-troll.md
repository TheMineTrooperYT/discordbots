"Ice Troll";;;_size_: Large giant
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Giant"
_skills_: "Perception +2"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_vulnerabilities_: "fire, slashing"
_damage_immunities_: "cold"
_speed_: "30 ft."
_hit points_: "68 (8d10 + 24)"
_armor class_: "12 (natural armor)"
_stats_: | 19 (+4) | 12 (+1) | 16 (+3) | 9 (-1) | 10 (+0) | 6 (-2) |

___Slashing Susceptibility.___ If an attack made with a slashing weapon
scores a critical hit on the ice troll, the troll must succeed on a DC 13
Constitution saving throw or lose a limb. Roll a d6. On a roll of 1–3,
the troll loses an arm, and on a roll of 4–6 the troll loses a leg. It is GM’s
choice as to whether it was the right or left arm or leg.

___Regeneration.___ The ice troll regains 10 hit points at the start of its turn.
If the troll is not making physical contact with ice or near-freezing water,
this trait doesn’t function at the start of the troll’s next turn. The cave dies
only if starts its turn with 0 hit points and doesn’t regenerate.

**Actions**

___Multiattack.___ The cave troll makes three attacks: one with its bite and
two with its claws.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 7 (1d6 + 4) piercing damage plus 7 (2d6) cold damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8
(1d8 + 4) slashing damage.
