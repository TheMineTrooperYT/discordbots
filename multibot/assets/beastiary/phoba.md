"Phoba";;;_size_: Medium monstrosity
_alignment_: lawful evil
_challenge_: "7 (2,900 XP)"
_languages_: "Common"
_senses_: "Darkvision 60 ft., Passive Perception 14"
_skills_: "Deception +5, Insight +4, Intimidation +5, Investigation +7, Perception +4, Persuasion +5, Stealth +8, Thieves’ Tools +5"
_saving_throws_: "Dex +5, Int +4"
_speed_: "30 ft., climb 30 ft."
_hit points_: "127 (17d8 + 51)"
_armor class_: "15 (natural armor)"
_stats_: | 10 (+0) | 15 (+2) | 16 (+3) | 12 (+1) | 13 (+1) | 15 (+2) |

___Cunning Action.___ On each of its turns, the thief can
use a bonus action to take the Dash, Disengage,
Hide, or Use an Object action, make a Dexterity
(Sleight of Hand) check, or use thieves’ tools to
disarm a trap or open a lock.

___Petrifying Gaze.___ When a creature that can see Phoba’s
eyes starts its turn within 30 feet of her, she can
force it to make a DC 14 Constitution saving throw
if she isn’t incapacitated and can see the creature. If
the saving throw fails by 5 or more, the creature is
instantly petrified. Otherwise, a creature that fails
the save begins to turn to stone and is restrained.
The restrained creature must repeat the saving
throw at the end of its next turn, becoming petrified
on a failure or ending the effect on a success. The
petrification lasts until the creature is freed by the
greater restoration spell or other magic.

Unless surprised, a creature can avert its eyes to avoid
the saving throw at the start of its turn. If the creature
does so, it can’t see Phoba until the start of its next turn,
when it can avert its eyes again. If the creature looks at
Phoba in the meantime, it must immediately make the
save. If Phoba sees herself reflected on a polished surface
within 30 feet of her and in an area of bright light,
she is, due to her curse, affected by her own gaze.

___Sneak Attack (1/turn).___ Phoba deals an extra 7
(2d6) damage when she hits a target with a weapon
attack and has advantage on the attack roll, or
when the target is within 5 feet of an ally of her
that isn’t incapacitated and she doesn’t have disadvantage
on the attack roll.

___Special Equipment.___ Phoba wears a _ring of invisibility_
and carries a _portable hole_.

**Actions**

___Multiattack.___ Phoba makes either three melee attacks
— one with her snake hair and two with her shortsword
— or two ranged attacks with her longbow.

___Snake Hair.___ Melee Weapon Attack: +5 to hit, reach
5 ft., one creature. Hit: 4 (1d4 + 2) piercing damage
plus 14 (4d6) poison damage.

___Shortsword.___ Melee Weapon Attack: +5 to hit, reach
5 ft., one target. Hit: 5 (1d6 +2) piercing damage.

___Longbow.___ Ranged Weapon Attack: +5 to hit, range
150/600 ft., one target. Hit: 6 (1d8 +2) piercing
damage plus 7 (2d6) poison damage.
