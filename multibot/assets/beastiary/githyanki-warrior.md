"Githyanki Warrior";;;_page_number_: 160
_size_: Medium humanoid (gith)
_alignment_: lawful evil
_challenge_: "3 (700 XP)"
_languages_: "Gith"
_senses_: "passive Perception 11"
_saving_throws_: "Con +3, Int +3, Wis +3"
_speed_: "30 ft."
_hit points_: "49 (9d8 + 9)"
_armor class_: "17 (half plate)"
_stats_: | 15 (+2) | 14 (+2) | 12 (+1) | 13 (+1) | 13 (+1) | 10 (0) |

___Innate Spellcasting (Psionics).___ The githyanki's innate spellcasting ability is Intelligence. It can innately cast the following spells, requiring no components:

* At will: _mage hand _(the hand is invisible)

* 3/day each: _jump, misty step, nondetection _(self only)

**Actions**

___Multiattack.___ The githyanki makes two greatsword attacks.

___Greatsword.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 9 (2d6 + 2) slashing damage plus 7 (2d6) psychic damage.
