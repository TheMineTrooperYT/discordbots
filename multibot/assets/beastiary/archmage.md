"Archmage";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "12 (8,400 XP)"
_languages_: "any six languages"
_skills_: "Arcana +13, History +13"
_saving_throws_: "Int +9, Wis +6"
_speed_: "30 ft."
_hit points_: "99 (18d8+18)"
_armor class_: "12 (15 with mage armor)"
_damage_resistances_: "damage from spells; non magical bludgeoning, piercing, and slashing (from stoneskin)"
_stats_: | 10 (0) | 14 (+2) | 12 (+1) | 20 (+5) | 15 (+2) | 16 (+3) |

___Magic Resistance.___ The archmage has advantage on saving throws against spells and other magical effects.

___Spellcasting.___ The archmage is an 18th-level spellcaster. Its spellcasting ability is Intelligence (spell save DC 17, +9 to hit with spell attacks). The archmage can cast disguise self and invisibility at will and has the following wizard spells prepared:

* Cantrips (at will): _fire bolt, light, mage hand, prestidigitation, shocking grasp_

* 1st level (4 slots): _detect magic, identify, mage armor* , magic missile_

* 2nd level (3 slots): _detect thoughts, mirror image, misty step_

* 3rd level (3 slots): _counterspell,fly, lightning bolt_

* 4th level (3 slots): _banishment, fire shield, stoneskin* _

* 5th level (3 slots): _cone of cold, scrying, wall of force_

* 6th level (1 slot): _globe of invulnerability_

* 7th level (1 slot): _teleport_

* 8th level (1 slot): _mind blank* _

* 9th level (1 slot): _time stop_

*The archmage casts these spells on itself before combat.

**Actions**

___Dagger.___ Melee or Ranged Weapon Attack: +6 to hit, reach 5 ft. or range 20/60 ft., one target. Hit: 4 (1d4 + 2) piercing damage.
