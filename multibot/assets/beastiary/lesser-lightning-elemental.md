"Lesser Lightning Elemental";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "3 (700 XP)"
_languages_: "Auran"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "lightning, poison"
_damage_resistances_: "acid, fire; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "0 ft., fly 50 ft."
_hit points_: "45 (6d8 + 18)"
_armor class_: "14"
_stats_: | 14 (+2) | 18 (+4) | 16 (+3) | 4 (-3) | 11 (+0) | 11 (+0) |

___Lightning.___ A creature that touches the lightning elemental or hits it with
a melee attack while within 5 feet of it takes 7 (2d6) lightning damage.

___Water Susceptibility.___ For every 5 feet that the lightning elemental
moves in water, or for every gallon of water splashed on it, it takes 1 cold
damage.

**Actions**

___Multiattack.___ The lightning elemental makes two slam attacks.

___Slam.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 8 (1d8 + 4) lightning damage.

___Lightning Bolt.___ Ranged Spell Attack: +6 to hit, range 20/60 ft., one target. Hit: 22 (4d8 + 4) lightning damage.

___Globe Lightning (1/Long Rest).___ The lightning elemental
discharges 3 globes of electricity that hover in its space for 1 minute.
Whenever a creature enters or starts its turn within 5 feet of the
elemental, one of the globes discharges. The target must make a DC 14
Dexterity saving throw, taking 7 (1d6 + 4) lightning damage on a failed
saving throw, or half as much damage on a successful one. As each globe
discharges, it disappears.
