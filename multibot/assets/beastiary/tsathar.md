"Tsathar";;;_size_: Medium monstrosity
_alignment_: chaotic evil
_challenge_: "1/2 (100 XP)"
_languages_: "Abyssal, Tsathar"
_skills_: "Stealth +4"
_senses_: "darkvision 60 ft., passive Perception 11"
_speed_: "30 ft., swim 30 ft."
_hit points_: "16 (3d8 + 3)"
_armor class_: "13 (leather armor)"
_stats_: | 13 (+1) | 14 (+2) | 12 (+1) | 12 (+1) | 12 (+1) | 10 (+0) |

___Amphibious.___ The tsathar can breathe air and water.

___Keen Smell.___ The tsathar has advantage on Wisdom (Perception) checks
that rely on smell.

___Slimy.___ Tsathar continuously cover themselves with muck and slime.
Creatures attempting to grapple a tsathar do so with disadvantage.

___Standing Leap.___ The tsathar’s long jump is up to 20 feet and its high
jump is up to 10 feet, with or without a running start.

**Actions**

___Multiattack.___ The tsathar makes two melee attacks: one with its bite and
one with its claws, or one with its bite and one with its spear.

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 3 (1d4 + 1) piercing damage.

___Claws.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit:
3 (1d4 + 1) slashing damage, and the target must succeed on a DC 13
Constitution saving throw or become the living host to a tsathar egg,
which over the course of the egg maturing, migrates to the chest cavity of
the host. The host creature must make another DC 13 Constitution saving
throw after 24 hours of the egg having been implanted. A failed saving
throw results in the host becoming violently ill, followed by a deep comalike state that lasts 2d6 + 2 days. At the end of each day, the host can
attempt another saving throw with a success indicating that its body has
managed to destroy the egg through normal immune response. At the end
of the incubation period, the host awakes to excruciating pain as the young
tsathar, freed from its egg, tears its way out of the host, who is reduced to
0 hit points in the process.

A DC 16 Wisdom (Medicine) check can be attempted to surgically
extract an egg from the host. A lesser restoration spell will also cure the
condition and purge the host of the egg.

___Spear.___ Melee Weapon Attack: +3 to hit, reach 5 ft. or 20/60 ft., one
target. Hit: 4 (1d6 + 1) piercing damage, or 5 (1d8 +1) piercing damage if
used with two hands to make the melee attack.
