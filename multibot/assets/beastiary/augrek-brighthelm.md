"Augrek Brighthelm";;;_size_: Medium humanoid (shield dwarf)
_alignment_: lawful good
_challenge_: "0 (10 XP)"
_languages_: "Common, Dwarvish"
_senses_: "darkvision 60 ft."
_skills_: "Athletics +4, Perception +2"
_speed_: "25 ft."
_hit points_: "13 (2d8+4)"
_armor class_: "15 (chain shirt, shield)"
_damage_resistances_: "poison"
_stats_: | 14 (+2) | 11 (0) | 15 (+2) | 10 (0) | 11 (0) | 11 (0) |

___Dwarven Resilience.___ Augrek has advantage on saving throws against poison.

**Roleplaying** Information

Sheriff's deputy Augrek guards the southwest gate of Bryn Shander and welcomes visitors to town. She has a good heart.

**Ideal:** "You'll get farther in life with a kind word than an axe"
**Bond:** "Bryn Shander is my home. It's my job to protect her."
**Flaw:** "I'm head over heels in love with Sheriff Southwell. One day I hope to marry him."

**Actions**

___Warhammer.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 6 (1d8+2) bludgeoning damage, or 7 (1d10+2) bludgeoning damage if used with two hands.

___Heavy Crossbow.___ Ranged Weapon Attack: +2 to hit, range 100/400 ft., one target. Hit: 5 (1d10) piercing damage. Augrek carries ten crossbow bolts.
