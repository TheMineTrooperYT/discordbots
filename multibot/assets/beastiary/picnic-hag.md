"Picnic Hag";;;_size_: Medium fey
_alignment_: chaotic evil
_challenge_: "6 (2,300 XP)"
_languages_: "Common, Sylvan"
_senses_: "darkvision 60 ft., Passive Perception 15"
_skills_: "Athletics +8, Perception +5"
_saving_throws_: "Con +8"
_damage_immunities_: "poison"
_damage_resistances_: "Bludgeoning, Piercing And Slashing From Nonmagical Attacks"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "95 (10d8 + 50)"
_armor class_: "17 (natural armor)"
_stats_: | 21 (+5) | 12 (+1) | 21 (+5) | 14 (+2) | 14 (+2) | 10 (+0) |

**Actions**

___Multiattack.___ The hag makes three attacks: one
with her jaw and two with her claws.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft.,
one target. Hit: 15 (3d6 + 5) slashing damage.

___Unhinge Jaw.___ Melee Weapon Attack: +8 to hit,
reach 5 ft., one target. Hit: 15 (3d6 + 5) piercing
damage, and the target is grappled (escape DC 15).
Until the grapple ends, the target is restrained, and
the hag can’t bite another target.

___Swallow.** The hag makes a jaw attack against a Medium
or smaller target it is grappling. If the attack
hits, the target is swallowed, and the grapple ends.
The swallowed target is blinded and restrained,
it has total cover against attacks and other effects
outside the hag, and it takes 10 (4d4) acid damage
at the start of each of the hag’s turns.

The hag can have only one target swallowed at
a time. If the hag takes 20 damage or more on a
single turn from a creature inside it, the hag must
succeed on a DC 20 Constitution saving throw at
the end of that turn or regurgitate the creature,
which falls prone in a space within 10 feet of the
hag. If the hag dies, a swallowed creature is no longer
restrained by it and can escape from the corpse
using 5 feet of movement, exiting prone.

___You Are What You Eat.___ If the hag has swallowed
a creature, it can use one of that creature’s actions
as if it were that creature. The hag’s physical form
mutates to reflect its stomach’s contents.

**Reactions**

___Belch.___ When a creature damages the hag, she can
choose to belch noxious gas. Each creature in a
15-foot cone originating from the hag must make
a DC 15 Constitution saving throw, taking 10 (3d6)
poison damage on a failed save or half as much
damage on a successful one. If the hag has swallowed
a creature she can choose to eject the creature
as part of her belch, depositing it prone within
10 feet of her.
