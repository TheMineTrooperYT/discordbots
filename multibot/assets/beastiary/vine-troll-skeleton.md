"Vine Troll Skeleton";;;_size_: Large plant
_alignment_: unaligned
_challenge_: "9 (5000 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 12"
_saving_throws_: "Con +12"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning,"
_condition_immunities_: "deafened, exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "119 (14d10 + 42)"
_armor class_: "16 (natural armor)"
_stats_: | 20 (+5) | 12 (+1) | 16 (+3) | 6 (-2) | 8 (-1) | 5 (-3) |

___Regeneration.___ The vine troll skeleton regains 5 hit points at the start of its turn if it is within 10 feet of the duskthorn dryad's vines and it hasn't taken acid or fire damage since its previous turn. The skeleton dies only if it starts its turn with 0 hit points and doesn't regenerate, or if the duskthorn dryad who created it dies, or if the troll's heart inside the dryad's or treant's tree is destroyed.

**Actions**

___Multiattack.___ The skeleton makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 21 (3d10 + 5) piercing damage.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 18 (3d8 + 5) slashing damage.

