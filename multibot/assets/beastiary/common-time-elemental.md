"Common Time Elemental";;;_size_: Medium elemental
_alignment_: neutral
_challenge_: "7 (2,900 XP)"
_languages_: "telepathy 120 ft."
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_immunities_: "poison, psychic"
_damage_resistances_: "bludgeoning, piercing, and slashing damage from nonmagical weapons"
_condition_immunities_: "exhaustion, grappled, paralyzed, petrified, poisoned, prone, restrained, unconscious"
_speed_: "50 ft., fly 50 ft. (hover)"
_hit points_: "97 (13d8 + 39)"
_armor class_: "17 (natural armor)"
_stats_: | 16 (+3) | 20 (+5) | 16 (+3) | 14 (+2) | 14 (+2) | 11 (+0) |

___Cell Death.___ Damage dealt by the elemental can only be healed
magically. In addition, a creature that is slain by a time elemental can only
be restored to life by a _true resurrection_ or _wish_ spell.

___Foresight.___ A time elemental can see a few seconds into the future. This
ability prevents it from being surprised.

___Immunity to Temporal Magic.___ Time elementals are immune to all
time-related spells and effects that are not cast by other time elementals.

**Actions**

___Multiattack.___ The time elemental makes two slam attacks.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14
(2d8 + 5) bludgeoning damage.

___Multi-Manifestation (Recharge 5–6).___ The time elemental summons 1d4
duplicate manifestations of itself from alternate dimensions. Each of these
manifestations have the same statistics of the time elemental but can only
use melee attacks. Each manifestation can move and attack immediately
after it is summoned. Attacks that deal damage to one manifestation deal
the same damage to the elemental and the other manifestations.
The elemental can have no more than four manifestations under
its control at any time. The manifestations disappear at the start of the
elemental’s next turn.

___Time Jaunt.___ A time elemental can slip through the time stream and
appear anywhere on the same plane of existence as if by teleport. This
ability transports the time elemental and up to four other creatures of the
elemental’s choice that are within 30 feet of it. Unwilling creatures must
succeed on a DC 15 Wisdom saving throw to avoid being carried away.
