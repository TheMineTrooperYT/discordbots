"Light Cadejo";;;_size_: Medium fey
_alignment_: Lawful good
_challenge_: "3 (700 XP)"
_languages_: "Common"
_skills_: "Deception +4, Perception +8, Stealth +8"
_senses_: "darkvision 60 ft., passive Perception 18"
_speed_: "30 ft."
_hit points_: "99 (18d8 + 18)"
_armor class_: "18 (Natural Armor)"
_stats_: | 10 (+0) | 18 (+4) | 12 (+1) | 10 (+0) | 18 (+4) | 10 (+0) |

___Nullification.___ The light cadejo exists to undo the evil done by its wicked
cousin. While a light cadejo is within 120 feet of a dark cadejo, the dark
cadejo makes ability checks, attacks, and saving throws at disadvantage.
Additionally, the dark cadejo’s Paralysis and Stench features do not
function. However, any attacks, ability checks, and saving throws resulting
from direct combat with a light cadejo are made without disadvantage.

**Actions**

___Multiattack.___ The light cadejo makes three
attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon: +6 to hit, reach 5 ft., one
target. Hit: 8 (1d8 + 4) piercing damage.

___Claw.___ Melee Weapon Attack: +6 to hit, reach
5 ft., one target. Hit: 7 (1d6 + 4) damage.

___Restorative Touch.___ A light cadejo touches
a willing creature. The touch acts as the spell
lesser restoration.
