"Camazotz, Demon Lord Of Bats And Fire";;;_size_: Large fiend
_alignment_: chaotic evil
_challenge_: "22 (41000 XP)"
_languages_: "Common, Darakhul, Derro, Draconic, Dwarvish, Infernal, Nurian, Primordial, Void Speech; telepathy 300 ft."
_skills_: "Acrobatics +13, Athletics +17, Deception +14, Insight +13, Intimidation +14, Perception +13"
_senses_: "blindsight 120 ft., darkvision 40 ft., passive Perception 23"
_saving_throws_: "Dex +13, Con +14, Wis +13, Cha +14"
_damage_vulnerabilities_: "cold"
_damage_immunities_: "fire, poison, thunder"
_damage_resistances_: "acid, lightning; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "charmed, deafened, frightened, paralyzed, petrified, poisoned, stunned"
_speed_: "30 ft., climb 30 ft., fly 80 ft."
_hit points_: "537 (43d10 + 301)"
_armor class_: "19 (natural armor)"
_stats_: | 30 (+10) | 22 (+6) | 25 (+7) | 12 (+1) | 22 (+6) | 25 (+7) |

___Shapechanger.___ Camazotz can use his action to polymorph into a form that resembles a giant bat covered in smoldering ashes, or back into his true, winged humanoid form. His statistics are the same in each form. Any equipment he is wearing or carrying isn't transformed. Either form turns into a pile of greasy ash if destroyed.

___Echolocation.___ Camazotz can't use his blindsight while deafened.

___Keen Hearing.___ Camazotz has advantage on Wisdom (Perception) checks that rely on hearing.

___Gift of Vampirism.___ Camazotz may choose to raise those slain through Strength loss as vampires. They rise after 1d4 days, permanently dominated by Camazotz until such time as he sees fit to grant them free will. Camazotz may have no more than ten enslaved vampires at any given time.

___Heat Mantle.___ Camazotz is infused with the heart of volcanoes. A creature who strikes Camazotz with a nonreach weapon or with an unarmed strike takes 7 (2d6) fire damage automatically.

___Innate Spellcasting.___ Camazotz' innate spellcasting ability is Charisma (spell save DC 22, +14 to hit with spell attacks). He can innately cast the following spells, requiring no material components:

At will: darkness, detect evil and good, dispel magic, plane shift, shapechange, telekinesis, teleport

3/day each: banishment, haste, symbol

1/day: earthquake

___Magic Resistance.___ Camazotz has advantage on saving throws against spells and other magical effects.

___Summon Bats (1/Day).___ Camazotz can summon 4d6 giant bats or 2d6 swarms of bats. The bats appear immediately and serve the demon for up to 1 hour.

___Summon Demons (1/Day).___ Camazotz can summon 2d4 barlgura of a variety native to his cavernous realm: squat and blubbery creatures with clawed batwings for arms and a fly speed of 40 ft.

**Actions**

___Multiattack.___ Camazotz makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +17 to hit, reach 5 ft., one target. Hit: 38 (8d6 + 10) piercing damage plus 7 (2d6) fire damage, and the target must make a successful DC 22 Constitution saving throw or have its Strength score reduced by 1d4. A creature reduced to 0 Strength dies.

___Claw.___ Melee Weapon Attack: +17 to hit, reach 10 ft., one target. Hit: 31 (6d6 + 10) slashing damage plus 7 (2d6) fire damage, and the target must make a successful DC 22 Constitution saving throw or have its Strength score reduced by 1d4. A creature reduced to 0 Strength dies.

___Fire Breath (Recharge 5-6).___ Camazotz can breathe a 30-foot cone of unholy fire. Any creature caught in the area takes 55 (10d10) damage, half of which is fire, the other half is necrotic, or half as much damage with a successful DC 22 Dexterity saving throw.

**Legendary** Actions

___Camazotz can take 2 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. Camazotz regains spent legendary actions at the start of its turn.

___Detect.___ Camazotz makes a Wisdom (Perception) check.

___Wing Attack (Costs 2 Actions).___ Camazotz beats his wings, extinguishing mundane and magical light sources alike. Each creature within 10 feet must succeed on a DC 21 Dexterity saving throw or take 14 (4d6) fire damage. Camazotz can then fly up to half his flying speed.

