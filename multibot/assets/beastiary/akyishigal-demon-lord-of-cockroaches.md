"Akyishigal, Demon Lord Of Cockroaches";;;_size_: Large fiend
_alignment_: chaotic evil
_challenge_: "12 (8400 XP)"
_languages_: "Abyssal, Common, Draconic, Elvish, Infernal; telepathy 60 ft."
_skills_: "Acrobatics +11, Athletics +9, Perception +6, Stealth +11"
_senses_: "darkvision 120 ft., truesight 60 ft., passive Perception 16"
_saving_throws_: "Str +9, Dex +7, Con +10, Wis +6, Cha +11"
_damage_immunities_: "cold, lightning, poison"
_damage_resistances_: "acid, fire; bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "poisoned"
_speed_: "40 ft., burrow 20 ft., climb 40 ft., fly 40 ft."
_hit points_: "138 (12d10 + 72)"
_armor class_: "18 (natural armor)"
_stats_: | 21 (+5) | 17 (+3) | 22 (+6) | 19 (+4) | 14 (+2) | 24 (+7) |

___Innate Spellcasting.___ Akyishigal's innate spellcasting ability is Charisma (spell save DC 19, +11 to hit with spell attacks). It can innately cast the following spells, requiring no material components:

At will: detect evil and good, magic circle, teleport

3/day: dispel magic, insect plague (6th level), shapechange (vermin only)

1/day: contagion (always filth fever)

___Magic Resistance.___ Akyishigal has advantage on saving throws against spells and other magical effects.

___Summon Demon (1/Day).___ Akyishigal can summon a chasme demon. The chasme appears in an unoccupied space within 60 feet of Akyishigal, acts as an ally of Akyishigal, and can't summon other demons. It remains for 1 minute, until it or its summoner dies, or until its summoner dismisses it as an action.

**Actions**

___Multiattack.___ Akyishigal makes four claw attacks.

___Claw.___ Melee Weapon Attack: +9 to hit, reach 10 ft., one target. Hit: 16 (2d10 + 5) slashing damage.

___Cloak of Swarms (Recharge 5-6).___ Akyishigal can emit a cloud of flying, stinging insects from his mouth, his eyes, and the tears in his skin. This cloud surrounds him to a depth of 5 feet. When the cloak is active, all attacks against him are made with disadvantage, and Akyishigal can see in all directions (through the insects' eyes). Any living creature that starts its turn within 5 feet of Akyishigal takes 11 (2d10) piercing damage plus 10 (3d6) poison damage, or half as much poison damage with a successful DC 17 Constitution saving throw. If the saving throw fails, the character is also poisoned for 1 hour. The swarm lasts for 5 rounds; it dissipates instantly if Akyishigal takes 30 or more damage from a nonweapon attack that he doesn't have resistance to. While poisoned by the Cloak of Swarms, a character emits a stench of decomposition. All uncontrolled vermin attack that character on sight and in preference to other targets.

**Legendary** Actions

___Akyishigal can take 2 legendary actions, choosing from the options below.___ Only one legendary action option can be used at a time and only at the end of another creature's turn. Akyishigal regains spent legendary actions at the start of its turn.

___Teleport.___ Akyishigal may teleport within line of sight.

___Skitter.___ Akyishigal combines an attack with a move up to 20 feet as if using a withdraw action, and is not subject to attacks of opportunity.

___Spellcasting (Costs 2 Actions).___ Akyishigal casts insect plague from its innate spellcasting ability.

