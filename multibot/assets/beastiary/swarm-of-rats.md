"Swarm of Rats";;;_size_: Medium swarm
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_senses_: "darkvision 30 ft."
_speed_: "30 ft."
_hit points_: "24 (7d8-7)"
_armor class_: "10"
_condition_immunities_: "charmed, frightened, grappled, paralyzed, petrified, prone, restrained, stunned"
_damage_resistances_: "bludgeoning, piercing, slashing"
_stats_: | 9 (-1) | 11 (0) | 9 (-1) | 2 (-4) | 10 (0) | 3 (-4) |

___Keen Smell.___ The swarm has advantage on Wisdom (Perception) checks that rely on smell.

___Swarm.___ The swarm can occupy another creature's space and vice versa, and the swarm can move through any opening large enough for a Tiny rat. The swarm can't regain hit points or gain temporary hit points.

**Actions**

___Bites.___ Melee Weapon Attack: +2 to hit, reach 0 ft., one target in the swarm's space. Hit: 7 (2d6) piercing damage, or 3 (1d6) piercing damage if the swarm has half of its hit points or fewer.