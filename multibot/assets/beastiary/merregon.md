"Merregon";;;_page_number_: 166
_size_: Medium fiend (devil)
_alignment_: lawful evil
_challenge_: "4 (1100 XP)"
_languages_: "understands Infernal but can't speak, telepathy 120 ft."
_senses_: "darkvision 60 ft., passive Perception 11"
_damage_immunities_: "fire, poison"
_speed_: "30 ft."
_hit points_: "45  (6d8 + 18)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "frightened, poisoned"
_damage_resistances_: "cold; bludgeoning, piercing, and slashing from nonmagical attacks that aren't silvered"
_stats_: | 18 (+4) | 14 (+2) | 17 (+3) | 6 (-2) | 12 (+1) | 8 (-1) |

___Devil's Sight.___ Magical darkness doesn't impede the merregon's darkvision.

___Magic Resistance.___ The merregon has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack___ The merregon makes two halberd attacks, or if an allied fiend of challenge rating 6 or higher is within 60 feet of it, the merregon makes three halberd attacks.

___Halberd___ Melee Weapon Attack: +6 to hit, reach 10 ft., one target. Hit: 9 (1d10 + 4) slashing damage.

___Heavy Crossbow___ Ranged Weapon Attack: +4 to hit, range 100/400 ft., one target. Hit: 7 (1d10 + 2) piercing damage.