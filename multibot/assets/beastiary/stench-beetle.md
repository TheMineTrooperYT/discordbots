"Stench Beetle";;;_size_: Small beast
_alignment_: unaligned
_challenge_: "1/4 (50 XP)"
_languages_: "--"
_senses_: "darkvision 60 ft., passive Perception 10"
_speed_: "30 ft., fly 30 ft."
_hit points_: "18 (4d6 + 4)"
_armor class_: "13 (natural armor)"
_stats_: | 11 (+0) | 12 (+1) | 12 (+1) | 1 (-5) | 10 (+0) | 6 (-2) |

___Death Throes.___ When a stench beetle dies, it explodes in a rush of
effluvium of nauseating fluids and gases in a 10-foot radius. Creatures
within that radius must make a DC 13 Constitution saving throw or be
poisoned for 1 minute. While poisoned, the creature can only take one
action or bonus action on its turn as it spends the rest of its turn retching.
A poisoned creature can repeat the saving throw at the end of each of
its turns, ending the effect on a success.

**Actions**

___Bite.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 6 (2d4 + 1) piercing damage.
