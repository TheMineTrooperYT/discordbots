"Ettin";;;_size_: Large giant
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Giant, Orc"
_senses_: "darkvision 60 ft."
_skills_: "Perception +4"
_speed_: "40 ft."
_hit points_: "85 (10d10+30)"
_armor class_: "12 (natural armor)"
_stats_: | 21 (+5) | 8 (-1) | 17 (+3) | 6 (-2) | 10 (0) | 8 (-1) |

___Two Heads.___ The ettin has advantage on Wisdom (Perception) checks and on saving throws against being blinded, charmed, deafened, frightened, stunned, and knocked unconscious.

___Wakeful.___ When one of the ettin's heads is asleep, its other head is awake.

**Actions**

___Multiattack.___ The ettin makes two attacks: one with its battleaxe and one with its morningstar.

___Battleaxe.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) slashing damage.

___Morningstar.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) piercing damage.