"Ghost-Faced Orc";;;_size_: Medium humanoid (orc)
_alignment_: chaotic evil
_challenge_: "1/4 (50 XP)"
_languages_: "Common, Orc"
_skills_: "Intimidation +0, Perception +2, Stealth +3 (+5 in dim light or darkness)"
_senses_: "darkvision 120 ft., passive Perception 12"
_speed_: "30 ft."
_hit points_: "9 (2d8)"
_armor class_: "12 (leather)"
_stats_: | 15 (+2) | 12 (+1) | 10 (+0) | 7 (-2) | 10 (+0) | 6 (-2) |

___Shadow Stealth.___ When in dim light or darkness, the ghost-faced orc can
Hide as a bonus action.

**Actions**

___Greataxe.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 8 (1d12 + 2) slashing damage.

___Shortbow.___ Ranged Weapon Attack: +3 to hit, range 80/320 ft., one
target. Hit: 4 (1d6 + 1) piercing damage.
