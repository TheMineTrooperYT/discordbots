"Gbahali (Postosuchus)";;;_size_: Huge beast
_alignment_: unaligned
_challenge_: "6 (2300 XP)"
_languages_: "--"
_skills_: "Perception +4, Stealth +8"
_senses_: ", passive Perception 14"
_speed_: "50 ft."
_hit points_: "126 (12d12 + 48)"
_armor class_: "15 (natural armor)"
_stats_: | 21 (+5) | 14 (+2) | 19 (+4) | 2 (-4) | 13 (+1) | 7 (-2) |

___Chameleon Hide.___ The gbahali has advantage on Dexterity (Stealth) checks. If the gbahali moves one-half its speed or less, attacks made against it before the start of the gbahali's next turn have disadvantage.

**Actions**

___Multiattack.___ The gbahali makes one bite attack and two claw attacks.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 24 (3d12 + 5) piercing damage. If the target is a Medium or smaller creature, it is grappled (escape DC 15). Until this grapple ends, the target is restrained, and the gbahali can't bite another target.

___Claw.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) slashing damage.

