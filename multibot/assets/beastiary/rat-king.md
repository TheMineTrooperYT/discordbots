"Rat King";;;_size_: Medium monstrosity
_alignment_: chaotic evil
_challenge_: "5 (1800 XP)"
_languages_: "Common, Thieves' Cant"
_skills_: "Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 12"
_damage_immunities_: "poison"
_damage_resistances_: "bludgeoning, piercing, slashing"
_condition_immunities_: "charmed, frightened, paralyzed, petrified, prone, restrained, stunned"
_speed_: "30 ft., burrow 20 ft."
_hit points_: "76 (9d8 + 36)"
_armor class_: "14 (natural armor)"
_stats_: | 6 (-2) | 16 (+3) | 18 (+4) | 11 (+0) | 15 (+2) | 16 (+3) |

___Keen Smell.___ The rat king has advantage on Wisdom (Perception) checks that rely on smell.

___Plague of Ill Omen.___ The rat king radiates a magical aura of misfortune in a 30-foot radius. A foe of the rat king that starts its turn in the aura must make a successful DC 14 Charisma saving throw or be cursed with bad luck until the start of its next turn. When a cursed character makes an attack roll, ability check, or saving throw, it must subtract 1d4 from the result.

**Actions**

___Multiattack.___ The rat king makes four bite attacks.

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 10 (2d6 + 3) piercing damage, and a bitten creature must succeed on a DC 15 Constitution saving throw or be infected with a disease. A diseased creature gains one level of exhaustion immediately. When the creature finishes a long rest, it must repeat the saving throw. On a failure, the creature gains another level of exhaustion. On a success, the disease does not progress. The creature recovers from the disease if its saving throw succeeds after two consecutive long rests or if it receives a lesser restoration spell or comparable magic. The creature then recovers from one level of exhaustion after each long rest.

___Summon Swarm (1/Day).___ The rat king summons three swarms of rats. The swarms appear immediately within 60 feet of the rat king. They can appear in spaces occupied by other creatures. The swarms act as allies of the rat king. They remain for 1 hour or until the rat king dies.

**Reactions**

___Absorption.___ When the rat king does damage to a rat or rat swarm, it can absorb the rat, or part of the swarm, into its own mass. The rat king regains hit points equal to the damage it did to the rat or swarm.

