"Rhinoceros";;;_size_: Large beast
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_speed_: "40 ft."
_hit points_: "45 (6d10+12)"
_armor class_: "11 (natural armor)"
_stats_: | 21 (+5) | 8 (-1) | 15 (+2) | 2 (-4) | 12 (+1) | 6 (-2) |

___Charge.___ If the rhinoceros moves at least 20 ft. straight toward a target and then hits it with a gore attack on the same turn, the target takes an extra 9 (2d8) bludgeoning damage. If the target is a creature, it must succeed on a DC 15 Strength saving throw or be knocked prone.

**Actions**

___Gore.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 14 (2d8 + 5) bludgeoning damage.