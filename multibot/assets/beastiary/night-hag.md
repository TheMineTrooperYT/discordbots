"Night Hag";;;_size_: Medium fiend
_alignment_: neutral evil
_challenge_: "5 (1,800 XP)"
_languages_: "Abyssal, Common, Infernal, Primordial"
_senses_: "darkvision 120 ft."
_skills_: "Deception +7, Insight +6, Perception +6, Stealth +6"
_speed_: "30 ft."
_hit points_: "112 (15d8+45)"
_armor class_: "17 (natural armor)"
_condition_immunities_: "charmed"
_damage_resistances_: "cold, fire, bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_stats_: | 18 (+4) | 15 (+2) | 16 (+3) | 16 (+3) | 14 (+2) | 16 (+3) |

___Innate Spellcasting.___ The hag's innate spellcasting ability is Charisma (spell save DC 14, +6 to hit with spell attacks). She can innately cast the following spells, requiring no material components:

* At will: _detect magic, magic missile_

* 2/day each: _plane shift _(self only)_, ray of enfeeblement, sleep_

___Magic Resistance.___ The hag has advantage on saving throws against spells and other magical effects.

___Night Hag Items.___ A night hag carries two very rare magic items that she must craft for herself If either object is lost, the night hag will go to great lengths to retrieve it, as creating a new tool takes time and effort.

Heartstone: This lustrous black gem allows a night hag to become ethereal while it is in her possession. The touch of a heartstone also cures any disease. Crafting a heartstone takes 30 days.

Soul Bag: When an evil humanoid dies as a result of a night hag's Nightmare Haunting, the hag catches the soul in this black sack made of stitched flesh. A soul bag can hold only one evil soul at a time, and only the night hag who crafted the bag can catch a soul with it. Crafting a soul bag takes 7 days and a humanoid sacrifice (whose flesh is used to make the bag).

___Hag Coven.___ When hags must work together, they form covens, in spite of their selfish natures. A coven is made up of hags of any type, all of whom are equals within the group. However, each of the hags continues to desire more personal power.

A coven consists of three hags so that any arguments between two hags can be settled by the third. If more than three hags ever come together, as might happen if two covens come into conflict, the result is usually chaos.

___Shared Spellcasting (Coven Only).___ While all three members of a hag coven are within 30 feet of one another, they can each cast the following spells from the wizard's spell list but must share the spell slots among themselves. For casting these spells, each hag is a 12th-level spellcaster that uses Intelligence as her spellcasting ability (spell save DC 15, +7 to hit with spell attacks):

* 1st level (4 slots): _identify, ray of sickness_

* 2nd level (3 slots): _hold person, locate object_

* 3rd level (3 slots): _bestow curse, counterspell, lightning bolt_

* 4th level (3 slots): _phantasmal killer, polymorph_

* 5th level (2 slots): _contact other plane, scrying_

* 6th level (1 slot): _eye bite_

___Hag Eye (Coven Only).___ A hag coven can craft a magic item called a hag eye, which is made from a real eye coated in varnish and often fitted to a pendant or other wearable item. The hag eye is usually entrusted to a minion for safekeeping and transport. A hag in the coven can take an action to see what the hag eye sees if the hag eye is on the same plane of existence. A hag eye has AC 10, 1 hit point, and darkvision with a radius of 60 feet. If it is destroyed, each coven member takes 3d10 psychic damage and is blinded for 24 hours.

A hag coven can have only one hag eye at a time, and creating a new one requires all three members of the coven to perform a ritual. The ritual takes 1 hour, and the hags can't perform it while blinded. During the ritual, if the hags take any action other than performing the ritual, they must start over.

**Actions**

___Claws (Hag Form Only).___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13 (2d8 + 4) slashing damage.

___Change Shape.___ The hag magically polymorphs into a Small or Medium female humanoid, or back into her true form. Her statistics are the same in each form. Any equipment she is wearing or carrying isn't transformed. She reverts to her true form if she dies.

___Etherealness.___ The hag magically enters the Ethereal Plane from the Material Plane, or vice versa. To do so, the hag must have a heartstone in her possession.

___Nightmare Haunting (1/Day).___ While on the Ethereal Plane, the hag magically touches a sleeping humanoid on the Material Plane. A protection from evil and good spell cast on the target prevents this contact, as does a magic circle. As long as the contact persists, the target has dreadful visions. If these visions last for at least 1 hour, the target gains no benefit from its rest, and its hit point maximum is reduced by 5 (1d10). If this effect reduces the target's hit point maximum to 0, the target dies, and if the target was evil, its soul is trapped in the hag's soul bag. The reduction to the target's hit point maximum lasts until removed by the greater restoration spell or similar magic.
