"Oaken Bolter";;;_page_number_: 126
_size_: Medium construct
_alignment_: unaligned
_challenge_: "5 (1800 XP)"
_languages_: "understands one language of its creator but can't speak"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "poison; bludgeoning, piercing, and slashing from nonmagical attacks that aren't adamantine"
_speed_: "30 ft."
_hit points_: "58  (9d8 + 18)"
_armor class_: "16 (natural armor)"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_stats_: | 12 (+1) | 18 (+4) | 15 (+2) | 3 (-3) | 10 (0) | 1 (-4) |

___Magic Resistance.___ The oaken bolter has advantage on saving throws against spells and other magical effects.

**Actions**

___Multiattack___ The oaken bolter makes two lancing bolt attacks or one lancing bolt attack and one harpoon attack.

___Lancing Bolt___ Melee or Ranged Weapon Attack: +7 to hit, reach 5 ft. or range 100/400 ft., one target. Hit: 15 (2d10 + 4) piercing damage.

___Harpoon___ Ranged Weapon Attack: +7 to hit, range 50/200 ft., one target. Hit: 9 (1d10 + 4) piercing damage, and the target is grappled (escape DC 12). While grappled in this way, a creature's speed isn't reduced, but it can move only in directions that bring it closer to the oaken bolter. A creature takes 5 (1d10) slashing damage if it escapes from the grapple or if it tries and fails. As a bonus action, the oaken bolter can pull a creature grappled by it 20 feet closer. The oaken bolter can grapple only one creature at a time.

___Explosive Bolt (Recharge 5-6)___ The oaken bolter launches an explosive charge at a point within 120 feet. Each creature within 20 feet of that point must make a DC 15 Dexterity saving throw, taking 17 (5d6) fire damage on a failed save, or half as much damage on a successful one.