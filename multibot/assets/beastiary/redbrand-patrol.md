"Redbrand Patrol";;;_size_: Medium humanoid
_alignment_: lawful neutral
_challenge_: "1 (200 XP)"
_languages_: "Common"
_skills_: "Perception +2"
_senses_: "passive Perception 12"
_saving_throws_: "Str +3, Con +3"
_speed_: "30 ft."
_hit points_: "27 (5d8 + 5)"
_armor class_: "16 (chain mail)"
_stats_: | 13 (+1) | 10 (+0) | 12 (+1) | 10 (+0) | 11 (+0) | 10 (+0) |

**Actions**

___Multiattack.___ The Red Plume patrol makes two melee attacks.

___Longsword.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 5 (1d8 + 1) slashing damage.

___Heavy Crossbow.___ Ranged Weapon Attack: +2 to hit, range 100/400 ft., one target. Hit: 5 (1d10) piercing damage.

___Sleeping Poison Bolt (recharge 6).___ As an action, the Redbrand Patrol loads a poisoned bolt into its heavy crossbow.  If hit, the target must succeed on a DC 13 Constitution saving throw or be poisoned for 1 hour. If the saving throw fails by 5 or more, the target is also unconscious while poisoned in this way. The target wakes up if it takes damage or if another creature takes an action to shake it awake.

___Net.___ Ranged weapon attack: +3 to hit, range 5/15 ft., one creature. Hit: A large or smaller creature hit by a net is restrained until it is freed. A creature can use its action to make a DC 10 Strength check, freeing itself or another creature within its reach on a success. Dealing 5 slashing damage to the net also frees the creature.

**Reactions**

___Parry.___ The Red Plume patrol adds 2 to its AC against one melee attack that would hit it. To do so, the Red Plume patrol must see the attacker and be wielding a melee weapon.
