"Pyre Zombie";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "3 (700 XP)"
_languages_: "understands the languages it knew in life but can’t speak"
_senses_: "darkvision 60 ft., passive Perception 8"
_damage_immunities_: "fire, poison"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "19 (3d8 + 6)"
_armor class_: "12 (natural armor)"
_stats_: | 13 (+1) | 6 (-2) | 16 (+3) | 3 (-4) | 6 (-2) | 5 (-3) |

___Undead Fortitude.___ If damage reduces the zombie to 0 hit points, it
makes a Constitution saving throw with a DC of 5+ the damage taken,
unless the damage is radiant or from a critical hit. On a success, the
zombie drops to 1 hit point instead.

___Magic Resistance.___ The zombie has advantage on saving throws against
spells and other magical effects.

___Violent Combustion.___ Whenever the zombie is hit, it violently explodes.
Every creature with a 5-foot radius must make a DC 13 Dexterity saving
throw, taking 7 (2d6) fire damage on a failed save, or half as much on a
successful one.

**Actions**

___Slam.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 9
(2d6 + 1) bludgeoning damage plus 7 (2d6) fire damage.
