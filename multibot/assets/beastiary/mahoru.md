"Mahoru";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "3 (700 XP)"
_languages_: "--"
_skills_: "Perception +3, Stealth +6"
_senses_: "darkvision 60 ft., passive Perception 13"
_speed_: "10 ft., swim 60 ft."
_hit points_: "91 (14d8 + 28)"
_armor class_: "15 (natural armor)"
_stats_: | 18 (+4) | 19 (+4) | 14 (+2) | 3 (-4) | 12 (+1) | 7 (-2) |

___Amphibious.___ The mahoru can breathe air and water.

___Keen Sight and Smell.___ The mahoru has advantage on Wisdom (Perception) checks that rely on sight or smell.

___Pack Tactics.___ The mahoru has advantage on attack rolls against a creature if at least one of the mahoru's allies is within 5 feet of the creature and the ally isn't incapacitated.

___Blood Frenzy.___ The mahoru has advantage on melee attack rolls against any creature that isn't at maximum hit points.

**Actions**

___Bite.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 20 (3d10 + 4) slashing damage.

___Roar.___ When a mahoru roars all creatures with hearing within 30 feet of it must succeed on a DC 14 Wisdom saving throw or become frightened until the end of the mahoru's next turn. If the target fails the saving throw by 5 or more, it's also paralyzed for the same duration. A target that succeeds on the saving throw is immune to the Roar of all mahoru for the next 24 hours.

___Vorpal Bite.___ A mahoru's saw-like jaws are excel at dismembering prey. When the mahoru scores a critical hit, the target must succeed on a DC 14 Strength saving throw or lose an appendage. Roll a d12 and consult the following table for the result:
* 1-2: right hand
* 3-4: left hand
* 5-6: right food
* 7-8: left foot
* 9: right forearm
* 10: left forearm
* 11: right lower leg
* 12: left lower leg
