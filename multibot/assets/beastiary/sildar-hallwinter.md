"Sildar Hallwinter";;;_size_: Medium humanoid (human)
_alignment_: neutral good
_challenge_: "1 (200 XP)"
_languages_: "Common"
_skills_: "Perception +2"
_saving_throws_: "Str +3, Con +3"
_speed_: "30 ft."
_hit points_: "27 (5d8+5)"
_armor class_: "16 (chain mail)"
_stats_: | 13 (+1) | 10 (0) | 12 (+1) | 10 (0) | 11 (0) | 10 (0) |

**Actions**

___Multiattack.___ Sildar makes two melee attacks.

___Longsword.___ Melee Weapon Attack: +3 to hit, reach 5 ft., one target. Hit: 5 (1d8 + 1) slashing damage.

___Heavy Crossbow.___ Ranged Weapon Attack: +2 to hit, range 100 ft./400 ft., one target. Hit: 5 (1d10) piercing damage.

**Reactions**

___Parry.___ When an attacker hits Sildar with a melee attack and Sildar can see the attacker, he can roll 1d6 and add the number rolled to his AC against the triggering attack, provided that he's wielding a melee weapon.