"Mi-Go";;;_size_: Medium plant
_alignment_: neutral evil
_challenge_: "5 (1800 XP)"
_languages_: "Common, Mi-go, Void Speech"
_skills_: "Arcana +10, Deception +7, Medicine +5, Perception +5, Stealth +7"
_senses_: "blindsight 30 ft., darkvision 240 ft., passive Perception 15"
_saving_throws_: "Str +6, Con +8, Cha +4"
_damage_resistances_: "radiant, cold"
_speed_: "30 ft., fly 60 ft."
_hit points_: "76 (8d8 + 40)"
_armor class_: "17 (natural armor)"
_stats_: | 16 (+3) | 19 (+4) | 21 (+5) | 25 (+7) | 15 (+2) | 13 (+1) |

___Astral Travelers.___ Mi-go do not require air or heat to survive, only sunlight (and very little of that). They can enter a sporulated form capable of surviving travel through the void and returning to consciousness when conditions are right.

___Sneak Attack (1/Turn).___ The mi-go does an extra 7 (2d6) damage when it hits a target with a claw attack and has advantage on the attack roll, or when the target is within 5 feet of an ally of the mi-go that isn't incapacitated and the mi-go doesn't have disadvantage on the attack roll.

___Disquieting Technology.___ The mi-go are a highly advanced race, and may carry items of powerful technology. Mi-go technology can be represented using the same rules as magic items, but their functions are very difficult to determine: identify is useless, but an hour of study and a successful DC 19 Arcana check can reveal the purpose and proper functioning of a mi.go item.

**Actions**

___Multiattack.___ The mi-go makes two attacks with its claws.

___Claw.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 14 (3d6 + 4) slashing damage, and the target is grappled (escape DC 13). If both claw attacks strike the same target in a single turn, the target takes an additional 13 (2d12) psychic damage.

**Reactions**

___Spore Release.___ When a mi-go dies, it releases its remaining spores. All living creatures within 10 feet take 14 (2d8 + 5) poison damage and become poisoned; a successful DC 16 Constitution saving throw halves the damage and prevents poisoning. A poisoned creature repeats the saving throw at the end of its turn, ending the effect on itself with a success.

