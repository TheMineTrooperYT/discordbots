"Shadow Hunter";;;_size_: Huge monstrosity
_alignment_: neutral
_challenge_: "9 (5,000 XP)"
_languages_: "--"
_skills_: "Perception +6, Stealth +6"
_senses_: "darkvision 60 ft., tremorsense 60 ft., passive Perception 16"
_condition_immunities_: "prone"
_speed_: "30 ft., climb 50 ft., swim 30 ft."
_hit points_: "105 (10d12 + 40)"
_armor class_: "14 (natural armor)"
_stats_: | 23 (+6) | 15 (+2) | 19 (+4) | 5 (-3) | 14 (+2) | 3 (-4) |

___Keen Smell.___ The shadow hunter has advantage on Wisdom (Perception)
checks that rely on smell.

___Shadow Stealth.___ While in dim light or darkness, the shadow hunter can take
the Hide action as a bonus action.

**Actions**

___Bite.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit:
15 (2d8 + 6) piercing damage and 27 (6d8) poison damage. The target
must succeed on a DC 15 Constitution saving throw or be poisoned
for 1 hour.

___Constrict.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit:
17 (2d10 + 6) bludgeoning damage and the target is grappled (escape DC
16). Until this grapple ends, the creature is restrained, and the shadow
hunter can’t constrict another target.
