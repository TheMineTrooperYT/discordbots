"Azer Lavashaper";;;_size_: Medium elemental
_alignment_: lawful neutral
_challenge_: "2 (450 XP)"
_languages_: "Ignan"
_senses_: "passive Perception 10"
_damage_immunities_: "fire, poison"
_condition_immunities_: "poisoned"
_speed_: "30 ft."
_hit points_: "38 (7d8 + 7)"
_armor class_: "13 (natural armor)"
_stats_: | 14 (+2) | 10 (+0) | 12 (+1) | 11 (+0) | 10 (+0) | 14 (+2) |

___Innate Spellcasting.___ The azer’s innate spellcasting
ability is Charisma (spell save DC 12, +4 to hit with
spell attacks). It can innately cast the following
spells, requiring no components:

* At will: _mending, produce flame, comprehend languages_

* 1/day each: _burning hands, scorching ray, enlarge/reduce, darkvision_

___Heated Weapons.___ When the azer hits with a metal
melee weapon, it deals an extra 3 (1d6) fire damage
(included in the attack).

___Illumination.___ The azer sheds bright light in a 10-foot
radius and dim light for an additional 10 feet.

**Actions**

___Multiattack.___ The azer makes two attacks with its
warhammer.

___Warhammer.___ Melee Weapon Attack: +5 to hit, reach
5ft., one target. Hit: 6 (1d8 + 2) bludgeoning
damage, or 7 (1d10 + 2) bludgeoning damage if
used with two hands to make a melee attack, plus 3
(1d6) fire damage.
