"Naina";;;_size_: Large dragon
_alignment_: lawful evil
_challenge_: "11 (7200 XP)"
_languages_: "Common, Darakhul, Draconic, Elvish, Sylvan"
_skills_: "Arcana +6, Deception +8, Insight +8, Perception +8,"
_senses_: "darkvision 60 ft., passive Perception 18"
_saving_throws_: "Dex +7, Con +9, Int +6, Wis +8, Cha +8"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "paralyzed, poisoned, unconscious"
_speed_: "40 ft., fly 120 ft."
_hit points_: "231 (22d10 + 110)"
_armor class_: "17 (natural armor)"
_stats_: | 20 (+5) | 16 (+3) | 21 (+5) | 15 (+2) | 18 (+4) | 18 (+4) |

___Magic Sensitive.___ The naina detects magic as if it were permanently under the effect of a detect magic spell.

___Spellcasting.___ The naina is a 9th-level spellcaster. Her spellcasting ability is Charisma (spell save DC 16, +8 to hit with spell attacks). The naina has the following sorcerer spells prepared:

Cantrips (at will): dancing lights, mage hand, mending, ray of frost, resistance, silent image

1st level (4 slots): charm person, thunderwave, witch bolt

2nd level (3 slots): darkness, invisibility, locate object

3rd level (3 slots): dispel magic, hypnotic pattern

4th level (3 slots): dimension door

5th level (1 slot): dominate person

___Shapechanger.___ The naina can use her action to polymorph into one of her two forms: a drake or a female humanoid. She cannot alter either form's appearance or capabilities (with the exception of her breath weapon) using this ability, and damage sustained in one form transfers to the other form.

**Actions**

___Multiattack.___ The naina makes two claw attacks and one bite attack.

___Bite (drake form only).___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 24 (3d12 + 5) piercing damage.

___Claw (drake form only).___ Melee Weapon Attack: +9 to hit, reach 5 ft., one target. Hit: 24 (3d12 + 5) slashing damage.

___Poison Breath (Recharge 5-6).___ While in drake form (only), the naina breathes a 20-foot cone of poison gas, paralytic gas, or sleep gas.

___Poison.___ A creature caught in this poison gas takes 18 (4d8) poison damage and is poisoned; a successful DC 17 Constitution saving throw reduces damage to half and negates the poisoned condition. While poisoned this way, the creature must repeat the saving throw at the end of each of its turns. On a failure, it takes 9 (2d8) poison damage and the poisoning continues; on a success, the poisoning ends.

___Paralysis.___ A creature caught in this paralytic gas must succeed on a DC 17 Constitution saving throw or be paralyzed for 2d4 rounds. A paralyzed creature repeats the saving throw at the end of each of its turns; a successful save ends the paralysis.

___Sleep.___ A creature caught in this sleeping gas must succeed on a DC 17 Constitution saving throw or fall unconscious for 6 rounds. A sleeping creature repeats the saving throw at the end of each of its turns; it wakes up if it makes the save successfully.

