"Cadaver Lord";;;_size_: Medium undead
_alignment_: chaotic evil
_challenge_: "4 (1,100 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft., passive Perception 10"
_damage_immunities_: "cold, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_condition_immunities_: "exhaustion, poisoned"
_speed_: "30 ft."
_hit points_: "32 (5d8 + 10)"
_armor class_: "14"
_stats_: | 15 (+2) | 18 (+4) | 14 (+2) | 13 (+1) | 10 (+0) | 16 (+3) |

___Innate Spellcasting.___ The cadaver lord’s spellcasting ability is
Charisma (spell save DC 13, +5 to hit with spell attacks). The
cadaver lord can innately cast the following spells,
requiring no material components: 

* 1/day each: _darkness, fear, create undead_

___Create Cadaver.___ A humanoid slain by a cadaver lord rises 24
hours later as a cadaver under the cadaver lord’s control. The
cadaver lord can have no more than three cadavers under its
control at one time.

___Magic Resistance.___ The cadaver lord has advantage on saving
throws against spells and other magical effects.

___Reanimation.___ When reduced to 0 hit points, the cadaver falls
inert and begins the process of reanimating. While in this state, the
cadaver regenerates 1 hit point at the start of its turn. Hit points lost
to magical weapons or radiant damage are not regained. When the
creature reaches its full hit point total, less any magical weapon or
radiant damage suffered, it rises, ready to fight again.

A fallen cadaver can be prevented from reanimating by salting and
burning the bones, casting gentle repose on it, or bathing the bones
in cleansing sacred flame.

**Actions**

___Multiattack.___ The cadaver lord makes one bite attack and two claw
attacks.

___Bite.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 11
(2d6 + 4) piercing damage. If the target is a creature, it must succeed
on a DC 13 Constitution saving throw against disease or become
poisoned until the disease is cured. Every 24 hours that elapse, the
target must repeat the saving throw, reducing its hit point maximum by 5
(1d10) on a failure. The disease is cured on a success. The target dies if the
disease reduces its hit point maximum to 0. This reduction to the target’s
hit point maximum lasts until the disease is cured.

___Claws.___ Melee Weapon Attack: +7 to hit, reach 5 ft., one target. Hit: 13
(2d8 + 4) slashing damage. If the target is a creature, it must succeed on
a DC 13 Constitution saving throw against disease or become poisoned
until the disease is cured. Every 24 hours that elapse, the target must
repeat the saving throw, reducing its hit point maximum by 5 (1d10) on
a failure. The disease is cured on a success. The target dies if the disease
reduces its hit point maximum to 0. This reduction to the target’s hit point
maximum lasts until the disease is cured.
