"Tri-Flower Frond";;;_size_: Medium plant
_alignment_: unaligned
_challenge_: "1/2 (100 XP)"
_senses_: "blindsight 30 ft., passive Perception 10"
_condition_immunities_: "blinded, deafened, exhaustion, prone"
_speed_: "5 ft."
_hit points_: "11 (2d8 +2)"
_armor class_: "10"
_stats_: | 1 (-5) | 10 (0) | 12 (+1) | 1 (-5) | 10 (0) | 1 (-5) |

**Actions**

___Multiattack.___ The tri-flower frond uses its orange blossom, then its yellow blossom, and then its red blossom.

___Orange Blossom.___ The tri -flower frond chooses one creature it can see within 5 feet of it. The target must succeed on a DC 11 Constitution saving throw or be poisoned for 1 hour. While poisoned in this way, the target is unconscious. At the end of each minute, the poisoned target can repeat the saving throw, ending the effect on itself on a success.

___Yellow Blossom.___ The tri-flower frond chooses one creature it can see within 5 feet of it. The target must succeed on a DC 11 Dexterity saving throw, or it is covered with corrosive sap and takes 5 acid damage at the start of each of its turns. Dousing the target with water reduces the acid damage by 1 point per pint or flask of water used.

___Red Blossom.___ Melee Weapon Aitack: +2 to hit, reach 5 ft., one creature. Hit: 2 (ld4) piercing damage, and the target is grappled (escape DC 11). Until this grapple ends , the target takes 5 (2d4) poison damage at the start of each of its turns. The red blossom can grapple only one target at a time. Another creature within reach of the tri-flower frond can use its action to end the grapple on the target.