"Adult Cloud Dragon";;;_size_: Huge dragon
_alignment_: neutral
_challenge_: "15 (13,000 XP)"
_languages_: "Auran, Aquan, Common, Draconic, Giant, Sylvan"
_skills_: "Arcana +9, History +9, Insight +7, Nature +9, Perception +12"
_senses_: "blindsight 120 ft., darkvision 120 ft., passive Perception 22"
_saving_throws_: "Dex +6, Con +10, Wis +7, Cha +8"
_damage_immunities_: "cold, lightning"
_speed_: "40 ft., fly 80 ft."
_hit points_: "195 (17d12 + 85)"
_armor class_: "18 (natural armor)"
_stats_: | 23 (+6) | 12 (+1) | 21 (+5) | 18 (+4) | 15 (+2) | 17 (+3) |

___Legendary Resistance (3/day).___ If the dragon fails a saving throw,
it can choose to succeed instead.

___Windborn.___ The dragon cannot be moved or knocked prone by
magical or nonmagical winds of any kind.

**Actions**

___Multiattack.___ The dragon uses its Frightful Presence and makes three
attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +11 to hit, reach 20 ft., one target. Hit: 17
(2d10 + 6) piercing damage plus 10 (3d6) cold damage.

___Claw.___ Melee Weapon Attack: +11 to hit, reach 10 ft., one target. Hit: 20
(4d6 + 6) slashing damage.

___Tail.___ Melee Weapon Attack: +11 to hit, reach 20 ft., one target. Hit: 15
(2d8 + 6) bludgeoning damage.

___Frightful Presence.___ Each creature of the dragon’s choice that is within
120 feet of the dragon and aware of it must succeed on a DC 16 Wisdom
saving throw or become frightened for 1 minute. A creature can repeat the
saving throw at the end of each of its turns, ending the effect on itself on
a success. If a creature’s saving throw is successful or the effect ends for
it, the creature is immune to the dragon’s Frightful Presence for the next
24 hours.

___Breath Weapons (Recharge 5–6).___ The dragon uses one of the following
breath weapons.

* _Cloud Breath._ The dragon exhales an icy blast in a 60-foot cone that
spreads around corners. Each creature in that area must make a DC 19
Constitution saving throw, taking 54 (12d8) cold damage on a failed
saving throw, or half as much damage on a successful one.

* _Wind Breath._ The dragon exhales gale-force winds in a 60-foot
line that is 10-feet wide. Creatures in this area must make a DC 19
Strength saving throw, taking 36 (8d8) bludgeoning damage on a failed
saving throw and are pushed 60 feet directly away from the dragon.
On a successful saving throw, the creature takes half damage and is
not pushed. A creature who is pushed and strikes a solid surface takes
1d6 bludgeoning damage per 10 feet traveled. Nonmagical flames are
completely extinguished, and magical flames created by a spell of 4th
level or lower are immediately dispelled.

___Cloud Form.___ The cloud dragon polymorphs into a Huge cloud of
mist, or back into its true form. While in mist form, the dragon can’t
take any actions, speak, or manipulate objects. It is weightless, has a
flying speed of 20 feet, can hover, and can enter a hostile creature’s
space and stop there. In addition, if air can pass through a space, the
mist can do so without squeezing, and it can’t pass through water. It
has advantage on Strength, Dexterity, and Constitution saving throws,
it is immune to all nonmagical damage, and still benefits from its
Windborn feature.

**Legendary** Actions

The dragon can take 3 legendary actions, choosing from the options
below. Only one legendary action option can be used at a time and only
at the end of another creature’s turn. The dragon regains spent legendary
actions at the start of its turn.

___Detect.___ The dragon makes a Wisdom (Perception) check.

___Tail Attack.___ The dragon makes a tail attack.

___Wing Attack (Costs 2 Actions).___ The dragon beats its wings. Each
creature within 15 feet of the dragon must succeed on a DC 19 Dexterity
saving throw or take 13 (2d6 + 6) bludgeoning damage and be knocked
prone. The dragon can then fly up to half its flying speed.
