"Nuckalavee";;;_size_: Large aberration
_alignment_: chaotic evil
_challenge_: "8 (3,900 XP)"
_languages_: "Common, Sylvan"
_skills_: "Perception +5, Stealth +5, Survival +5"
_senses_: "darkvision 60 ft., passive Perception 15"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons"
_speed_: "50 ft."
_hit points_: "84 (8d10 + 40)"
_armor class_: "17 (natural armor)"
_stats_: | 20 (+5) | 15 (+2) | 20 (+5) | 8 (-1) | 15 (+2) | 14 (+2) |

___Charge.___ If the nuckalavee moves at least 20 feet straight toward a
creature and then hits it with a greataxe attack, that target must succeed
on a DC 16 Strength saving throw or be knocked prone. If the target is
prone, the nuckalavee can make one attack with its hooves against it as a
bonus action.

___Horrific Appearance.___ Any creature that starts its turn within 20 feet
of the nuckalavee must make a DC 13 Wisdom saving throw. On a failed
save, the creature is frightened until the start of its next turn. If a creature’s
saving throw is successful, the creature is immune to the nuckalavee’s
horrific appearance for the next 24 hours.

___Magic Resistance.___ The nuckalavee has advantage on saving throws
against spells and other magic effects.

**Actions**

___Multiattack.___ The nuckalavee makes four attacks: two with its greataxe,
one bite attack, and one attack with its hooves.

___Greataxe.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit:
12 (2d6 + 5) slashing damage.

___Hooves.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 14
(2d8 + 5) slashing damage.

___Bite.___ Melee Weapon Attack: +8 to hit, reach 5 ft., one target. Hit: 10
(2d4 + 5) piercing damage.

___Poisonous Breath (Recharge 5–6).___ The nuckalavee exhales a cloud of
caustic gas in a 20-foot cone. Creatures in this area must make a DC 15
Dexterity saving throw, taking 21 (6d6) acid damage on a failed save, or
half as much damage on a successful one.
