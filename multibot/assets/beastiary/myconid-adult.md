"Myconid Adult";;;_size_: Medium plant
_alignment_: lawful neutral
_challenge_: "1/2 (100 XP)"
_senses_: "darkvision 120 ft."
_speed_: "20 ft."
_hit points_: "22 (4d8+4)"
_armor class_: "12 (natural armor)"
_stats_: | 10 (0) | 10 (0) | 12 (+1) | 10 (0) | 13 (+1) | 7 (-2) |

___Distress Spores.___ When the myconid takes damage, all other myconids within 240 feet of it can sense its pain.

___Sun Sickness.___ While in sunlight, the myconid has disadvantage on ability checks, attack rolls, and saving throws. The myconid dies if it spends more than 1 hour in direct sunlight.

**Actions**

___Fist.___ Melee Weapon Attack: +2 to hit, reach 5 ft., one target. Hit: 5 (2d4) bludgeoning damage plus 5 (2d4) poison damage.

___Pacifying Spores (3/Day).___ The myconid ejects spores at one creature it can see within 5 feet of it. The target must succeed on a DC 11 Constitution saving throw or be stunned for 1 minute. The target can repeat the saving throw at the end of each of its turns, ending the effect on itself on a success.

___Rapport Spores.___ A 20-foot radius of spores extends from the myconid. These spores can go around corners and affect only creatures with an Intelligence of 2 or higher that aren't undead, constructs, or elementals. Affected creatures can communicate telepathically with one another while they are within 30 feet of each other. The effect lasts for 1 hour.