Aberrant Fungi;;;_size_: Large plant
_alignment_: unaligned
_challenge_: 1/2 (100 XP)
_languages_: --
_senses_: blindisght 30 ft. (blind beyond this radius), passive Perception 5
_damage_resistances_: slashing
_damage_vulnerabilities_: fire
_condition_immunities_: Blinded, Deafened, Exhaustion, Prone
_speed_: 5 ft., climb 5 ft.
_hit points_: 45 (6d8 + 18)
_armor class_: 6 (natural armor)
_stats_: | 5 (-3) | 1 (-5) | 3 (-4) | 1 (-5) | 1 (-5) | 1 (-5) |

___Death Burst.___ When the aberrant fungi drops to 0
hit points, it explodes. Each creature within 20 ft.
of it must succeed on a DC 13 Constitution saving
throw or take 10 (3d6) psychic damage and become
infected with a disease. If a humanoid drops to 0
hit points as a result of this damage, it dies. After
24 hours, 1d4 Tiny aberrant fungi sprout from the
body, growing to full size in 7 days.

The spores are destroyed if the creature is raised
from the dead, or if the corpse is targeted by a
remove curse or similar magic, before the aberrant
fungi sprout.

Spores invade the affected creature’s mind, afflicting
the creature with short term madness for 1d10
minutes. After that time elapses, the creature must
succeed on a DC 13 Constitution saving throw or
become afflicted with long term madness for 1d10 x
10 hours. The creature must then succeed on a DC 11
Constitution saving throw or be afflicted with indefinite
madness. The madness can be cured by a lesser
restoration or similar magic.
