"Roving Ruffian";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "5 (1,800 XP)"
_languages_: "Any two languages"
_skills_: "Acrobatics +7, Animal Handling +6, Athletics +8, Perception +6"
_senses_: "passive Perception 16"
_saving_throws_: "Str +8, Con +7"
_speed_: "30 ft."
_hit points_: "92 (7d12 + 3d10 + 30)"
_armor class_: "18 (unarmored defense, shield)"
_stats_: | 18 (+4) | 17 (+3) | 16 (+3) | 10 (+0) | 14 (+2) | 7 (-2) |

___Danger Sense.___ The ruffian has advantage on Dexterity
saving throws.

___Dueling Fighting Style.___ The ruffian gains a +2 bonus to
damage rolls while wielding a melee weapon in one
hand and no other weapons (included in the attack).

___Horde Breaker (1/Turn).___ When the ruffian makes a
weapon attack, it can make another attack roll with the
same weapon against a different creature within 5 feet
of the original target and within range of its weapon.

___Rage (4/Day).___ As a bonus action, the ruffian can enter a
rage for 1 minute. While raging, the ruffian gains the
following benefits:
* Advantage on Strength checks and Strength saving
throws
* Melee weapon attacks deal an addition 2 damage.
* The ruffian gains resistance to bludgeoning, piercing,
and slashing damage.

___Spellcasting.___ The ruffian is a 3th-level spellcaster. Its
spellcasting ability is Wisdom (spell save DC 14, +6 to
hit with spell attacks). The ruffian has the following
ranger spells prepared:

* 1st level (2 slots): _cure wounds, jump, speak with animals_

___Unarmored Defense.___ While not wearing armor, the
ruffian's AC includes its Constitution modifier.

**Actions**

___Multiattack.___ The ruffian makes three battleaxe attacks or
two longbow attacks.

___Battleaxe.___ Melee Weapon Attack: +8 to hit, reach 5 ft.,
one target. Hit: 10 (1d8 + 6) slashing damage, or 12
(1d8 + 8) slashing damage is used while raging.

___Longbow.___ Ranged Weapon Attack: +7 to hit, range
150/600 ft., one target. Hit: 7 (1d8 + 3) piercing
damage.
