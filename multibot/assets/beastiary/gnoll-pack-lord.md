"Gnoll Pack Lord";;;_size_: Medium humanoid (gnoll)
_alignment_: chaotic evil
_challenge_: "2 (450 XP)"
_languages_: "Gnoll"
_senses_: "darkvision 60 ft."
_speed_: "30 ft."
_hit points_: "49 (9d8+9)"
_armor class_: "15 (chain shirt)"
_stats_: | 16 (+3) | 14 (+2) | 13 (+1) | 8 (-1) | 11 (0) | 9 (-1) |

___Rampage.___ When the gnoll reduces a creature to 0 hit points with a melee attack on its turn, the gnoll can take a bonus action to move up to half its speed and make a bite attack.

**Actions**

___Multiattack.___ The gnoll makes two attacks, either with its glaive or its longbow, and uses its Incite Rampage if it can.

___Bite.___ Melee Weapon Attack: +5 to hit, reach 5 ft., one creature. Hit: 5 (1d4 + 3) piercing damage.

___Glaive.___ Melee Weapon Attack: +5 to hit, reach 10 ft., one target. Hit: 8 (1d10 + 3) slashing damage.

___Longbow.___ Ranged Weapon Attack: +4 to hit, range 150/600 ft., one target. Hit: 6 (1d8 + 2) piercing damage.

___Incite Rampage (Recharge 5-6).___ One creature the gnoll can see within 30 feet of it can use its reaction to make a melee attack if it can hear the gnoll and has the Rampage trait.