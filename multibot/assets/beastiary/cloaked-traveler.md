"Cloaked Traveler";;;_size_: Medium humanoid (any race)
_alignment_: any alignment
_challenge_: "3 (700 XP)"
_languages_: "Any two languages"
_skills_: "Acrobatics +6, Insight +6, Stealth +6, Survival +6"
_senses_: "passive Perception 13"
_saving_throws_: "Str +4, Dex +6"
_speed_: "50 ft."
_hit points_: "55 (10d8 + 10)"
_armor class_: "16 (unarmored defense)"
_stats_: | 13 (+1) | 16 (+3) | 12 (+1) | 14 (+2) | 16 (+3) | 10 (+0) |

___Flurry of Blows (2/Short Rest).___ If the traveler has
attacked this turn, it can make an additional
unarmed strike as a bonus action.

___Shadow Arts (2/Short Rest).___ The traveler duplicates
the effects of certain spells. As an action, it can cast
_darkness, darkvision, pass without trace, _or _silence_
without providing material components.

___Shadow Step.___ When the traveler is in dim light or
darkness, as a bonus action it can teleport up to 60
feet to an unoccupied space it can see that is also
in dim light and darkness. The traveler then has
advantage on the fight melee attack it makes before
the end of the turn.

___Unarmored Defense.___ While not wearing armor, the
traveler's AC includes its Wisdom modifier.

**Actions**

___Multiattack.___ The traveler makes three attacks, two
with its quarterstaff and one with its unarmed
strike.

___Quarterstaff.___ Melee Weapon Attack: +6 to hit, reach
5 ft., one target. Hit: 7 (1d8 + 3) bludgeoning
damage.

___Unarmed Strike.___ Melee Weapon Attack: +6 to hit,
reach 5 ft., one target. Hit: 5 (1d4 + 3) bludgeoning
damage.
