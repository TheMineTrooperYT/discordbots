"Drowned Maiden";;;_size_: Medium undead
_alignment_: neutral evil
_challenge_: "5 (1800 XP)"
_languages_: "Common"
_senses_: "darkvision 60 ft., passive Perception 11"
_saving_throws_: "Dex +6, Cha +7"
_damage_immunities_: "necrotic, poison"
_damage_resistances_: "bludgeoning, piercing, and slashing from nonmagical weapons that aren't silvered"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, poisoned"
_speed_: "30 ft., swim 40 ft."
_hit points_: "90 (20d8)"
_armor class_: "15 (natural armor)"
_stats_: | 15 (+2) | 16 (+3) | 10 (+0) | 10 (+0) | 12 (+1) | 18 (+4) |

___Grasping Hair.___ The drowned maiden's hair attacks as though it were three separate limbs, each of which can be attacked (AC 19; 15 hit points; immunity to necrotic, poison, and psychic damage; resistance to bludgeoning, piercing, and slashing damage from nonmagical weapons that aren't silvered). A lock of hair can be broken if a creature takes an action and succeeds on a DC 15 Strength check against it.

___Innate Spellcasting.___ The drowned maiden's innate spellcasting ability is Charisma (spell save DC 15). She can innately cast the following spells, requiring no material components:

At will: disguise self, silence

**Actions**

___Multiattack.___ The drowned maiden makes two claw attacks and one hair attack, each of which it can replace with one kiss attack.

___Claw.___ Melee Weapon Attack: +6 to hit, reach 5 ft., one target. Hit: 6 (1d8 + 3) slashing damage.

___Hair.___ Melee Weapon Attack: +6 to hit, reach 20 ft., one target. Hit: 14 (2d10 + 3) slashing damage, and the target is grappled (escape DC 16). Three creatures can be grappled at a time.

___Kiss.___ The drowned maiden can kiss one target that is grappled and adjacent to her. The target must succeed on a DC 15 Charisma saving throw or take 1d6 Strength damage.

___Reel.___ The drowned maiden pulls a grappled creature of Large size or smaller up to 15 feet straight toward herself.

