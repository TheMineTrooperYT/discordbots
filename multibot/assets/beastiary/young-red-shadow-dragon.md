"Young Red Shadow Dragon";;;_size_: Large dragon
_alignment_: chaotic evil
_challenge_: "13 (10,000 XP)"
_languages_: "Common, Draconic"
_senses_: "blindsight 30 ft., darkvision 120 ft."
_skills_: "Perception +8, Stealth +8"
_damage_immunities_: "fire"
_saving_throws_: "Dex +4, Con +9, Wis +4, Cha +8"
_speed_: "40 ft., climb 40 ft., fly 80 ft."
_hit points_: "178 (17d10+85)"
_armor class_: "18 (natural armor)"
_damage_resistances_: "necrotic"
_stats_: | 23 (+6) | 10 (0) | 21 (+5) | 14 (+2) | 11 (0) | 19 (+4) |

___Living Shadow.___ While in dim light or darkness, the dragon has resistance to damage that isn't force, psychic, or radiant.

___Shadow Stealth.___ While in dim light or darkness, the dragon can take the Hide action as a bonus action.

___Sunlight Sensitivity.___ While in sunlight, the dragon has disadvantage on attack rolls, as well as on Wisdom (Perception) checks that rely on sight.

**Actions**

___Multiattack.___ The dragon makes three attacks: one with its bite and two with its claws.

___Bite.___ Melee Weapon Attack: +10 to hit, reach 10 ft., one target. Hit: 17 (2d10 + 6) piercing damage plus 3 (1d6) necrotic damage.

___Claw.___ Melee Weapon Attack: +10 to hit, reach 5 ft., one target. Hit: 13 (2d6 + 6) slashing damage.

___Shadow Breath (Recharge 5-6).___ The dragon exhales shadowy fire in a 30-foot cone. Each creature in that area must make a DC 18 Dexterity saving throw, taking 56 (16d6) necrotic damage on a failed save, or half as much damage on a successful one. A humanoid reduced to 0 hit points by this damage dies, and an undead shadow rises from its corpse and acts immediately after the dragon in the initiative count. The shadow is under the dragon's control.