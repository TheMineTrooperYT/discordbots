"Carrion Crawler";;;_size_: Large monstrosity
_alignment_: unaligned
_challenge_: "2 (450 XP)"
_senses_: "darkvision 60 ft."
_skills_: "Perception +3"
_speed_: "30 ft., climb 30 ft."
_hit points_: "51 (6d10+18)"
_armor class_: "13 (natural armor)"
_stats_: | 14 (+2) | 13 (+1) | 16 (+3) | 1 (-5) | 12 (+1) | 5 (-3) |

___Keen Smell.___ The carrion crawler has advantage on Wisdom (Perception) checks that rely on smell.

___Spider Climb.___ The carrion crawler can climb difficult surfaces, including upside down on ceilings, without needing to make an ability check.

**Actions**

___Multiattack.___ The carrion crawler makes two attacks: one with its tentacles and one with its bite.

___Tentacles.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one creature. Hit: 4 (1d4 + 2) poison damage, and the target must succeed on a DC 13 Constitution saving throw or be poisoned for 1 minute. Until this poison ends, the target is paralyzed. The target can repeat the saving throw at the end of each of its turns, ending the poison on itself on a success.

___Bite.___ Melee Weapon Attack: +4 to hit, reach 5 ft., one target. Hit: 7 (2d4 + 2) piercing damage.