"Reliquary Guardian";;;_size_: Large construct
_alignment_: lawful evil
_challenge_: "12 (8,400 XP)"
_languages_: "speaks and understands the languages of its creator"
_skills_: "Perception +7"
_senses_: "darkvision 60 ft., passive Perception 17"
_damage_immunities_: "bludgeoning, piercing, and slashing from nonmagical weapons that aren’t adamantine"
_condition_immunities_: "charmed, exhaustion, frightened, paralyzed, petrified, poisoned"
_speed_: "30 ft., fly 60 ft."
_hit points_: "170 (20d10 + 60)"
_armor class_: "19 (natural armor)"
_stats_: | 19 (+4) | 12 (+1) | 17 (+3) | 10 (+0) | 17 (+3) | 21 (+5) |

___Divine Blessing.___ The reliquary guardian deals an additional 14 (4d6)
damage with its weapon attacks, included below. This damage is either
radiant, if lawfully aligned, or necrotic, if chaotically aligned.

___Immutable Form.___ The reliquary guardian is immune to any spell or
effect that would alter its form.

___Innate Spellcasting.___ The reliquary guardian’s innate spellcasting ability
is Charisma (spell save DC 17, +9 to hit with spell attacks). It can innately
cast the following spells, requiring no material components:

* At will: _chill touch _(17th level, if chaotically aligned)_, protection from
evil and good, sacred flame _(17th level, if lawfully aligned)

* 3/day each: _lightning bolt, flame strike _(deals necrotic instead of radiant
if chaotically aligned)_, spirit guardians, spiritual weapon_

* 1/day each: _commune, dispel evil and good, dispel magic_

___Magic Resistance.___ The reliquary guardian has advantage on saving
throws against spells and other magical effects.

___Magic Weapons.___ The reliquary guardian’s weapon attacks are magical.

**Actions**

___Multiattack.___ The reliquary guardian makes two attacks with either its
greatsword or its slam attack.

___Greatsword.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target.
Hit: 18 (4d6 + 4) slashing damage and 14 (4d6) necrotic or radiant damage.

___Slam.___ Melee Weapon Attack: +8 to hit, reach 10 ft., one target. Hit: 22
(4d8 + 4) bludgeoning damage and 14 (4d6) necrotic or radiant damage.

___Pronouncement (1/day).___ All creatures within 120 feet of the reliquary
guardian that can hear it must succeed on a DC 17 Wisdom saving throw.
On a failed saving throw, the target takes 66 (12d10) thunder damage and
is stunned for 1 minute. On a successful saving throw, the target takes half
damage and is not stunned. A stunned creature can repeat the saving throw
at the end of each of its turns, ending the effect on a success.
