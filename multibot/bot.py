# system imports
import os
import traceback

# discord imports
import discord
from discord import Color, Message
from discord.ext import commands
# cogs
import cogs.utils.utils as utils
import generators.generators as generators
import generators.gens as gens
# import Oracle.oracle as oracle
from importlib import reload
import discord.ext.commands.errors as errors
from discord.ext.commands import Context

DESCRIPTION = """
A self-hosted music discord bot, with detailed
documentation and constantly upgrading features.
"""


class MultiBot(commands.Bot):
    """ MultiBot discord bot, basic bot with many cogs """

    def __init__(self):
        intents = discord.Intents.all()  # set default intents
        intents.members = True  # add members
        super().__init__(
            command_prefix=utils.prefix,
            description=DESCRIPTION,
            heartbeat_timeout=150.0,
            intents=intents,
            help_command=None,
        )  # init commands.Bot
        # database.create_tables()  # init DB
        self.config = utils.config

    async def _load_extensions(self) -> None:
        """ Loads all of the available cogs into the bot """
        cogs_dir = os.listdir("cogs")  # get dir
        for filename in cogs_dir:  # run on files
            if filename.endswith(".py") and not filename.startswith("__"):  # if its a cog
                cog = filename[:-3]  # remove extension
                try:
                    # try to load the cog
                    t = self.load_extension(f"cogs.{cog}")
                    if t is not None:
                        await t
                except Exception as e:  # if failed,
                    traceback.print_exc()  # print the traceback

    async def reload_cogs(self):
        """ unloads and reloads all of the cogs, useful for adding a new cog withought shutting down the bot """
        print("Reloading...\nunloading extensions...")
        for ext in self.extensions.copy():  # run on loaded extensions
            print("unloading", ext)
            await self.unload_extension(ext)  # unload
        print("Done.\nReimporting Modules...")
        reload(utils)
        reload(generators)
        generators.Generator().load_cfg(self.config.generator_config)
        reload(gens)
        print("Done.\nReloading all extensions...")
        await self._load_extensions()  # reload
        print("Done.")

    last_message = None
    last_ctx = None

    async def on_message(self, message: Message | str, console: bool = False):
        """ an event called when a message is received """
        # print(message.content)
        # if the message has a mention of the bot
        # print("Message: ",message.content)

        if not console and self.user.mentioned_in(message):
            ctx = message.channel  # get the context of the msg

            embed = discord.Embed(
                title="MultiBot - The bot of many things",
                description=f"General bot info:",
                color=Color.teal()
            )  # create a new embed

            cogs = [cog for cog in self.cogs]  # copy cogs
            en_cogs = [cog for cog in cogs if utils.cog_enabled(
                cog, message.channel.guild)]  # get enabled cogs
            dis_cogs = [cog for cog in cogs if not utils.cog_enabled(
                cog, message.channel.guild)]  # get disabled cogs

            for n, v in [("Prefix:", f"``{self.command_prefix}``"),
                         ("Available Cogs:",
                          (f"``{', '.join(en_cogs)}``" if len(en_cogs) > 0 else "None.")),
                         ("Cogs Disabled in this Server:",
                          (f"``{', '.join(dis_cogs)}``" if len(dis_cogs) > 0 else "None.")),
                         ("Help Commands:", f"Run ``{self.command_prefix}h`` to get general help about all the cogs, or ``{self.command_prefix}h <cog name>`` to get cog-specifc help, and ``{self.command_prefix}h <cog name> <command>`` for command specific help.")]:
                embed.add_field(
                    name=n, value=v
                )  # add the different fields to the embed item

            embed.add_field(
                name="Developer:", value="Thaldir o Aharonyn", inline=False
            )  # add the end field

            await ctx.send(embed=embed)  # send the message
            await message.delete()
        # if the message is a reboot command from me
        elif not console and message.content == f"{self.command_prefix}reload" and (await self.is_owner(message.author)) or console and message == f"{self.command_prefix}reboot":
            check = type(message) != str
            if check:
                await message.channel.send("**Reloading...**")
            await self.reload_cogs()  # reboot
            if check:
                await message.channel.send("**Done.**")
                await message.delete()
        else:

            if not console and message.content == "..":  # if message is "repeat last command"
                if self.last_message is None:
                    return await utils.error(message.channel, "Repeat Last Command Error", "Tried to call last command, but no last command is saved.")
                else:
                    last_m = await message.channel.send(self.last_message)
                    auth = message.author
                    await message.delete()
                    message = last_m
                    message.author = auth

                    ctx = await self.get_context(message, cls=Context)
                    if ctx.valid:
                        return await self.invoke(ctx)
                    else:
                        return await utils.error(message.channel, "Repeat Last Command Error", "Received Invalid Context From self.get_context on the Message!")
            elif not console and message.author != self.user:
                self.last_message = message.content

            # wait for the command processing
            await self.process_commands(message)

    async def on_command(self, ctx: Context):
        # delete author's msg on command
        await ctx.message.delete()

    async def on_command_error(self, ctx: Context, exc):
        # # on command error the on_command isnt called, so delete author's msg
        # await ctx.message.delete()
        # send error msg
        traceback.print_exception(exc)
        await utils.error(ctx, exc)
