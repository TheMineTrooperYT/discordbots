import discord
from discord.ext import commands, tasks

import sys
import os
import json
import time
import datetime

from cogs.utils.utils import *


class test(commands.Cog):

    def __init__(self, bot):
        self.bot = self.client = bot

    @commands.command(
        name="echo"
    )
    async def echo(self, ctx: commands.Context, *args):
        emojis = [
            '\U0001f973'  # face
            '\U0001f389'  # pop-stick-thingy
            '\U0001f38a'  # confetty
            '\U0001f382'  # cake
        ]
        await ctx.send(t := "\n".join(args))
        print(t)


import platform
cog=test
# match platform.system():
#     case "Linux" | "Darwin":
#         def setup(bot):
#             bot.add_cog(cog(bot))
#     case "Windows":


async def setup(bot):
    t = bot.add_cog(cog(bot))
    if t is not None:
        await t