import platform
import discord
from discord.ext import commands

import cogs.utils.utils as utils
from difflib import get_close_matches


class help(commands.Cog):
    """Cog for the help command."""

    def __init__(self, bot):
        self.bot = bot
        self.bot_cogs = self.bot.cogs

    @commands.command(name="help", aliases=["h"])
    async def _help(self, ctx: commands.Context, cog_query=None, command=None):
        """Returns all not hidden commands from a cog."""

        if cog_query is None:
            cogs = utils.get_active_cogs(self.bot, ctx.guild)
            embed = discord.Embed(
                title="MultiBot",
                description="The bot of many things.",
                color=discord.Color.orange()
            )
            embed.add_field(
                name='Active cogs in this server:',
                value=f"``{', '.join(cogs)}``" if len(cogs) > 0 else "``None``"
            )
            embed.add_field(
                name="Extra Info:", value="Another useful command is '..', which calls the last command again.")
            embed.add_field(
                name="Developer:", value="Thaldir o Aharonyn", inline=False
            )
            return await ctx.send(embed=embed)

        try:
            cog = self.bot_cogs[cog_query]
        except KeyError:
            query_error_embed = discord.Embed(
                title="Query Error. Perhaps you meant:",
                description=f"{', '.join(get_close_matches(cog_query,list(self.bot_cogs.keys())))}.",
                color=utils.embed_colors.error.value,
            )
            return await ctx.send(embed=query_error_embed)

        commands = cog.get_commands()

        if command == None:
            cog_help_embed = discord.Embed(
                title=f"Bot Commands",
                color=discord.Color.orange(),
            )

            for command in commands:

                command_aliases = (
                    f"{', '.join(command.aliases)}" if len(
                        command.aliases) > 0 else "None"
                )
                cog_help_embed.add_field(
                    name=(
                        f"{self.bot.command_prefix}{command.name} , {command_aliases}")
                    if (command_aliases != "None")
                    else (f"{self.bot.command_prefix}{command.name}"),
                    value=f"`{command.help}`",
                    inline=False,
                )
        elif command is not None and (command in (c_names := [c.name for c in commands]) or any([command in c.aliases for c in commands])):
            for c in commands:
                if c.name == command or command in c.aliases:
                    command = c
                    break

            cog_help_embed = discord.Embed(
                title=f"Bot command help for command {cog_query}.{command}:",
                color=discord.Color.orange()
            )

            command_aliases = (
                f" , {', '.join(command.aliases)}" if len(
                    command.aliases) > 0 else ""
            )
            brief = '\n``'+command.brief+'``' if command.brief is not None else ''
            cog_help_embed.add_field(
                name=(f"{self.bot.command_prefix}{command.name}{command_aliases}"),
                value=f"Usage: {command.usage}\nDescription: ``{command.help}``{brief}",
                inline=False
            )

        elif command is not None and command not in c_names:
            query_error_embed = discord.Embed(
                title=f"Query Error. Command not found in cog {cog_query}, prehaps you meant:",
                description=f"{', '.join(get_close_matches(command,c_names))}.",
                color=utils.embed_colors.error.value,
            )
            return await ctx.send(embed=query_error_embed)
        else:
            query_error_embed = discord.Embed(
                title=f"Unrecognized Error. Commands in cog {cog_query}:",
                description=f"{', '.join(c_names)}.",
                color=utils.embed_colors.error.value,
            )
            return await ctx.send(embed=query_error_embed)

        await ctx.send(embed=cog_help_embed)


cog = help
# match platform.system():
#     case "Linux" | "Darwin":
#         def setup(bot):
#             bot.add_cog(cog(bot))
#     case "Windows":


async def setup(bot):
    t = bot.add_cog(cog(bot))
    if t is not None:
        await t