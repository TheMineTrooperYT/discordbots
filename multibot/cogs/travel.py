import platform
from os import listdir
from ast import alias
import discord
from discord.ext import commands
import d20
import json

import cogs.utils.utils as utils
import cogs.oracle as oracle
import generators.generators as generators
from bot import MultiBot
from generators.generators import Generator
import cogs.utils.utils as utils
oracle = oracle.Oracle_interface
gen: Generator = generators.Generator()


wildemount_maps = [d.split(".")[0] for d in listdir("assets/wildemount-maps")]


class travel(commands.Cog):
    def __init__(self, bot: MultiBot):
        with open("assets/wildemount.json", "r") as f:
            self.wmg = json.loads(f.read())
        self.bot = bot

    @commands.command(
        name="wildemount-map",
        help="Sends the requested Wildemount map section in chat. Maps: " +
        ", ".join([m.replace("-", " ") for m in wildemount_maps]),
        aliases=["wmm"]
    )
    @utils.validate_cog("travel")
    async def wildemount(self, ctx: commands.Context, *map_name):
        file = ""
        if len(map_name) == 0:
            await ctx.send(embed=utils.create_embed("_Available Maps_", "Maps: _"+"_, _".join([f"{i+1}. "+m.replace("-", " ") for i, m in enumerate(wildemount_maps)]) + "_"))
            return
        if len(map_name) == 1:
            if utils.isint(map_name[0]) and (i := int(map_name[0])) > 0 and i <= len(wildemount_maps):
                file = wildemount_maps[i-1]
            else:
                await utils.error(ctx, "**Invalid Input**", "Given map index, '%s', isn't valid." % map_name[0])
                return
        elif len(map_name) != 1:
            map_name = "-".join([m.lower() for m in map_name])
            if map_name in wildemount_maps:
                file = map_name
            else:
                await utils.error(ctx, "**Invalid Input**", "Given map name, '%s', doesn't exist." % map_name.replace("-", " "))
                return

        msg: discord.Message = await ctx.send("Sending...")
        await ctx.send(file=discord.File("assets/wildemount-maps/%s.png" % file))
        await msg.delete()

    @commands.command(
        name="wildemount-gazetteer",
        help="An interface with wildemount gazetteer.",
        aliases=["wmg"],
        usage="<prefix>wildemount-gazetteer <area> <place>\nIf no place given, it lists all the places in the area.\nIf no are is given, it lists all the areas in Wildemount."
    )
    @utils.validate_cog("travel")
    async def wildemount_gazetteer(self, ctx: commands.Context, *args):
        if len(args) == 0:  # if no input
            # print all areas
            await ctx.send(embed=utils.create_embed("**Wildemount Gazetteer**", ", ".join([f"{i+1}. _{k}_" for i, k in enumerate(self.wmg.keys())])))
            return  # exit

        k = args[0].lower()  # get key
        key = ""  # res key
        keys = [i.lower() for i in self.wmg.keys()]  # lowered area keys
        if utils.isint(k):  # if k is a number
            k = int(k)-1  # get index
            if k < len(keys) and k >= 0:  # if in range
                key = list(self.wmg.keys())[k]  # get key
        elif k in keys:  # if key in lowered keys
            key = list(self.wmg.keys())[keys.index(k)]  # get key by index
        else:  # if any other input
            # error
            await utils.error(ctx, "**Invalid Input**", "Given gazetteer area, %s, doesn't exist." % k)
            return

        area_keys = self.wmg[key]  # keys of the area

        if len(args) == 1:  # if only 1 input
            keys = self.wmg[key]  # get keys
            # sends the keys
            await ctx.send(embed=utils.create_embed("**Wildemount Gazetteer**", f"__{key}:__\n"+", ".join([f"{i+1}. _{k}_" for i, k in enumerate(keys)])))
            return

        k = args[1].lower()  # get other args
        value = ""  # value index
        keys = [i.lower() for i in area_keys.keys()]  # lowered area keys
        if utils.isint(k):  # if integer
            k = int(k)-1  # get index
            if k < len(keys) and k >= 0:  # if in range
                value = list(area_keys.keys())[k]  # get value key by index
        elif k in keys:  # if key in lowered keys
            value = list(area_keys.keys())[keys.index(k)]  # get keyname
        else:  # if anything else
            # error
            await utils.error(ctx, "**Invalid Input**", "Given gazetteer area, %s, doesn't exist." % k)
            return

        data = area_keys[value]  # get data

        embeds = utils.long_embed(
            "Wildemount Gazetteer: " + value, data[0], data[1:])

        for e in embeds:
            await ctx.send(embed=e)

    @commands.command(
        name="settlement",
        help="Randomzies general ingo about a settlement (DMG 112)",
        aliases=["stlmnt"]
    )
    @utils.validate_cog("travel")
    async def settlement(self, ctx: commands.Context):
        # roll tables. many of them.
        races = ["Harmony", "Harmony", "Harmony", "Harmony", "Harmony", "Harmony", "Harmony", "Harmony", "Harmony", "Harmony", "Tension or rivalry", "Tension or rivalry", "Tension or rivalry", "Tension or rivalry","Racial majority are conquerors", "Racial majority are conquerors", "Racial minority are rulers", "Racial minority are refugees", "Racial majority oppresses minority", "Racial minority oppresses majority"][utils.rindex(20)]
        ruler = ["Respected, fair, and just", "Respected, fair, and just", "Respected, fair, and just", "Respected, fair, and just", "Respected, fair, and just", "Feared tyrant", "Feared tyrant", "Feared tyrant", "Weakling manipulated by others", "Ill egitimate ruler, simme ring civil war","Ruled or controlled by a powerful monster", "Mysterious, anonymous cabal", "Contested leadership, open fighting", "Cabal se ized power openly", "Doltish lout", "On deathbed , claimants compete for power", "I ron-willed but respected", "I ron-willed but respected", "Religious leader", "Religious leader"][utils.rindex(20)]
        traits = ["Canals in place of streets", "Massive statue or monument", "Grand temple", "Large fortress", "Verdant parks and orchards", "River divides town", "Major trade center", "Headquarters of a powerful family or guild", "Population mostly wealthy", "Destitute, rundown","Awful smell (tanneries, open sewers)", "Center of trade for one specific good", "Site of many battles", "Site of a mythic or magical event", "Important library or archive", "Worship of all gods banned", "Sinister reputation", "Notable library or academy", "Site of important tomb or graveyard", "Built atop ancient ruins"][utils.rindex(20)]
        known_for = ["Delicious cuisine ", "Rude people ", "Greedy merchants ", "Artists and writers ", "Great herojsavior ", "Flowers ", "Hordes of beggars ", "Tough warriors ", "Dark magic ","Decadence ", "Piety", "Gambling", "Godlessness", "Education", "Wines", "High fashion", "Political intrigue", "Powerful guilds", "Strong drink", "Patriotism"][utils.rindex(20)]
        calamity = ["Suspected vampire infestation", "New cult seeks converts", "Important figure died (murder suspected)", "War between rival thieves' guild s", "Plague or famine (sparks riots)", "Plague or famine (sparks riots)", "Corrupt officials", "Marauding monsters", "Marauding monsters", "Powerful wizard has moved into town","Economic depression (trade disrupted)", "Flooding", "Undead stirring in cemeteries", "Prophecy of doom", "Brink of war", "Internal stri fe (leads to anarchy)", "Besieged by enemies", "Scandal threatens powerful families", "Dungeon discovered (adventurers flock to town)", "Religious sects struggle for power"][utils.rindex(20)]

        await ctx.send(embed=utils.create_embed("Settlement info", "", [
            ("Race Dynamics:", races, True),
            ("Ruler's Status:", ruler, True),
            ("Notable Traits:", traits, True),
            ("Known For...", known_for, True),
            ("Current Calamity:", calamity, True)
        ]))

    @commands.command(
        name="random-building",
        help="Generates a random building for a settlement",
        aliases=["rbuild"]
    )
    @utils.validate_cog("travel")
    async def rbuild(self, ctx: commands.Context):
        # tables all day long...
        def warehouse(): return [
            "Empty or abandoned warehouse",
            "Empty or abandoned warehouse",
            "Empty or abandoned warehouse",
            "Empty or abandoned warehouse",
            "Heavily guarded warehouse, expensive goods",
            "Heavily guarded warehouse, expensive goods",
            "Warehouse of cheap goods",
            "Warehouse of cheap goods",
            "Warehouse of cheap goods",
            "Warehouse of cheap goods",
            "Warehouse of bulk goods",
            "Warehouse of bulk goods",
            "Warehouse of bulk goods",
            "Warehouse of bulk goods",
            "Warehouse of live animals",
            "Warehouse of weapons & armor",
            "Warehouse of weapons & armor",
            "Warehouse of goods from a distant land",
            "Warehouse of goods from a distant land",
            "Warehouse that's a secret smuggler's den"][utils.rindex(20)]

        def residence(): return [
            "An abandoned squat",
            "An abandoned squat",
            "A middle-class home",
            "A middle-class home",
            "A middle-class home",
            "A middle-class home",
            "A middle-class home",
            "A middle-class home",
            "An upper-class home",
            "An upper-class home",
            "A crowded tenement",
            "A crowded tenement",
            "A crowded tenement",
            "A crowded tenement",
            "A crowded tenement",
            "An orphanage",
            "An orphanage",
            "A hidden slavers' den",
            "A front for a secret cult",
            "A lavish, guarded mansion"
        ][utils.rindex(20)]

        def religious(): return [
            "Temple to a good or neutral deity",
            "Temple to a good or neutral deity",
            "Temple to a good or neutral deity",
            "Temple to a good or neutral deity",
            "Temple to a good or neutral deity",
            "Temple to a good or neutral deity",
            "Temple to a good or neutral deity",
            "Temple to a good or neutral deity",
            "Temple to a good or neutral deity",
            "Temple to a good or neutral deity",
            "Temple to a false deity (run by charlatan priests)", "Temple to a false deity (run by charlatan priests)", "Home of ascetics", "Abandoned shrine", "Abandoned shrine", "Library dedicated to religious study", "Library dedicated to religious study", "Hidden shrine to a fiend or an evil deity", "Hidden shrine to a fiend or an evil deity", "Hidden shrine to a fiend or an evil deity"][utils.rindex(20)]

        def tavern(): return "A tavern (call <prefix>random-tavern command for tavern info)"
        def shop(): return "A shop (call <prefix>generate-shops to see avalble shops)"
        building = [
            residence,
            residence,
            residence,
            residence,
            residence,
            residence,
            residence,
            residence,
            residence,
            residence,
            religious,
            religious,
            tavern,
            tavern,
            tavern,
            warehouse,
            warehouse,
            shop,
            shop,
            shop,
        ][utils.rindex(20)]()

        if ctx is not None:
            await ctx.send(embed=utils.create_embed("Randonm Building", building))
        return building

    @commands.command(
        name="random-tavern",
        help="Generates a random tavern",
        aliases=["rtavern"]
    )
    @utils.validate_cog("travel")
    async def random_tavern(self, ctx: commands.Context):
        def tavern_name(): return ["The Silver", "The Golden", "The Staggering", "The Laughing", "The Prancing", "The Gilded", "The Running", "The Howling", "The Slaughtered", "The Leering", "The Drunken", "The Leaping", "The Roaring", "The Frowning", "The Lonely", "The Wandering", "The Mysterious", "The Barking", "The Black", "The Gleaming"
                                   ][utils.rindex(20)] + " " + ["Eel", "Dolphin", "Dwarf", "Pegasus", "Pony", "Rose", "Stag", "Wolf", "Lamb", "Demon", "Goat", "Spirit", "Horde", "jester", "Mountain", "Eagle", "Satyr", "Dog", "Spider", "Star"
                                                                ][utils.rindex(20)]  # name randomizer
        # tavern_info = [
        #     [("Quality:","Atrocious",True), ("Bartender:","Male gnome",True), ("Costumer Service","Unfriendly", True)],
        #     [("Rooms:","1 room",True), ("Quality:","Poor",True), ("Bartender:","Female halfling",True), ("Rumours:","1",True), ("Costumer Service","Neutral",True)],
        #     [("Rooms:","2 rooms",True), ("Quality:","Poor",True), ("Bartender:","Male dwarf",True), ("Rumours:","1",True), ("Costumer Service","Neutral",True)],
        #     [("Rooms:","3 rooms",True), ("Quality:","Average",True), ("Bartender:","Tiefling",True), ("Rumours:","2",True), ("Costumer Service","Civil",True)],
        #     [("Rooms:","4 rooms",True), ("Quality:","Average",True), ("Bartender:","Human male",True), ("Rumours:","2",True), ("Costumer Service","Civil",True)],
        #     [("Rooms:","5 rooms",True), ("Quality:","Average",True), ("Bartender:","Human female",True), ("Rumours:","2",True), ("Costumer Service","Cordial",True)],
        #     [("Rooms:","6 rooms",True), ("Quality:","Good",True), ("Bartender:","Male Halfling",True), ("Rumours:","3",True), ("Costumer Service","Cordial",True)],
        #     [("Rooms:","8 rooms",True), ("Quality:","Good",True), ("Bartender:","Female gnome",True), ("Rumours:","3",True), ("Costumer Service","Warm & Welcoming",True)],
        #     [("Rooms:","10 rooms",True), ("Quality:","Excellent",True), ("Bartender:","Orc or Half-orc",True), ("Rumours:","4",True), ("Costumer Service","Warm & Welcoming",True)],
        #     [("Rooms:","20 rooms",True), ("Quality:","Outstanding",True), ("Bartender:","Player’s Choice",True), ("Rumours:","4",True), ("Costumer Service","Treated like a monarch!",True)],
        # ]

        def tavern(): return tavern_name() + ", " + [
            "a quiet, low-key bar",
            "a quiet, low-key bar",
            "a quiet, low-key bar",
            "a quiet, low-key bar",
            "a quiet, low-key bar",
            "a raucous dive",
            "a raucous dive",
            "a raucous dive",
            "a raucous dive",
            "a thieves' guild hangout",
            "a gathering place for a secret society",
            "an upper-class dining club",
            "an upper-class dining club",
            "a gambling den",
            "a gambling den",
            "a caters to specific race or guild",
            "a caters to specific race or guild",
            "a members-only club",
            "a brothel",
            "a brothel"][utils.rindex(20)]  # tavern type random table

        # Rumour tables
        # craetures table
        def creature(): return ["Monster!", "Bear", "Cat", "Dire Wolf", "Dog", "Draft Horse", "Eagle", "Elephant", "Elk", "Flying Snake", "Ape", "Giant Ape", "Giant Badger", "Giant Boar", "Giant Eagle", "Giant Elk", "Giant Fire Beetle", "Giant Frog", "Giant Lizard", "Giant Owl",
                                "Giant Rat", "Giant Spider", "Goat", "Hawk", "Mastiff", "Mule", "Owl", "Riding Horse", "Panther", "Poisonous Snake", "Pony", "Rat", "Raven", "Swarm of Insects", "Swarm of rats", "Swarm of ravens", "Vulture", "Weasel", "Wolf"][utils.rindex(20)+utils.rindex(20)]
        # places table

        def place(): return utils.roll_dist([
            lambda: "In this settlement / area",
            lambda: "Just outside settlement",
            lambda: f"{utils.rindex(4)+1} miles away, in a structure {'SAT Chap 9 Structure Table - not implemented yet.'}",
            lambda: "In the nearest forest",
            lambda: "In the nearest hills",
            lambda: "In the nearest mountains",
            lambda: "In the nearest swamp",
            lambda: "In / beside the nearest body of water",
            lambda: "In the next village",
            lambda: "In the next large town",
            lambda: "In the capital of the realm",
            lambda: "In the next realm",
        ], [5, 5, 4, 4, 3, 2, 2, 2, 4, 4, 4, 4, 1])()

        # verbs shortcut
        def verbs(i): return lambda: ", ".join(
            [oracle.focus.verb() for _ in range(i)])
        # rumours table
        rumours = [
            lambda: gen.generate("npc", ["", "", "", ""], 1)[
                0] + "; " + verbs(2)(),
            lambda: creature()+", "+verbs(2)(),
            lambda: place()+", "+verbs(2)(),
            verbs(4)
        ]
        # rumours count roll
        rs = utils.roll_dist([0, 1, 2, 3, 4], [1, 2, 3, 2, 2])
        arr = []  # arr
        for r in range(rs):  # run on rumour count
            # add rumour
            arr.append(
                f"_{f'#{r+1} ' if rs > 1 else ''}Rumour's Subject:_ {rumours[utils.rindex(4)]()}")
        arr = "\n".join(arr)  # join rumours
        tavern = tavern()
        if ctx is not None:
            em = utils.create_embed("Generated Tavern", tavern, [] if rs == 0 else [
                                    ("Rumours:", arr, True)])  # create embed
            await ctx.send(embed=em)  # send
        return (tavern, arr)

    @commands.command(
        name="random-shops",
        help="Given settlement size, generates list of shops in the settlement and their quality.",
        aliases=["rshops"]
    )
    @utils.validate_cog("travel")
    async def random_shops(self, ctx: commands.Context, town_size: str):
        sizes = [
            "settlement", "hamlet", "village", "town", "city", "metropolis"
        ]  # the different town sizes
        if utils.isint(town_size):  # if given arg is a number
            town_size = int(town_size) - 1  # convert to index
            if town_size >= len(sizes) or town_size < 0:  # if out of range
                # error
                await utils.error(ctx, "**Input Error**", f"Given town_size index was out of range!", [("town_size:", town_size+1), ("range:", f"[1-{len(sizes)}]")])
                return
        elif town_size.lower() in sizes:  # if in defined sizes
            town_size = sizes.index(town_size.lower())  # get the index
        else:  # not in sizes and not a number
            # error
            await utils.error(ctx, "**Input Error**", f"Given town_size isn't defined!", [("town_size:", town_size)])
            return

        def roll(l: list):
            """ helper function for rolling against the shop's DC """
            i = l[town_size]  # get DC for the townsize
            if i == -1:  # if not possible (university in a settlement)
                return False  # false
            return utils.rindex(20) >= i  # roll against the DC

        q_names = [
            "Atrocious",
            "Poor",
            "Medium",
            "Good",
            "Excellent",
        ]  # quality names
        table = [[7, 4, 4, 3, 2, ], [6, 4, 4, 3, 3, ], [5, 4, 4, 4, 3, ], [
            4, 4, 4, 4, 4, ], [3, 4, 4, 4, 5, ], [2, 3, 4, 5, 6, ]]  # distribution table for the qulities - per town_size

        # for each shop, roll against its DC table using the roll function
        # if passed, return shop, failed, return None
        # filter out the Nones out, using the outer for loop
        # roll a quality value and add to the shop name - in the outer for loop
        # join the entire thing with \n's
        shops = "\n".join([i + f", _{utils.roll_dist(q_names,table[town_size])}_ quality." for i in [
            "Inn / Tavern" if roll([10, 8, 6, 4, 2, 1]) else None,
            "Adventuring Supplies" if roll([17, 14, 10, 5, 2, 1]) else None,
            "Animals and Mounts" if roll([12, 11, 9, 7, 5, 3]) else None,
            "Books and Maps" if roll([18, 17, 15, 13, 11, 9]) else None,
            "Jewelry and Gem trader" if roll(
                [20, 19, 18, 16, 14, 12]) else None,
            "Armourer" if roll([18, 16, 14, 12, 10, 8]) else None,
            "Bank" if roll([17, 15, 13, 10, 8, 6]) else None,
            "Tinkerer / Finesmith" if roll([18, 17, 15, 13, 11, 9]) else None,
            "Tailor" if roll([15, 13, 11, 10, 8, 6]) else None,
            "Potions, poisons, herbs" if roll(
                [18, 17, 16, 14, 12, 10]) else None,
            "Religious idols & Blessings" if roll(
                [16, 15, 13, 11, 9, 7]) else None,
            "Food & drink seller" if roll([14, 12, 10, 8, 6, 4]) else None,
            "Temple (also rituals, funerals, name-giving, weddings)" if roll(
                [16, 14, 12, 10, 8, 6]) else None,
            "Spell tomes and scrolls" if roll(
                [20, 19, 17, 15, 13, 11]) else None,
            "Thieving supplies" if roll([19, 18, 16, 14, 12, 10]) else None,
            "Weapons Shop" if roll([17, 15, 13, 11, 9, 7]) else None,
            "Vehicles and transportation" if roll(
                [15, 12, 13, 10, 8, 6]) else None,
            "Adventurer’s Guild" if roll([19, 18, 16, 14, 12, 10]) else None,
            "Magic Items" if roll([25, 22, 19, 16, 15, 14]) else None,
            "Blacksmith" if roll([12, 10, 8, 6, 4, 2]) else None,
            "Necromancy / Resurrection" if roll(
                [20, 19, 18, 17, 16, 16]) else None,
            "Couriers" if roll([19, 18, 17, 15, 13, 11]) else None,
            "Brothel" if roll([17, 15, 13, 11, 9, 7]) else None,
            "Land Sales" if roll([16, 15, 13, 10, 8, 6]) else None,
            "Carpenter / Cooper / Cartwright" if roll(
                [15, 13, 10, 8, 6, 4]) else None,
            "Entertainer’s Guild" if roll([20, 18, 16, 13, 11, 9]) else None,
            "Healer / Physician" if roll([18, 15, 12, 10, 7, 4]) else None,
            "Shipping Contracts / Boatbuilding (must be coastal)" if roll(
                [19, 18, 16, 14, 12, 10]) else None,
            "Worker’s Union (any type)" if roll(
                [19, 18, 16, 11, 9, 7]) else None,
            "Stonemason" if roll([18, 15, 12, 10, 7, 4]) else None,
            "University" if roll([1, 1, 18, 16, 14, 12]) else None,
            "Mercenaries" if roll([19, 18, 17, 16, 14, 12]) else None] if i is not None])
        # send the entire thing
        if ctx is not None:
            await ctx.send(embed=utils.create_embed("Randomized shops for a %s" % sizes[town_size], shops))
        return shops

    @commands.command(
        name="get-item",
        help="Returns a table showing what quality of item is found based on the merchant's quality.",
        aliases=["geti"]
    )
    @utils.validate_cog("travel")
    async def get_item(self, ctx: commands.Context):
        item_q = [
            "Common",
            "Uncommon",
            "Rare",
            "Legendary"
        ]

        tables = [
            [14, 18, 20, 99],
            [12, 17, 19, 99],
            [10, 15, 18, 20],
            [8, 13, 17, 19],
            [6, 12, 16, 18]
        ]

        r = utils.roll(20)

        def comp(l):
            # print(r, l)
            for i, v in enumerate(l):
                if r >= v:
                    continue
                else:
                    break
            return utils.clamp(i-1, 0, 3)

        #  print("rolled ", r)
        res = [
            ("Atrocious shop:", item_q[comp(
                [14, 18, 20, 99])] + " and lower quality item/s.", False),
            ("Poor shop:", item_q[comp([12, 17, 19, 99])
                                  ] + " and lower quality item/s.", False),
            ("Medium shop:", item_q[comp([10, 15, 18, 20])
                                    ] + " and lower quality item/s.", False),
            ("Good shop:", item_q[comp([8, 13, 17, 19])
                                  ] + " and lower quality item/s.", False),
            ("Excellent shop:", item_q[comp(
                [6, 12, 16, 18])] + " and lower quality item/s.", False),
        ]
        if ctx is not None:
            await ctx.send(embed=utils.create_embed("Available Items", "Item rarities available per shop quality", ))
        return res

    @commands.command(
        name="urban-encounter",
        help="Generates a random urban encounter.",
        aliases=["urbanenc"]
    )
    @utils.validate_cog("travel")
    async def urban_enc(self, ctx: commands.Context):
        # long story short: Roll tables.
        def no_enc(): return "Nothing out of the ordinery"

        def enc(): return [
            "Animals on the loose",
            "An announcement",
            "A Brawl",
            "Bullies",
            "A companion",
            "A contest",
            "A corpse",
            "Draft",
            "A drunkard",
            "A fire",
            "Found a trinket",
            "Guard harassment",
            "Pick pocket",
            "Procession",
            "A protest",
            "A runaway cart",
            "a Shady transaction",
            "A spectacle",
            "An urchin",
        ][utils.rindex(8)+utils.rindex(12)]

        table = utils.roll_dist([no_enc, enc], [3, 20])()
        if ctx is not None:
            await ctx.send(embed=utils.create_embed("Random Urban Encounter", table))
        return table

    @commands.command(
        name="wild-sights",
        help="Generates some random sights to be seen in the wilderness.",
        aliases=["wilds"]
    )
    @utils.validate_cog("travel")
    async def wild_sights(self, ctx: commands.Context):
        def encounters(): return ("Encounter:", utils.roll_dist([
            "Weather change (re-roll weather)",
            "Herd of passive animals",
            "A few passive animals",
            "Messenger on a horse quickly passing by",
            "Migrating flock birds",
            "Giant flying creature (bird/dragon/bat/etc.) flying far above",
            "Merchant caravan",
            "Marching Army",
            "Army Encampement",
            "Bandits (night)",
            "Hostile beast/s",
            "Monsterous hunting party (depends on monsterous population)",
            "Monster encounter"
        ], [4, 5, 5, 2, 5, 4, 2, 1, 1, 2, 2, 1, 3]), False)

        def monuments(): return ("Monument (10% monster lair [1/~50mil²], 20% dungeon):", [
            "Sealed burial mound or pyramid",
            "Plundered burial mound or pyramid",
            "Faces carved into a mountainside or cliff",
            "Giant statues carved out of a mountainside or cliff",
            "Intact obelisk etched with a warning, historical lore, dedication, or religious iconography",
            "Intact obelisk etched with a warning, historical lore, dedication, or religious iconography",
            "Ruined or toppled obelisk",
            "Ruined or toppled obelisk",
            "Intact statue of a person or deity",
            "Intact statue of a person or deity",
            "Ruined or toppled statue of a person or deity",
            "Ruined or toppled statue of a person or deity",
            "Ruined or toppled statue of a person or deity",
            "Great stone wall, intact, with tower fortifications spaced at one-mile intervals",
            "Great stone wall in ruins",
            "Great stone arch",
            "Fountain",
            "Intact circle of standing stones",
            "Ruined or toppled circle of standing stones",
            "Totem pole"
        ][utils.rindex(20)], False)

        def weird_locales(): return ("Weird Locales (30% monster lair [1/~50mil²]):", [
            "Dead magic zone (similar to an anti magic field)",
            "Dead magic zone (similar to an anti magic field)",
            "Wild magic zone (use the wild-surge generator whenever a spell is cast with in the zone)",
            "Boulder carved with talking faces",
            "Crystal cave that mystically answers questions",
            "Ancient tree containing a trapped spirit",
            "Battlefield where lingering fog occasionally assumes humanoid forms",
            "Battlefield where lingering fog occasionally assumes humanoid forms",
            "Permanent portal to another plane of existence",
            "Permanent portal to another plane of existence",
            "Wishing well",
            "Giant crystal shard protruding from the ground",
            "Wrecked ship, which might be nowhere near water",
            "Haunted hill or barrow mound",
            "Haunted hill or barrow mound",
            "River ferry guided by a skeletal captain",
            "Field of petrified soldiers or other creatures",
            "Forest of petrified or awakened trees",
            "Canyon containing a dragons' graveyard",
            "Floating earth mote with a tower on it"
        ][utils.rindex(20)], False)
        def none(): return ("Just another day,", "Nothing out of the ordinary", False)

        event = utils.roll_dist(
            [none, encounters, monuments, weird_locales], [6, 6, 5, 3])()
        if ctx is not None:
            await ctx.send(embed=utils.create_embed("Wild Sights", "Randomly generated Wild Sights", [event, ]))
        return event

    @commands.command(
        name="weather",
        help="Randomizes the weather - don't use too often."
    )
    @utils.validate_cog("travel")
    async def weather(self, ctx: commands.Context):
        extreme_conds = []  # arr of the extreme conditions

        def hot():
            """ called if the weather is hot"""
            extreme_conds.append(("***Extreme Heat***", """When the temperature is at or above 37 degrees Celsius, a creature exposed to the heat and without access to drinkable water must succeed on a Constitution saving throw at the end of each hour or gain one level of exhaustion. The DC is 5 for the first hour and increases by 1 for each additional hour. Creatures wearing medium or heavy armor, or who are clad in heavy clothing, have disadvantage on the saving throw. Creatures with resistance or immunity to fire damage automatically succeed on the saving throw, as do creatures naturally adapted to hot climates."""))
            return f"{utils.roll(3)*5} degrees Celsius hotter than normal"

        def cold():
            """ called if the weather is cold """
            extreme_conds.append(("Extreme Cold", """Whenever the temperature is at or below -17 degrees Celsius, a creature exposed to the cold must succeed on a DC 10 Constitution saving throw at the end of each hour or gain one level of exhaustion. Creatures with resistance or immunity to cold damage automatically succeed on the saving throw , as do creatures wearing cold weather gear (thick coats, gloves, and the like) and creatures naturally adapted to cold climates"""))
            return f"{utils.roll(3)*5} degrees Celsius colder than normal"

        temp = ("Temperature:", utils.roll_dist([
            lambda: "Normal temperature for the season",
                cold,
                hot
                ], [14, 3, 6])())  # randomize temperature

        def strong():
            """ called if there's strong wind """
            extreme_conds.append(("***Strong Wind***", """A strong wind imposes disadvantage on ranged weapon attack rolls and Wisdom (Perception) checks that rely on hearing. A strong wind also extinguishes open flames, disperses fog, and makes flying by nonmagical means nearly impossible. A flying creature in a strong wind must land at the end of its turn or fall.
A strong wind in a desert can create a sandstorm that imposes disadvantage on Wisdom (Perception) checks that rely on sight."""))
            return "Strong wind"
        wind = ("Wind:", utils.roll_dist([
            lambda: "No wind",
            lambda: "Light wind",
            strong,
        ], [12, 5, 3])())  # randomize wind

        def heavy():
            """ called if there's heavy rain or snowfall """
            extreme_conds.append(("***Heavy Precipitation***", """Everything within an area of heavy rain or heavy snowfall is lightly obscured, and creatures in the area have disadvantage on Wisdom (Perception) checks that rely on sight. Heavy rain also extinguishes open flames and imposes disadvantage on Wisdom (Perception) checks that rely on hearing."""))
            return "Heavy rain or heavy snowfall"
        precip = ("Precipitation:", utils.roll_dist([
            lambda: "No precipitation",
            lambda: "Light rain or light snowfall",
            heavy,
        ], [12, 5, 4])())  # randomize precipitation

        if ctx is not None:  # if called from command
            # send
            await ctx.send(embed=utils.create_embed("Weather", "Randomly generated weather.", [temp, wind, precip, *extreme_conds], discord.Color.light_gray()))
        return [temp, wind, precip]  # return

    @commands.command(
        name="day-of-travel",
        help="A shortcut for weather command and wilds command, info for a day of travel.",
        aliases=["dot"]
    )
    @utils.validate_cog("travel")
    async def day_of_travel(self, ctx: commands.Context):
        await self.weather(ctx)
        await self.wild_sights(ctx)
        await self.wild_sights(ctx)


cog = travel
# match platform.system():
#     case "Linux" | "Darwin":
#         def setup(bot):
#             bot.add_cog(cog(bot))
#     case "Windows":


async def setup(bot):
    t = bot.add_cog(cog(bot))
    if t is not None:
        await t