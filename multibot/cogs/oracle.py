import platform
from pprint import pprint
import discord
from discord.ext import commands

if __name__ != "__main__":
    from bot import MultiBot
    import cogs.utils.utils as utils
    import generators.generators as generators
else:
    MultiBot = None
    utils = None
    generators = None

import d20

import requests
import re
from bs4 import BeautifulSoup as BS


class Oracle_interface:
    @staticmethod
    def yes_no(dc: int = 4):
        # print((dc if type(dc) == int else {"likely": 3,
        #                                    "even": 4,
        #                                    "unlikely": 5}[dc.lower()] if type(dc) == str else 4))
        r = utils.rindex()
        if r == 0:
            return "No, but..."
        if r == 5:
            return "Yes, and..."
        if r >= (dc if type(dc) == int else {"likely": 3,
                                             "even": 4,
                                             "unlikely": 5}[dc.lower()] if type(dc) == str else 4) - 1:
            return "Yes."
        else:
            return "No."

    @staticmethod
    def how():
        table = ["Surprisingly lacking ", "Less than expected", "About average",
                 "About average", "More than expected", "Extraordinary"]
        r = utils.rindex()
        return table[r]

    class _focus:
        def roll(self, table: list[str]):
            r = d20.roll("d13").total-1
            return (table[r] if len(table) > r else "None") + ", " + ["Physical (appearance, existence)",
                                                                      "Technical (mental, operation)",
                                                                      "Mystical (meaning, capability)",
                                                                      "Social (personal, connection)"][utils.rindex(4)]

        def action(self):
            return self.roll(["Seek", "Oppose", "Communicate", "Move", "Harm", "Create", "Reveal", "Command", "Take", "Protect", "Assist", "Transform", "Deceive"])

        def detail(self):
            return self.roll(["Small", "Large", "Old", "New", "Mundane", "Simple", "Complex", "Unsavory", "Specialized", "Unexpected", "Exotic", "Dignified", "Unique"])

        def topic(self):
            return self.roll(["Current Need", "Allies", "Community", "History", "Future Plans", "Enemies", "Knowledge", "Rumors", "A Plot Arc", "Recent Events", "Equipment", "A Faction", "The PCs"])

        def verb(self):
            return ["abandon", "abolish", "abuse", "accuse", "address", "aggravate", "agitate", "aid", "aim", "alarm", "alert", "alter", "amaze", "ambush", "amuse", "annihilate", "annoy", "antagonize", "appeal", "applaud", "apprehend", "approach", "argue", "arise", "arouse", "arrange", "arrest", "ask", "assassinate", "assault", "assemble", "astonish", "attack", "attempt", "attend", "auction", "audition", "avenge", "avert", "babble", "badmouth", "bait", "ban", "banish", "baptize", "bargain", "bark", "barricade", "barter", "bash", "bawl", "beautify", "beckon", "befriend", "beg", "beguile", "belch", "belittle", "bellow", "bemoan", "bequeath", "berate", "besiege", "bestow", "bet", "betray", "bewilder", "bewitch", "bid", "bite", "blab", "blame", "blast", "bleed", "bless", "blunder", "blurt", "boast", "bother", "bow", "brag", "break", "bribe", "broadcast", "build", "capture", "careen", "caress", "carry", "carve", "catch", "celebrate", "challenge", "charm", "chase", "cheat", "choke", "claim", "climb", "collaborate", "collapse", "collide", "command", "complain", "compliment", "conceal", "concoct", "condemn", "confiscate", "conflict", "confront", "confuse", "congratulate", "congregate", "conjure", "consecrate", "consider", "construct", "contact", "contaminate", "contestconverse", "cook", "corner", "corrupt", "cremate", "crowd", "crown", "customize", "damage", "dance", "dare", "dash", "dazzle", "deal", "debate", "decay", "deceive", "declare", "decline", "decompose", "decorate", "decree", "dedicate", "deduce", "deface", "defeat", "defend", "defuse", "deliver", "demand", "demolish", "denounce", "deride", "despair", "destroy", "devour", "dig", "disappear", "disarm", "discipline", "discover", "discriminate", "discuss", "disgrace", "disguise", "disgust", "dishonor", "dismount", "dispel", "disperse", "display", "displease", "dispute", "disrespect", "disrupt", "distill", "distress", "disturb", "divert", "dodge", "drag", "dress", "drink", "drop", "drown", "drum", "dump", "dupe", "duplicate", "earn", "eat", "elude", "employ", "enchant", "end", "endanger", "endear", "endorse", "enforce", "engage", "engineer", "enjoy", "enlarge", "enlighten", "enlist", "enquire", "enrage", "enrich", "enroll", "enshrine", "ensnare", "entangle", "enter", "entertain", "entice", "erect", "escape", "escort", "evacuate", "evade", "evict", "exaggerate", "examine", "excavate", "exchange", "exclaim", "exclude", "execute", "exhibit", "experiment", "explode", "expose", "faint", "fake", "fall", "falsify", "fashion", "flaunt", "flee", "fling", "flirt", "follow", "force", "foresee", "foretell", "forge", "forgive", "fracture", "frame", "free", "freeze", "fret", "frighten", "frustrate", "fumble", "fund", "fuss", "gallop", "gamble", "gather", "gaze", "gesture", "gift", "giggle", "give", "glare", "gleam", "glimpse", "goad", "gob", "goggle", "gossip", "grab", "grapple", "grieve", "guard", "hamper", "harass", "harm", "help", "hide", "hit", "hold", "humiliate", "hurry", "hurtle", "ignite", "impede", "implore", "imprison", "infect", "infest", "inflame", "inflict", "inform", "infringe", "infuriate", "injure", "inspect", "inspire", "instigate", "insult", "interfere", "interrogate", "intimidate", "intoxicate", "investigate", "invite", "involve", "irritate", "jaywalk", "jeer", "joke", "jostle", "kick", "kill", "kiss", "lament", "laugh", "leap", "lecture", "leer", "look", "loot", "lose", "love", "lunge", "lurk", "maim", "manhandle", "march", "market", "massacre", "meddle", "mesmerize", "mimic", "misspell", "mob", "mock", "murder", "mutate", "mutilate", "nag", "narrate", "near", "notify", "obscure", "observe", "obstruct", "offend", "oppress", "order", "overhear", "overpower", "overturn", "parade", "parley", "patrol", "pelt", "penalize", "perform", "persecute", "persuade", "petition", "play", "plead", "plunder", "pollute", "pounce", "practice", "praise", "preach", "proclaim", "prohibit", "promote", "pronounce", "prophesize", "prosecute", "protect", "protest", "provide", "provoke", "prowl", "pry", "punch", "punish", "purchase", "pursue", "push", "quarrel", "query", "queue", "rage", "raid", "ransack", "rebel", "recite", "recount", "recruit", "rejoice", "remark", "renege", "repair", "research", "rescue", "resist", "restrain", "resurrect", "reveal", "revolt", "reward", "ridicule", "riot", "rob", "run", "rush", "salute", "scamper", "scare", "scavenge", "scream", "scrounge", "search", "secure", "seduce", "segregate", "seize", "sell", "ship", "shout", "shove", "show", "silence", "sing", "slaughter", "sleep", "smash", "sob", "solicit", "speak", "spill", "spit", "spy", "stab", "stage", "startle", "steal", "stop", "strangle", "strike", "subdue", "suffer", "summon", "surround", "suspect", "take", "taunt", "tease", "tempt", "terrify", "terrorize", "thank", "threaten", "throw", "torment", "torture", "trade", "transform", "translate", "transport", "trap", "travel", "tug", "unite", "unleash", "unload", "unveil", "vandalize", "vanish", "victimize", "violate", "volunteer", "wait", "warn", "wave", "weep", "welcome", "whittle", "wield", "win", "wink", "witness", "worship", "wound", "wreck", "wrestle", "write", "yell", "yield"][d20.roll("d500").total-1]

    focus = _focus()


class oracle(commands.Cog):
    def __init__(self, bot: MultiBot):
        self.bot = self.client = bot
        self.oracle = Oracle_interface
        self.gens = generators.Generator()

    @commands.command(
        name="oracle",
        help="Ask the oracle a question of the given type",
        usage="<prefix>oracle <question type> <question arguments...>\nQuestion Types: ask,how,focus.\n'ask' arguments: <dc: number in range 1-6, or 'Likely','Unlikely','Even'>\n'how' argumetns: None.\n'focus' Question Options: action,detail,topic,verb.",
        aliases=['o']
    )
    @utils.validate_cog("oracle")
    async def ask_oracle(self, ctx: commands.Context, _type: str, args: str = "", n: int = 1):
        match _type.lower():  # match on different types
            case "ask":  # if basic ask (yes/no question)
                if args != "":  # if args given
                    # set dc to int(arg) if arg is a number
                    dc = int(args) if utils.isint(args) else args
                else:  # no args
                    dc = "Even"  # default
                # print(dc)
                res = self.oracle.yes_no(dc)  # get reply
                return await ctx.send(res)  # msg
            case "how":  # if how question
                return await ctx.send(self.oracle.how())  # ask
            case "focus":  # if focus question
                if args == "":  # if no args
                    # error
                    return await utils.error(ctx, "Invalid Syntax.", "Oracle focus question requires at least one argument.")

                match args:  # match on args
                    case "action":  # if action focus
                        # msg action response
                        return await ctx.send(self.oracle.focus.action())
                    case "detail":  # if detail focus
                        # msg detail response
                        return await ctx.send(self.oracle.focus.detail())
                    case "topic":  # if topic focus
                        # msg topic response
                        return await ctx.send(self.oracle.focus.topic())
                    case "verb":  # if verb focus
                        if not utils.isint(n):  # if no number given
                            n = 1  # set to default 1
                        # msg verb(s) response
                        return await ctx.send(", ".join([self.oracle.focus.verb() for _ in range(n)]))
                    case default:  # if none of the above
                        # error
                        return await utils.error(ctx, "Invalid Argument.", f"Given Oracle focus type doesn't exists!", [("focus type:", args)])

    @commands.command(
        name="dm-move",
        help="A pacing or a failure move, taking the place of the DM",
        usage="<prefix>dm-move <type: 'pacing' or 'failure'>",
        aliases=["dmm"]
    )
    @utils.validate_cog("oracle")
    async def dm_move(self, ctx: commands.Context, tp: str):
        match tp.lower():  # match on dm-move type
            case "pacing":  # if pacing
                # pacing move table
                table = ["Foreshadow Trouble", "Reveal a New Detail", "An NPC Takes Action",
                         "Advance a Threat", "Advance a Plot", "Add a RANDOM EVENT to the scene"]
                r = utils.rindex()  # roll the dice
                if r < 5:  # if first 5
                    if ctx is not None:  # if ctx is defined
                        await ctx.send(table[r])  # msg table at index
                    return table[r]  # return table at index
                else:  # if last one (random event)
                    # get random event
                    event = f"What happens: {self.oracle.focus.action()}\nInvolving: {self.oracle.focus.topic()}"
                    if ctx is not None:  # if ctx is not None,
                        await ctx.send(event)  # msg
                    return event  # return the even
            case "failure":  # if failure move
                # failure move table
                table = ["Cause Harm", "Put Someone in a Spot", "Offer a Choice",
                         "Advance a Threat", "Reveal an Unwelcome Truth", "Foreshadow Trouble"]
                r = utils.rindex()  # roll the dice
                if ctx is not None:  # if ctx is not None,
                    await ctx.send(table[r])  # msg
                return table[r]  # return
            case default:  # anything else
                if ctx is not None:  # if ctx is not None
                    # msg error
                    await utils.error(ctx, "Invalid Argument.", f"Given dm-move doesn't exist!", [("dm-move:", tp)])
                return None  # return error

    @commands.command(
        name="set-the-scene",
        help="Gives some info about the scene.",
        aliases=["sts"]
    )
    @utils.validate_cog("oracle")
    async def set_the_scene(self, ctx: commands.Context):
        # scene complication table
        scene_comp = ["Hostile forces oppose you", "An obstacle blocks your way", "Wouldn't it suck if...",
                      "An NPC acts suddenly", "All is not as is seems", "Things actually go as planned"]
        # altered scene
        altered_scene = ["A major detail of the scene is enhanced or somehow worse", "The environment is different",
                         "Unexpected NPCs are present", "Add a SCENE COMPLICATION", "Add a PACING MOVE", "Add a RANDOM EVENT"]
        comp = utils.rindex()  # roll the dice
        comp_scene = scene_comp[comp]  # get the scene comp
        res = ""  # the result string
        if utils.rindex() >= 4:  # see if its an altered scene
            alt = utils.rindex()  # roll the dice
            alt_scene = altered_scene[alt]  # get altered scene
            match alt:  # match on alt for special cases
                case 3:  # scene complication
                    # get another scene complication
                    alt_scene = scene_comp[utils.rindex()]
                case 4:  # pacing move
                    # call the pacing move
                    alt_scene = self.dm_move(None, "pacing")
                case 5:  # random event
                    # get a random event
                    alt_scene = f"What happens: {self.oracle.focus.action()}\nInvolving: {self.oracle.focus.topic()}"
            res = comp_scene + ",\n" + alt_scene  # set the result string
        else:  # if not an altered scene
            res = comp_scene  # set the result string
        await ctx.send(res)  # msg

    @commands.command(
        name="plot-hook",
        help="Generates a random plot hook.",
        aliases=["phook"]
    )
    @utils.validate_cog("oracle")
    async def plot_hook(self, ctx: commands.Context):
        # tables
        objective = ["Eliminate a threat", "Learn the truth", "Recover something valuable",
                     "Escort or deliver to safety", "Restore something broken", "Save an ally in peril"]
        adverseries = ["A powerful organization", "Outlaws", "Guardians",
                       "Local inhabitants", "Enemy horde or force", "A new or recurring villain"]
        reward = ["Money or valuables", "Money or valuables", "Knowledge and secrets",
                  "Support of an ally", "Advance a plot arc", "A unique item of power"]

        obj = objective[utils.rindex()]  # roll the dices
        advers = adverseries[utils.rindex()]
        reward = reward[utils.rindex()]
        # send embed
        await ctx.send(embed=utils.create_embed("Plot Hook", fields=[("Hook Objective:", obj, False), ("Adversaries:", advers, False), ("Reward:", reward, False)], color=discord.Color.green()))

    class _dungeon:
        ''' holds info about a dungeon '''
        desc: str = ""  # how it looks
        level: int = 0  # level of the dungeon
        rooms: list[tuple[str, str, bool]] = []  # generated rooms
        creator: str = ""  # the creator of the dungeon
        purpose: str = ""  # the original purpose of the dungeon
        history: str = ""  # the current history of the dungeon
        image: str = ""

        def get_desc(self):
            """ returns the formatted description of the dungeon """
            return self.desc+" Created by "+self.creator+" as a " + self.purpose+", " + self.history + ".\nDungeon Level: " + str(self.level) + ". Current room count: " + str(len(self.rooms))+"."  # add all the stuff together

        def get_rooms(self):
            """ returns all of the generated rooms """
            # r = []  # ret list
            # for i, room in enumerate(self.rooms):  # run on rooms
            #     r.append(
            #         (f"**Room #{i+1}:**", "; ".join([f"*{v[0]}*: {v[1]}" for v in room]), False))  # format
            return self.rooms  # return

    current_dungeons: dict[str, _dungeon] = {}  # map of channel:dungeon

    # the required arguments for the dungeon command
    dungeon_args = {
        "Level": {
            "desc": "Level of the players entering the dungeon (for balancing the encounters. But remember, if an encounter seems too hard, just use the `<prefix>gen encounter` generator)",
            "key": "level",
            "values": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        },
        "Width": {
            "desc": "Width of the dungeon (size'll be about value*2+1squares)",
            "key": "width",
            "default": 0,
            "values": [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        },
        "Height": {
            "desc": "Height of the dungeon (size'll be about value*2+1squares)",
            "key": "height",
            "default": 0,
            "values": [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        },
        "Room Count": {
            "desc": "",
            "key": "roomcnt",
            "default": 2,
            "values": ["none", "few", "some", "many", "lots"]
        },
        "Room Size": {
            "desc": "",
            "keys": ["minroomw", "maxroomw", "minroomh", "maxroomh"],
            "default": "medium",
            "values": {
                "tiny": [1, 3, 1, 3],
                "small": [2, 5, 2, 5],
                "medium": [3, 7, 3, 7],
                "large": [4, 9, 4, 9],
            }
        }
    }

    @commands.command(
        name="dungeon",
        help="Dungeon Crawler sort of thing.",
        aliases=["dngn"],
        usage="<prefix>dungeon <dungeon command>\nCommands: enter - starts a new dungeon: "+"\n\t".join([f"""<{argname}>{" - "+argv["desc"] if "desc" in argv and argv["desc"] != "" else ""}. Values: {str(argv["values"]) if type(argv["values"]) is list else str(list(argv["values"].keys()))}{", default: "+str(argv["values"][argv["default"]] if type(argv["values"]) is list else argv["default"]) if "default" in argv else ""}""" for argname, argv in dungeon_args.items(
        )])+"\n\troom - gets info about a room by its index: <index of room>\n\texit - exit the current dungeon: no arguments\n\tlist - shows all the rooms so far: no arguments"
    )
    @utils.validate_cog("oracle")
    async def dungeon(self, ctx: commands.Context, command: str, *args):
        ID = str(ctx.message.channel.id)  # get the channel's id
        # print(command,args)

        current_dungeon = None  # init
        if ID in self.current_dungeons:  # if a dungeon is loaded for current channel
            current_dungeon = self.current_dungeons[ID]  # get current_dungeon

        exits = None
        match command.lower():  # match on command
            case "enter":  # start a dungeon
                if len(args) == 0:  # check args
                    return await utils.error(ctx, "Invalid Syntax.", "Dungeon enter command requries the player level.")
                if not utils.isint(args[0]):  # needs to be int
                    return await utils.error(ctx, "Invalid Argument.", "Dungeon enter command requires and integer player level.")

                if ID in self.current_dungeons:  # channel is already in a dungeon
                    # error
                    return await utils.error(ctx, "Invalid Usage.", "Only one dungeon can be active per channel!")

                self.current_dungeons[ID] = self._dungeon()  # init dungeon
                # save to current_dungeon
                current_dungeon = self.current_dungeons[ID]

                # generate a human dungeon creator
                def humans(): return ["LG", "LG", "NG", "NG", "CG", "CG", "LN", "LN",
                                      "LN", "N", "N", "CN", "LE", "LE", "LE", "NE", "NE", "NE", "CE", "CE", ][utils.rindex(20)] + " Human " + ["Barbarians", "Bards", "Clerics", "Clerics", "Druids", "Fighters", "Fighters", "Monks", "Paladins", "Rangers", "Rogues", "Rogues", "Rogues", "Rogues", "Sorcerers", "Warlocks", "Wizards", "Wizards", "Wizards"][utils.rindex(20)]

                # generate a cult dungeon creator
                def cults(): return ["Demon-worshiping cult", "Devil-worshiping cult", "Elemental Air cult", "Elemental Air cult", "Element al Earth cult", "Element al Earth cult", "Elemental Fire cult", "Elemental Fire cult", "Elemental Water cult", "Elemental Water cult", "Worshipers of an evil deity",
                                     "Worshipers of an evil deity", "Worshipers of an evil deity", "Worshipers of an evil deity", "Worshipers of an evil deity", "Worshipers of a good deity", "Worshipers of a good deity", "Worshipers of a neutral deity", "Worshipers of a neutral deity", "Worshipers of a neutral deity"][utils.rindex(20)]

                # generate a dungeon creator
                current_dungeon.creator = [lambda:"a Beholder", cults, cults, cults, lambda:"Dwarves", lambda:"Dwarves", lambda:"Dwarves", lambda:"Dwarves",
                                           lambda:"Elves (including drow)", lambda:"Giants", lambda:"Hobgoblins", humans, humans, humans, humans, lambda:"Kuo-toa", lambda:"a Lich", lambda:"Mindflayers", lambda:"Yuan-ti", lambda:"No creator (natural caverns)"][utils.rindex(20)]()
                # generate a dungeon purpose
                current_dungeon.purpose = ["Death trap", "Lair", "Lair", "Lair", "Lair", "Maze", "Maze", "Mine", "Mine", "Mine", "Planar gate", "Stronghold", "Stronghold",
                                           "Stronghold", "Stronghold", "Temple or shrine", "Temple or shrine", "Temple or shrine", "Tomb", "Tomb", "Treasure vault"][utils.rindex(20)]
                # generate a dungeon's history
                current_dungeon.history = ["the place was abandoned by creators",
                                           "the place was abandoned by creators",
                                           "the place was abandoned by creators",
                                           "the place was abandoned due to plague",
                                           "the place was conquered by invaders",
                                           "the place was conquered by invaders",
                                           "the place was conquered by invaders",
                                           "the place was conquered by invaders",
                                           "Creators were destroyed by attacking raiders",
                                           "Creators were destroyed by attacking raiders",
                                           "Creators were destroyed by discovery made within the site",
                                           "Creators were destroyed by internal conflict",
                                           "Creators were destroyed by magical catastrophe",
                                           "Creators were destroyed by magical catastrophe",
                                           "Creators were destroyed by natural disaster",
                                           "The location was cursed by the gods and shunned",
                                           "original Creators are still in control",
                                           "original Creators are still in control",
                                           "the place was overrun by planar creatures",
                                           "the place was a site of a great miracle"][utils.rindex(20)]

                defs = {  # init defs
                }

                for i, (k, v) in enumerate(self.dungeon_args.items()):  # enumerate dungeon args
                    default = False  # not a default value
                    if len(args) > i:  # if args where given
                        arg = args[i]  # take them
                        if utils.isint(arg):  # if a number
                            arg = int(arg)  # parse
                    elif "default" in v:  # if no arg is given and there's a default value
                        arg = v["values"][v["default"]]  # take it
                        default = True  # set default value flag
                    else:  # otherwise
                        # error
                        await utils.error(ctx, "**Input Error**", f"Missing values for argument {k}.")
                        return

                    if not default:  # if not default value
                        # if given value not in the optional values
                        if not arg in v["values"]:
                            # error
                            await utils.error(ctx, "**Input Error**", f"Invalid value for argument {k}.", [("Value:", arg)])
                            return

                    # if not default value and if values is a dict (room size)
                    if type(v["values"]) is dict and not default:
                        arg = v["values"][arg]  # get the actual value

                    if type(arg) is list:  # if dict value
                        for i, a in enumerate(arg):  # enumerate on values
                            # add key/value pair to defs
                            defs[v["keys"][i]] = a
                    else:  # if single value
                        defs[v["key"]] = arg  # add key/value pair to defs

                for k, v in {"sparse": "moderate",
                             "random": 40,
                             "deadends": 75,
                             "secret": 5,
                             "concealed": 10,
                             "resolution": 25,
                             "seed": ""}.items():  # add other required defs
                    defs[k] = v

                # print("defs:: ",defs)

                def parse(s):
                    """ parses a room list item into simple list of values """
                    return [i[:-1].replace("<br/>", "") for i in str(s)[9:].replace("</li>", "").replace("</ul>", "").split("<li>")]

                # get the link with the defs
                l = "https://www.myth-weavers.com/cgi-bin/npc/dungeon.cgi?" + \
                    "&".join([f"{k}={v}" for k, v in defs.items()])
                # print(l)
                res = requests.get(l)  # get request

                if res.status_code == 200:  # if success
                    soup = BS(res.content, "html.parser")  # get beautifulSoup
                    image = "https://www.myth-weavers.com" + \
                        str(soup.find("img"))[
                            10:-3].replace("&amp;", "&")  # get image address
                    rs = soup.findAll("ul")  # get room list
                    rooms = []  # init
                    cons = 0  # continue number
                    for i, room in enumerate(rs):  # enumerate rooms
                        if cons > 0:  # if continue
                            cons -= 1  # decrease counter
                            continue  # continue
                        room = str(room)  # stringify
                        # print(room)
                        c = room.count("<ul>")  # get count
                        # print(c)
                        if c == 1:  # if only one
                            rooms.append(
                                (f"Room #{len(rooms)+1}:", "\n".join(parse(room)), True))  # add parsed value
                            # print(rooms[-1])
                        else:  # if more than one (subitems)
                            c -= 1  # remove parent count
                            cons = c  # set continues
                            # get the stringified subitems
                            els = list(map(str, rs[i+1:i+1+c]))
                            # print(els)
                            # rm = []
                            t = ""
                            for el in els:  # run on subitems
                                # print(el)
                                # remove the subitem from the parent and replace with marker
                                room = room.replace(el, ";;;")
                                # print(room)
                                title = re.findall(
                                    r"(?:<li>)(.+?)(?=\n;;;)", room)  # get title
                                # erase title and surroundings from room string
                                room = re.sub(
                                    r"<li>.+?(?=\n;;;)\n;;;(\n)?", "", room)
                                el = parse(
                                    re.sub(r"<(/)?i>", "_", re.sub(r"<(/)?b>", "**", el)))  # add some parsing for discord formatting
                                # add into a final string
                                t += "\n_"+title[0]+":_\n - "+"\n - ".join(el)
                            room = parse(room)  # parse the room
                            room.append(t[1:])  # append the subitems
                            rooms.append(
                                (f"Room #{len(rooms)+1}:", "\n".join(room), True))  # add room to rooms list
                    await ctx.send(image)  # send image

                    level = defs["level"]

                    # TODO: Save rooms to current_dungeon under channel id and whatnot.
                else:  # if get failed
                    # error
                    await utils.error(ctx, "**Server Error**", f"Received an error code from the GET request: {res.status_code}")
                    return

                current_dungeon.level = level  # save level
                # save description
                current_dungeon.desc = f"The Dungeon looks {self.oracle.focus.detail()}."
                current_dungeon.rooms = rooms  # save rooms
                current_dungeon.image = image
                room = [current_dungeon.rooms[0], ]
            case "room":  # if next room
                if not ID in self.current_dungeons:  # if not ID initialized
                    # error
                    return await utils.error(ctx, "Invalid Usage.", "You must first enter a dungeon!")
                if len(args) == 0:  # check args
                    return await utils.error(ctx, "Invalid Syntax.", "Dungeon room command requries the room index.")
                if not utils.isint(args[0]):  # needs to be int
                    return await utils.error(ctx, "Invalid Argument.", "Dungeon room command requires an integer room index.")
                index = int(args[0]) - 1
                if index >= len(current_dungeon.rooms):
                    return await utils.error(ctx, "Invalid Argument.", f"Given room index doesn't exist.", [("Index:", index)])
                room = [current_dungeon.rooms[index], ]

            case "exit":  # if exit a dungeon
                embed = utils.create_embed(
                    "Generated Dungeon", current_dungeon.get_desc(), [*current_dungeon.get_rooms()], color=discord.Color.dark_gold())  # create return embad
                await ctx.send(embed=embed)  # send
                del self.current_dungeons[ID]  # delete dungeon
                return  # return
            case default:
                # error
                return await utils.error(ctx, "Invalid Argument.", f"Given dungeon crawler command `{command}` doesn't exist.")
        # msg
        await ctx.send(embed=utils.create_embed("Dungeon Room", current_dungeon.get_desc(), room, color=discord.Color.dark_gold()))


cog = oracle
# match platform.system():
#     case "Linux" | "Darwin":
#         def setup(bot):
#             bot.add_cog(cog(bot))
#     case "Windows":


async def setup(bot):
    t = bot.add_cog(cog(bot))
    if t is not None:
        await t